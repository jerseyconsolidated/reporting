<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
if (isset($_GET['phpinfo'])) {
    echo phpinfo();
}

/*
 * Formats the 'echo' statements in the log files so that it is more pretty
 */
function logIt($error)
{
    if($error !== "")
    {
        echo (date('r', time())." (UTC) : \n");
        echo $error."\n";
    }
}

function initialize_db($mysqli, $username="biohealth1", $password="7s63VfusbHVFuq"){
//function initialize_db($mysqli){
    //var_dump(debug_backtrace());
    // die();
    //$mysqli = (!isset($mysqli) ? $mysqli = 'limelight' : $mysqli);

    //echo $mysqli;
    //open up our connection
    $mysqli = new mysqli("localhost",$username,$password,getDBName());
    // Check connection
    if (mysqli_connect_errno()) {
        //printf("Connect failed: %s\n", mysqli_connect_error());
        logIt(mysqli_connect_error());
        exit();
    }
    return $mysqli;
}

function doDBthing($query)
{
    $mysqli = initialize_db(getDBName());

    if($prepared_mysqli = $mysqli->prepare($query))
    {
        $prepared_mysqli->execute();

        $prepared_mysqli->store_result();

        $prepared_mysqli->free_result();

        $prepared_mysqli->close();
    }
    else
    {
        logIt("\n $query \n");
        logIt($mysqli->error);
    }
    return true;
}
?>
