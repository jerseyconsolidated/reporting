<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require ('../includes/RayGun/RayGun.php');
require ('general_functions_meritus.php');
ini_set('memory_limit', '-1');
global $mysqli;
date_default_timezone_set('UTC');

echo "\n"; // just make things look purdy


/*
 *
 * GLOBAL VARIABLES BELOW!
 *
 */
$start_date         = "09/18/2013"; //Format: MM/DD/YYYY
$end_date = date('m/d/Y',strtotime("-1 days"));

$page_size    = "1000"; //How big should each "page" be?


/*
 * reporting servadora
 */
$php_command        = "php /var/www/reporting.jcoffice.net/meritus/doing_meritus_pull.php";
/*
 * Jon's machine
 */
//$php_command        = "php /Users/jonjenne/reporting/meritus/doing_meritus_pull.php";

/*
 *
 * UPDATE 4/22/2015:
 * error log files are stupid and old and deprecated. We use DB shit now. #dealwithit
 *
 * Find error information on general_log_table
 *
 * The Gateway/worker is the name of the error file. So, the path should be something like:
 * /Users/jonjenne/reporting/nmi/errors/
 *
 * BECAUSE! This script makes lots of error files, so we need to feed a DIRECTORY not a FILE.
 * AND GOD HELP YOU IF YOU FORGET THE LAST / AT THE END OF THE PATH!!!
 * WHO KNOWS WHAT WILL HAPPEN
 *
 *
 */
/*
 * Jon's machine
 */
//$php_error_location = "/Users/jonjenne/reporting/nmi/errors/";
/*
 * Reporting Servadora
 */
//$php_error_location = "/var/www/reporting.jcoffice.net/meritus/errors/"; //where to put errors



/*
 *                      Done with configuration stuff. Don't mind the Bender lower down the page.

							               ,'``.._   ,'``.
										  :,--._:)\,:,._,.:       All Glory to
										  :`--,''   :`...';\      the HYPNO TOAD!
										   `,'       `---'  `.
										   /                 :
										  /                   \
										,'                     :\.___,-.
									   `...,---'``````-..._    |:       \
										 (                 )   ;:    )   \  _,-.
										  `.              (   //          `'    \
										   :               `.//  )      )     , ;
										 ,-|`.            _,'/       )    ) ,' ,'
										(  :`.`-..____..=:.-':     .     _,' ,'
										 `,'\ ``--....-)='    `._,  \  ,') _ '``._
									  _.-/ _ `.       (_)      /     )' ; / \ \`-.'
									 `--(   `-:`.     `' ___..'  _,-'   |/   `.)
										 `-. `.`.``-----``--,  .'
										   |/`.\`'        ,',');
											   `         (/  (/

 */

$postData= array(
	"MerchantID" => "32387",
	"MerchantKey" => "bba19729-a1ed-48fb-8e21-5b3b38bab01b",
	"ReportName" => "BatchDetail",
	"ReportFormat" => "JSON",
	"StartDate" => "$start_date",
	"EndDate" => "$end_date",
	"PageSize" => "$page_size",
	"CurrentPage" => "1" //just for "getnumberofpages". This value is re-set elsewhere later on.
);

/*
 * Truncate all tables
 */
prepare_database();
/*
 * Figure out how many pages we're dealing with for our above data requirements
 */
$pages = getNumberOfPages($postData);
databaseGeneralLog("Number of pages (soon to be threads): ".$pages);


//foreach page, spawn a new worker thread
/*
	 * THIS IS SO SUPER IMPORTANT AND SUPER STUPID!!!
	 * CURRENT PAGE ($index) _CANNOT_ BE 0!! IT MUST BEGIN WITH 1
	 * PAGE 0 DOES NOT EXIST FOR THESE CRAZY HUMANS
	 *
	 * (page 0 returns a blank array with no useful data, so don't waste 2 hours like I did)
	 *
 *   _________________________________
    |.--------_--_------------_--__--.|
    ||    /\ |_)|_)|   /\ | |(_ |_   ||
    ;;`,_/``\|__|__|__/``\|_| _)|__ ,:|
   ((_(-,-----------.-.----------.-.)`)
    \__ )        ,'     `.        \ _/
    :  :        |_________|       :  :
    |-'|       ,'-.-.--.-.`.      |`-|
    |_.|      (( (*  )(*  )))     |._|
    |  |       `.-`-'--`-'.'      |  |
    |-'|        | ,-.-.-. |       |._|
    |  |        |(|-|-|-|)|       |  |
    :,':        |_`-'-'-'_|       ;`.;
     \  \     ,'           `.    /._/
      \/ `._ /_______________\_,'  /
       \  / :   ___________   : \,'
        `.| |  |           |  |,'
          `.|  | (kill all |  |
            |  |  humans)  |  |

 */
//$index is the page number to be worked on. Don't let it be 0.
for($index=1; $index <= $pages; $index++)
{
	//^^^^^^^^^^ See that shit up there? don't let $index be anything less than or equal to 1.

	$run_command = "$php_command $start_date $end_date $page_size $index & > /dev/null";

	//echo $run_command."\n";
	/*
	 *
	 $log_files = $php_error_location.$index.".txt"; //make a unique error file for each page

	if(! file_exists($log_files))
    {
        //touch($log_files); //turning this off because all it does is blow up the raygun thing
    }

	$command = $run_command.$log_files." &";
	*/
	$command = $run_command;
	//the popen NOT being pclosed is probably a memory leak, but for some reason it breaks the workers when they are immediately pclosed.
	$result = popen($command, 'r');
	//echo $result;

    if($result)
    {
		//logIt("Failed to start $command ... This command did not start.");
		databaseGeneralLog("Successfully started $command ");
    }
	else
	{
		databaseGeneralLog("Failed to start $command ");
	}
		sleep(1);
	pclose($result);
}

databaseGeneralLog("Done starting everything. This thread will probably die now, and the prisoners {workers} are taking over the asylum!!!");

/*
 * Go ahead and ask the API how many pages we have to go through.
 * This could be a potentially time-intensive task. I've noticed for what (for example)
 * eventually returns '24' pages, it could take like 2-3 minutes.
 *
 * This can't really be threaded otherwise tho. :/
 */
function getNumberOfPages($data)
{
	$url = "https://webservice.paymentxp.com/rpt/Report.aspx";

	$curlSession = curl_init();
	curl_setopt($curlSession, CURLOPT_URL, $url);
	curl_setopt($curlSession, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($curlSession, CURLOPT_POST, 1);
	curl_setopt($curlSession, CURLOPT_POSTFIELDS, $data);
	curl_setopt($curlSession, CURLOPT_TIMEOUT,500000);
	if (php_uname('s') == "Darwin")  //OSX is called Darwin for some reason
	{
		curl_setopt($curlSession, CURLOPT_CAINFO, "/Users/jonjenne/reporting/nmi/ca-bundle.crt"); //HTTPS cert
	}
	else if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') // this might work for windows, idk
	{
		logIt('This is Windows! (Ryan, set your path to the cert here - We are now dying)');
		die();
	}
	else //some kind of Linux
	{
		curl_setopt($curlSession, CURLOPT_CAINFO, "/var/www/reporting.jcoffice.net/nmi/ca-bundle.crt"); //HTTPS cert
		//logIt("running under linux CHECK THE CERT PATH BEFORE CONTINUING (dieing)");
		//die(); //kill this so it reminds me to change this for the linux path.
	}
	$rawresponse = curl_exec($curlSession);
	curl_close ($curlSession);
	$transactions = json_decode($rawresponse, true);
	$current_page = 1;
	$pages = $transactions['rows'][0]['TotalRecordCount'] / $data['PageSize'];
	$pages = $pages + 1;
	$pages = round($pages);

	return $pages;
}
?>