<?php
ini_set('max_execution_time', 3000);
error_reporting(E_ERROR);
error_reporting(E_ALL);
ini_set("display_errors", 1);
require ('../includes/RayGun/RayGun.php');
require ('general_functions.php');
require ('config.php');

ini_set('memory_limit', '-1');

global $mysqli;
echo "\n"; // just make things look purdy
$mysqli = initialize_db("meritus");

echo "something";
$query_empty_table = "TRUNCATE TABLE meritus_transactions";

$url = "https://webservice.paymentxp.com/rpt/Report.aspx";
$pages = 2;
for ($page = 0; $page < $pages ; ++$page)
{
	echo "about to run pcntl";
    $pid = pcntl_fork();
	echo "after pcntl";
    if ($pid == -1)
    {
		echo "could not fork";
        die('could not fork');
    }
    else if ($pid)
    {
		echo "in the parent";
		die();
        // we are the parent
        pcntl_wait($status); //Protect against Zombie children
    }
    else
    {
		echo "child process";
		die();
        $data1 = array(
            "MerchantID" => "32387",
            "MerchantKey" => "bba19729-a1ed-48fb-8e21-5b3b38bab01b",
            "ReportName" => "BatchDetail",
            "ReportFormat" => "JSON",
            "StartDate" => "09/18/2013",
            // "EndDate" => "10/01/2013",
            "EndDate" => date('m/d/Y'),
            "PageSize" => "1000",
            "CurrentPage" => $current_page
        );

        $curlSession = curl_init();
        curl_setopt($curlSession, CURLOPT_URL, $url);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curlSession, CURLOPT_POST, 1);
        curl_setopt($curlSession, CURLOPT_POSTFIELDS, $data1);
        curl_setopt($curlSession, CURLOPT_TIMEOUT,500000);
        if (php_uname('s') == "Darwin")  //OSX is called Darwin for some reason
        {
            //curl_setopt($curlSession, CURLOPT_CAINFO, "/Users/jonjenne/reporting/nmi/cacert.pem"); //HTTP cert
            curl_setopt($curlSession, CURLOPT_CAINFO, "/Users/jonjenne/reporting/nmi/ca-bundle.crt"); //HTTPS cert
        }
        else if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') // this might work for windows, idk
        {
            logIt('This is Windows! (Ryan, set your path to the cert here - We are now dying)');
            die();
        }
        else //some kind of Linux
        {
            curl_setopt($curlSession, CURLOPT_CAINFO, "/var/www/reporting.jcoffice.net/nmi/ca-bundle.crt"); //HTTPS cert
            logIt("running under linux CHECK THE CERT PATH BEFORE CONTINUING (dieing)");
            //die(); //kill this so it reminds me to change this for the linux path.
        }

        $rawresponse = curl_exec($curlSession);
        curl_close ($curlSession);
        $transactions = json_decode($rawresponse, true);

        $current_page++;

        for($i=1;$i< count($transactions['rows']);$i++)
        {
            $pages = $transactions['rows'][$i]['TotalRecordCount']; //set how many pages total

            $insertSQL = sprintf("INSERT INTO meritus_transactions (api_username,api_password,gateway_id,AuthCode,BatchNo,CardNo,CardType,ReportDate,TerminalNumber,TotalRecordCount,TransAmount,TransDate,TransType,TransTypeID) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                GetSQLValueString("32387","text", $mysqli),
                GetSQLValueString("bba19729-a1ed-48fb-8e21-5b3b38bab01b","text", $mysqli),
                GetSQLValueString("6","text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['AuthCode'], "text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['BatchNo'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['CardNo'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['CardType'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['ReportDate'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['TerminalNumber'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['TotalRecordCount'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['TransAmount'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['TransDate'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['TransType'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['TransTypeID'], "text", $mysqli));

            doDBthing($insertSQL);
            //mysql_select_db($database_bio_db, $bio_db);
            //$Result1 = mysql_query($insertSQL, $bio_db) or die(mysql_error());
        }
    }

}

echo "Meritus Done - 6";

function GetSQLValueString($theValue, $theType, $mysqli, $theDefinedValue = "", $theNotDefinedValue = "")
{
    $theValue = $mysqli->real_escape_string($theValue);
    switch ($theType) {
        case "text":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "long":
        case "int":
            $theValue = ($theValue != "") ? intval($theValue) : "NULL";
            break;
        case "double":
            $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
            break;
        case "date":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "defined":
            $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
            break;
    }
    return $theValue;
}
?> 
