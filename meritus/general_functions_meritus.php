<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
if (isset($_GET['phpinfo'])) {
    echo phpinfo();
}

/*
 * Formats the 'echo' statements in the log files so that it is more pretty
 */
function logIt($error)
{
    if($error !== "")
    {
    //echo (date('r', time())." (UTC) : \n");
    echo $error."\n";
    }
}

/*
 * inserts the message into the general_log_table
 */
function databaseGeneralLog($message)
{
    if($message !== "")
    {
        $query =
               "INSERT INTO
               general_log_table
               SET
               message = '$message'
               ";
        doDBthing($query);
    }
}

function initialize_db($mysqli, $username="biohealth1", $password="7s63VfusbHVFuq")
{
    $mysqli = new mysqli("localhost",$username,$password,$mysqli);
    // Check connection
    if (mysqli_connect_errno()) {
        //printf("Connect failed: %s\n", mysqli_connect_error());
        logIt(mysqli_connect_error());
        exit();
    }
    return $mysqli;
}
function doDBthing($query)
{
   $mysqli = initialize_db("meritus");

    if($prepared_mysqli = $mysqli->prepare($query))
    {
        $prepared_mysqli->execute();

        $prepared_mysqli->store_result();

        $prepared_mysqli->free_result();

        $prepared_mysqli->close();
    }
    else
    {
        //logIt("\n $query \n");
        //databaseGeneralLog(mysqli_connect_error());
		databaseGeneralLog($mysqli->error);
		databaseGeneralLog($prepared_mysqli->error);
    }
    return true;
}
/*
 * Truncate & Create the database starting at this year:
 */
function prepare_database()
{
    $mysqli = initialize_db('meritus');

    if ($mysqli->connect_error)
    {
        logIt("Failed to connect to MySQL. Unable to Truncate and Create.: " .$mysqli->connect_error);
        die();
    }
    $mysqli->query("TRUNCATE TABLE meritus_transactions");
	$mysqli->query("TRUNCATE TABLE general_log_table");
	echo $mysqli->error;
	$mysqli->close();
}

?>
