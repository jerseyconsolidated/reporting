<?php
ini_set('max_execution_time', 3000);
error_reporting(E_ERROR);
error_reporting(E_ALL);
ini_set("display_errors", 1);
require ('../includes/RayGun/RayGun.php');
require ('general_functions_meritus.php');
require ('config.php');

ini_set('memory_limit', '-1');

global $mysqli;
echo "\n"; // just make things look purdy

if (defined('STDIN'))
{
	$start_date = $argv[1];
	$end_date = $argv[2];
	$page_size = $argv[3];
	/*
	 * THIS IS SO SUPER IMPORTANT AND SUPER STUPID!!!
	 * CURRENT PAGE _CANNOT_ BE 0!! IT MUST BEGIN WITH 1
	 * PAGE 0 DOES NOT EXIST FOR THESE CRAZY HUMANS
	 *
	 * (page 0 returns a blank array with no useful data, so don't waste 2 hours like I did)
	 *   _________________________________
    |.--------_--_------------_--__--.|
    ||    /\ |_)|_)|   /\ | |(_ |_   ||
    ;;`,_/``\|__|__|__/``\|_| _)|__ ,:|
   ((_(-,-----------.-.----------.-.)`)
    \__ )        ,'     `.        \ _/
    :  :        |_________|       :  :
    |-'|       ,'-.-.--.-.`.      |`-|
    |_.|      (( (*  )(*  )))     |._|
    |  |       `.-`-'--`-'.'      |  |
    |-'|        | ,-.-.-. |       |._|
    |  |        |(|-|-|-|)|       |  |
    :,':        |_`-'-'-'_|       ;`.;
     \  \     ,'           `.    /._/
      \/ `._ /_______________\_,'  /
       \  / :   ___________   : \,'
        `.| |  |           |  |,'
          `.|  | (kill all |  |
            |  |  humans)  |  |

	 */
	$current_page = $argv[4];
	//$php_error_location = $argv[5];
}
else
{
	$error = "problem getting the input fields for the meritus worker thing (doing_meritus_pull.php)";
	databaseGeneralLog($error);
	//logIt($error);
	die();
}
	databaseGeneralLog("Ok. worker started with page $current_page");

		$data1 = array(
            "MerchantID" => "32387",
            "MerchantKey" => "bba19729-a1ed-48fb-8e21-5b3b38bab01b",
            "ReportName" => "BatchDetail",
            "ReportFormat" => "JSON",
            "StartDate" => "$start_date",
            "EndDate" => "$end_date",
            "PageSize" => "$page_size",
            "CurrentPage" => "$current_page"
        );

		$url = "https://webservice.paymentxp.com/rpt/Report.aspx";

        $curlSession = curl_init();
        curl_setopt($curlSession, CURLOPT_URL, $url);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curlSession, CURLOPT_POST, 1);
        curl_setopt($curlSession, CURLOPT_POSTFIELDS, $data1);
        curl_setopt($curlSession, CURLOPT_TIMEOUT,500000);
        if (php_uname('s') == "Darwin")  //OSX is called Darwin for some reason
        {
            //curl_setopt($curlSession, CURLOPT_CAINFO, "/Users/jonjenne/reporting/nmi/cacert.pem"); //HTTP cert
            curl_setopt($curlSession, CURLOPT_CAINFO, "/Users/jonjenne/reporting/meritus/cacert.pem"); //HTTPS cert
        }
        else if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') // this might work for windows, idk
        {
            //logIt('This is Windows! (Ryan, set your path to the cert here - We are now dying)');
			databaseGeneralLog('This is Windows! (Ryan, set your path to the cert here - We are now dying)');
            die();
        }
        else //some kind of Linux
        {
            curl_setopt($curlSession, CURLOPT_CAINFO, "/var/www/reporting.jcoffice.net/nmi/ca-bundle.crt"); //HTTPS cert
            //databaseGeneralLog("running under linux CHECK THE CERT PATH BEFORE CONTINUING (not dieing)");
            //die(); //kill this so it reminds me to change this for the linux path.
        }
        $rawresponse = curl_exec($curlSession);
        curl_close ($curlSession);
        $transactions = json_decode($rawresponse, true);

        for($i=1;$i< count($transactions['rows']);$i++)
        {
            $pages = $transactions['rows'][$i]['TotalRecordCount']; //set how many pages total

			$mysqli = initialize_db("meritus");

			$insertSQL = sprintf("INSERT INTO meritus_transactions (api_username,api_password,gateway_id,AuthCode,BatchNo,CardNo,CardType,ReportDate,TerminalNumber,TotalRecordCount,TransAmount,TransDate,TransType,TransTypeID) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                GetSQLValueString("32387","text", $mysqli),
                GetSQLValueString("bba19729-a1ed-48fb-8e21-5b3b38bab01b","text", $mysqli),
                GetSQLValueString("6","text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['AuthCode'], "text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['BatchNo'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['CardNo'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['CardType'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['ReportDate'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['TerminalNumber'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['TotalRecordCount'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['TransAmount'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['TransDate'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['TransType'],"text", $mysqli),
                GetSQLValueString($transactions['rows'][$i]['TransTypeID'], "text", $mysqli));
			//databaseGeneralLog($insertSQL);
            doDBthing($insertSQL);
      	}

databaseGeneralLog("Page $current_page done. Date range $start_date - $end_date ");

function GetSQLValueString($theValue, $theType, $mysqli, $theDefinedValue = "", $theNotDefinedValue = "")
{
    $theValue = $mysqli->real_escape_string($theValue);
    switch ($theType) {
        case "text":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "long":
        case "int":
            $theValue = ($theValue != "") ? intval($theValue) : "NULL";
            break;
        case "double":
            $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
            break;
        case "date":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "defined":
            $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
            break;
    }
    return $theValue;
}
?> 
