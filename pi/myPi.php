<?php
/**
 * Created by PhpStorm.
 * User: Ryan
 * Date: 2015-05-20
 * Time: 10:38 AM
 */

require('../includes/general_functions_all.php');

$mysqli = initialize_db('pi');
$pi = $mysqli->query("SELECT tt.*
FROM roleCall tt
INNER JOIN
    (SELECT piName, MAX(checkTime) AS MaxDateTime
    FROM roleCall
    GROUP BY piName) groupedtt
ON tt.piName = groupedtt.piName
AND tt.checkTime = groupedtt.MaxDateTime") or die($mysqli->error);

$pi_fields = mysqli_fetch_fields($pi);

echo "<table border='1'><tr>";
foreach ($pi_fields as $field) {
    echo "<td><b>" . $field->name . "</b></td>";
}
echo "</tr>";


while ($get_pi = $pi->fetch_assoc()) {
    echo "<tr>";
    foreach ($get_pi as $key => $value) {
        echo "<td>" .  $value  . "</td>";
    }
    echo "</tr>";
}
echo "</tr></table>";

$mysqli->close();

