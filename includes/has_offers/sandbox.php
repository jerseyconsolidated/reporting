
<?
require ("src/HasOffersApi.php");
require '../../../secure/includes/kint/Kint.class.php';
$api = new SimpleHasOffersApi();
$api->prepare('Report', 'getConversions');
$api->addParameter('fields', array(
		'Stat.id'
		,'Stat.offer_id'
		,'Stat.affiliate_id'
		,'Stat.payout'
		,'Stat.revenue'
	));
$result = $api->execute();
d($result);
?>