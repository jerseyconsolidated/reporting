<?php

    $xml = new AuthnetXML(AUTHNET_LOGIN, AUTHNET_TRANSKEY, AuthnetXML::USE_PRODUCTION_SERVER);

    $xml->ARBCreateSubscriptionRequest(array(

        'refId' => $arb_rebill_num,

        'subscription' => array(

            'name' => $arb_subscription_name,

            'paymentSchedule' => array(

                'interval' => array(

                    'length' => $arb_interval_lenth,

                    'unit' => $arb_interval_unit

                ),

                'startDate' => $arb_rebill_date,

                'totalOccurrences' => $arb_total_occurences,

//                'trialOccurrences' => '1'

            ),

            'amount' => $arb_amount,

//            'trialAmount' => '0.00',

            'payment' => array(

                'creditCard' => array(

                    'cardNumber' => $arb_payment_cc_num,

                    'expirationDate' => $arb_payment_exp,

					'cardCode' => $arb_payment_cvv

                )

            ),

			 'order' => array(

                'invoiceNumber' => $arb_order_order_num,

                'description' => $arb_order_order_descr

            ),

			 'customer' => array(

                'id' => $arb_customer_cust_no,

                'email' => $arb_customer_email,

                'phoneNumber' => $arb_customer_phone

            ),

            'billTo' => array(

                'firstName' => $arb_bill_to_first_name,

                'lastName' => $arb_bill_to_last_name,

                'address' => $arb_bill_to_address1,

                'city' => $arb_bill_to_city,

                'state' => $arb_bill_to_state,

                'zip' => $arb_bill_to_zip,

                'country' => $arb_bill_to_country

            ),

            'shipTo' => array(

                'firstName' => $arb_ship_to_first_name,

                'lastName' => $arb_ship_to_last_name,

                'address' => $arb_ship_to_address1,

                'city' => $arb_ship_to_city,

                'state' => $arb_ship_to_state,

                'zip' => $arb_ship_to_zip,

                'country' => $arb_ship_to_country

            )

        )

    ));

$arb_ref_id = $xml->refId;

$arb_subscr_id = $xml->subscriptionId;

$arb_message = $xml->messages->message->code;

?>