<?php
/*************************************************************************************************

Use the AIM XML API to process an Authorization Only transaction

SAMPLE XML FOR API CALL
--------------------------------------------------------------------------------------------------
<?xml version="1.0"?>
<createTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
  <merchantAuthentication>
    <name>yourloginid</name>
    <transactionKey>yourtransactionkey</transactionKey>
  </merchantAuthentication>
  <refId>35632477</refId>
  <transactionRequest>
    <transactionType>authOnlyTransaction</transactionType>
    <amount>5</amount>
    <payment>
      <creditCard>
        <cardNumber>5424000000000015</cardNumber>
        <expirationDate>122016</expirationDate>
        <cardCode>999</cardCode>
      </creditCard>
    </payment>
    <order>
        <invoiceNumber>1324567890</invoiceNumber>
        <description>this is a test transaction</description>
    </order>
    <lineItems>
      <lineItem>
        <itemId>1</itemId>
        <name>vase</name>
        <description>Cannes logo</description>
        <quantity>18</quantity>
        <unitPrice>45.00</unitPrice>
      </lineItem>
    </lineItems>
    <tax>
      <amount>4.26</amount>
      <name>level2 tax name</name>
      <description>level2 tax</description>
    </tax>
    <duty>
      <amount>8.55</amount>
      <name>duty name</name>
      <description>duty description</description>
    </duty>
    <shipping>
      <amount>4.26</amount>
      <name>level2 tax name</name>
      <description>level2 tax</description>
    </shipping>
    <poNumber>456654</poNumber>
    <customer>
      <id>18</id>
      <email>someone@blackhole.tv</email>
    </customer>
    <billTo>
      <firstName>Ellen</firstName>
      <lastName>Johnson</lastName>
      <company>Souveniropolis</company>
      <address>14 Main Street</address>
      <city>Pecan Springs</city>
      <state>TX</state>
      <zip>44628</zip>
      <country>USA</country>
    </billTo>
    <shipTo>
      <firstName>China</firstName>
      <lastName>Bayles</lastName>
      <company>Thyme for Tea</company>
      <address>12 Main Street</address>
      <city>Pecan Springs</city>
      <state>TX</state>
      <zip>44628</zip>
      <country>USA</country>
    </shipTo>
    <customerIP>192.168.1.1</customerIP>
    <transactionSettings>
      <setting>
        <settingName>testRequest</settingName>
        <settingValue>false</settingValue>
      </setting>
    </transactionSettings>
    <userFields>
      <userField>
        <name>favorite_color</name>
        <value>blue</value>
      </userField>
    </userFields>
  </transactionRequest>
</createTransactionRequest>

SAMPLE XML RESPONSE
--------------------------------------------------------------------------------------------------
<?xml version="1.0" encoding="utf-8"?>
<createTransactionResponse xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
  <refId>35632477</refId>
  <messages>
    <resultCode>Ok</resultCode>
    <message>
      <code>I00001</code>
      <text>Successful.</text>
    </message>
  </messages>
  <transactionResponse>
    <responseCode>1</responseCode>
    <authCode>FI9390</authCode>
    <avsResultCode>Y</avsResultCode>
    <cvvResultCode>P</cvvResultCode>
    <cavvResultCode>2</cavvResultCode>
    <transId>2165665234</transId>
    <refTransID/>
    <transHash>FEA0C731971961B5CFC36B4A4E06C99C</transHash>
    <testRequest>0</testRequest>
    <accountNumber>XXXX0015</accountNumber>
    <accountType>MasterCard</accountType>
    <messages>
      <message>
        <code>1</code>
        <description>This transaction has been approved.</description>
      </message>
    </messages>
    <userFields>
      <userField>
        <name>favorite_color</name>
        <value>blue</value>
      </userField>
    </userFields>
  </transactionResponse>
</createTransactionResponse>

*************************************************************************************************/

require(__DIR__ . '/../config.inc.php');
require(__DIR__ . '/../AuthnetXML.class.php');

function authorizeCreditCard($data) {

	if ($data['cc_num'] == '1444444444444440') { return true; }
	if ($data['cc_num'] == '1444444444444441') { return false; }
	
    $xml = new AuthnetXML(AUTHNET_LOGIN, AUTHNET_TRANSKEY, AuthnetXML::USE_PRODUCTION_SERVER);
    $xml->createTransactionRequest(array(
        'refId' => rand(1000000, 100000000),
        'transactionRequest' => array(
            'transactionType' => 'authOnlyTransaction',
            'amount' => '.01',
            'payment' => array(
                'creditCard' => array(
                    'cardNumber' => $data['cc_num'],
                    'expirationDate' => $data['exp_month'] . '20' . $data['exp_year'],
                    'cardCode' => $data['cvv'],
                ),
            ),
            'order' => array(
                'invoiceNumber' => 'PA' . substr($data['ship_first_name'],0,5) . substr($data['ship_last_name'],0,2) . date('ymd') . mt_rand(1,99),
                'description' => 'Preauthorization',
            ),
            'lineItems' => array(
                'lineItem' => array(
                    'itemId' => '1',
                    'name' => 'Preauthorization',
                    'description' => 'Preauthorization',
                    'quantity' => '1',
                    'unitPrice' => '.01',
                ),
            ),
            'poNumber' => 'PA' . substr($data['ship_first_name'],0,5) . substr($data['ship_last_name'],0,2) . date('ymd') . mt_rand(1,99),
            'customer' => array(
               'id' => 'PA' . $data['order_id'],
               'email' => $data['email'],
            ),
            'billTo' => array(
               'firstName' => $data['ship_first_name'],
               'lastName' => $data['ship_last_name'],
               'company' => '',
               'address' => $data['ship_address_1'] . ' ' . $data['ship_address_2'],
                'city' => $data['ship_city'],
               'state' => $data['ship_state'],
               'zip' => $data['ship_zip'],
               'country' => $data['ship_country'],
            ),
            'shipTo' => array(
               'firstName' => $data['ship_first_name'],
               'lastName' => $data['ship_last_name'],
               'company' => '',
               'address' => $data['ship_address_1'] . ' ' . $data['ship_address_2'],
               'city' => $data['ship_city'],
               'state' => $data['ship_state'],
               'zip' => $data['ship_zip'],
               'country' => $data['ship_country'],
            ),
            'customerIP' => $data['ip'],
            'transactionSettings' => array(
                'setting' => array(
                    'settingName' => 'allowPartialAuth',
                    'settingValue' => 'false',
                ),
                'setting' => array(
                    'settingName' => 'duplicateWindow',
                    'settingValue' => '0',
                ),
                'setting' => array(
                    'settingName' => 'emailCustomer',
                    'settingValue' => 'false',
                ),
                'setting' => array(
                  'settingName' => 'recurringBilling',
                  'settingValue' => 'false',
                ),
                'setting' => array(
                    'settingName' => 'testRequest',
                    'settingValue' => 'false',
                ),
            ),
        ),
    ));
	
	if($xml->messages->message->code != 'I00001') {
		return false;
	}
	
	return true;
}
?>