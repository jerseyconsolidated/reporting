<?php

require('/home/biohealt/public_html/secure/api/auth_api/config.inc.php');

require('/home/biohealt/public_html/secure/api/auth_api/AuthnetXML.class.php');



    $xml = new AuthnetXML(AUTHNET_LOGIN, AUTHNET_TRANSKEY, AuthnetXML::USE_DEVELOPMENT_SERVER);

    $xml->createTransactionRequest(array(

        'refId' => rand(1000000, 100000000),

        'transactionRequest' => array(

            'transactionType' => 'authCaptureTransaction',

            'amount' => $total_amount,

            'payment' => array(

                'creditCard' => array(

                    'cardNumber' => $cc_num,

                    'expirationDate' => $cc_exp_month.$cc_exp_year,

//                    'cardCode' => $cc_cvv,

                ),

            ),

            'order' => array(

                'invoiceNumber' => $order_no,

                'description' => $order_descr,

            ),

            'lineItems' => array(

                'lineItem' => array(

                    'itemId' => $item1_id,

                    'name' => $item1_name,

                    'description' => $item1_descr,

                    'quantity' => $item1_qty,

                    'unitPrice' => $item1_price_ea,

                ),

/*               'lineItem' => array(

                    'itemId' => $item2_id,

                    'name' => $item2_name,

                    'description' => $item2_descr,

                    'quantity' => $item2_qty,

                    'unitPrice' => $item2_price_ea,

                ),

                 'lineItem' => array(

                    'itemId' => $item3_id,

                    'name' => $item3_name,

                    'description' => $item3_descr,

                    'quantity' => $item3_qty,

                    'unitPrice' => $item3_price_ea,

                ),*/

			),

/*            'tax' => array(

               'amount' => $tax_amount,

               'name' => $tax_name,

               'description' => $tax_descr,

            ),*/

/*            'duty' => array(

               'amount' => '8.88',

               'name' => 'duty name',

               'description' => 'duty description',

            ),*/

            'shipping' => array(

               'amount' => $shipping_amount,

               'name' => $shipping_name,

               'description' => $shipping_descr,

            ),

/*            'poNumber' => '456654',

            'customer' => array(

               'id' => '18',

               'email' => 'ryan@kielventures.com',

            ),*/

            'customer' => array(

               'id' => $customer_no,

               'email' => $email,

            ),

            'billTo' => array(

               'firstName' => $first_name,

               'lastName' => $last_name,

//               'company' => 'company',

               'address' => $address1,

               'city' => $city,

               'state' => $state,

               'zip' => $zip,

               'country' => $country,

			   'phoneNumber' => $phone,

            ),

            'shipTo' => array(

               'firstName' => $ship_to_first_name,

               'lastName' => $ship_to_last_name,

//               'company' => 'company',

               'address' => $ship_to_address1,

               'city' => $ship_to_city,

               'state' => $ship_to_state,

               'zip' => $ship_to_zip,

               'country' => $ship_to_country,

            ),

            'customerIP' => $customer_ip,

            'transactionSettings' => array(

                'setting' => array(

                    'settingName' => 'allowPartialAuth',

                    'settingValue' => 'false',

                ),

                'setting' => array(

                    'settingName' => 'duplicateWindow',

                    'settingValue' => '0',

                ),

                'setting' => array(

                    'settingName' => 'emailCustomer',

                    'settingValue' => 'false',

                ),

                'setting' => array(

                  'settingName' => 'recurringBilling',

                  'settingValue' => 'true',

                ),

                'setting' => array(

                    'settingName' => 'testRequest',

                    'settingValue' => 'false',

                ),

            ),

            'userFields' => array(

                'userField' => array(

                    'name' => 'aff_id',

                    'value' => $aff_id,

                ),

                'userField' => array(

                    'name' => 'aff_traf_source',

                    'value' => $aff_traf_source,

                ),

            ),

        ),

    ));

	

$auth_refId = $xml->refId;

$auth_transId = $xml->transactionResponse->transId;

$auth_resultCode = $xml->messages->resultCode;

$auth_tans_code = $xml->messages->message->code;

$auth_trans_text = $xml->messages->message->text;

$auth_responseCode = $xml->transactionResponse->responseCode;

$auth_authCode = $xml->transactionResponse->authCode;

$auth_avsResultCode = $xml->transactionResponse->avsResultCode;

$auth_cvvResultCode = $xml->transactionResponse->cvvResultCode;

$auth_cavvResultCode = $xml->transactionResponse->cavvResultCode;

$auth_accountNumber = $xml->transactionResponse->accountNumber;

$auth_accountType = $xml->transactionResponse->accountType;

$auth_code = $xml->transactionResponse->messages->message->code;

$auth_description = $xml->transactionResponse->messages->message->description;

$auth_errorCode = $xml->transactionResponse->errors->error->errorCode;

$auth_errorText = $xml->transactionResponse->errors->error->errorText;

$auth_name = $xml->transactionResponse->userFields->userField->name;

$auth_value = $xml->transactionResponse->userFields->userField->value;



?>