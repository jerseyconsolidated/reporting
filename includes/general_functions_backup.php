<?php

if (isset($_GET['phpinfo'])) {
    echo phpinfo();
}

/*
 * This allows any thread to write to a file.
 * Hopefully, the only foreign threads writing to a file are actually writing to files that they own, and not a shared resource.
 *
 * Things will probably break if they all try to write to 1 file.
 */
function log_error($error, $file="default") {
    $myfile = fopen("/var/www/reporting.jcoffice.net/errors/$file", "a+"); //or die("Unable to open file!");
    fwrite($myfile, $error . " "  . date('Y-m-d H:i:s') . "\r\n\"");
    fclose($myfile);
}
function authIP(){
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    $allowedIPs = array(
        '47.22.48.130',
        '47.22.48.140',
        '47.22.48.149',
        '192.168.1.51',
        '192.168.1.51',
        '100.38.12.125',
        '127.0.0.1',
        'localhost',
        '47.22.48.140',
        '192.168.1.104',
        //PTELL
        '207.173.207.194',
        '50.249.110.209',
        //LL
        '206.188.6.215',
        '206.188.6.214',
        '206.188.6.213',
        '206.188.6.212',
        '206.188.6.211',
        '206.188.6.210',
        '206.188.6.209',
        '206.188.6.208',
        '209.18.66.15',
        '209.18.66.14',
        '209.18.66.13',
        '209.18.66.12',
        '209.18.66.11',
        '209.18.66.10',
        '209.18.66.9',
        '209.18.66.8',
        '209.18.66.7',
        '209.18.66.6',
        '209.18.66.5',
        '209.18.66.4',
        '209.18.66.3',
        '209.18.66.2',
        '209.18.66.1',
        '209.18.66.0'
        //END LL
    );
    if (!in_array($ip,$allowedIPs)) {
        header('Location: http://google.com');
    }
      return (in_array($ip,$allowedIPs) ?  true :  false);
}
function prepare_xero_auth(){
    require ('C:\inetpub\wwwroot\reports\includes\xero\lib\XeroOAuth.php');
    define ('BASE_PATH', dirname(__FILE__));
    define ("XRO_APP_TYPE", "Private");
    define ("OAUTH_CALLBACK", "oob");
    $useragent = "jc";

    $signatures = array(
        'consumer_key' => 'G0BZY0D3BXKSZYTHMTRAJWSOPLITHS',
        'shared_secret' => 'NJ2RDQUCMVREDBKSSZQ9R9MGQPOYOG',
        // API versions
        'core_version' => '2.0',
        'payroll_version' => '1.0',
        'file_version' => '1.0'
    );

    if (XRO_APP_TYPE == "Private" || XRO_APP_TYPE == "Partner") {
        $signatures ['rsa_private_key'] = 'C:\inetpub\wwwroot\certs\privatekey.pem';
        $signatures ['rsa_public_key'] = 'C:\inetpub\wwwroot\certs\publickey.cer';
    }

    $XeroOAuth = new XeroOAuth (array_merge(array(
        'application_type' => XRO_APP_TYPE,
        'oauth_callback' => OAUTH_CALLBACK,
        'user_agent' => $useragent
    ), $signatures));
    include 'C:\inetpub\wwwroot\reports\includes\xero\tests\testRunner.php';

    $initialCheck = $XeroOAuth->diagnostics();
    $checkErrors = count($initialCheck);
    if ($checkErrors > 0) {
        // you could handle any config errors here, or keep on truckin if you like to live dangerously
        foreach ($initialCheck as $check) {
            echo 'Error: ' . $check . PHP_EOL;
        }
    } else {
        $session = persistSession(array(
            'oauth_token' => $XeroOAuth->config ['consumer_key'],
            'oauth_token_secret' => $XeroOAuth->config ['shared_secret'],
            'oauth_session_handle' => ''
        ));
        $oauthSession = retrieveSession();

        if (isset ($oauthSession ['oauth_token'])) {
            $XeroOAuth->config ['access_token'] = $oauthSession ['oauth_token'];
            $XeroOAuth->config ['access_token_secret'] = $oauthSession ['oauth_token_secret'];
        }
    }
    return $XeroOAuth;
}
function GetSQLValueString($theValue, $theType, $mysqli, $theDefinedValue = "", $theNotDefinedValue = "")
{
    $theValue = $mysqli->real_escape_string($theValue);
    switch ($theType) {
        case "text":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "long":
        case "int":
            $theValue = ($theValue != "") ? intval($theValue) : "NULL";
            break;
        case "double":
            $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
            break;
        case "date":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "defined":
            $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
            break;
    }
    return $theValue;
}
function debug($DebugData, $mysqli) {
    return $DebugData . "<br />" . $mysqli->error;
}
function hasUserPurchasedLL() {
    if(isset($_GET['jc']) && ($_GET['jc'] == 'test')) {
        return false;
    }

    if(LlOrderModel::model()->find('status=:status and ip=:ip',array(':status'=>LlOrderModel::STATUS_PROCESSED,':ip'=>getRealIpAddr()))) {
        return true;
    }

    return false;
}
function initialize_db($mysqli, $username="biohealth1", $password="7s63VfusbHVFuq"){
//function initialize_db($mysqli){
    //var_dump(debug_backtrace());
    // die();
    //$mysqli = (!isset($mysqli) ? $mysqli = 'limelight' : $mysqli);

    //echo $mysqli;
    //open up our connection
    $mysqli = new mysqli("localhost",$username,$password,'limelight');
    // Check connection
    if (mysqli_connect_errno()) {
        //printf("Connect failed: %s\n", mysqli_connect_error());
        log_error(mysqli_connect_error(), "error".getmypid());
        exit();
    }
    return $mysqli;
}
function doRedirect($url,$return_json_on_ajax = true) {
    if($_GET['ajax']) {
        if($return_json_on_ajax) {
            echo json_encode(array('redirect'=>$url));
        }
        else {
            echo $url;
        }
    }
    else {
        header('Location: ' . $url);
    }

    exit;
}
function ll_api($data_in)
{
    $url = "https://www.jcsecured.com/admin/membership.php";
    $api_acounts = array(
        "biohealth1" => "7s63VfusbHVFuq",
        "biohealth2" => "JaG3HMA86HxQc",
        "biohealth3" => "RvTfExUyrf8HEX",
        "biohealth4" => "FVJzA42euHVnE",
        "biohealth5" => "c8WJabw4RqjEVu",
        "biohealth6" => "qFAEgHEyM5CNRR",
        "biohealth7" => "pd8hdHn9KFzcaS",
        "biohealth8" => "qbERYrGWNgWMms",
        "biohealth9" => "JwxVaYuzChFyEA",
        "biohealth10" => "3nJmtDarHNwSJU",
        "biohealth11" => "vB3CAe9TgN9cax",
        "biohealth12" => "TWSva4KXDEq6kC",
        "biohealth13" => "aTkNp9k2rVttYf",
        "biohealth14" => "sDpwfF6kvZn9r",
        "biohealth15" => "wmBvW8hVDxwWTh",
        "biohealth16" => "xRSy2CnhQysu",
        "biohealth17" => "EZNhYXn6Zcbs3",
        "biohealth18" => "JF9Bfhzu4FFwgJ",
        "biohealth19" => "m7pKdtqpQpsv5",
        "biohealth20" => "qrQ2MHYRj9yvrg",
        "biohealth21" => "6mMsrbtuH5UR5K",
        "biohealth22" => "XF7yPJnMpnCB5Z",
        "biohealth23" => "TypgxDt6vXkBfZ",
        "biohealth24" => "U2zuPZbck83jXT",
        "biohealth25" => "btUpuWVyFjCQaA",
        "biohealth26" => "yvsssJJ2Ey6Ngw",
        "biohealth27" => "GThxpNWDZjGHdv",
        "biohealth28" => "HB9H3aCfGGGQX2",
        "biohealth29" => "TWBd9kFh5TxB3",
        "biohealth30" => "jCj9x5zTdJ8tJx",
        "biohealth31" => "VspMtsJ4sYYxkC",
        "biohealth32" => "rtAKKc5bh4nNMz",
        "biohealth33" => "Hjy5ABqpuwzdKQ",
        "biohealth34" => "4YVkApehYFmwjd",
        "biohealth35" => "ued8xBuDskfAap",
        "biohealth36" => "nVyDQrDBWqFcxT",
        "biohealth37" => "svK8E4T4wXSeMb",
        "biohealth38" => "7NBVPV6htYgQz2",
        "biohealth39" => "h4bwtsBPjkNXZh",
        "biohealth40" => "HrhDx74Pnsk5f"
    );
    $rand = rand(1,count($api_acounts));
    $username = "biohealth".$rand;
    $password = $api_acounts['biohealth'.$rand];

    $data = array(
        "username" => $username,
        "password" => $password,
    );
    $data = array_merge_recursive($data, $data_in);
    $curlSession = curl_init();
    curl_setopt($curlSession, CURLOPT_URL, $url);
    curl_setopt($curlSession, CURLOPT_HEADER, 0);
    curl_setopt($curlSession, CURLOPT_POST, 1);
    curl_setopt($curlSession, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curlSession, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($curlSession, CURLOPT_TIMEOUT,5000);
    curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curlSession, CURLOPT_SSL_VERIFYHOST, 2);
    $rawresponse = curl_exec($curlSession);
    $output = array();
    parse_str($rawresponse,$output);
    curl_close ($curlSession);

    return $output;

}
function _send_criteria($criteria,$modifier) {
/////BREAK DATE INTO HASOFFERS STYLE
    $ha_start_date = explode('-',$_POST['start_date']);
    $ha_start_date = $ha_start_date[1]."/".$ha_start_date[2]."/".$ha_start_date[0];
    $ha_end_date = explode('-',$_POST['end_date']);
    $ha_end_date = $ha_end_date[1]."/".$ha_end_date[2]."/".$ha_end_date[0];

    //SET STAT ARRAY
    $data1 = array(
        "method" => "order_find",
        "campaign_id" => "all", //CSV-Numeric or all
//	"start_date" => $ha_start_date, //MM/DD/YYYY
//	"end_date" => $ha_end_date,
        "start_date" => "09/01/2013", //MM/DD/YYYY
        "end_date" => "09/06/2013",
        "return_type" => "order_view", //Blank or "order_view"
        "search_type" => "all" //all,any
    );
    $data = array_merge_recursive($data1, $criteria);
    $affiliate_data = _send_ll_Request($data);

    if ($modifier == "qty") {
        if (is_array($affiliate_data)) { $affiliate_data_result = $affiliate_data['total_orders']; } else { $affiliate_data_result = "0"; }
        return $affiliate_data_result;
    }
    if ($modifier == "order_ids") {
        $affiliate_data_result = $affiliate_data['order_ids'];
        return $affiliate_data_result;
    }
    if ($modifier == "json") {
        $rev_data = json_decode($affiliate_data['data'], true);
        return $rev_data;
    }
    else return 0;
}
function _ping_hasoffers($target, $method, $fields, $filters, $groups, $totals)
{
    $base = 'https://api.hasoffers.com/Api?';
    $params = array(
        'Format' => 'json'
    ,'Target' => $target
    ,'Method' => $method
    ,'Service' => 'HasOffers'
    ,'Version' => 2
    ,'NetworkId' => 'ianternet'
    ,'NetworkToken' => 'NETxHqUUAunywEOAQO8Eh2XbshwBSh'
    ,'fields' => $fields
    ,'filters' => $filters
    ,'groups' => $groups
    ,'totals' => $totals
    ,'sort' => $sort
    );

    $url = $base . http_build_query( $params );
    $result = file_get_contents( $url );
    $final_result = json_decode( $result, true ) ;
    return $final_result;
}
function getRealIpAddr(){
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
    {$ip=$_SERVER['HTTP_CLIENT_IP'];}
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
    {$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];}
    else
    {$ip=$_SERVER['REMOTE_ADDR'];}
    return $ip;
}
function do_xdebug()
{
    ini_set('xdebug.collect_params', '4');
    ini_set('xdebug.collect_vars', 'on');
    ini_set('xdebug.dump_globals', 'on');
    ini_set('xdebug.show_local_vars', 'on');
    ini_set('xdebug.overload_var_dump', '0');
    ini_set('xdebug.dump.COOKIE', '*');
    ini_set('xdebug.dump.GLOBALS', '*');
    ini_set('xdebug.dump.FILES', '*');
    ini_set('xdebug.dump.GET', '*');
    ini_set('xdebug.dump.POST', '*');
    ini_set('xdebug.dump.REQUEST', '*');
    ini_set('xdebug.dump.SESSION', '*');
//ini_set('xdebug.dump.SERVER', '*');

    xdebug_debug_zval();


    xdebug_get_headers();
    xdebug_print_function_stack();
    xdebug_get_function_stack();
    xdebug_call_line();
    xdebug_get_declared_vars();
    xdebug_dump_superglobals();
    var_dump(xdebug_get_declared_vars());
}
function worker_ll_api($data_in,$username,$password)
{
    $url = "https://www.jcsecured.com/admin/membership.php";

    $data = array(
        "username" => $username,
        "password" => $password,
    );
    $data = array_merge_recursive($data, $data_in);
    $curlSession = curl_init();
    curl_setopt($curlSession, CURLOPT_URL, $url);
    curl_setopt($curlSession, CURLOPT_HEADER, 0);
    curl_setopt($curlSession, CURLOPT_POST, 1);
    curl_setopt($curlSession, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curlSession, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($curlSession, CURLOPT_TIMEOUT,5000);
    curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curlSession, CURLOPT_SSL_VERIFYHOST, 2);
    $rawresponse = curl_exec($curlSession);
    $output = array();
    parse_str($rawresponse,$output);
    curl_close ($curlSession);
    return $output;
}

function worker_ll_api_jon($data_in,$username,$password)
{

    $url = "https://www.jcsecured.com/admin/membership.php";

    $data = array(
        "username" => $username,
        "password" => $password,
    );
    $data = array_merge_recursive($data, $data_in);
    $curlSession = curl_init();
    curl_setopt($curlSession, CURLOPT_URL, $url);
    curl_setopt($curlSession, CURLOPT_HEADER, 0);
    curl_setopt($curlSession, CURLOPT_POST, 1);
    curl_setopt($curlSession, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curlSession, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($curlSession, CURLOPT_TIMEOUT,5000);
    curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curlSession, CURLOPT_SSL_VERIFYHOST, 2);
    $rawresponse = curl_exec($curlSession);

    //$decode = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $rawresponse);

    //remove spaces on either end
    $rawresponse = trim($rawresponse);
    // drop all non utf-8 characters
    //$rawresponse = iconv("UTF-8", "UTF-8//IGNORE", $rawresponse);
    //remove bad UTF-8 characters, hopefully?
    /*$rawresponse = preg_replace('/(?>[\x00-\x1F]|\xC2[\x80-\x9F]|\xE2[\x80-\x8F]{2}|\xE2\x80[\xA4-\xA8]|\xE2\x81[\x9F-\xAF])/', ' ', $rawresponse);*/
    //reduce all multiple whitespace to a single space
    //$rawresponse = preg_replace('/\s+/', ' ', $rawresponse);

    $rawresponse = preg_replace('[A-Za-z0-9();{}\[\]\\!@#$%^&*()_+\-=.,<>:/?\'"]', '', $rawresponse);

    $rawresponse = utf8_decode($rawresponse);
    //$decode = mb_convert_encoding($rawresponse, 'UTF-8', 'UTF-8');
    $decode = utf8_encode($rawresponse);
    $output = array();
    parse_str($rawresponse,$output);
    curl_close ($curlSession);
	var_dump($output);
    return $output;
}

function doDBthing($query, $mysqli="limelight")
{
    //echo "\nbefore the query\n";


    $mysqli = initialize_db($mysqli);

    if($prepared_mysqli = $mysqli->prepare($query))
    {
        $prepared_mysqli->execute();

        $prepared_mysqli->store_result();

        $prepared_mysqli->free_result();

        $prepared_mysqli->close();

    }
    else
    {
        echo "\n $query \n";
        //die($mysqli->error);
        log_error(mysqli_connect_error(), "error".getmypid());
    }

    //echo "\n$query\n";
    //$mysqli->store_result();

    //$mysqli->query($query) or die($mysqli->error);

    //echo "\nafter the query\n";
    return true;
}

function update_orders_to_process($id, $worker, $mysqli)
{
    $query = "UPDATE orders_to_process SET worker = '$worker', complete = '1' WHERE order_id = '$id'";
    doDBthing($query, $mysqli);
}
function insert_orders_jon($id, $data, $mysqli) {
    //var_dump($data);
    //die();
    $order_fields = "response_code,ancestor_id,customer_id,parent_id,child_id,order_status,is_recurring,shipping_first_name,shipping_last_name,shipping_street_address,shipping_street_address2,shipping_city,shipping_state,shipping_state_id,shipping_postcode,shipping_country,customers_telephone,main_product_id,main_product_quantity,upsell_product_id,upsell_product_quantity,time_stamp,recurring_date,retry_date,shipping_method_name,shipping_id,tracking_number,on_hold,on_hold_by,hold_date,gateway_id,order_confirmed,order_confirmed_date,is_chargeback,is_fraud,is_rma,rma_number,rma_reason,ip_address,affiliate,sub_affiliate,campaign_id,order_total,order_sales_tax,order_sales_tax_amount,processor_id,created_by_user_name,created_by_employee_name,billing_cycle,click_id";
    $order_fields_arr = explode(',', $order_fields);

    $values = array();
    foreach ($order_fields_arr as $field) {
        //call getsqlvaluestring to put quotes around and escape bad characters
        if(substr($data[$field],2,1) == "/" && substr($data[$field],5,1) == "/") {
            $values[] = "'".date("Y-m-d h:i:s", strtotime($data[$field]))."'";
        }
        else {
            $values[] = GetSQLValueString($data[$field], "text", $mysqli);
        }
    }
    $values = implode(",",$values);
    $order_stuff = 'order_id,'.$order_fields;
    $value_stuff = $id.','.$values;
    doDBthing("INSERT INTO orders ($order_stuff) VALUES ($value_stuff)", $mysqli);
    //$mysqli->query("INSERT INTO orders ('order_id,'.$order_fields) VALUES ($id.','.$values)") or die($mysqli->error);
    return $data['response_code'];
}
function insert_overdue_orders_jon($id, $data, $mysqli) {
    $order_fields = "response_code,ancestor_id,customer_id,parent_id,child_id,order_status,is_recurring,shipping_first_name,shipping_last_name,shipping_street_address,shipping_street_address2,shipping_city,shipping_state,shipping_state_id,shipping_postcode,shipping_country,customers_telephone,main_product_id,main_product_quantity,upsell_product_id,upsell_product_quantity,time_stamp,recurring_date,retry_date,shipping_method_name,shipping_id,tracking_number,on_hold,on_hold_by,hold_date,gateway_id,order_confirmed,order_confirmed_date,is_chargeback,is_fraud,is_rma,rma_number,rma_reason,ip_address,affiliate,sub_affiliate,campaign_id,order_total,order_sales_tax,order_sales_tax_amount,processor_id,created_by_user_name,created_by_employee_name,billing_cycle,click_id";
    $order_fields_arr = explode(',', $order_fields);

    $values = array();
    foreach ($order_fields_arr as $field) {
        //call getsqlvaluestring to put quotes around and escape bad characters
        if(substr($data[$field],2,1) == "/" && substr($data[$field],5,1) == "/") {
            $values[] = "'".date("Y-m-d h:i:s", strtotime($data[$field]))."'";
        }
        else {
            $values[] = GetSQLValueString($data[$field], "text", $mysqli);
        }
    }
    $values = implode(",",$values);
    $mysqli->store_result();

    $overdue_order_stuff = 'order_id,'.$order_fields;
    $value_stuff = $id.','.$values;
    doDBthing("INSERT INTO overdue_orders ($overdue_order_stuff) VALUES ($value_stuff)", $mysqli);

    return $data['response_code'];
}
function insert_customers_jon($id, $data, $mysqli){
    $customer_fields = "customer_id,billing_first_name,billing_last_name,billing_street_address,billing_street_address2,billing_city,billing_state,billing_postcode,billing_country,customers_telephone,email_address";
    $customer_fields_arr = explode(',', $customer_fields);
    //make sure customer_id is not 0
    if($data['customer_id']) {
        $values = array();
        foreach ($customer_fields_arr as $field) {
            //call getsqlvaluestring to put quotes around and escape bad characters
            if(substr($data[$field],2,1) == "/" && substr($data[$field],5,1) == "/") {
                $values[] = "'".date("Y-m-d h:i:s", strtotime($data[$field]))."'";
            }
            else {
                $values[] = GetSQLValueString($data[$field], "text", $mysqli);
            }
        }
        $values = implode(",", $values);
        doDBthing("INSERT INTO customers ($customer_fields) VALUES ($values)", $mysqli);

    }
    return $data['response_code'];
}
function insert_transactions_jon($id, $data, $mysqli){
    $transactions_fields = "cc_type,cc_number,cc_expires,prepaid_match,transaction_id,auth_id,gateway_id,preserve_gateway,amount_refunded_to_date,decline_reason,order_total,order_sales_tax,order_sales_tax_amount,processor_id,billing_cycle";
    $transactions_fields_arr = explode(',', $transactions_fields);
    $values = array();
    foreach ($transactions_fields_arr as $field) {
        //call getsqlvaluestring to put quotes around and escape bad characters
        if(substr($data[$field],2,1) == "/" && substr($data[$field],5,1) == "/") {
            $values[] = "'".date("Y-m-d h:i:s", strtotime($data[$field]))."'";
        }
        else {
            $values[] = GetSQLValueString($data[$field], "text", $mysqli);
        }
    }
    $values = implode(",",$values);
    $transaction_stuff = 'order_id,'.$transactions_fields;
    $value_stuff = $id.','.$values;
    doDbthing("INSERT INTO transactions ($transaction_stuff) VALUES ($value_stuff)",$mysqli);

    return $data['response_code'];
}
function insert_order_details_jon($id, $data, $mysqli){
    $order_id = $id;
    foreach($data['products'] as $product_each){
        $values = array();
        if(isset($product_each['subscription_type'])){
            $order_details_fields = "product_id,sku,price,name,on_hold,is_recurring,recurring_date,product_qty,subscription_id,subscription_type,subscription_desc";
        } else {
            $order_details_fields = "product_id,sku,price,name,on_hold,is_recurring,recurring_date,product_qty,subscription_id";
        }
        $order_details_fields_arr = explode(',', $order_details_fields);
        foreach ($order_details_fields_arr as $field) {
            //call getsqlvaluestring to put quotes around and escape bad characters
            if (isset($product_each[$field])){
                $values[] = GetSQLValueString($product_each[$field], "text", $mysqli);
            }
        }
        $values = implode(",", $values);
        doDBthing("INSERT INTO order_details (order_id,$order_details_fields) VALUES ('$order_id',$values)", $mysqli);

        return $data['response_code'];
    }
}
function insert_notes_jon($id, $data, $mysqli){
    $notes_system_fields = "system_note, note_date";
    $notes_employee_fields = "employee_note, note_date";
    $order_id =$id;


//INSERT SYSTEM NOTES
    if(isset($data['systemNotes']) > 0){
        foreach ($data['systemNotes'] as $system_note_each) {
            //call getsqlvaluestring to put quotes around and escape bad characters
            $values = GetSQLValueString($system_note_each, "text", $mysqli);
            //get a useful date of when the note was made
            preg_match("/^([\S]+\s){3}/", $system_note_each, $output_array);
            $note_date = date('Y-m-d h:i:s',strtotime($output_array[0]));
            doDBthing("INSERT INTO notes_system (order_id,$notes_system_fields) VALUES ('$order_id',$values,'$note_date')",$mysqli);

        }
    }
    //INSERT EMPLOYEE NOTES
    if(isset($data['employeeNotes']) > 0) {
        foreach ($data['employeeNotes'] as $employee_note_each) {
            //call getsqlvaluestring to put quotes around and escape bad characters
            $values = GetSQLValueString($employee_note_each, "text", $mysqli);
            //get a useful date of when the note was made
            preg_match("/^([\S]+\s){3}/", $employee_note_each, $output_array);
			//var_dump($output_array);
			echo "\n\n";
			echo $output_array[0]."\n\n";
			var_dump($data);
            $note_date = date('Y-m-d h:i:s',strtotime($output_array[0]));
            doDBthing("INSERT INTO notes_employee (order_id,$notes_employee_fields) VALUES ('$order_id',$values,'$note_date')",$mysqli);

        }
    }
    return $data['response_code'];
}
function insert_orders_ryan($data, $mysqli) {
    //var_dump($data);
    //die();
    $order_fields = "order_id,response_code,ancestor_id,customer_id,parent_id,child_id,order_status,is_recurring,shipping_first_name,shipping_last_name,shipping_street_address,shipping_street_address2,shipping_city,shipping_state,shipping_state_id,shipping_postcode,shipping_country,customers_telephone,main_product_id,main_product_quantity,upsell_product_id,upsell_product_quantity,time_stamp,recurring_date,retry_date,shipping_method_name,shipping_id,tracking_number,on_hold,on_hold_by,hold_date,gateway_id,order_confirmed,order_confirmed_date,is_chargeback,is_fraud,is_rma,rma_number,rma_reason,ip_address,affiliate,sub_affiliate,campaign_id,order_total,order_sales_tax,order_sales_tax_amount,processor_id,created_by_user_name,created_by_employee_name,billing_cycle,click_id";
    $order_fields_arr = explode(',', $order_fields);

    $values = array();
    foreach ($order_fields_arr as $field) {
        //call getsqlvaluestring to put quotes around and escape bad characters
        if(substr($data[$field],2,1) == "/" && substr($data[$field],5,1) == "/") {
            $values[] = "'".date("Y-m-d", strtotime($data[$field]))."'";
        }
        else {
            $values[] = GetSQLValueString($data[$field], "text", $mysqli);
        }
    }
    $values = implode(",",$values);
    $mysqli->query("INSERT INTO orders ($order_fields) VALUES ($values)") or die($mysqli->error);
    //$mysqli->execute();
    //$mysqli->store_result();
    return $data['response_code'];
}
function insert_overdue_orders($data, $mysqli) {
    $order_fields = "order_id,response_code,ancestor_id,customer_id,parent_id,child_id,order_status,is_recurring,shipping_first_name,shipping_last_name,shipping_street_address,shipping_street_address2,shipping_city,shipping_state,shipping_state_id,shipping_postcode,shipping_country,customers_telephone,main_product_id,main_product_quantity,upsell_product_id,upsell_product_quantity,time_stamp,recurring_date,retry_date,shipping_method_name,shipping_id,tracking_number,on_hold,on_hold_by,hold_date,gateway_id,order_confirmed,order_confirmed_date,is_chargeback,is_fraud,is_rma,rma_number,rma_reason,ip_address,affiliate,sub_affiliate,campaign_id,order_total,order_sales_tax,order_sales_tax_amount,processor_id,created_by_user_name,created_by_employee_name,billing_cycle,click_id";
    $order_fields_arr = explode(',', $order_fields);

    $values = array();
    foreach ($order_fields_arr as $field) {
        //call getsqlvaluestring to put quotes around and escape bad characters
        if(substr($data[$field],2,1) == "/" && substr($data[$field],5,1) == "/") {
            $values[] = "'".date("Y-m-d", strtotime($data[$field]))."'";
        }
        else {
            $values[] = GetSQLValueString($data[$field], "text", $mysqli);
        }
    }
    $values = implode(",",$values);
    $mysqli->query("INSERT INTO overdue_orders ($order_fields) VALUES ($values)") or die($mysqli->error);
    return $data['response_code'];
}
function insert_customers($data, $mysqli){
    $customer_fields = "customer_id,billing_first_name,billing_last_name,billing_street_address,billing_street_address2,billing_city,billing_state,billing_postcode,billing_country,customers_telephone,email_address";
    $customer_fields_arr = explode(',', $customer_fields);
    //make sure customer_id is not 0
    if($data['customer_id']) {
        $values = array();
        foreach ($customer_fields_arr as $field) {
            //call getsqlvaluestring to put quotes around and escape bad characters
            if(substr($data[$field],2,1) == "/" && substr($data[$field],5,1) == "/") {
                $values[] = "'".date("Y-m-d", strtotime($data[$field]))."'";
            }
            else {
                $values[] = GetSQLValueString($data[$field], "text", $mysqli);
            }
        }
        $values = implode(",", $values);
        $mysqli->query("INSERT INTO customers ($customer_fields) VALUES ($values)") or die($mysqli->error);
    }
    return $data['response_code'];
}
function insert_transactions($data, $mysqli){
    $transactions_fields = "order_id,cc_type,cc_number,cc_expires,prepaid_match,transaction_id,auth_id,gateway_id,preserve_gateway,amount_refunded_to_date,decline_reason,order_total,order_sales_tax,order_sales_tax_amount,processor_id,billing_cycle";
    $transactions_fields_arr = explode(',', $transactions_fields);
    $values = array();
    foreach ($transactions_fields_arr as $field) {
        //call getsqlvaluestring to put quotes around and escape bad characters
        if(substr($data[$field],2,1) == "/" && substr($data[$field],5,1) == "/") {
            $values[] = "'".date("Y-m-d", strtotime($data[$field]))."'";
        }
        else {
            $values[] = GetSQLValueString($data[$field], "text", $mysqli);
        }
    }
    $values = implode(",",$values);
    $mysqli->query("INSERT INTO transactions ($transactions_fields) VALUES ($values)") or die($mysqli->error);

    return $data['response_code'];
}
function insert_order_details($data, $mysqli){
    $order_id = $data['order_id'];
    foreach($data['products'] as $product_each){
        $values = array();
        if(isset($product_each['subscription_type'])){
            $order_details_fields = "product_id,sku,price,name,on_hold,is_recurring,recurring_date,product_qty,subscription_id,subscription_type,subscription_desc";
        } else {
            $order_details_fields = "product_id,sku,price,name,on_hold,is_recurring,recurring_date,product_qty,subscription_id";
        }
        $order_details_fields_arr = explode(',', $order_details_fields);
        foreach ($order_details_fields_arr as $field) {
            //call getsqlvaluestring to put quotes around and escape bad characters
            if (isset($product_each[$field])){
                $values[] = GetSQLValueString($product_each[$field], "text", $mysqli);
            }
        }
        $values = implode(",", $values);
        $mysqli->query("INSERT INTO order_details (order_id,$order_details_fields) VALUES ('$order_id',$values)") or die($mysqli->error);

        return $data['response_code'];
    }
}
function insert_notes($data, $mysqli){
    $notes_system_fields = "system_note, note_date";
    $notes_employee_fields = "employee_note, note_date";
    $order_id = $data['order_id'];


//INSERT SYSTEM NOTES
    if(isset($data['systemNotes']) > 0){
        foreach ($data['systemNotes'] as $system_note_each) {
            //call getsqlvaluestring to put quotes around and escape bad characters
            $values = GetSQLValueString($system_note_each, "text", $mysqli);
            //get a useful date of when the note was made
            preg_match("/^([\S]+\s){3}/", $system_note_each, $output_array);
            $note_date = date('Y-m-d',strtotime($output_array[0]));
            $mysqli->query("INSERT INTO notes_system (order_id,$notes_system_fields) VALUES ('$order_id',$values,'$note_date')") or die($mysqli->error);
        }
    }
    //INSERT EMPLOYEE NOTES
    if(isset($data['employeeNotes']) > 0) {
        foreach ($data['employeeNotes'] as $employee_note_each) {
            //call getsqlvaluestring to put quotes around and escape bad characters
            $values = GetSQLValueString($employee_note_each, "text", $mysqli);
            //get a useful date of when the note was made
            preg_match("/^([\S]+\s){3}/", $employee_note_each, $output_array);
            $note_date = date('Y-m-d',strtotime($output_array[0]));
            $mysqli->query("INSERT INTO notes_employee (order_id,$notes_employee_fields) VALUES ('$order_id',$values,'$note_date')") or die($mysqli->error);
        }
    }
    return $data['response_code'];
}
function do_a_prospect($worker, $username, $password, $mysqli)
{
    $prospect_fields = "prospect_id,response_code,campaign_id,first_name,last_name,address,address2,city,state,zip,county,phone,email,ip_adress,date_created";
    $prospect_fields_arr = explode(',', $prospect_fields);

    //get a prospect_id to work on
    $curr_prospect_id = mysqli_fetch_row($mysqli->query("SELECT prospect_id FROM prospects_to_process WHERE (worker = '0' OR worker = '$worker') AND complete = '0' LIMIT 1"));
    //tell DB  that prospect is being worked on
    $mysqli->query("UPDATE orders_to_process SET worker = '$worker' WHERE order_id = '$curr_prospect_id'") or die(mysqli_errno($mysqli));

    //call LL order_view with order id
    $data = array(
        "method" => "prospect_view",
        "order_id" => $curr_prospect_id
    );
//get results from LL API
    $output = array('prospect_id' => $curr_prospect_id) + worker_ll_api($data, $username, $password);

    if ($output['response_code'] == '100') {
        //double check again to make sure another worker didn't already update this one
        $check_collision = $$mysqli->query("SELECT prospect_id, worker FROM prospects_to_process WHERE prospect_id = '$curr_prospect_id' LIMIT 1")->fetch_assoc();
        if ($check_collision['worker'] != $worker) {
            return 0;
        }
        $check_collision = $mysqli->query("SELECT prospect_id FROM prospects_to_process WHERE prospect_id = '$curr_prospect_id' LIMIT 1");
        if (mysqli_num_rows($check_collision) > 0) {
            return 0;
        }

        if($data['prospect_id']) {
            $values = array();
            foreach ($prospect_fields_arr as $field) {
                //call getsqlvaluestring to put quotes around and escape bad characters
                if(substr($data[$field],2,1) == "/" && substr($data[$field],5,1) == "/") {
                    $values[] = "'".date("Y-m-d", strtotime($data[$field]))."'";
                }
                else {
                    $values[] = GetSQLValueString($data[$field], "text", $mysqli);
                }
            }
            $values = implode(",", $values);
            $mysqli->query("INSERT INTO prospects ($prospect_fields) VALUES ($values)") or die($mysqli->error);
        }
        return $data['response_code'];

        //update DB to let works know order id is processed
        $mysqli->query("UPDATE prospects_to_process SET worker = '$worker', complete = '1' WHERE prospect_id = '$curr_prospect_id'") or die(mysqli_errno($mysqli));
    } else {
        $myfile = fopen("C:\\inetpub\\wwwroot\\errors\\ll_errors_$worker.txt", "w");
        fwrite($myfile, $worker . " " . $output['response_code']);
        fclose($myfile);
    }
}

function insert_multi_orders($data, $mysqli) {
    foreach ($data as $key => $value) {
        $order_fields = "response_code,ancestor_id,customer_id,parent_id,child_id,order_status,is_recurring,shipping_first_name,shipping_last_name,shipping_street_address,shipping_street_address2,shipping_city,shipping_state,shipping_state_id,shipping_postcode,shipping_country,customers_telephone,main_product_id,main_product_quantity,upsell_product_id,upsell_product_quantity,time_stamp,recurring_date,retry_date,shipping_method_name,shipping_id,tracking_number,on_hold,on_hold_by,hold_date,gateway_id,order_confirmed,order_confirmed_date,is_chargeback,is_fraud,is_rma,rma_number,rma_reason,ip_address,affiliate,sub_affiliate,campaign_id,order_total,order_sales_tax,order_sales_tax_amount,processor_id,created_by_user_name,created_by_employee_name,billing_cycle,click_id";
        $order_fields_arr = explode(',', $order_fields);
        $values = array();
        $value['order_id'] = $key;
        foreach ($order_fields_arr as $field) {
            if (substr($value[$field], 2, 1) == "/" && substr($value[$field], 5, 1) == "/") {
                $values[] = "'" . date("Y-m-d", strtotime($value[$field])) . "'";
            } else {
                $values[] = GetSQLValueString($value[$field], "text", $mysqli);
            }
        }
        $update_query = array();
        foreach ($order_fields_arr as $order_field) {
            $update_query[] = "$order_field=".GetSQLValueString($value[$order_field], "text", $mysqli);
        }

        $values = implode(",", $values);
        //       echo "UPDATE orders SET $update_query WHERE order_id = '$key'<BR>";
        //echo "INSERT INTO orders (order_id,$order_fields) VALUES ('$key',$values)<BR><BR>";
        $mysqli->query("INSERT INTO orders (order_id,$order_fields) VALUES ('$key',$values)");
    }
}

function insert_multi_customers($data, $mysqli) {
    foreach ($data as $key => $value) {
        $customer_fields = "customer_id,billing_first_name,billing_last_name,billing_street_address,billing_street_address2,billing_city,billing_state,billing_postcode,billing_country,customers_telephone,email_address";
        $customer_fields_arr = explode(',', $customer_fields);
        //make sure customer_id is not 0
        echo "its here ".$value['customer_id']."<BR>";
        if ($value['customer_id']) {
            $values = array();
            foreach ($customer_fields_arr as $field) {
                if (substr($value[$field], 2, 1) == "/" && substr($value[$field], 5, 1) == "/") {
                    $values[] = "'" . date("Y-m-d", strtotime($value[$field])) . "'";
                } else {
                    $values[] = GetSQLValueString($value[$field], "text", $mysqli);
                }
            }
            $values = implode(",", $values);
            $mysqli->query("INSERT INTO customers ($customer_fields) VALUES ($values)") or die($mysqli->error);
        }
    }
}

function insert_multi_transactions($data, $mysqli){
    foreach ($data as $key => $value) {
        $transactions_fields = "order_id,cc_type,cc_number,cc_expires,prepaid_match,transaction_id,auth_id,gateway_id,preserve_gateway,amount_refunded_to_date,decline_reason,order_total,order_sales_tax,order_sales_tax_amount,processor_id,billing_cycle";
        $transactions_fields_arr = explode(',', $transactions_fields);
        $values = array();
        $value['order_id'] = $key;
        foreach ($transactions_fields_arr as $field) {
            //call getsqlvaluestring to put quotes around and escape bad characters
            if (substr($value[$field], 2, 1) == "/" && substr($value[$field], 5, 1) == "/") {
                $values[] = "'" . date("Y-m-d", strtotime($value[$field])) . "'";
            } else {
                $values[] = GetSQLValueString($value[$field], "text", $mysqli);
            }
        }
        $values = implode(",", $values);
        $mysqli->query("INSERT INTO transactions ($transactions_fields) VALUES ($values)") or die($mysqli->error);
    }
}

function insert_multi_order_details($data, $mysqli){
    foreach ($data as $key => $value) {
        print_r($key);
        print_r($value);
        foreach($value['products'] as $product_each) {
            $values = array();
            $value['order_id'] = $key;
            $order_id = $key;
            if (isset($product_each['subscription_id'])) {
                $order_details_fields = "product_id,sku,price,name,on_hold,is_recurring,recurring_date,product_qty,subscription_id,subscription_type,subscription_desc";
            } else {
                $order_details_fields = "product_id,sku,price,name,on_hold,is_recurring,recurring_date,product_qty,subscription_id";
            }
            $order_details_fields_arr = explode(',', $order_details_fields);
            foreach ($order_details_fields_arr as $field) {
                //call getsqlvaluestring to put quotes around and escape bad characters
                if (isset($product_each[$field])) {
                    $values[] = GetSQLValueString($product_each[$field], "text", $mysqli);
                }
            }
            $values = implode(",", $values);
            $mysqli->query("INSERT INTO order_details (order_id,$order_details_fields) VALUES ('$order_id',$values)") or die($mysqli->error);
        }
    }
}
/*function insert_multi_notes($data, $mysqli){
    $notes_system_fields = "system_note, note_date";
    $notes_employee_fields = "employee_note, note_date";
    $order_id = $data['order_id'];


//INSERT SYSTEM NOTES
    if(isset($data['systemNotes']) > 0){
        foreach ($data['systemNotes'] as $system_note_each) {
            //call getsqlvaluestring to put quotes around and escape bad characters
            $values = GetSQLValueString($system_note_each, "text", $mysqli);
            //get a useful date of when the note was made
            preg_match("/^([\S]+\s){3}/", $system_note_each, $output_array);
            $note_date = date('Y-m-d',strtotime($output_array[0]));
            $mysqli->query("INSERT INTO notes_system (order_id,$notes_system_fields) VALUES ('$order_id',$values,'$note_date')") or die($mysqli->error);
        }
    }
    //INSERT EMPLOYEE NOTES
    if(isset($data['employeeNotes']) > 0) {
        foreach ($data['employeeNotes'] as $employee_note_each) {
            //call getsqlvaluestring to put quotes around and escape bad characters
            $values = GetSQLValueString($employee_note_each, "text", $mysqli);
            //get a useful date of when the note was made
            preg_match("/^([\S]+\s){3}/", $employee_note_each, $output_array);
            $note_date = date('Y-m-d',strtotime($output_array[0]));
            $mysqli->query("INSERT INTO notes_employee (order_id,$notes_employee_fields) VALUES ('$order_id',$values,'$note_date')") or die($mysqli->error);
        }
    }
    return $data['response_code'];
}*/



