<?php

require_once __DIR__ . '/vendor/autoload.php';

class RayGun
{
	public $client = null;

	public function init()
	{
		$this->client = new \Raygun4php\RaygunClient("UjeogZj4oNkQ3j2CVwaIIw==");

		$this->client->setFilterParams(array(
			'password' => true,
			'cc_num' => true,
			'ccv' => true,
			'cvv' => true,
			'payment.cc_num' => true,
			'php_auth_pw' => true, // filters basic auth from $_SERVER
		));

		set_error_handler(array($this,'error_handler'));
		set_exception_handler(array($this,'exception_handler'));
	}

	function error_handler($errno, $errstr, $errfile, $errline)
	{
		$this->client->SendError($errno, $errstr, $errfile, $errline);
	}

	function exception_handler($exception,$tags = null,$customData = null)
	{
		$this->client->SendException($exception,$tags,$customData);
	}
}

$raygun = new RayGun();
$raygun->init();