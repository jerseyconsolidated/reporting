<?php
/*
 * Logs errors to the DB
 */
function log_error($error, $worker="none_given")
{
	$mysqli = initialize_db("limelight");
	doDBthing('INSERT INTO limelight_errors (error, worker) VALUES ("'.$error.'", "'.$worker.'")', $mysqli);
	$mysqli->close();
}
function GetSQLValueString($theValue, $theType, $mysqli, $theDefinedValue = "", $theNotDefinedValue = "")
{
    $theValue = $mysqli->real_escape_string($theValue);
    switch ($theType) {
        case "text":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "long":
        case "int":
            $theValue = ($theValue != "") ? intval($theValue) : "NULL";
            break;
        case "double":
            $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
            break;
        case "date":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "defined":
            $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
            break;
    }
    return $theValue;
}

function worker_ll_api_jon($data_in,$username,$password)
{
	//log_error("in the worker LL api");
    $url = "https://www.jcsecured.com/admin/membership.php";
    $data = array(
        "username" => $username,
        "password" => $password,
    );
    $data = array_merge_recursive($data, $data_in);
	//log_error("after array merge");
    $curlSession = curl_init();
    curl_setopt($curlSession, CURLOPT_URL, $url);
    curl_setopt($curlSession, CURLOPT_HEADER, 0);
    curl_setopt($curlSession, CURLOPT_POST, 1);
    curl_setopt($curlSession, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curlSession, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($curlSession, CURLOPT_TIMEOUT,5000);
    curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curlSession, CURLOPT_SSL_VERIFYHOST, 2);
    $rawresponse = curl_exec($curlSession);
	//log_error("after curl exec");
    //$decode = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $rawresponse);

    //remove spaces on either end
    $rawresponse = trim($rawresponse);
    // drop all non utf-8 characters
    //$rawresponse = iconv("UTF-8", "UTF-8//IGNORE", $rawresponse);
    //remove bad UTF-8 characters, hopefully?
    /*$rawresponse = preg_replace('/(?>[\x00-\x1F]|\xC2[\x80-\x9F]|\xE2[\x80-\x8F]{2}|\xE2\x80[\xA4-\xA8]|\xE2\x81[\x9F-\xAF])/', ' ', $rawresponse);*/
    //reduce all multiple whitespace to a single space
    //$rawresponse = preg_replace('/\s+/', ' ', $rawresponse);

    $rawresponse = preg_replace('[A-Za-z0-9();{}\[\]\\!@#$%^&*()_+\-=.,<>:/?\'"]', '', $rawresponse);
	//log_error("after preg replace");
    $rawresponse = utf8_decode($rawresponse);
	//log_error("after utf8 decodei");
    //$decode = mb_convert_encoding($rawresponse, 'UTF-8', 'UTF-8');
    $decode = utf8_encode($rawresponse);
	//log_error("after utf8 encode");
    $output = array();
    parse_str($rawresponse,$output);
	//log_error("after parse str");
    curl_close ($curlSession);
	//var_dump($output);
    return $output;
}

function update_orders_to_process($id, $worker, $mysqli)
{
    $query = "UPDATE orders_to_process SET worker = '$worker', complete = '1' WHERE order_id = '$id'";
    doDBthing($query, $mysqli);
}
function insert_orders_jon($id, $data, $mysqli)
{
	/*
	 * The reason that there's 2 of these order_fields variables is becasue the top one (commented out) contains all of the
	 * fields that limelight said would be returned by this call. However, not all of them are. So, I've removed the ones that aren't there.
	 */
//    $order_fields = "response_code,ancestor_id,customer_id,parent_id,child_id,order_status,is_recurring,shipping_first_name,shipping_last_name,shipping_street_address,shipping_street_address2,shipping_city,shipping_state,shipping_state_id,shipping_postcode,shipping_country,customers_telephone,main_product_id,main_product_quantity,upsell_product_id,upsell_product_quantity,time_stamp,recurring_date,retry_date,shipping_method_name,shipping_id,tracking_number,on_hold,on_hold_by,hold_date,gateway_id,order_confirmed,order_confirmed_date,is_chargeback,is_fraud,is_rma,rma_number,rma_reason,ip_address,affiliate,sub_affiliate,campaign_id,order_total,order_sales_tax,order_sales_tax_amount,processor_id,created_by_user_name,created_by_employee_name,billing_cycle,click_id,cc_first_6,cc_last_4,credit_card_number,decline_salvage_discount_percent,gateway_descriptor,coupon_discount_amount,is_blacklisted,rebill_discount_percent";
	$order_fields = "response_code,ancestor_id,customer_id,parent_id,child_id,order_status,is_recurring,shipping_first_name,shipping_last_name,shipping_street_address,shipping_street_address2,shipping_city,shipping_state,shipping_state_id,shipping_postcode,shipping_country,customers_telephone,main_product_id,main_product_quantity,upsell_product_id,upsell_product_quantity,time_stamp,recurring_date,retry_date,shipping_method_name,shipping_id,tracking_number,on_hold,on_hold_by,hold_date,gateway_id,order_confirmed,order_confirmed_date,is_chargeback,is_fraud,is_rma,rma_number,rma_reason,ip_address,affiliate,sub_affiliate,campaign_id,order_total,order_sales_tax,order_sales_tax_amount,processor_id,created_by_user_name,created_by_employee_name,billing_cycle,click_id,cc_first_6,decline_salvage_discount_percent,coupon_discount_amount,is_blacklisted,rebill_discount_percent";

	$order_fields_arr = explode(',', $order_fields);

    $values = array();
    foreach ($order_fields_arr as $field) {
        //call getsqlvaluestring to put quotes around and escape bad characters
        if(substr($data[$field],2,1) == "/" && substr($data[$field],5,1) == "/")
		{
            $values[] = "'".date("Y-m-d H:i:s", strtotime($data[$field]))."'";
        }
        else
		{
            $values[] = GetSQLValueString($data[$field], "text", $mysqli);
        }
    }
    $values = implode(",",$values);
    $order_stuff = 'order_id,'.$order_fields;
    $value_stuff = $id.','.$values;
    doDBthing("INSERT INTO orders ($order_stuff) VALUES ($value_stuff)", $mysqli);
    return $data['response_code'];
}
function insert_overdue_orders_jon($id, $data, $mysqli)
{
    $order_fields = "response_code,ancestor_id,customer_id,parent_id,child_id,order_status,is_recurring,shipping_first_name,shipping_last_name,shipping_street_address,shipping_street_address2,shipping_city,shipping_state,shipping_state_id,shipping_postcode,shipping_country,customers_telephone,main_product_id,main_product_quantity,upsell_product_id,upsell_product_quantity,time_stamp,recurring_date,retry_date,shipping_method_name,shipping_id,tracking_number,on_hold,on_hold_by,hold_date,gateway_id,order_confirmed,order_confirmed_date,is_chargeback,is_fraud,is_rma,rma_number,rma_reason,ip_address,affiliate,sub_affiliate,campaign_id,order_total,order_sales_tax,order_sales_tax_amount,processor_id,created_by_user_name,created_by_employee_name,billing_cycle,click_id";
    $order_fields_arr = explode(',', $order_fields);

    $values = array();
    foreach ($order_fields_arr as $field) {
        //call getsqlvaluestring to put quotes around and escape bad characters
        if(substr($data[$field],2,1) == "/" && substr($data[$field],5,1) == "/")
		{
            $values[] = "'".date("Y-m-d H:i:s", strtotime($data[$field]))."'";
        }
        else
		{
            $values[] = GetSQLValueString($data[$field], "text", $mysqli);
        }
    }
    $values = implode(",",$values);
    $mysqli->store_result();
    $overdue_order_stuff = 'order_id,'.$order_fields;
    $value_stuff = $id.','.$values;
    doDBthing("INSERT INTO overdue_orders ($overdue_order_stuff) VALUES ($value_stuff)", $mysqli);
    return $data['response_code'];
}
function insert_customers_jon($id, $data, $mysqli)
{
    $customer_fields = "customer_id,billing_first_name,billing_last_name,billing_street_address,billing_street_address2,billing_city,billing_state,billing_postcode,billing_country,customers_telephone,email_address";
    $customer_fields_arr = explode(',', $customer_fields);
    //make sure customer_id is not 0
    if($data['customer_id']) {
        $values = array();
        foreach ($customer_fields_arr as $field)
		{
            //call getsqlvaluestring to put quotes around and escape bad characters
            if(substr($data[$field],2,1) == "/" && substr($data[$field],5,1) == "/")
			{
                $values[] = "'".date("Y-m-d H:i:s", strtotime($data[$field]))."'";
            }
            else
			{
                $values[] = GetSQLValueString($data[$field], "text", $mysqli);
            }
        }
        $values = implode(",", $values);
        doDBthing("INSERT IGNORE INTO customers ($customer_fields) VALUES ($values)", $mysqli);
    }
    return $data['response_code'];
}
function insert_transactions_jon($id, $data, $mysqli)
{
    $transactions_fields = "cc_type,cc_number,cc_expires,prepaid_match,transaction_id,auth_id,gateway_id,preserve_gateway,amount_refunded_to_date,decline_reason,order_total,order_sales_tax,order_sales_tax_amount,processor_id,billing_cycle";
    $transactions_fields_arr = explode(',', $transactions_fields);
    $values = array();
    foreach ($transactions_fields_arr as $field) {
        //call getsqlvaluestring to put quotes around and escape bad characters
        if(substr($data[$field],2,1) == "/" && substr($data[$field],5,1) == "/")
		{
            $values[] = "'".date("Y-m-d H:i:s", strtotime($data[$field]))."'";
        }
        else
		{
            $values[] = GetSQLValueString($data[$field], "text", $mysqli);
        }
    }
    $values = implode(",",$values);
    $transaction_stuff = 'order_id,'.$transactions_fields;
    $value_stuff = $id.','.$values;
    doDbthing("INSERT INTO transactions ($transaction_stuff) VALUES ($value_stuff)",$mysqli);
    return $data['response_code'];
}
function insert_order_details_jon($id, $data, $mysqli)
{
    $order_id = $id;
    foreach($data['products'] as $product_each)
	{
        $values = array();
        if(isset($product_each['subscription_type']))
		{
            $order_details_fields = "product_id,sku,price,name,on_hold,is_recurring,recurring_date,product_qty,subscription_id,subscription_type,subscription_desc";
        }
		else
		{
            $order_details_fields = "product_id,sku,price,name,on_hold,is_recurring,recurring_date,product_qty,subscription_id";
        }
        $order_details_fields_arr = explode(',', $order_details_fields);
        foreach ($order_details_fields_arr as $field)
		{
            //call getsqlvaluestring to put quotes around and escape bad characters
            if (isset($product_each[$field]))
			{
                $values[] = GetSQLValueString($product_each[$field], "text", $mysqli);
            }
        }
        $values = implode(",", $values);
        doDBthing("INSERT INTO order_details (order_id,$order_details_fields) VALUES ('$order_id',$values)", $mysqli);
        return $data['response_code'];
    }
}
function insert_notes_jon($id, $data, $mysqli)
{
    $notes_system_fields = "system_note, note_date";
    $notes_employee_fields = "employee_note, note_date";
    $order_id =$id;

//INSERT SYSTEM NOTES
    if(isset($data['systemNotes']) > 0)
	{
        foreach ($data['systemNotes'] as $system_note_each)
		{
            //call getsqlvaluestring to put quotes around and escape bad characters
            $values = GetSQLValueString($system_note_each, "text", $mysqli);
            //get a useful date of when the note was made
            preg_match("/^([\S]+\s){5}/", $system_note_each, $output_array);
            $note_date = date('Y-m-d H:i:s',strtotime($output_array[0]));
			doDBthing("INSERT INTO notes_system (order_id,$notes_system_fields) VALUES ('$order_id',$values,'$note_date')",$mysqli);
        }
    }
    //INSERT EMPLOYEE NOTES
    if(isset($data['employeeNotes']) > 0)
	{
        foreach ($data['employeeNotes'] as $employee_note_each)
		{
            //call getsqlvaluestring to put quotes around and escape bad characters
            $values = GetSQLValueString($employee_note_each, "text", $mysqli);
            //get a useful date of when the note was made
            preg_match("/^([\S]+\s){5}/", $employee_note_each, $output_array);
			//var_dump($output_array);
			//echo "\n\n";
			//var_dump($output_array);
			//echo $output_array[1]."\n\n";
			//var_dump($data);
            $note_date = date('Y-m-d H:i:s',strtotime($output_array[0]));
			//echo $note_date;
            doDBthing("INSERT INTO notes_employee (order_id,$notes_employee_fields) VALUES ('$order_id',$values,'$note_date')",$mysqli);
        }
    }
    return $data['response_code'];
}