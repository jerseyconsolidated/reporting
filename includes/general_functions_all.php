<?php
/**
 * Created by PhpStorm.
 * User: jonjenne
 * Date: 4/16/15
 * Time: 2:11 PM
 */

function getCAPath()
{
	if (php_uname('s') == "Darwin")  //OSX is called Darwin for some reason
	{
		$path = "/Users/jonjenne/reporting/includes/ca-bundle.crt";
	}
	else if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') // this might work for windows, idk
	{
		echo 'This is Windows! (Ryan, set your path to the cert in general_functions_all.php in the method getCAPath() - We are now dying)';
		die();
	}
	else //some kind of Linux
	{
		$path = "/var/www/reporting.jcoffice.net/includes/ca-bundle.crt"; //HTTPS cert
	}
	return $path;
}

function doDBthing($query, $mysqli)
{
	if(! isset($mysqli))
	{
		$mysqli = initialize_db($mysqli);
	}
	if($prepared_mysqli = $mysqli->prepare($query))
	{
		$prepared_mysqli->execute();
		$prepared_mysqli->store_result();
		$prepared_mysqli->free_result();
		$prepared_mysqli->close();
	}
	else
	{
		echo "\n Error with $query \n";
		echo mysqli_connect_error();
		echo $mysqli->error;
		//die($mysqli->error);
		//log_error(mysqli_connect_error(), "error".getmypid());
	}
	return true;
}
function initialize_db($mysqli, $username="biohealth1", $password="7s63VfusbHVFuq")
{
	if(is_string($mysqli))
	{
	//open up our connection
		$mysqli = new mysqli("localhost",$username,$password,$mysqli);
		// Check connection
		if (mysqli_connect_errno())
		{
			echo $mysqli->error;
			echo mysqli_connect_error();
			//log_error(mysqli_connect_error(), "error".getmypid());
			exit();
		}
	}
	return $mysqli;
}