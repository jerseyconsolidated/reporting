<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require ('../includes/RayGun/RayGun.php');
ini_set('memory_limit', '-1');

global $mysqli;
$mysqli = initialize_db('limelight');



if(isset($_POST['orders']) && $_POST['orders'] != "" && isset($_POST['fields']) && $_POST['fields'] != "")
{
	$fields = $_POST['fields'];
	$orders = $_POST['orders'];
	$orders = explodify($orders);
	$data = getOrderData($orders);

	if(isset($_POST['downloadCSV']))
	{
		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: application/octet-stream; charset=utf-8');
		header('Content-Disposition: attachment; filename=data.csv');
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		// output the column headings
		fputcsv($output, $fields);
		// loop over the rows, outputting them
		foreach($data as $record)
		{
			$rows = array();
			$index =0;
			foreach($fields as $field)
			{
				$field = str_replace(" ", "_", $field); //strip out the space and make it a _ so it lines up with the DB
				$rows[$index] = $record[$field];
				$index++;
			}
			fputcsv($output, $rows);
		}
		fclose($output);
		die();
	}
	else
	{
		printCheckboxes(getFieldArray(), $fields, $_POST['orders']);
	}
	echo "<table border=1>
	<tr>
	";
	doTitle($fields);
	echo "
	</tr>
	";
	foreach($data as $record)
	{
		echo "<tr>";
		doRow($fields, $record);
		echo "</tr>";
	}
	echo "</table>";
}
else if(isset($_POST['force_bill_orders']))
{
	$query = "
SELECT order_id, ancestor_id, order_status, time_stamp, recurring_date, gateway_id, campaign_id
FROM force_bill_these_orders
";
	//echo $query."<br><br>";
	$query_result = $mysqli->prepare("$query");
	$query_result->execute(); // why is this failing?
	$query_result->bind_result(
		$order_id, $ancestor_id, $order_status, $time_stamp, $recurring_date,
		$gateway_id, $campaign_id);

	$myorders= "";
	$index = 0;
	while ($query_result->fetch())
	{
		$myorders .= $order_id.",";
		$index++;
	}
	$mysqli->close();

	$cols = 140;
	$rows = 30;

	if(isset($_POST['fields']) && $_POST['fields'] != "")
	{
		$fields = $_POST['fields'];
		printCheckboxes(getFieldArray(), $fields, $myorders, $rows, $cols);
		echo "<table border=1>
	<tr>
	";
		doTitle($fields);
		echo "
	</tr>
	";
		$myorders = explodify($myorders);
		$data = getOrderData($myorders);
		foreach($data as $record)
		{
			echo "<tr>";
			doRow($fields, $record);
			echo "</tr>";
		}
		echo "</table>";
	}
	else
	{
		printCheckboxes(getFieldArray(), 0, $myorders, $rows, $cols);
		echo "Fields are not set";
	}
}
else
{
	printCheckboxes(getFieldArray(), 0, '');
	echo "Nothing is set.";
}

/*
 * return an array with all fields
 */
function getFieldArray()
{
	$fields[] = "Order ID";
	$fields[] = "Response Code";
	$fields[] = "Ancestor ID";
	$fields[] = "Most Recently Approved Ancestor Id";
	$fields[] = "Customer ID";
	$fields[] = "Parent ID";
	$fields[] = "Child ID";
	$fields[] = "Customer ID";
	$fields[] = "Order Status";
	$fields[] = "Is Recurring";
	$fields[] = "Shipping First Name";
	$fields[] = "Shipping Last Name";
	$fields[] = "Shipping Street Address";
	$fields[] = "Shipping Street Address 2";
	$fields[] = "Shipping City";
	$fields[] = "Shipping State";
	$fields[] = "Shipping State ID";
	$fields[] = "Shipping PostCode";
	$fields[] = "Shipping Country";
	$fields[] = "Customers Telephone";
	$fields[] = "Main Product ID";
	$fields[] = "Main Product Quantity";
	$fields[] = "Upsell Product ID";
	$fields[] = "Upsell Product Quantity";
	$fields[] = "Time stamp";
	$fields[] = "Recurring Date";
	$fields[] = "Retry Date";
	$fields[] = "Shipping Method Name";
	$fields[] = "Shipping ID";
	$fields[] = "Tracking Number";
	$fields[] = "On Hold";
	$fields[] = "On Hold By";
	$fields[] = "Hold Date";
	$fields[] = "Gateway ID";
	$fields[] = "Order Confirmed";
	$fields[] = "Order Confirmed Date";
	$fields[] = "Is Chargeback";
	$fields[] = "Is Fraud";
	$fields[] = "Is RMA";
	$fields[] = "RMA Number";
	$fields[] = "RMA Reason";
	$fields[] = "IP Address";
	$fields[] = "Affiliate";
	$fields[] = "Sub Affiliate";
	$fields[] = "Campaign ID";
	$fields[] = "Order Total";
	$fields[] = "Order Sales Tax";
	$fields[] = "Order Sales Tax Amount";
	$fields[] = "Processor ID";
	$fields[] = "Created By User Name";
	$fields[] = "Created By Employee Name";
	$fields[] = "Billing Cycle";
	$fields[] = "Click ID";

	return $fields;
}

/*
 * Print the HTML checkboxes
 */
function printCheckboxes($checked, $checked_fields, $textarea, $defaultrows = 4, $defaultcols = 50)
{
	?>
	<form method="post">
		Order ID's (Separated by Spaces, Newlines, or Commas) <br>
		<textarea id="orders" name="orders" rows="<?php echo $defaultrows; ?>" cols="<?php echo $defaultcols; ?>" placeholder="Insert your CSVs here..."><?php echo $textarea; ?></textarea>
		<br>
		<table id="inputs">
			<tr><td>
					<?php
					for($index =0; $index < count($checked); $index++)
					{
						$field = strtolower($checked[$index]);
						$field_name = $checked[$index];
						if(is_array($checked_fields) && in_array($field, $checked_fields))
						{
							echo '<input type="checkbox" name="fields[]" value="'.$field.'" checked />'.$field_name.'<br />';
						}
						else
						{
							echo '<input type="checkbox" name="fields[]" value="'.$field.'" />'.$field_name.'<br />';
						}
						if(($index%10) == 9)
						{
							echo "</td><td>";
						}
					}
					?>
				</td></tr></table>
		<br>
		<input type="checkbox" name="selectAll" id="selectAll" />
		Select All <br>
		<input type="checkbox" name="downloadCSV" id="downloadCSV" />
		Download CSV of generated data
		<br>
		<input type="checkbox" name="force_bill_orders" id="force_bill_orders" />
		Get Force Billable Orders (BETA) <a href="https://jcoffice.net/mid/forceBilling/force_billing" target="_blank">Force Billing Page</a>
		<br>
		<input type="submit" value="Submit">
		<br>
		*Reminder: All Limelight data is updated every night between (approximately) 2-3am.
	</form>
	<script src="https://code.jquery.com/jquery-1.10.2.js"></script>

	<script>
		$(document).ready(function() {
			$('#selectAll').click (function () {
				var checkedStatus = this.checked;
				$('#inputs tbody tr').find(':checkbox').each(function () {
					$(this).prop('checked', checkedStatus);
				});
			});
		});
	</script>

<?php
}
/*
 * Fill in a table row for all given fields for this record
 */
function doRow($fields, $record)
{
	foreach($fields as $field)
	{
		$field = str_replace(" ", "_", $field); //strip out the space and make it a _ so it lines up with the DB
		if(isset($record[$field]))
		{
			echo "<td>&nbsp;$record[$field]&nbsp;</td>";
		}
		else
		{
			echo "<td>&nbsp;&nbsp;</td>";
		}
	}
}
/*
 * Generate the HTML table titles
 */
function doTitle($fields)
{

	foreach($fields as $field)
	{
		echo "<th>$field</th>";
	}
}
function getLLData()
{
	$url = "https://www.jcsecured.com/admin/membership.php";

		$data = array(
			"campaign_id" => "all",
			"product_id" => "all",
			"search_type" => "all", //this is the line that is supposed to make it "AND" instead of "OR" (OR is called "ANY" instead of "ALL")
//		"criteria" => "all",
			"username" => "biohealth1",
			//"criteria" => "first_4_cc=1234,last_4_cc=3413",
			"criteria" => "first_6_cc=$first6,last_4_cc=$last4",
			//"criteria" => "first_name=dav*,all_refunds,state=mi,email=*gmail*,billing_cycle=1",
			//"criteria" => "first_4_cc=$first4",

			"password" => "7s63VfusbHVFuq",
			"method" => "order_find",
			"start_date" => "01/01/2014",
			"end_date" => date('m/d/Y',strtotime("today")),
			"return_type" => "order_view",
			//"last_4_cc" => "$last4",
			//"first_4_cc" => "$first4",
		);
		//var_dump($data);
		//$data = array_merge_recursive($data, $data_in);
		$curlSession = curl_init();
		curl_setopt($curlSession, CURLOPT_URL, $url);
		curl_setopt($curlSession, CURLOPT_HEADER, 0);
		curl_setopt($curlSession, CURLOPT_POST, 1);
		curl_setopt($curlSession, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curlSession, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($curlSession, CURLOPT_TIMEOUT,5000);
		curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curlSession, CURLOPT_SSL_VERIFYHOST, 2);
		$rawresponse = curl_exec($curlSession);

		$rawresponse = trim($rawresponse);
		$rawresponse = preg_replace('[A-Za-z0-9();{}\[\]\\!@#$%^&*()_+\-=.,<>:/?\'"]', '', $rawresponse);

		$rawresponse = utf8_decode($rawresponse);
		$output = array();
		parse_str($rawresponse,$output);
		curl_close ($curlSession);

		return $output;
}
/*
 * Get Order Data
 */
function getOrderData($orders)
{
	$mysqli = initialize_db('limelight');

	$query = "
SELECT order_id, response_code, ancestor_id, most_recently_approved_ancestor_id, customer_id, parent_id,
child_id, order_status, is_recurring, shipping_first_name, shipping_last_name, shipping_street_address,
shipping_street_address2, shipping_city, shipping_state, shipping_state_id, shipping_postcode,
shipping_country, customers_telephone, main_product_id, main_product_quantity, upsell_product_id,
upsell_product_quantity, time_stamp, recurring_date, retry_date, shipping_method_name, shipping_id,
tracking_number, on_hold, on_hold_by, hold_date, gateway_id, order_confirmed,
order_confirmed_date, is_chargeback, is_fraud, is_rma, rma_number, rma_reason, ip_address,
affiliate, sub_affiliate, campaign_id, order_total, order_sales_tax, order_sales_tax_amount, processor_id,
created_by_user_name, created_by_employee_name, billing_cycle, click_id
FROM orders
WHERE
";
	foreach($orders as $order)
	{
		$order = trim($order);
		$query.=" order_id LIKE '$order' OR";
	}
	$query = substr($query, 0, -3); //chop off last ' OR'
	//$query.="AND time_stamp + INTERVAL 45 DAY <= NOW()";
	//echo $query;
	//die();
	$query_result = $mysqli->prepare("$query");
	$query_result->execute(); // why is this failing?
	$query_result->bind_result(
		$order_id, $response_code, $ancestor_id, $most_recently_approved_ancestor_id, $customer_id,
		$parent_id, $child_id, $order_status, $is_recurring, $shipping_first_name,
		$shipping_last_name, $shipping_street_address, $shipping_street_address2,
		$shipping_city, $shipping_state, $shipping_state_id, $shipping_postcode,
		$shipping_country, $customers_telephone, $main_product_id, $main_product_quantity,
		$upsell_product_id, $upsell_product_quantity, $time_stamp, $recurring_date,
		$retry_date, $shipping_method_name, $shipping_id, $tracking_number, $on_hold,
		$on_hold_by, $hold_date, $gateway_id, $order_confirmed, $order_confirmed_date, $is_chargeback, $is_fraud,
		$is_rma, $rma_number, $rma_reason, $ip_address, $affiliate, $sub_affiliate,
		$campaign_id, $order_total, $order_sales_tax, $order_sales_tax_amount, $processor_id,
		$created_by_user_name, $created_by_employee_name, $billing_cycle, $click_id);

	$myorders[] = "";
	$index = 0;
	while ($query_result->fetch())
	{
		$this_array["order_id"] = $order_id;
		$this_array["response_code"] = $response_code;
		$this_array["ancestor_id"] = $ancestor_id; //this used to be set to $order_id .... ?
		$this_array["most_recently_approved_ancestor_id"] = $most_recently_approved_ancestor_id;
		$this_array["customer_id"] = $customer_id;
		$this_array["parent_id"] = $parent_id;
		$this_array["child_id"] = $child_id;
		$this_array["order_status"] = $order_status;
		$this_array["is_recurring"] = $is_recurring;
		$this_array["shipping_first_name"] = $shipping_first_name;
		$this_array["shipping_last_name"] = $shipping_last_name;
		$this_array["shipping_street_address"] = $shipping_street_address;
		$this_array["shipping_street_address2"] = $shipping_street_address2;
		$this_array["shipping_city"] = $shipping_city;
		$this_array["shipping_state"] = $shipping_state;
		$this_array["shipping_state_id"] = $shipping_state_id;
		$this_array["shipping_postcode"] = $shipping_postcode;
		$this_array["shipping_country"] = $shipping_country;
		$this_array["customers_telephone"] = $customers_telephone;
		$this_array["main_product_id"] = $main_product_id;
		$this_array["main_product_quantity"] = $main_product_quantity;
		$this_array["upsell_product_id"] = $upsell_product_id;
		$this_array["upsell_product_quantity"] = $upsell_product_quantity;
		$this_array["time_stamp"] = $time_stamp;
		$this_array["recurring_date"] = $recurring_date;
		$this_array["retry_date"] = $retry_date;
		$this_array["shipping_method_name"] = $shipping_method_name;
		$this_array["shipping_id"] = $shipping_id;
		$this_array["tracking_number"] = $tracking_number;
		$this_array["on_hold"] = $on_hold;
		$this_array["on_hold_by"] = $on_hold_by;
		$this_array["hold_date"] = $hold_date;
		$this_array["gateway_id"] = $gateway_id;
		$this_array["order_confirmed"] = $order_confirmed;
		$this_array["order_confirmed_date"] = $order_confirmed_date;
		$this_array["is_chargeback"] = $is_chargeback;
		$this_array["is_fraud"] = $is_fraud;
		$this_array["is_rma"] = $is_rma;
		$this_array["rma_number"] = $rma_number;
		$this_array["rma_reason"] = $rma_reason;
		$this_array["ip_address"] = $ip_address;
		$this_array["affiliate"] = $affiliate;
		$this_array["sub_affiliate"] = $sub_affiliate;
		$this_array["campaign_id"] = $campaign_id;
		$this_array["order_total"] = $order_total;
		$this_array["order_sales_tax"] = $order_sales_tax;
		$this_array["order_sales_tax_amount"] = $order_sales_tax_amount;
		$this_array["processor_id"] = $processor_id;
		$this_array["created_by_user_name"] = $created_by_user_name;
		$this_array["created_by_employee_name"] = $created_by_employee_name;
		$this_array["billing_cycle"] = $billing_cycle;
		$this_array["click_id"] = $click_id;

		$myorders[$index] = $this_array;
		$index++;
	}

	echo $mysqli->error;
	echo $mysqli->connect_error;
	
	$mysqli->close();

	return $myorders;
}
function initialize_db($mysqli, $username="biohealth1", $password="7s63VfusbHVFuq")
{
	$mysqli = new mysqli("localhost",$username,$password,'limelight');
	// Check connection
	if (mysqli_connect_errno())
	{
		printf("Connect failed: %s\n", mysqli_connect_error());
		//log_error(mysqli_connect_error(), "error".getmypid());
		exit();
	}
	return $mysqli;
}
/*
 * Filter some stuff and explodify
 */
function explodify($string)
{
	$string = str_replace('    ', ',', $string); //TAB

	$string = str_replace('
', ',', $string); //NOT A SPACE! (new line character)
	$string = str_replace(' ', ',',$string); //a space
	$myArray = explode(',', $string);
	return $myArray;
}

?>