<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require ('../includes/RayGun/RayGun.php');
ini_set('memory_limit', '-1');


if(isset($_POST['BIN']) && $_POST['BIN'] != "")
{
	$bins = $_POST['BIN'];
	$bins = explodify($bins);
	$fields = getFieldArray();

	if(isset($_POST['downloadCSV']))
	{
		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: application/octet-stream; charset=utf-8');
		header('Content-Disposition: attachment; filename=data.csv');
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		// output the column headings
		fputcsv($output, $fields);

		foreach($bins as $bin)
		{
			$array_good = doCurl($bin);
			fputcsv($output, $array_good);
		}
		fclose($output);
		die();
	}
	else
	{
		printHTML($_POST['BIN']);

		echo "<table border=1>";
		foreach($fields as $field)
		{
			echo "<th>$field</th>";
		}
		foreach($bins as $bin)
		{
			$array_good = doCurl($bin);
			echo "<tr>";
			echo "<td>$bin</td>";
			foreach($fields as $field)
			{
				if($field == "given bin")
				{
					continue;
				}
				echo "<td>".$array_good["$field"]."</td>";
			}
			echo "</tr>";
		}
	}
	echo "</table>";
}
else
{
	printHTML("");
}
/*
 *     /////// FUNCTIONS FOLLOW BELOW ////////
 */
/*
 * Do the curl request for this given $bin
 */
function doCurl($bin)
{
	$url = "http://www.binlist.net/json/".$bin;
	$curlSession = curl_init();
	curl_setopt($curlSession, CURLOPT_URL, $url);
	curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1);
	//curl_setopt($curlSession, CURLOPT_POST, 1);
	//curl_setopt($curlSession, CURLOPT_POSTFIELDS, $post_data);
	curl_setopt($curlSession, CURLOPT_TIMEOUT, 500000);
	//curl_setopt($curlSession, CURLOPT_SSLVERSION, 3);

	$rawresponse = curl_exec($curlSession);
	//echo "<br>".curl_error($curlSession);

	curl_close($curlSession);
	$this_array = json_decode($rawresponse, 1);
	return $this_array;
}
/*
 * return an array with all field names
 */
function getFieldArray()
{
	$fields[] = "given bin";
	$fields[] = "bin";
	$fields[] = "brand";
	$fields[] = "sub_brand";
	$fields[] = "country_code";
	$fields[] = "country_name";
	$fields[] = "bank";
	$fields[] = "card_type";
	$fields[] = "card_category";
	$fields[] = "latitude";
	$fields[] = "longitude";
	$fields[] = "query_time";

	return $fields;
}
/*
 * Print the HTML checkboxes
 */
function printHTML($textarea="")
{
	?>
	<form method="post">
		BINs (Separated by Spaces, Newlines, or Commas)
		<br>
		This will also take any *** or extra digits out. So, you can put in 123456********1234 and it will chop off the ***s and the last 4.
		<br>
		<textarea id="BIN" name="BIN" rows="4" cols="50" placeholder="Insert your BINs here..."><?php echo $textarea; ?></textarea>
		<br>
		<input type="checkbox" name="downloadCSV" id="downloadCSV" />
		Download CSV of generated data
		<br>
		<input type="submit" value="Submit">
	</form>
<?php
}
/*
 * Filter some stuff and explodify
 */
function explodify($string)
{
	$string = str_replace('    ', ',', $string); //TAB

	$string = str_replace('
', ',', $string); //NOT A SPACE! (new line character)
	$string = str_replace(' ', ',',$string); //a space
	$myArray = explode(',', $string);
	/*
	 * Now, since these are BINS coming in as format:
	 * 123456******1234 (6 *s)
	 * we have to remove the *s and the last 4.
	 */
	$index=0;
	foreach($myArray as $bin)
	{
		$myArray[$index] = substr($bin, 0, 6);
		$index++;
	}
	return $myArray;
}

?>