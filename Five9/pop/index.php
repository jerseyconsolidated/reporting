<html>
<head>
    <style>
        body{
            font: 12px Verdana, sans-serif;
        }
        #tracking {
            float: left;
            width: 100%;
            padding-bottom: 10px;
        }
        #notes {
            float: left;
            width: 46%;
            padding-right: 2%;
        }
        #tix {
            float: left;
            width: 46%;
            padding-left: 2%;
        }
        .customer_note {
            padding-bottom: 10px;
        }
        .indent_notes {
            margin-left: 2%;
        }
    </style>
</head>
<body>
<?php
include 'pop_includes.php';
include("../vendor/autoload.php");
require_once('usps/USPSTrackConfirm.php');
use Zendesk\API\Client as ZendeskAPI;

$customers = array();
$order_ids = array();
$tracking_numbers = array();
$all_notes = array();

if (isset($_REQUEST['tik_phone'])) {

$no_result = "noresult";
$phone = $_REQUEST['tik_phone'];
//$phone = '3866750358';

$customer = new find_local();
$customer_id_email = $customer->customer_ids_email_from_phone($phone);
if ($customer_id_email->num_rows > 0) {
    while ($customer_row = $customer_id_email->fetch_assoc()) {

        $customers[] = array('customer_id' => $customer_row['customer_id'],
            'customer_email' => $customer_row['email_address']);
        ////
        //get orders associated
        ///
        $orders = new find_local();
        $order_num_trk = $orders->tracking_from_customer_id($customer_row['customer_id']);
        if ($order_num_trk->num_rows > 0) {
            while ($order_row = $order_num_trk->fetch_assoc()) {
                $order_ids[] = array('customer_id' => $customer_row['customer_id'],
                    'order_id' => $order_row['order_id']);
                if (isset($order_row['tracking_number'])) {
                    $tracking_numbers[] = array('order_id' => $order_row['order_id'],
                        'tracking_number' => $order_row['tracking_number']);
                }
                /////
                ///get employee  notes associated
                /////
                $notes = new find_local();
                $notes_result = $notes->notes_from_order_id($order_row['order_id'], 'employee');
                if ($notes_result->num_rows > 0) {
                    while ($notes_row = $notes_result->fetch_assoc()) {
                        if (!isset($all_notes[$customer_row['email_address']])) {
                            $all_notes[$customer_row['email_address']] = array();
                        }
                        if (!isset($all_notes[$customer_row['email_address']][$order_row['order_id']])) {
                            $all_notes[$customer_row['email_address']][$order_row['order_id']] = array('system' => array(), 'employee' => array());
                        }

                        $all_notes[$customer_row['email_address']][$order_row['order_id']]['employee'][] = array(
                            'employee_note' => $notes_row['employee_note'],
                            'note_date' => $notes_row['note_date']
                        );
                    }
                }
                $notes = new find_local();
                $notes_result = $notes->notes_from_order_id($order_row['order_id'], 'system');
                if ($notes_result->num_rows > 0) {
                    while ($notes_row = $notes_result->fetch_assoc()) {
                        if (!isset($all_notes[$customer_row['email_address']])) {
                            $all_notes[$customer_row['email_address']] = array();
                        }
                        if (!isset($all_notes[$customer_row['email_address']][$order_row['order_id']])) {
                            $all_notes[$customer_row['email_address']][$order_row['order_id']] = array('system' => array(), 'employee' => array());
                        }

                        $all_notes[$customer_row['email_address']][$order_row['order_id']]['system'][] = array(
                            'system_note' => $notes_row['system_note'],
                            'note_date' => $notes_row['note_date']
                        );
                    }
                }
            }
        }
    }
} else {
    echo $no_result;
}
/*
print_r($customers); //1:1 customer_id, customer_email
print_r($order_ids); //1:many customer_id, order_id
print_r($tracking_numbers); //1:1 order_id, tracking_number
print_r($notes_employee); //1:1 customer_email, order_id, employee_note, note_date
print_r($notes_system); //1:1 customer_email, order_id, system_note, note_date
*/
?>
<div id="tracking">
    <?php
    /*
     * Your Username is 787JERSE1078
    Your Password is 528DC00TD640
     */
    //    print_r($tracking_numbers); //1:1 order_id, tracking_number
    foreach ($tracking_numbers as $tracking_number) {
        // Initiate and set the username provided from usps{}{}){}){}
        $tracking = new USPSTrackConfirm('787JERSE1078');

        // During test mode this seems not to always work as expected
        $tracking->setTestMode(true);

        // Add the test package id to the trackconfirm lookup class
        $tracking->addPackage($tracking_number['tracking_number']);

        $tracking->getTracking();
        $response = $tracking->getArrayResponse();
        echo "Order ID: " . $tracking_number['order_id'] . "<br />";
        if (isset($response['TrackResponse']['TrackInfo']['Error'])) {
            echo "Event: " . $response['TrackResponse']['TrackInfo']['Error']['Description'] . "<br />";
        } else {
            echo "Event: " . $response['TrackResponse']['TrackInfo']['TrackSummary']['Event'] . "<br />";
        }
        // Check if it was completed
        if ($tracking->isSuccess()) {
        } else {
            echo 'Error: ' . $tracking->getErrorMessage();
        }
        echo "<br />";
    }
    ?>
</div>
<div id="notes">
    All Notes<br/>
    <?php
    foreach ($customers as $customer) {
        ?>
        <div class="customer_note">
            <?php
            echo "<b>Customer Email: </b>" . $customer['customer_email'] . "<br />";
            foreach ($all_notes[$customer['customer_email']] as $order_id => $the_note) {
                echo "<br /><b>Order ID: </b>" . $order_id . "<br />";
                ?>
                <div class="indent_notes">
                    <?php
                    foreach ($the_note['employee'] as $note) {
                        echo "<b>Employee Note: </b>" . $note['employee_note'] . "<br />";
                    }
                    foreach ($the_note['system'] as $note) {
                        echo "<b>System Note: </b>" . $note['system_note'] . "<br />";
                    }
                    ?>
                </div>
            <?php
            }
            ?>
        </div>
        <hr/>
    <?php
    }
    ?>
</div>
<div id="tix">
    All Tix<br/>
    <?php
    foreach ($customers as $customer) {
        $email = $customer['customer_email'];

        $subdomain_derma = "dermaliv";
        $username_derma = "lily@dermaliv.com";
        $token_derma = "L9ZOofXWsJfnxxkbxbVs8fyMYhqSNP7OxZ8i4Ukt"; // replace this with your token
        $client_derma = new ZendeskAPI($subdomain_derma, $username_derma);
        $client_derma->setAuth('token', $token_derma); // set either token or password
        ////
        $subdomain_hub = "synergylife";
        $username_hub = "lily@synergylife.com";
        $token_hub = "5yg3eZnoalPP6elJ7yUfpWIOXDvxOLCZTqnro99U"; // replace this with your token
        $client_hub = new ZendeskAPI($subdomain_hub, $username_hub);
        $client_hub->setAuth('token', $token_hub); // set either token or password

        /////
        //find in derma and match the ones in hub
        $email_derma = $email;
        $find_tix_derma_email = array('query' => '' . $email_derma . '');
        $tickets_derma = $client_derma->search($find_tix_derma_email);

        $tik_count = count($tickets_derma->results);
        echo "Searching: " . $email . "<br />";
        echo "Tickets found in Dermaliv Zendesk: " . $tik_count . "<br />";

        for ($i = 0; $i < $tik_count; $i++) {
            $derma_tik_subject = $tickets_derma->results[$i]->subject;
            $derma_created_at = $tickets_derma->results[$i]->created_at;
            $derma_created_at = new DateTime($derma_created_at);
            $derma_created_at = $derma_created_at->format('Y-m-d');
            //match spoke->hub KEY
            $ticket_hub_found = $client_hub->search(array('query' => 'subject:' . $derma_tik_subject . ' created:' . $derma_created_at . ''));

            echo "<b>Ticket #: </b>" . $i . "<br />";
            echo "<b>Ticket ID: </b>" . $ticket_hub_found->results[1]->id . "<br />";
            echo "<b>Ticket Email: </b>" . $email_derma . "<br />";
            echo "<b>Ticket Status: </b>" . $ticket_hub_found->results[1]->status . "<br />";
            echo "<b>Ticket Subject: </b>" . $ticket_hub_found->results[1]->subject . "<br />";
            $description = $ticket_hub_found->results[1]->description;
            $description = str_replace('>>', '<br />', $description);
            echo "<b>Ticket Message: </b><br />" . nl2br($description) . "<br />";
            echo "<HR><HR>";
        }

        ///find in the hub
        $email_hub = $email;
        $find_tix_hub_email = array('query' => '' . $email_hub . '');
        $tickets_hub = $client_hub->search($find_tix_hub_email);
        //$tickets_hub = $client_hub->search(array( 'query' => '48909'));
        $tik_count = count($tickets_hub->results);
        echo "Tickets found in the hub: " . $tik_count . "<br /><br />";

        for ($i = 1; $i < $tik_count; $i++) {
            echo "<b>Ticket #: </b>" . $i . "<br />";
            echo "<b>Ticket ID: </b>" . $tickets_hub->results[$i]->id . "<br />";
            echo "<b>Ticket Email: </b>" . $email_hub . "<br />";
            echo "<b>Ticket Status: </b>" . $tickets_hub->results[$i]->status . "<br />";
            echo "<b>Ticket Subject: </b>" . $tickets_hub->results[$i]->subject . "<br />";
            $description = $tickets_hub->results[$i]->description;
            $description = str_replace('>>', '<br />', $description);
            echo "<b>Ticket Message: </b><br />" . nl2br($description) . "<br />";
            echo "<HR><HR>";
        }
    }
    } else {
        echo "no";
    }
    ?>
</div>
</body>
</html>