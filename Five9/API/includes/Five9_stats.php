<?php
/**
 *
 * Class f9
 */

class f9stats
{
    public $_connection;
    /**
     * @var $_connection contains the five9 connection
     */
    function __construct()
    {
        $wsdl_five9 = "https://api.five9.com/wssupervisor/v3/SupervisorWebService?wsdl&user=api@jerseyconsolidated.com";

        try {
            $soap_options = array('login' => 'api_testing@jerseyconsolidated.com', 'password' => 'fea!D4352', 'trace' => true);
            $this->_connection = new SoapClient($wsdl_five9, $soap_options);
        } catch (Exception $e) {
            $error_message = $e->getMessage();
            echo $error_message;
            exit;
        }
    }

    /**
     * @param $statsisticType
     * @param $columnName
     * @return mixed
     */
    function getStatistics($statsisticType, $columnName)
    {
        $data = array(
            'statisticType' => $statsisticType,
            'columnNames' => $columnName
        );
        $result = $this->_connection->getStatistics($data);
   //     $result = $this->checkImport($result, 'getCrmImportResult');
        return $result;
    }


    /**
     * @return string
     */
    function getLastRequest()
    {
        return $this->_connection->__getLastRequest();
    }

    /**
     * @param $result
     * @param $importType
     * @return string
     */

    //for calls including identifier
    function checkImport($result, $importType)
    {
        $variables = get_object_vars($result);
        $resp = get_object_vars($variables['return']);
        $identifier = $resp['identifier']; //the ID for the import

        //-------check progress of import ()----------------------

        $import_running = true;
        $IIR_p = array('identifier' => array('identifier' => $identifier),
           // 'waitTime' => 10
        );
        while ($import_running) {
            try {
                $IIR_result = $this->_connection->isImportRunning($IIR_p);
                $variables = get_object_vars($IIR_result);
                $import_running = $variables['return'];
            } catch (Exception $e) {
                $error_message = $e->getMessage();
                echo $error_message;
            }
        }
        //------get result ()---------------------------------
        try
        {
            $GLIR_p = array('identifier'=>array('identifier'=>$identifier));
            $GLIR_result = $this->_connection->$importType($GLIR_p);
            return $GLIR_result;
        }
        catch (Exception $e)
        {
            $error_message = $e->getMessage();
            return $error_message;
        }
    }

    //for calls including only array return
    function returnVariables($result)
    {
        $variables = get_object_vars($result);
        return $variables;
    }
}
