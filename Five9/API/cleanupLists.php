<?php
include_once 'includes/Five9.php';
/**
 *
 * ------GETS GENERAL INFO ABOUT A LIST------
 * SERVICE CALLS:
 * listNamePattern string
 *
 * RETURNS:
 * return->listInfo
 */
$five9 = new f9();

$listNamePattern = null;            //Returns all lists
//$listNamePattern = "test list";   //Returns specified list

$result = $five9->getListsInfo($listNamePattern);


foreach ($result->return as $result_ea) {
    $lists[] = $result_ea->name;
}

foreach ($lists as $list) {
    $listName = $list;
    $basicImportSettings = array(
        'allowDataCleanup' => 'true',          //required bool: remove duplicates on key, default false
//    'failOnFieldParseError' => 'false',     //optional bool: stop import on incorrect data, default false
        'fieldsMapping' =>                      //required array: call getContactFields() for full list, columnNumber:int, fieldName:string, key:bool
            array(
                array("columnNumber" => '1', "fieldName" => "number1", "key" => true)
            ),
//    'reportEmail' => '',                    //optional string: if/where to send a report, default empty
        'seperator' => '',                      //required string: deliminator of list is sent, default empty
        'skipHeaderLine' => false               //required bool: skip or use top row
    );

    $listDeleteMode = array(
        'listDeleteMode' => 'DELETE_ALL'         //required array: DELETE_ALL (doesn't apply to single record like deleteRecordFromList), DELETE_IF_SOLE_CRM_MATCH (only delete if a single match is found), DELETE_EXCEPT_FIRST (delete all except first match)
    );

//IMPORTANT: crmUpdateSettings EXTENDS basicImportSettings.. MERGE THEM
//ALL SERVICE PARAMETERS BELOW
    $listDeleteSettings = array_merge($basicImportSettings, $listDeleteMode);
    $record = array ( $_REQUEST['number1']);
    $result = $five9->deleteRecordFromList($listName, $listDeleteSettings, $record);
}

/*[0] => test list
    [1] => Derma Live Decline
    [2] => Derma Live Partials
    [3] => Derma Aged Decline
    [4] => Derma Aged Partials*/
