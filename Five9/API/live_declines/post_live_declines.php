<?php
include_once '../includes/Five9.php';
/**
 *
 * ------ADDS MULTIPLE RECORDS------
 * SERVICE CALLS:
 * listName string
 * listUpdateSettings->listUpdateSettings (extends basicImportSettings)
 * importData
 *
 * RETURNS:
 * return->importIdentifier
 */
$five9 = new f9();

    $listName = "Derma Live Partials";
    $basicImportSettings = array(
        'allowDataCleanup' => 'true',          //required bool: remove duplicates on key, default false
//    'failOnFieldParseError' => 'false',     //optional bool: stop import on incorrect data, default false
        'fieldsMapping' =>                      //required array: call getContactFields() for full list, columnNumber:int, fieldName:string, key:bool
            array(
                array("columnNumber" => '1', "fieldName" => "number1", "key" => true)
            ),
//    'reportEmail' => '',                    //optional string: if/where to send a report, default empty
        'seperator' => '',                      //required string: deliminator of list is sent, default empty
        'skipHeaderLine' => false               //required bool: skip or use top row
    );
    $listDeleteMode = array(
        'listDeleteMode' => 'DELETE_ALL'         //required array: DELETE_ALL (doesn't apply to single record like deleteRecordFromList), DELETE_IF_SOLE_CRM_MATCH (only delete if a single match is found), DELETE_EXCEPT_FIRST (delete all except first match)
    );

//IMPORTANT: crmUpdateSettings EXTENDS basicImportSettings.. MERGE THEM
//ALL SERVICE PARAMETERS BELOW
    $listDeleteSettings = array_merge($basicImportSettings, $listDeleteMode);
    $record = array ( $_REQUEST['number1']);
    $result = $five9->deleteRecordFromList($listName, $listDeleteSettings, $record);


$listName = "Derma Live Decline";

$fields = array(
    "number1" => array ("key" => true),
    "number2" => array ("key" => false),
    "number3" => array ("key" => false),
    "first_name" => array ("key" => false),
    "last_name" => array ("key" => false),
    "company" => array ("key" => false),
    "street" => array ("key" => false),
    "city" => array ("key" => false),
    "state" => array ("key" => false),
    "zip" => array ("key" => false),
    "email" => array ("key" => false),
    "click_id" => array ("key" => false),
    "last_order_id" => array ("key" => false),
    "customer_id" => array ("key" => false),
    "last_campaign_name" => array ("key" => false),
    "last_decline_reason" => array ("key" => false),
    "last_gateway_id" => array ("key" => false),
    "last_ip" => array ("key" => false),
    "product_id_csv" => array ("key" => false),
    "last_order_date" => array ("key" => false),
    "was_reprocessed" => array ("key" => false),
    "address1" => array ("key" => false),
    "address2" => array ("key" => false),
    "country" => array ("key" => false),
    "last_product_names_csv" => array ("key" => false),
    "last_page" => array ("key" => false),
    "other" => array ("key" => false),
    "order_id" => array ("key" => false)
);

$field_keys = array_keys($fields);

$setColumn = 1;

foreach($_REQUEST as $request_field => $request_value) {
    if (isset($fields[$request_field])) {
        //builds array ( "columnNumber" => '1', "fieldName" => "number1", "key" => true ),
        $fieldsMapping[] = array("columnNumber" => $setColumn++, "fieldName" => $request_field, "key" => $fields[$request_field]['key']);
        //builds array ( "5555776741" , "Don0454545" , "Draper" ),
        $importData[0][] = $request_value;
    }
}

$basicImportSettings = array(
    'allowDataCleanup' => 'false',          //required bool: remove duplicates on key, default false
//    'failOnFieldParseError' => 'false',     //optional bool: stop import on incorrect data, default false
    'fieldsMapping' => $fieldsMapping,                     //required array: call getContactFields() for full list, columnNumber:int, fieldName:string, key:bool
//    'reportEmail' => '',                    //optional string: if/where to send a report, default empty
//    'seperator' => '',                      //required string: deliminator of list is sent, default empty
    'skipHeaderLine' => false               //required bool: skip or use top row
);


$listUpdateSettings = array(
//      'callNowColumnNumber' => '', //starting with 1 column indicating a call now (1,T,Y, Yes
//      'callNowMode' => 'ANY', //optional NONE, NEW_CRM_ONLY (only if new to crm), NEW_LIST_ONLY (only if new to list), ANY
//      'callTime' => '', //epoch time in miliseconds
//      'callTimeColumnNumber' => '', //epoch time in miliseconds
    'cleanListBeforeUpdate' => false, //required bool: remove all records from list before adding
    "crmAddMode" => "ADD_NEW",  //ADD_NEW, DONT_ADD should the record be added to the contact list
    "crmUpdateMode" => "UPDATE_FIRST", ///UPDATE_FIRST (update only the first matched record), UPDATE_ALL (update all matched records), UPDATE_SOLE_MATCHES (update only if one matched record is found), DONT_UPDATE (dont update anything)
    "listAddMode" => "ADD_FIRST" //ADD_FIRST (adds only first when multiple matches occur), ADD_ALL (does not work with addRecordToList), ADD_IF_SOLE_CRM_MATCH (add record if only one match exists in the database
);
//IMPORTANT: crmUpdateSettings EXTENDS basicImportSettings.. MERGE THEM
//ALL SERVICE PARAMETERS BELOW
$listUpdateSettings = array_merge($basicImportSettings, $listUpdateSettings);
$result = $five9->asyncAddRecordsToList($listName, $listUpdateSettings, $importData);


/*print_r($fieldsMapping);
print_r($importData);
print_r($result);*/

/*$failureMessage = $result->return->failureMessage();
$keyFields = $result->return->keyFields();
$uploadDuplicatesCount = $result->return->uploadDuplicatesCount();
$uploadErrorsCount = $result->return->uploadErrorsCount();
$warningsCount = $result->return->warningsCount;
$crmRecordsInserted = $result->return->crmRecordsInserted();
$crmRecordsUpdated = $result->return->crmRecordsUpdated;
$listName = $result->return->listName();
$listRecordsDeleted = $result->return->listRecordsDeleted();
$listRecordsInserted = $result->return->listRecordsInserted();

echo $crmRecordsUpdated;

print_r($result);


ALL FIELDS:
number1
number2
number3
first_name
last_name
company
street
city
state
zip
email
click_id
last_order_id
customer_id
last_campaign_name
last_decline_reason
last_gateway_id
last_ip
product_id_csv
last_order_date
was_reprocessed
address1
address2
country
last_product_names_csv
last_page
other
order_id
*/

