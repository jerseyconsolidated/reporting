<?php
class f9
{
    public $_connection;

    /**
     * @var $_connection contains the five9 connection
     */
    function __construct()
    {
        $wsdl_five9 = "https://api.five9.com/wsadmin/v3/AdminWebService?wsdl&user=Jersey Consolidated";
        try {
            $soap_options = array('login' => 'api@jerseyconsolidated.com', 'password' => 'M0rebottles**', 'trace' => true);
            $this->_connection = new SoapClient($wsdl_five9, $soap_options);
        } catch (Exception $e) {
            $error_message = $e->getMessage();
            echo $error_message;
            exit;
        }
    }

    /**
     * @param mixed[] $basicImportSettings Array fieldMapping
     * @param mixed[] $crmDeleteSettings Array contains crmDeleteMode
     * @param mixed[] $importData Array matches field values for fieldMapping
     * @return cid
     */
    function deleteFromContacts($basicImportSettings, $crmDeleteSettings, $importData)
    {
        $crmDeleteSettings = array_merge($basicImportSettings, $crmDeleteSettings);
        $data = array(
            'crmDeleteSettings' => $crmDeleteSettings,
            'importData' => $importData
        );
        $result = $this->_connection->deleteFromContacts($data);

        $variables = get_object_vars($result);
        $resp = get_object_vars($variables['return']);
        $identifier = $resp['identifier']; //the ID for the import
        $result = $this->checkImport($identifier, $this->_connection);
        return $result;
    }

    function deleteFromContactsCsv($basicImportSettings, $crmDeleteSettings, $importData)
    {
        $crmDeleteSettings = array_merge($basicImportSettings, $crmDeleteSettings);
        $data = array(
            'crmDeleteSettings' => $crmDeleteSettings,
            'importData' => $importData
        );
        $result = $this->_connection->deleteFromContacts($data);

        $variables = get_object_vars($result);
        $resp = get_object_vars($variables['return']);
        $identifier = $resp['identifier']; //the ID for the import
        $result = $this->checkImport($identifier, $this->_connection);
        return $result;
    }


    function checkImport($identifier)
    {
        //-------check progress of import ()----------------------

        $import_running = true;
        $IIR_p = array('identifier' => array('identifier' => $identifier),
            'waitTime' => 10);
        while ($import_running) {
            try {
                $IIR_result = $this->_connection->isImportRunning($IIR_p);
                $variables = get_object_vars($IIR_result);
                $import_running = $variables['return'];
            } catch (Exception $e) {
                $error_message = $e->getMessage();
                echo $error_message;
            }
        }
        //------get result ()---------------------------------
        try
        {
            $GLIR_p = array('identifier'=>array('identifier'=>$identifier));
            $GLIR_result = $this->_connection->getCrmImportResult($GLIR_p);
            return $GLIR_result;
        }
        catch (Exception $e)
        {
            $error_message = $e->getMessage();
            return $error_message;
        }
    }

}
