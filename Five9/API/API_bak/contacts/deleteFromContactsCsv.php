<?php
echo "Begin<br/><br/>";

$wsdl_five9 = "https://api.five9.com/wsadmin/v3/AdminWebService?wsdl&user=Jersey Consolidated";
try
{
    $soap_options = array( 'login' => 'api@jerseyconsolidated.com', 'password' => 'M0rebottles**', 'trace' => true );
    $client_five9 = new SoapClient( $wsdl_five9 , $soap_options );
}
catch (Exception $e)
{
    $error_message = $e->getMessage();
    echo $error_message;
}
//readCSV//////////
$csv = file_get_contents('../includes/testList.csv');
///////////////////////
$crmDeleteSettings = array ( "fieldsMapping" => array (
    array ( "columnNumber" => '1', "fieldName" => "number1", "key" => true ),
    array ( "columnNumber" => '2', "fieldName" => "first_name", "key" => false ),
    array ( "columnNumber" => '3', "fieldName" => "last_name", "key" => false) ),
//    "reportEmail" => "email@email.com",
//    "separator" => ',',
    "skipHeaderLine" => true,
    "cleanListBeforeUpdate" => false, //DONT CHANGE
    "crmDeleteMode" => "DELETE_ALL");//DELETE_ALL (doesn't apply to single record like deleteRecordFromList), DELETE_IF_SOLE_CRM_MATCH (only delete if a single match is found), DELETE_EXCEPT_FIRST (delete all except first match)
$xml_data = array ('crmDeleteSettings' => $crmDeleteSettings, 'csvData' => $csv); //request parameters
$result = $client_five9->deleteFromContactsCsv($xml_data);
//print_r("REQUEST:\n" . $client_five9->__getLastRequest() . "\n");

$variables = get_object_vars($result);
$resp = get_object_vars($variables['return']);
$identifier = $resp['identifier']; //the ID for the import
//echo $identifier;
//-------check progress of import ()----------------------
$import_running = true;
$IIR_p = array('identifier'=>array('identifier'=>$identifier),
    'waitTime'=>10);
while($import_running)
{
    try
    {
        $IIR_result = $client_five9->isImportRunning($IIR_p);
        $variables = get_object_vars($IIR_result);
        $import_running = $variables['return'];
    }
    catch (Exception $e)
    {
        $error_message = $e->getMessage();
        echo $error_message;
    }
}
//------get result ()---------------------------------
try
{
    $GLIR_p = array('identifier'=>array('identifier'=>$identifier));
    $GLIR_result = $client_five9->getCrmImportResult($GLIR_p);
    print_r($GLIR_result);
}
catch (Exception $e)
{
    $error_message = $e->getMessage();
    echo $error_message;
}
echo "<br/><br/>";
echo "END";
/*
RESULT

ssh://ryan@192.168.1.145:22/usr/bin/php /home/ryan/www/reporting/Five9/API/contacts/deleteFromContactsCsv.php
Begin<br/><br/>stdClass Object
(
    [return] => stdClass Object
        (
            [keyFields] => number1
            [uploadDuplicatesCount] => 0
            [uploadErrorsCount] => 0
            [warningsCount] => stdClass Object
                (
                )

            [crmRecordsDeleted] => 5
            [crmRecordsInserted] => 0
            [crmRecordsUpdated] => 0
        )

)
<br/><br/>END
Process finished with exit code 0

?>