<?php
echo "Begin<br/><br/>";

$wsdl_five9 = "https://api.five9.com/wsadmin/v3/AdminWebService?wsdl&user=Jersey Consolidated";
try
{
    $soap_options = array( 'login' => 'api@jerseyconsolidated.com', 'password' => 'M0rebottles**', 'trace' => true );
    $client_five9 = new SoapClient( $wsdl_five9 , $soap_options );
}
catch (Exception $e)
{
    $error_message = $e->getMessage();
    echo $error_message;
}

$crmDeleteSettings = array (
    "fieldsMapping" =>
        array(
            array ( "columnNumber" => '1', "fieldName" => "number1", "key" => true ),
            array ( "columnNumber" => '2', "fieldName" => "first_name", "key" => false ),
            array ( "columnNumber" => '3', "fieldName" => "last_name", "key" => false )
        ),
//    "reportEmail" => "email@email.com",
//    "separator" => ',',
    "skipHeaderLine" => false,
    //   "cleanListBeforeUpdate" => false, //DONT CHANGE
//    "callNowMode" => "ANY", //optional NONE, NEW_CRM_ONLY (only if new to crm), NEW_LIST_ONLY (only if new to list), ANY
//    "callNowColumnNumber" => 4, //optional column set to 1 if it needs to be called right away
    //   "cleanListBeforeUpdate" => false,
    "crmDeleteMode" => "DELETE_ALL");//DELETE_ALL (doesn't apply to single record like deleteRecordFromList), DELETE_IF_SOLE_CRM_MATCH (only delete if a single match is found), DELETE_EXCEPT_FIRST (delete all except first match)

$data = array (
    array ( "7419504618","test","name"),
    array ( "5555776749","Don0454545","Draper"),
    array ( "5551112239","Bettffy","Smith"),
    array ( "8787951283","test","name"),
    array ( "5555776754","Don1","Draper")
);

$xml_data = array ('crmDeleteSettings' => $crmDeleteSettings, 'importData' => $data); //request parameters
$result = $client_five9->deleteFromContacts($xml_data);
//print_r("REQUEST:\n" . $client_five9->__getLastRequest() . "\n");

$variables = get_object_vars($result);
$resp = get_object_vars($variables['return']);
$identifier = $resp['identifier']; //the ID for the import
echo $identifier;
//-------check progress of import ()----------------------
$import_running = true;
$IIR_p = array('identifier'=>array('identifier'=>$identifier),
    'waitTime'=>10);
while($import_running)
{
    try
    {
        $IIR_result = $client_five9->isImportRunning($IIR_p);
        $variables = get_object_vars($IIR_result);
        $import_running = $variables['return'];
    }
    catch (Exception $e)
    {
        $error_message = $e->getMessage();
        echo $error_message;
    }
}
//------get result ()---------------------------------
try
{
    $GLIR_p = array('identifier'=>array('identifier'=>$identifier));
    $GLIR_result = $client_five9->getCrmImportResult($GLIR_p);
    print_r($GLIR_result);
}
catch (Exception $e)
{
    $error_message = $e->getMessage();
    echo $error_message;
}
echo "<br/><br/>";
echo "END";
/*
RETURNS
ssh://ryan@192.168.1.145:22/usr/bin/php /home/ryan/www/reporting/Five9/API/contacts/deleteFromContacts.php
Begin<br/><br/>601fa926-f058-45d5-86e8-43b720beaf76stdClass Object
(
    [return] => stdClass Object
        (
            [failureMessage] => There were errors during the upload
            [keyFields] => number1
            [uploadDuplicatesCount] => 0
            [uploadErrorsCount] => 0
            [warningsCount] => stdClass Object
                (
                    [entry] => stdClass Object
                        (
                            [key] => There were 0 matches found in Contacts
                            [value] => 4
                        )

                )

            [crmRecordsDeleted] => 1
            [crmRecordsInserted] => 0
            [crmRecordsUpdated] => 0
        )

)
<br/><br/>END
Process finished with exit code 0

*/
?>