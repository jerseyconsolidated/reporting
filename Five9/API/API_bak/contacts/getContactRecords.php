<?php
echo "Begin<br/><br/>";

$wsdl_five9 = "https://api.five9.com/wsadmin/v3/AdminWebService?wsdl&user=Jersey Consolidated";

function format(&$simpleXmlObject){

    if( ! is_object($simpleXmlObject) ){
        return "";
    }
    //Format XML to save indented tree rather than one line
    $dom = new DOMDocument('1.0');
    $dom->preserveWhiteSpace = false;
    $dom->formatOutput = true;
    $dom->loadXML($simpleXmlObject->asXML());

    return $dom->saveXML();
}

try
{
    $soap_options = array( 'login' => 'api@jerseyconsolidated.com', 'password' => 'M0rebottles**', 'trace' => true );
    $client_five9 = new SoapClient( $wsdl_five9 , $soap_options );
}
catch (Exception $e)
{
    $error_message = $e->getMessage();
    echo $error_message;
}

$lookupCriteria = array(
    'contactIdField' => '123',
    'criteria' =>array(
        array(
            'field' => 'first_name',
            'value' => 'Don1'),
        array(
            'field' => 'last_name',
            'value' => 'Draper'))

);
$xml_data = array ('lookupCriteria' => $lookupCriteria); //request parameters
$result = $client_five9->getContactRecords($xml_data);
$returned = $client_five9->__getLastRequest() . "\n";
$xml_formated = simplexml_load_string($returned);
var_dump(format($xml_formated));
?>
<pre lang="xml">
    <?php print_r($returned); ?>
</pre>
<?php
$variables = get_object_vars($result);
echo '<pre>';
var_dump($variables);
echo '</pre>';
echo "END";
/*
ssh://ryan@192.168.1.145:22/usr/bin/php /home/ryan/www/reporting/Five9/API/contacts/getContactRecords.php
Begin<br/><br/>asdfasdfstring(583) "<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://service.admin.ws.five9.com/">
  <SOAP-ENV:Body>
    <ns1:getContactRecords>
      <lookupCriteria>
        <contactIdField>123</contactIdField>
        <criteria>
          <field>first_name</field>
          <value>Don1</value>
        </criteria>
        <criteria>
          <field>last_name</field>
          <value>Draper</value>
        </criteria>
      </lookupCriteria>
    </ns1:getContactRecords>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
"
asdfasdf<pre lang="xml">
    <?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://service.admin.ws.five9.com/"><SOAP-ENV:Body><ns1:getContactRecords><lookupCriteria><contactIdField>123</contactIdField><criteria><field>first_name</field><value>Don1</value></criteria><criteria><field>last_name</field><value>Draper</value></criteria></lookupCriteria></ns1:getContactRecords></SOAP-ENV:Body></SOAP-ENV:Envelope>

</pre>
<pre>array(1) {
  'return' =>
  class stdClass#3 (2) {
    public $fields =>
    array(19) {
      [0] =>
      string(3) "123"
      [1] =>
      string(7) "number1"
      [2] =>
      string(7) "number2"
      [3] =>
      string(7) "number3"
      [4] =>
      string(10) "first_name"
      [5] =>
      string(9) "last_name"
      [6] =>
      string(7) "company"
      [7] =>
      string(6) "street"
      [8] =>
      string(4) "city"
      [9] =>
      string(5) "state"
      [10] =>
      string(3) "zip"
      [11] =>
      string(10) "Last Agent"
      [12] =>
      string(13) "Last Campaign"
      [13] =>
      string(5) "email"
      [14] =>
      string(21) "Last Attempted Number"
      [15] =>
      string(8) "Order ID"
      [16] =>
      string(21) "Last Disposition Time"
      [17] =>
      string(16) "Last Disposition"
      [18] =>
      string(13) "Call Attempts"
    }
    public $records =>
    class stdClass#4 (1) {
      public $values =>
      class stdClass#5 (1) {
        public $data =>
        array(19) {
          [0] =>
          string(3) "394"
          [1] =>
          string(10) "5555776754"
          [2] =>
          string(0) ""
          [3] =>
          string(0) ""
          [4] =>
          string(4) "Don1"
          [5] =>
          string(6) "Draper"
          [6] =>
          string(0) ""
          [7] =>
          string(0) ""
          [8] =>
          string(0) ""
          [9] =>
          string(0) ""
          [10] =>
          string(0) ""
          [11] =>
          string(0) ""
          [12] =>
          string(0) ""
          [13] =>
          string(0) ""
          [14] =>
          string(0) ""
          [15] =>
          string(0) ""
          [16] =>
          string(0) ""
          [17] =>
          string(0) ""
          [18] =>
          string(0) ""
        }
      }
    }
  }
}
</pre>END
Process finished with exit code 0


*/


?>