<?php
//FOR INSERTING ONE RECORD AT A TIME

echo "Begin<br/><br/>";

$wsdl_five9 = "https://api.five9.com/wsadmin/v3/AdminWebService?wsdl&user=Jersey Consolidated";
try
{
    $soap_options = array( 'login' => 'api@jerseyconsolidated.com', 'password' => 'M0rebottles**', 'trace' => true );
    $client_five9 = new SoapClient( $wsdl_five9 , $soap_options );
}
catch (Exception $e)
{
    $error_message = $e->getMessage();
    echo $error_message;
}

$listNamePattern = "test list";
//$data = array ("4444431344" , "Don1" , "Draper" );
$xml_data = array ('listNamePattern' => $listNamePattern); //request parameters
$result = $client_five9->getListsInfo($xml_data);
//print_r("REQUEST:\n" . $client_five9->__getLastRequest() . "\n");

$variables = get_object_vars($result);
echo '<pre>';
var_dump($variables);
echo '</pre>';
echo "END";
/*
RETURNS
ssh://ryan@192.168.1.145:22/usr/bin/php /home/ryan/www/reporting/Five9/API/getListsInfo.php
Begin<br/><br/>REQUEST:
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://service.admin.ws.five9.com/v2/"><SOAP-ENV:Body><ns1:getListsInfo><listNamePattern>test list</listNamePattern></ns1:getListsInfo></SOAP-ENV:Body></SOAP-ENV:Envelope>

<pre>array(1) {
  'return' =>
  class stdClass#3 (2) {
    public $name =>
    string(9) "test list"
    public $size =>
    int(5)
  }
}
</pre>END
Process finished with exit code 0
*/
?>