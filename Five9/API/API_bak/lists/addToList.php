<?php
echo "Begin<br/><br/>";
//ADD MULTIPLE RECORDS IN 2 DIMENSIONAL ARRAY
$wsdl_five9 = "https://api.five9.com/wsadmin/v3/AdminWebService?wsdl&user=Jersey Consolidated";
try
{
    $soap_options = array( 'login' => 'api@jerseyconsolidated.com', 'password' => 'M0rebottles**', 'trace' => true );
    $client_five9 = new SoapClient( $wsdl_five9 , $soap_options );
}
catch (Exception $e)
{
    $error_message = $e->getMessage();
    echo $error_message;
}
$listUpdateSettings = array ( "fieldsMapping" => array (
    array ( "columnNumber" => '1', "fieldName" => "number1", "key" => true ),
    array ( "columnNumber" => '2', "fieldName" => "first_name", "key" => false ),
    array ( "columnNumber" => '3', "fieldName" => "last_name", "key" => false)
),
//    "reportEmail" => "email@email.com",
//    "separator" => ',',
    "skipHeaderLine" => false,
    "cleanListBeforeUpdate" => false, //DONT CHANGE
//    "callNowMode" => "ANY", //optional NONE, NEW_CRM_ONLY (only if new to crm), NEW_LIST_ONLY (only if new to list), ANY
//    "callNowColumnNumber" => 4, //optional column set to 1 if it needs to be called right away
 //   "cleanListBeforeUpdate" => false,
    "crmAddMode" => "ADD_NEW",  //ADD_NEW, DONT_ADD should the record be added to the contact list
    "crmUpdateMode" => "UPDATE_ALL", ///UPDATE_FIRST (update only the first matched record), UPDATE_ALL (update all matched records), UPDATE_SOLE_MATCHES (update only if one matched record is found), DONT_UPDATE (dont update anything)
    "listAddMode" => "ADD_ALL" ); //ADD_FIRST (adds only first when multiple matches occur), ADD_ALL (adds all records in bulk), ADD_IF_SOLE_CRM_MATCH (add record if only one match exists in the database
$data = array (
    array ( "5555776749" , "Don0454545" , "Draper" ),
    array ( "5551112239" , "betty" , "Smith" ),
    array ( rand(1111111111,9999999999), "test", "name")
);
$xml_data = array ('listName' => "test list", 'listUpdateSettings' => $listUpdateSettings, 'importData' => $data); //request parameters
$result = $client_five9->addToList($xml_data);
//echo "REQUEST:\n" . $client_five9->__getLastRequest() . "\n";
$variables = get_object_vars($result);
$resp = get_object_vars($variables['return']);
$identifier = $resp['identifier']; //the ID for the import
//echo $identifier;
//-------check progress of import ()----------------------
$import_running = true;
$IIR_p = array('identifier'=>array('identifier'=>$identifier),
    'waitTime'=>10);
while($import_running)
{
    try
    {
        $IIR_result = $client_five9->isImportRunning($IIR_p);
        $variables = get_object_vars($IIR_result);
        $import_running = $variables['return'];
    }
    catch (Exception $e)
    {
        $error_message = $e->getMessage();
        echo $error_message;
    }
}
//------get result ()---------------------------------
try
{
    $GLIR_p = array('identifier'=>array('identifier'=>$identifier));
    $GLIR_result = $client_five9->getListImportResult($GLIR_p);
    print_r($GLIR_result);
}
catch (Exception $e)
{
    $error_message = $e->getMessage();
    echo $error_message;
}
echo "<br/><br/>";
echo "END";
/*
stdClass Object
(
    [return] => stdClass Object
        (
            [keyFields] => number1
            [uploadDuplicatesCount] => 0
            [uploadErrorsCount] => 0
            [warningsCount] => stdClass Object
                (
                )

            [callNowQueued] => 0
            [crmRecordsInserted] => 1
            [crmRecordsUpdated] => 2
            [listName] => test list
            [listRecordsDeleted] => 0
            [listRecordsInserted] => 1
        )

)
<br/><br/>END
Process finished with exit code 0
*/
?>