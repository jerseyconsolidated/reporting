<?php
echo "Begin<br/><br/>";
//ADD MULTIPLE RECORDS IN 2 DIMENSIONAL ARRAY
$wsdl_five9 = "https://api.five9.com/wsadmin/v3/AdminWebService?wsdl&user=Jersey Consolidated";
try
{
    $soap_options = array( 'login' => 'api@jerseyconsolidated.com', 'password' => 'M0rebottles**', 'trace' => true );
    $client_five9 = new SoapClient( $wsdl_five9 , $soap_options );
}
catch (Exception $e)
{
    $error_message = $e->getMessage();
    echo $error_message;
}
//---------------initiate import (asyncAddRecordsToList)-----------------
$listDeleteSettings = array ( "fieldsMapping" => array (
    array ( "columnNumber" => '1', "fieldName" => "number1", "key" => true ),
    array ( "columnNumber" => '2', "fieldName" => "first_name", "key" => false ),
    array ( "columnNumber" => '3', "fieldName" => "last_name", "key" => false) ),
//    "reportEmail" => "email@email.com",
//    "separator" => ',',
    "skipHeaderLine" => false,
    "cleanListBeforeUpdate" => false, //DONT CHANGE
//    "callNowMode" => "ANY", //optional NONE, NEW_CRM_ONLY (only if new to crm), NEW_LIST_ONLY (only if new to list), ANY
//    "callNowColumnNumber" => 4, //optional column set to 1 if it needs to be called right away
 //   "cleanListBeforeUpdate" => false,
    "listDeleteMode" => "DELETE_IF_SOLE_CRM_MATCH"//DELETE_ALL (doesn't apply to single record like deleteRecordFromList), DELETE_IF_SOLE_CRM_MATCH (only delete if a single match is found), DELETE_EXCEPT_FIRST (delete all except first match)
);
$data = array (
    array ( "2515705856","test","name" ),
    array ( "8612296920","test","name" ),
    array ( "2015052367","test","name" ),
    array ( "9234404961","test","name" ),
    array ( "9192823631","test","name" ),
    array ( "7445031714","test","name" ),
);
$xml_data = array ('listName' => "test list", 'listDeleteSettings' => $listDeleteSettings, 'importData' => $data); //request parameters
$result = $client_five9->deleteFromList($xml_data);
$variables = get_object_vars($result);
$resp = get_object_vars($variables['return']);
$identifier = $resp['identifier']; //the ID for the import
//echo $identifier;
//-------check progress of import ()----------------------
$import_running = true;
$IIR_p = array('identifier'=>array('identifier'=>$identifier),
    'waitTime'=>10);
while($import_running)
{
    try
    {
        $IIR_result = $client_five9->isImportRunning($IIR_p);
        $variables = get_object_vars($IIR_result);
        $import_running = $variables['return'];
    }
    catch (Exception $e)
    {
        $error_message = $e->getMessage();
        echo $error_message;
    }
}
//------get result ()---------------------------------
try
{
    $GLIR_p = array('identifier'=>array('identifier'=>$identifier));
    $GLIR_result = $client_five9->getListImportResult($GLIR_p);
    print_r($GLIR_result);
}
catch (Exception $e)
{
    $error_message = $e->getMessage();
    echo $error_message;
}
echo "<br/><br/>";
echo "END";
/*
 RETURNS
ssh://ryan@192.168.1.145:22/usr/bin/php /home/ryan/www/reporting/Five9/API/deleteFromList.php
Begin<br/><br/>stdClass Object
(
    [return] => stdClass Object
        (
            [uploadDuplicatesCount] => 0
            [uploadErrorsCount] => 0
            [warningsCount] => stdClass Object
                (
                )

            [callNowQueued] => 0
            [crmRecordsInserted] => 0
            [crmRecordsUpdated] => 0
            [listName] => test list
            [listRecordsDeleted] => 6
            [listRecordsInserted] => 0
        )

)
<br/><br/>END
Process finished with exit code 0
 */
?>