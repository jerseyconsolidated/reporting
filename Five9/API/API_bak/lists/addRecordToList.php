<?php
//FOR INSERTING ONE RECORD AT A TIME

echo "Begin<br/><br/>";

$wsdl_five9 = "https://api.five9.com/wsadmin/v3/AdminWebService?wsdl&user=Jersey Consolidated";
try
{
    $soap_options = array( 'login' => 'api@jerseyconsolidated.com', 'password' => 'M0rebottles**', 'trace' => true );
    $client_five9 = new SoapClient( $wsdl_five9 , $soap_options );
}
catch (Exception $e)
{
    $error_message = $e->getMessage();
    echo $error_message;
}
//---------------initiate import (asyncAddRecordsToList)-----------------
$listUpdateSettings = array ( "fieldsMapping" => array (
    array ( "columnNumber" => '1', "fieldName" => "number1", "key" => true ),
    array ( "columnNumber" => '2', "fieldName" => "first_name", "key" => false ),
    array ( "columnNumber" => '3', "fieldName" => "last_name", "key" => false) ),
//    "reportEmail" => "email@email.com",
//    "separator" => ',',
    "skipHeaderLine" => false,
    "cleanListBeforeUpdate" => false, //DONT CHANGE
//    "callNowMode" => "ANY", //optional NONE, NEW_CRM_ONLY (only if new to crm), NEW_LIST_ONLY (only if new to list), ANY
//    "callNowColumnNumber" => 4, //optional column set to 1 if it needs to be called right away
 //   "cleanListBeforeUpdate" => false,
    "crmAddMode" => "ADD_NEW",  //ADD_NEW, DONT_ADD should the record be added to the contact list
    "crmUpdateMode" => "UPDATE_FIRST", ///UPDATE_FIRST (update only the first matched record), UPDATE_ALL (doesn't work with addRecordToLists), UPDATE_SOLE_MATCHES (update only if one matched record is found), DONT_UPDATE (dont update anything)
    "listAddMode" => "ADD_FIRST" ); //ADD_FIRST (adds only first when multiple matches occur), ADD_ALL (does not work with addRecordToList), ADD_IF_SOLE_CRM_MATCH (add record if only one match exists in the database
$data = array ( "5555773754" , "Don1" , "Draper" );

//$data = array ("4444431344" , "Don1" , "Draper" );
$xml_data = array ('listName' => "test list", 'listUpdateSettings' => $listUpdateSettings, 'record' => $data); //request parameters
$result = $client_five9->addRecordToList($xml_data);
//print_r("REQUEST:\n" . $client_five9->__getLastRequest() . "\n");
$variables = get_object_vars($result);
echo '<pre>';
var_dump($variables);
echo '</pre>';
echo "END";
/*
RETURNS

ssh://ryan@192.168.1.145:22/usr/bin/php /home/ryan/www/reporting/Five9/API/addRecordToList.php
Begin<br/><br/><pre>array(1) {
    'return' =>
  class stdClass#3 (10) {
    public $failureMessage =>
    string(0) ""
    public $uploadDuplicatesCount =>
    int(0)
    public $uploadErrorsCount =>
    int(0)
    public $warningsCount =>
    class stdClass#4 (0) {
    }
    public $callNowQueued =>
    int(0)
    public $crmRecordsInserted =>
    int(1)
    public $crmRecordsUpdated =>
    int(0)
    public $listName =>
    string(9) "test list"
    public $listRecordsDeleted =>
    int(0)
    public $listRecordsInserted =>
    int(1)
  }
}
</pre>END
Process finished with exit code 0*/
?>