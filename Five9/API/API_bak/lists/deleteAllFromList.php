<?php
echo "Begin<br/><br/>";
//DELETE ONLY ONE RECORD ONLY FROM A LIST
$wsdl_five9 = "https://api.five9.com/wsadmin/v3/AdminWebService?wsdl&user=Jersey Consolidated";
try
{
    $soap_options = array( 'login' => 'api@jerseyconsolidated.com', 'password' => 'M0rebottles**', 'trace' => true );
    $client_five9 = new SoapClient( $wsdl_five9 , $soap_options );
}
catch (Exception $e)
{
    $error_message = $e->getMessage();
    echo $error_message;
}
//---------------initiate import (asyncAddRecordsToList)-----------------
$reportEmail = "ryan@jerfseyconsolidated.com";

$xml_data = array ('listName' => "test list", 'reportEmail' => $reportEmail); //request parameters
$result = $client_five9->deleteAllFromList($xml_data);
$variables = get_object_vars($result);
$resp = get_object_vars($variables['return']);
$identifier = $resp['identifier']; //the ID for the import
//echo $identifier;
//-------check progress of import ()----------------------
$import_running = true;
$IIR_p = array('identifier'=>array('identifier'=>$identifier),
    'waitTime'=>10);
while($import_running)
{
    try
    {
        $IIR_result = $client_five9->isImportRunning($IIR_p);
        $variables = get_object_vars($IIR_result);
        $import_running = $variables['return'];
    }
    catch (Exception $e)
    {
        $error_message = $e->getMessage();
        echo $error_message;
    }
}
//------get result ()---------------------------------
try
{
    $GLIR_p = array('identifier'=>array('identifier'=>$identifier));
    $GLIR_result = $client_five9->getListImportResult($GLIR_p);
    print_r($GLIR_result);
}
catch (Exception $e)
{
    $error_message = $e->getMessage();
    echo $error_message;
}
echo "<br/><br/>";
echo "END";
/*
 *
ssh://ryan@192.168.1.145:22/usr/bin/php /home/ryan/www/reporting/Five9/API/deleteAllFromList.php
Begin<br/><br/>stdClass Object
(
    [return] => stdClass Object
        (
            [uploadDuplicatesCount] => 0
            [uploadErrorsCount] => 0
            [warningsCount] => stdClass Object
                (
                )

            [callNowQueued] => 0
            [crmRecordsInserted] => 0
            [crmRecordsUpdated] => 0
            [listName] => test list
            [listRecordsDeleted] => 20
            [listRecordsInserted] => 0
        )

)
<br/><br/>END
Process finished with exit code 0

*/
?>