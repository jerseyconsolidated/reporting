<?php
echo "Begin<br/><br/>";
//DELETE ONLY ONE RECORD ONLY FROM A LIST
$wsdl_five9 = "https://api.five9.com/wsadmin/v3/AdminWebService?wsdl&user=Jersey Consolidated";
try
{
    $soap_options = array( 'login' => 'api@jerseyconsolidated.com', 'password' => 'M0rebottles**', 'trace' => true );
    $client_five9 = new SoapClient( $wsdl_five9 , $soap_options );
}
catch (Exception $e)
{
    $error_message = $e->getMessage();
    echo $error_message;
}
//---------------initiate import (asyncAddRecordsToList)-----------------
$listDeleteSettings = array ( "fieldsMapping" => array (
    array ( "columnNumber" => '1', "fieldName" => "number1", "key" => true )),
    "skipHeaderLine" => false,

//    array ( "columnNumber" => '2', "fieldName" => "first_name", "key" => false ),
//    array ( "columnNumber" => '3', "fieldName" => "last_name", "key" => false) ),
    "listDeleteMode" => "DELETE_IF_SOLE_CRM_MATCH"//DELETE_ALL (doesn't apply to single record like deleteRecordFromList), DELETE_IF_SOLE_CRM_MATCH (only delete if a single match is found), DELETE_EXCEPT_FIRST (delete all except first match)
);
//$data = array ( "5555776754" , "Don1" , "Draper" );
$data = array ( "5555776749");

$xml_data = array ('listName' => "test list", 'listDeleteSettings' => $listDeleteSettings, 'record' => $data); //request parameters
$result = $client_five9->deleteRecordFromList($xml_data);
$variables = get_object_vars($result);
echo '<pre>';
var_dump($variables);
echo '</pre>';
echo "END";
/*
RETURNS
ssh://ryan@192.168.1.145:22/usr/bin/php /home/ryan/www/reporting/Five9/API/deleteRecordFromList.php
Begin<br/><br/><pre>array(1) {
  'return' =>
  class stdClass#3 (10) {
    public $failureMessage =>
    string(0) ""
    public $uploadDuplicatesCount =>
    int(0)
    public $uploadErrorsCount =>
    int(0)
    public $warningsCount =>
    class stdClass#4 (0) {
    }
    public $callNowQueued =>
    int(0)
    public $crmRecordsInserted =>
    int(0)
    public $crmRecordsUpdated =>
    int(0)
    public $listName =>
    string(9) "test list"
    public $listRecordsDeleted =>
    int(1)
    public $listRecordsInserted =>
    int(0)
  }
}
</pre>END
Process finished with exit code 0

*/
?>