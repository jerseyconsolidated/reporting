<?php
include_once 'includes/Five9.php';
$deleteFromContacts = new f9();

$basicImportSettings = array(
    'allowDataCleanup' => 'false',          //required bool: remove duplicates on key, default false
//    'failOnFieldParseError' => 'false',     //optional bool: stop import on incorrect data, default false
    'fieldsMapping' =>                      //required array: call getContactFields() for full list, columnNumber:int, fieldName:string, key:bool
        array(
            array ( "columnNumber" => '1', "fieldName" => "number1", "key" => true ),
            array ( "columnNumber" => '2', "fieldName" => "first_name", "key" => false ),
            array ( "columnNumber" => '3', "fieldName" => "last_name", "key" => false )
        ),
//    'reportEmail' => '',                    //optional string: if/where to send a report, default empty
    'seperator' => '',                      //required string: deliminator of list is sent, default empty
    'skipHeaderLine' => false               //required bool: skip or use top row

);

$crmDeleteSettings = array(
    'crmDeleteMode' => 'DELETE_ALL'         //required array: DELETE_ALL (doesn't apply to single record like deleteRecordFromList), DELETE_IF_SOLE_CRM_MATCH (only delete if a single match is found), DELETE_EXCEPT_FIRST (delete all except first match)
);

$importData = array (                       //required array: values matching in 'fieldsMapping'
    array ( "3748867524","test","name"),
    array ( "5555776749","Don0454545","Draper"),
    array ( "5551112239","Bettffy","Smith"),
    array ( "8787951283","test","name"),
    array ( "5555776754","Don1","Draper")
);

$result = $deleteFromContacts->deleteFromContacts($basicImportSettings, $crmDeleteSettings, $importData);
print_r($result);
