<?php
include_once '../includes/Five9_stats.php';
/**
 *
 * ------Get Statistics------
 * SERVICE CALLS:
 * statisticType->statisticType
 * columnNames->row
 *
 * RETURNS:
 * return->statistics
 */
$five9 = new f9stats();

$statisticType = 'OutboundCampaignStatistics'; //AgentState, AgentStatistics, ACDStatus, CampaignState, OutboundCampaignManager, OutboundCampaignStatistics, InboundCampaignStatistics, AutodialCampaignStatistics

$columnName = array(
    'values'  => array(
        'data' => array('Username'),
    )
);
$result = $five9->getStatistics($statisticType, $columnName);
$variables = get_object_vars($result);
echo '<pre>';
//var_dump($variables);
var_dump($result);
echo '</pre>';
echo "END";
/*
RETURNS

ssh://ryan@192.168.1.145:22/usr/bin/php /home/ryan/www/reporting/Five9/API/addRecordToList.php
Begin<br/><br/><pre>array(1) {
    'return' =>
  class stdClass#3 (10) {
    public $failureMessage =>
    string(0) ""
    public $uploadDuplicatesCount =>
    int(0)
    public $uploadErrorsCount =>
    int(0)
    public $warningsCount =>
    class stdClass#4 (0) {
    }
    public $callNowQueued =>
    int(0)
    public $crmRecordsInserted =>
    int(1)
    public $crmRecordsUpdated =>
    int(0)
    public $listName =>
    string(9) "test list"
    public $listRecordsDeleted =>
    int(0)
    public $listRecordsInserted =>
    int(1)
  }
}
</pre>END
Process finished with exit code 0*/
?>