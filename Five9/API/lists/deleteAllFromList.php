<?php
include_once '../includes/Five9.php';
/**
 *
 * ------DELETES ALL RECORDS IN A LIST, DOES NOT DELETE CONTACTS-----
 * SERVICE CALLS:
 * listName string
 * reportEmail
 *
 * RETURNS:
 * return->importIdentifier
 */
$five9 = new f9();

$listName = 'Derma Live Partials';
$reportEmail = "ryan@googe.com";

$result = $five9->deleteAllFromList($listName, $reportEmail);
print_r($result);
/*
 *
ssh://ryan@192.168.1.145:22/usr/bin/php /home/ryan/www/reporting/Five9/API/lists/deleteAllFromList.php
stdClass Object
(
    [return] => stdClass Object
        (
            [keyFields] => number1
            [uploadDuplicatesCount] => 0
            [uploadErrorsCount] => 0
            [warningsCount] => stdClass Object
                (
                )

            [callNowQueued] => 0
            [crmRecordsInserted] => 0
            [crmRecordsUpdated] => 0
            [listName] => test web2campaign list
            [listRecordsDeleted] => 16
            [listRecordsInserted] => 0
        )

)

Process finished with exit code 0


*/
?>