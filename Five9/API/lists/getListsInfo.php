<?php
include_once '../includes/Five9.php';
/**
 *
 * ------GETS GENERAL INFO ABOUT A LIST------
 * SERVICE CALLS:
 * listNamePattern string
 *
 * RETURNS:
 * return->listInfo
 */
$five9 = new f9();

$listNamePattern = null;            //Returns all lists
//$listNamePattern = "test list";   //Returns specified list

$result = $five9->getListsInfo($listNamePattern);

foreach ($result->return as $result_ea) {
    $lists[] = $result_ea->name;
}

print_r($lists);
//print_r($result);

/*
RETURNS



ssh://ryan@192.168.1.145:22/usr/bin/php /home/ryan/www/reporting/Five9/API/lists/getListsInfo.php
stdClass Object
(
    [return] => Array
        (
            [0] => stdClass Object
                (
                    [name] => test list
                    [size] => 2
                )

            [1] => stdClass Object
                (
                    [name] => test web2campaign list
                    [size] => 1
                )

            [2] => stdClass Object
                (
                    [name] => Derma Live Declined
                    [size] => 0
                )

            [3] => stdClass Object
                (
                    [name] => Derna Period Declined
                    [size] => 0
                )

            [4] => stdClass Object
                (
                    [name] => Live Decline Derma
                    [size] => 0
                )

        )

)

Process finished with exit code 0


*/
?>