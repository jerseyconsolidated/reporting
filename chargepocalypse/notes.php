<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '-1');


function doNotes($case, $data)
{
?>
<div class="wrapper">
	<p id="casenumbertop">Case #<?php echo $case; ?></p>
	<div class="headlinemerchantorder">
		MERCHANT NOTES
	</div>
	<p>As you can see on the previous page we have provided all necessary evidence to dispute this chargeback. Within this document, you will find all the evidence to support the information provided on the first page. We strive to provide an expedient response to our customers and their card issues.</p>
	<div class="headlinemerchantorder">
		ORDER HISTORY
	</div>
	<table  width="816px" class="orderfonts" cellpadding="0">
		<tr style="background-color:#b9b9b9 !important;">
			<td width="46px;">
				Id
			</td>
			<td width="763px">
				History
			</td>
		</tr>
		<?php
		$index = 0;
		foreach($data["systemNotes"] as $note)
		{
			$index++;
			?>
			<tr class="lightercells"><td class="smallercell"><?php echo $index; ?></td><td class="largercell"><?php echo $note; ?></td></tr>
		<?php
		}
		?>
	</table>
	<div class="headlinemerchantorder">
		ORDER NOTES
	</div>
	<table  width="816px" class="orderfonts" cellpadding="0">
		<tr style="background-color:#b9b9b9 !important;">
			<td width="46px;">
				Id
			</td>
			<td width="763px">
				Notes
			</td>
		</tr>
		<?php
		$index = 0;
		foreach($data["employeeNotes"] as $note)
		{
			$index++;
			?>
			<tr class="lightercells"><td class="smallercell"><?php echo $index; ?></td><td class="largercell"><?php echo $note; ?> </td></tr>
		<?php
		}
		?>
	</table>
</div>
</body>
</html>
<?php
}
?>