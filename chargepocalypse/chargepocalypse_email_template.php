<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '-1');


function printEmail($inc_company, $data)
{
	$oo = strtotime($data["originally_ordered"]);
	$date = date("m-d-Y", $oo);
	$bill_name = $data["billing_first_name"]." ".$data["billing_last_name"];
	$bill_add = $data["billing_street_address"];
	$bill_add2 = $data["billing_city"].", ".$data["billing_state"]." ".$data["billing_postcode"];
	$ship_name = $data["shipping_first_name"]." ".$data["shipping_last_name"];
	$ship_add = $data["shipping_street_address"];
	$ship_add2 = $data["shipping_city"].", ".$data["shipping_state"]." ".$data["shipping_postcode"];

	$order_id = $data["order_id"];
	$order_total = $data["order_total"];

	$logo = "";
	if($inc_company == "dermaliv")
	{
		$company = "Dermaliv";
		$logo 	= "images/logo.png";
	}
	else if($inc_company == "synergylife")
	{
		$company = "Synergy Life";
		$logo 	= "images/SL_logo.png";
	}
	else if($inc_company == "baylieskin")
	{
		$company = "Baylie Skin";
		$logo 	= "images/baylie.png";
	}
	else if($inc_company == "camileskin")
	{
		$company = "Camile Skin";
		$logo 	= "images/camile.png";
	}
	else if($inc_company == "biohealthresearch")
	{
		$company = "BioHealth Research";
		$logo 	= "images/biohealth.png";
	}
?>
	<div style="background-color: rgb(242, 242, 242); ">
		<br />
		<br />
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody>
			<tr>
				<td align="left" style="padding-bottom: 8px; padding-left: 2px;" valign="bottom">
					<span style="font-family: &quot;Lucida Grande&quot;,&quot;Lucida Sans&quot;,&quot;Lucida Sans Unicode&quot;,Arial,Helvetica,Verdana,sans-serif; color: rgb(68, 68, 68); font-size: 20px; line-height: 1em ! important;"><!--Add Your Company Name Or Logo Here--></span><img alt="<?php echo $company;?>" src="<?php echo $logo;?>" style="color: rgb(68, 68, 68); font-family: 'Lucida Grande', 'Lucida Sans', 'Lucida Sans Unicode', Arial, Helvetica, Verdana, sans-serif; font-size: 20px; line-height: 20px; text-align: -webkit-right; width: 150px; height: auto;" width="161px" /></td>
				<td align="right" style="padding-bottom: 8px;" valign="bottom">
					<div style="font-family: Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif; color: rgb(68, 68, 68); font-size: 20px; line-height: 1em ! important;">
						Order Confirmation</div>
				</td>
			</tr>
			</tbody>
		</table>
		<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody>
			<tr>
				<td bgcolor="#ffffff" style="border: 1px solid rgb(227, 227, 227);" width="100%">
					<table align="center" border="0" cellpadding="0" cellspacing="0">
						<tbody>
						<tr>
							<td style="padding: 20px 0pt;">
								<table align="center" border="0" cellpadding="0" cellspacing="0" width="570">
									<tbody>
									<tr>
										<td align="left">
											<div style="font-family: Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif; color: rgb(0, 0, 0); font-size: 13px; line-height: 0.92em ! important; font-weight: bold;">
												Order Number: <a style="color: rgb(0, 0, 0); font-weight: normal;"><?php echo $order_id;?></a></div>
										</td>
										<td align="right">
											<div style="font-family: Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif; color: rgb(0, 0, 0); font-size: 12px; line-height: 1em ! important;">
												Ordered On: <?php echo $date;?></div>
										</td>
									</tr>
									<tr>
										<td colspan="2" style="padding-top: 15px; border-bottom: 1px solid rgb(204, 204, 204); font-size: 1px; line-height: 1px;">
											&nbsp;</td>
									</tr>
									<tr>
										<td colspan="2" style="padding: 15px 0pt 34px;">
											<table align="center" border="0" cellpadding="0" cellspacing="0" width="548">
												<tbody>
												<tr>
													<td valign="top" width="45">
														<div style="font-family: Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif; color: rgb(102, 102, 102); font-size: 11px; line-height: 1.45em; padding-right: 5px;">
															Bill to</div>
													</td>
													<td style="border-right: 1px solid rgb(227, 227, 227);" valign="top" width="172">
														<div style="font-family: Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif; color: rgb(0, 0, 0); font-size: 10px; line-height: 1.6em;">
															<?php echo $bill_name;?><br />
															<?php echo $bill_add;?><br />
															<?php echo $bill_add2;?><br />
															</div>
													</td>
													<td bgcolor="#e5e5e5" style="padding-top: 15px; font-size: 1px; line-height: 1px;" width="1">
														&nbsp;</td>
													<td style="padding-left: 22px;" valign="top" width="45">
														<div style="font-family: Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif; color: rgb(102, 102, 102); font-size: 11px; line-height: 1.45em; font-weight: bold; padding-right: 10px;">
															Ship to</div>
													</td>
													<td valign="top" width="154">
														<div style="font-family: Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif; color: rgb(0, 0, 0); font-size: 10px; line-height: 1.6em;">
															<?php echo $ship_name;?><br />
															<?php echo $ship_add;?><br />
															<?php echo $ship_add2;?><br />
															</div>
													</td>
												</tr>
												</tbody>
											</table>
										</td>
									</tr>
									</tbody>
								</table>
								<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
									<tbody>
									<tr>
										<td align="left" bgcolor="#2d5e9f" style="padding: 6px 0pt 8px 10px; background: none repeat scroll 0% 0% rgb(0, 0, 0);" valign="top">
											<div style="color: white; font-size: 13px; font-weight: bold; line-height: 1em ! important; font-family: Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;">
												Items in Your Order</div>
										</td>
									</tr>
									<tr>
										<td align="left" valign="top">
											&nbsp;</td>
									</tr>
									</tbody>
								</table>
								<table style="font-family:arial;font-size:12px;" align="center" border="0" cellpadding="0" cellspacing="0" width="90%">
									<tbody>
									<tr>
										<td>
								        Product Name
                                        </td>
                                        <td>
                                        Base
                                        </td>
                                        <td>
                                        Qty
                                        </td>
                                        <td>
                                        Price
                                        </td>
									</tr>
									<?php
	foreach($data["products"] as $product)
	{
		$total_price = $product["product_qty"] * $product["price"];
		?>
                                    <tr>
                                        <td>
                                        <? echo $product["name"]; ?>
                                        </td>    
                                        <td>
										<? echo $product["price"]; ?>
                                        </td>    
                                        <td>
                                        <? echo $product["product_qty"]; ?>
                                        </td>
                                        <td>
                                        <? echo $total_price; ?>
                                        </td>
                                    </tr>
		<?php
	}
	?>
									</tbody>
								</table>
							</td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
		<p style="color: rgb(0, 0, 0); font-family: 'Lucida Grande', 'Lucida Sans', 'Lucida Sans Unicode', Arial, Helvetica, Verdana, sans-serif; font-size: 10px; line-height: 15px;">
			Customer Service<br />
			Dermaliv</p>
		<p style="color: rgb(0, 0, 0); font-family: 'Lucida Grande', 'Lucida Sans', 'Lucida Sans Unicode', Arial, Helvetica, Verdana, sans-serif; font-size: 10px; line-height: 15px;">
			tel: 1-888-771-2337<br />
			email: cs@dermaliv.com<br />
			<br />
			<a href="https://synergylife.com/secure/myorder/dermaliv/fromEmail?zip={shippingzipcode}&amp;order_id={orderid}">View Your Order</a></p>
		<p>
			&nbsp;</p>
		<p>
			&nbsp;</p>
	</div>
	<p>
		&nbsp;</p>



<?php
}
?>