<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '-1');

function doInvoice($case, $data, $product, $inc_company)
{
	$oo = strtotime($data["originally_ordered"]);
	$date = date("m-d-Y", $oo);
	$bill_name = $data["billing_first_name"]." ".$data["billing_last_name"];
	$bill_add = $data["billing_street_address"];
	$bill_add2 = $data["billing_city"].", ".$data["billing_state"]." ".$data["billing_postcode"];
	$ship_name = $data["shipping_first_name"]." ".$data["shipping_last_name"];
	$ship_add = $data["shipping_street_address"];
	$ship_add2 = $data["shipping_city"].", ".$data["shipping_state"]." ".$data["shipping_postcode"];

	$order_id = $data["order_id"];
	$order_total = $data["order_total"];
	$order_tax = $data["order_sales_tax_amount"];

	$company 	= makeCompanyPretty($inc_company);
	$logo 		= getCompanyLogo($inc_company);

	if($product == "eye")
	{
		$product = "Dermaliv Rejuvenating Eye Treatment";
	}
	else
	{
		$product = "Dermaliv Rejuvenating Face Treatment";
	}
	?>
<html>
<head>
	<meta name="description" content="Invoice">
	<meta name="author" content="Dermaliv">
	<meta name="viewport" content="width=device-width">
	<link href="css/style.css" rel="stylesheet" />
	<link href="css/print.css" media="print" rel="stylesheet" />
</head>
<body>
    <div class="wrapper">
        <div id="top">
            <div id="address">
                <?php echo $company; ?><br/>
                84 Washington st, 3rd floor West, NJ 07030
                <br/>Tel 1-888-852-1051
            </div>
            <div id="logo">
                <img src="<?php echo $logo; ?>"/>
            </div>
        </div>
        <div class="clearfix">
        </div>
        <div id="topbar">
            <img src="images/top-bar.png"/>
            <h4 id="invoice">Invoice #<?php echo  $order_id. "\n";;?></h4>
            <h4 id="date"><?php echo "" . $date . "<br>";?></h4>
        </div>
        <div id="billandship">
            <div id="billto">
                BILL TO
            </div>
            <div id="shipto">
                SHIP TO
            </div>
        </div>
        <div class="clearfix"></div>
        <div id="addresses">
            <div id="address1">
                <?php echo$bill_name; ?><br/><br/>
                <?php echo$bill_add; ?><br/><br/>
                <?php echo$bill_add2; ?><br/><br/>
            </div>
            <div id="address2">
				<?php echo$ship_name; ?><br/><br/>
				<?php echo$ship_add; ?><br/><br/>
				<?php echo$ship_add2; ?><br/><br/>
            </div>
        </div>
        <div id="fourtable">
            <table style="display:block;margin-top:200px;">
                <tr>
                    <td style="border:1px solid #232323;padding-top:10px; padding-bottom:10px" width="204px">
                    Quantity
                    </td>
                    <td style="border:1px solid #232323;padding-top:10px; padding-bottom:10px" width="204px">
                    Description
                    </td>
                    <td style="border:1px solid #232323;padding-top:10px; padding-bottom:10px" width="204px">
                    Unit Price
                    </td>
                    <td style="border:1px solid #232323;padding-top:10px; padding-bottom:10px" width="204px">
                    Total
                    </td>
                </tr>
				<?php
				$total = 0;
				foreach($data["products"] as $product)
				{
					$this_total = $product["product_qty"] * $product["price"];
					$total = $total+$this_total;
				?>
					<tr>
						<td style="border:1px solid #232323;padding-top:10px; padding-bottom:10px" width="204px">
						<?php echo$product["product_qty"]; ?>
						</td>
						<td style="border:1px solid #232323;padding-top:10px; padding-bottom:10px" width="204px">
						<?php echo$product["name"]; ?>
						</td>
						<td style="border:1px solid #232323;padding-top:10px; padding-bottom:10px" width="204px">
						<?php echo "$".$product["price"]; ?>
						</td>
						<td style="border:1px solid #232323;padding-top:10px; padding-bottom:10px" width="204px">
						<?php echo "$".$this_total?>
						</td>
					</tr>

				<?php
				}
				?><!--
                <tr>
                    <td style="border:1px solid #232323;padding-top:10px; padding-bottom:10px" width="204px">
                    
                    </td>
                    <td style="border:1px solid #232323;padding-top:10px; padding-bottom:10px" width="204px">
                    
                    </td>
                    <td style="border:1px solid #232323;padding-top:10px; padding-bottom:10px" width="204px">
                    
                    </td>
                    <td style="border:1px solid #232323;padding-top:10px; padding-bottom:10px" width="204px">

                    </td>
                </tr>
                <tr>
                    <td style="border:1px solid #232323;padding-top:10px; padding-bottom:10px" width="204px">
                    
                    </td>
                    <td style="border:1px solid #232323;padding-top:10px; padding-bottom:10px" width="204px">
                    
                    </td>
                    <td style="border:1px solid #232323;padding-top:10px; padding-bottom:10px" width="204px">
                    
                    </td>
                    <td style="border:1px solid #232323;padding-top:10px; padding-bottom:10px" width="204px">

                    </td>
                </tr>
                -->
            </table>
        </div>
        <div id="totaltable">
            <table style="fl">
                <tr style="border:1px solid #232323;padding-top:10px; padding-bottom:10px">
                    <td style="border-bottom:1px solid; width:204px; padding-top:10px; padding-bottom:10px;">
                    SUBTOTAL
                    </td>
                    <td style="border-bottom:1px solid; width:204px; padding-top:10px; padding-bottom:10px;">
					<?php echo"$".$total; ?>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom:1px solid; width:204px; padding-top:10px; padding-bottom:10px;">
                    SALES TAX
                    </td>
                    <td style="border-bottom:1px solid; width:204px; padding-top:10px; padding-bottom:10px;">
                    <?php echo "$".$order_tax; ?>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom:1px solid; width:204px; padding-top:10px; padding-bottom:10px;">
                    SHIPPING & HANDLING
                    </td>
                    <td style="border-bottom:1px solid; width:204px; padding-top:10px; padding-bottom:10px;">
                    $0.00
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom:1px solid; width:204px; padding-top:10px; padding-bottom:10px;">
                    PAYMENT RECEIVED
                    </td>
                    <td style="border-bottom:1px solid; width:204px; padding-top:10px; padding-bottom:10px;">
					<?php echo "$".$total; ?>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom:1px solid; width:204px; padding-top:10px; padding-bottom:10px;">
                    AMOUNT DUE
                    </td>
                    <td style="border-bottom:1px solid; width:204px; padding-top:10px; padding-bottom:10px;">
                    $0.00
                    </td>
                </tr>
            </table>
        </div>
    </div>
    
</body>
</html>
<?php
}
?>