<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '-1');

function makeCompanyPretty($inc_company)
{
	$inc_company = strtolower($inc_company);
	if($inc_company == "dermaliv")
	{
		$company = "Dermaliv";
	}
	else if($inc_company == "synergylife")
	{
		$company = "Synergy Life";
	}
	else if($inc_company == "baylieskin")
	{
		$company = "Baylie Skin";
	}
	else if($inc_company == "camileskin")
	{
		$company = "Camile Skin";
	}
	else if($inc_company == "biohealthresearch")
	{
		$company = "BioHealth Research";
	}
	else
	{
		$company = $inc_company;
	}
	return $company;
}
function getCompanyLogo($inc_company)
{
	if($inc_company == "dermaliv")
	{
		$logo 	= "images/logo.png";
	}
	else if($inc_company == "synergylife")
	{
		$logo 	= "images/SL_logo.png";
	}
	else if($inc_company == "baylieskin")
	{
		$logo 	= "images/baylie.png";
	}
	else if($inc_company == "camileskin")
	{
		$logo 	= "images/camile.png";
	}
	else if($inc_company == "biohealthresearch")
	{
		$logo 	= "images/biohealth.png";
	}
	else
	{
		$logo 	= "images/background_blue.png";
	}
	return $logo;
}
function getCompanyURL($inc_company)
{
	if($inc_company == "dermaliv")
	{
		$url 	= "http://dermaliv.com";
	}
	else if($inc_company == "synergylife")
	{
		$url 	= "http://synergylife.com";
	}
	else if($inc_company == "baylieskin")
	{
		$url 	= "http://baylieskin.com";
	}
	else if($inc_company == "camileskin")
	{
		$url 	= "http://camileskin.com";
	}
	else if($inc_company == "biohealthresearch")
	{
		$url 	= "http://biohealthresearch.com";
	}
	else
	{
		$url 	= "http://www.GOOGLE.com";
	}
	return $url;
}
function getCompanyPhone($inc_company)
{
	/*
	 * Baylie 9087606322
Camile 9087606322
Bourke 8552611345
Apolline 8553207552
Caressas 8555251468
Celesse 8557249802
Dania 8557587006
Devan 8558090810
Elaine 8558565329

change baylie to 8552309459
camile to 8558565329
	 */
	if($inc_company == "dermaliv")
	{
		$url 	= "9087606322";
	}
	else if($inc_company == "synergylife")
	{
		$url 	= "9087606322";
	}
	else if($inc_company == "baylieskin")
	{
		$url 	= "9087606322";
	}
	else if($inc_company == "camileskin")
	{
		$url 	= "9087606322";
	}
	else if($inc_company == "biohealthresearch")
	{
		$url 	= "9087606322";
	}
	else
	{
		$url 	= "1234567890";
	}
	return $url;
}
function getLLData($first6, $last4)
{
	$url = "https://www.jcsecured.com/admin/membership.php";

	$data = array(
		"campaign_id" => "all",
		"product_id" => "all",
		"search_type" => "all", //this is the line that is supposed to make it "AND" instead of "OR" (OR is called "ANY" instead of "ALL")
//		"criteria" => "all",
		"username" => "biohealth1",
		//"criteria" => "first_4_cc=1234,last_4_cc=3413",
		"criteria" => "first_6_cc=$first6,last_4_cc=$last4",
		//"criteria" => "first_name=dav*,all_refunds,state=mi,email=*gmail*,billing_cycle=1",
		//"criteria" => "first_4_cc=$first4",

		"password" => "7s63VfusbHVFuq",
		"method" => "order_find",
		"start_date" => "01/01/2014",
		"end_date" => date('m/d/Y',strtotime("today")),
		"return_type" => "order_view",
		//"last_4_cc" => "$last4",
		//"first_4_cc" => "$first4",
	);
	//var_dump($data);
	//$data = array_merge_recursive($data, $data_in);
	$curlSession = curl_init();
	curl_setopt($curlSession, CURLOPT_URL, $url);
	curl_setopt($curlSession, CURLOPT_HEADER, 0);
	curl_setopt($curlSession, CURLOPT_POST, 1);
	curl_setopt($curlSession, CURLOPT_POSTFIELDS, $data);
	curl_setopt($curlSession, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($curlSession, CURLOPT_TIMEOUT,5000);
	curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($curlSession, CURLOPT_SSL_VERIFYHOST, 2);
	$rawresponse = curl_exec($curlSession);

	$rawresponse = trim($rawresponse);
	$rawresponse = preg_replace('[A-Za-z0-9();{}\[\]\\!@#$%^&*()_+\-=.,<>:/?\'"]', '', $rawresponse);

	$rawresponse = utf8_decode($rawresponse);
	$output = array();
	parse_str($rawresponse,$output);
	curl_close ($curlSession);

	return $output;
}
function initialize_db($mysqli, $username="biohealth1", $password="7s63VfusbHVFuq")
{
	$mysqli = new mysqli("localhost",$username,$password,'limelight');
	// Check connection
	if (mysqli_connect_errno())
	{
		//printf("Connect failed: %s\n", mysqli_connect_error());
		//log_error(mysqli_connect_error(), "error".getmypid());
		exit();
	}
	return $mysqli;
}
/*
 * Filter some stuff and explodify
 */
function explodify($string)
{
	$string = str_replace('    ', ',', $string); //TAB

	$string = str_replace('
', ',', $string); //NOT A SPACE! (new line character)
	$string = str_replace(' ', ',',$string); //a space
	$myArray = explode(',', $string);
	return $myArray;
}
/*
 * Step 1 of fixing the LL fields:
 */
function fixData($output)
{
	if($output['response_code'] == '100')
	{
		if(isset($output['data']))
		{
			$output['data'] = str_replace("\t",'',$output['data']);
			/*
			 * The following crazy pregreplace thing removes all non-alphanumeric characters
			 *
			 * and allows php's json_decode function to work properly. Apparently limelight was giving us non-utf8 stuff?
			 */
			$replace = '/[^A-Za-z0-9\s!@#$%\^&*()\[\]\{\}\"\\\';:,<\.>\?\`\~\\\|\/\+\-\_\=*]/';
			$output['data'] = preg_replace($replace, '', $output['data'], -1, $count);

			$orders = json_decode($output['data'],true);

			//if the json parse didn't work =(
			if(json_last_error() == 5)
			{
				//var_dump($output['data']);
				echo "Bad data in some way:

                  ".$output['data']."

                ".date('Y-m-d H:i:s')."

                ".json_last_error_msg();

				var_dump(json_last_error_msg());
				exit();
			}
			$tracking="";
			foreach($orders as $order_id=>$order_data)
			{
				$all_orders[$order_id] = array();
				$all_orders[$order_id]["order_id"] = $order_id; //always have order_id
				foreach($order_data as $field=>$value)
				{
					if(strpos($field,'[') !== false)
					{
						$arr = array();
						parse_str($field,$arr);
						fixLLFields($all_orders[$order_id],$arr,$value);
					}
					else
					{
						/*
						 * Take previous tracking # if this one is blank
						 */
						if($field == "tracking_number")
						{
							if($value =="")
							{
								$all_orders[$order_id][$field] = $tracking;
							}
							else
							{
								$all_orders[$order_id][$field] = $value;
								$tracking = $value;
							}
						}
						else
						{
							$all_orders[$order_id][$field] = $value;
						}
						if($field == "time_stamp")
						{
							$all_orders[$order_id]["originally_ordered"] = $value;
						}
					}
				}
			}
			return $all_orders;
		}
	}
	else
	{
		echo "error fixing LL data";
		die();
	}
}
/*
 * Jeremy's crazy-awesome LL data repair thing
 */
function fixLLFields(&$order_array_nest,$fields,$final_value)
{
	foreach($fields as $key=>$value)
	{
		if(is_array($value))
		{
			if(!isset($order_array_nest[$key]))
			{
				$order_array_nest[$key] = array();
			}
			fixLLFields($order_array_nest[$key],$value,$final_value);
			//var_dump($order_array_nest[$key]);
		}
		else
		{
			$order_array_nest[$key] = $final_value;
		}
		return;
	}
}
function printBankJS()
{
	?>
	<script type="text/javascript">
	function showMerchant()
	{
		var merchant = document.getElementById("merchant").value;
		var merchant_text = document.getElementById("merchant_text");
		if(merchant == "1363")
		{
			merchant_text.innerHTML = "Choice Merchant Reuven Rebill Derma";
		}
		else if(merchant == "7264")
		{
			merchant_text.innerHTML = "VPS";  //unknown
		}
		else if(merchant == "9860")
		{
			merchant_text.innerHTML = "Cardworks - Derma - Baylie";
		}
		else if(merchant =="0149")
		{
			merchant_text.innerHTML = "Select Bank";
		}
		else if(merchant =="0461")
		{
			merchant_text.innerHTML = "TrustOne";
		}
		else if(merchant =="8585")
		{
			merchant_text.innerHTML = "Vision Payments";
		}
		else if(merchant =="0629")
		{
			merchant_text.innerHTML = "Select Bank";
		}
		else if(merchant =="7266")
		{
			merchant_text.innerHTML = "BUSA";
		}
		else if(merchant =="7559")
		{
			merchant_text.innerHTML = "SignaPay";
		}
		else if(merchant =="9130")
		{
			merchant_text.innerHTML = "BUSA";
		}
		else if(merchant =="5627")
		{
			merchant_text.innerHTML = "Choice Merchant";
		}
		else if(merchant =="0001")
		{
			merchant_text.innerHTML = "NMC";
		}
		else if(merchant =="3294")
		{
			merchant_text.innerHTML = "CMS";
		}
		else if(merchant =="7266")
		{
			merchant_text.innerHTML = "Busa";
		}
		else if(merchant =="8076")
		{
			merchant_text.innerHTML = "PowerPay";
		}
		else if(merchant =="3604")
		{
			merchant_text.innerHTML = "Offspring";
		}
		else if(merchant =="4324")
		{
			merchant_text.innerHTML = "Meritus";
		}
		else if(merchant =="3289")
		{
			merchant_text.innerHTML = "Meritus RB Synergy";
		}
		else if(merchant =="1639")
		{
			merchant_text.innerHTML = "AVPS";
		}
		else if(merchant =="5883")
		{
			merchant_text.innerHTML = "GMA";
		}
		else if(merchant =="7885")
		{
			merchant_text.innerHTML = "Humbolt";
		}
		else if(merchant =="6140")
		{
			merchant_text.innerHTML = "NDMS";
		}
		else if(merchant =="4728")
		{
			merchant_text.innerHTML = "Vantiv";
		}
		else if(merchant =="0499")
		{
			merchant_text.innerHTML = "Payscout FDR";
		}
		else if(merchant =="5887")
		{
			merchant_text.innerHTML = "Payscout Rebill";
		}
		else if(merchant =="7757")
		{
			merchant_text.innerHTML = "Choice Rebill";
		}
		else if(merchant =="5724")
		{
			merchant_text.innerHTML = "Elavon";
		}
		else if(merchant =="7988")
		{
			merchant_text.innerHTML = "Elavon";
		}
		else if(merchant =="0838")
		{
			merchant_text.innerHTML = "Amex (Ryans old acct)";
		}
		else if(merchant =="2408")
		{
			merchant_text.innerHTML = "Amex (old)";
		}
		else if(merchant =="9413")
		{
			merchant_text.innerHTML = "Amex (updated)";
		}
		else if(merchant =="6882")
		{
			merchant_text.innerHTML = "Amex";
		}
		else if(merchant =="0131")
		{
			merchant_text.innerHTML = "Amex";
		}
		else if(merchant =="9495")
		{
			merchant_text.innerHTML = "Amex";
		}
		else if(merchant =="1957")
		{
			merchant_text.innerHTML = "Choice - Rebill - Camile";
		}
		else if(merchant =="0958")
		{
			merchant_text.innerHTML = "Choice - RB - Baylie";
		}
		else if(merchant =="9413")
		{
			merchant_text.innerHTML = "Amex";
		}
		else if(merchant =="8080")
		{
			merchant_text.innerHTML = "PayAlliance - RB - Derma - Cynergy - Camile";
		}
		else if(merchant =="8072")
		{
			merchant_text.innerHTML = "PayAlliance - RB - Derma - Cynergy - Baylie";
		}
		else if(merchant =="9860")
		{
			merchant_text.innerHTML = "PayAlliance - Derma - HBMS - Camile";
		}
		else if(merchant =="5883")
		{
			merchant_text.innerHTML = "PayAlliance - Derma - HBMS - Camile";
		}
		else if(merchant =="6881")
		{
			merchant_text.innerHTML = "PayAlliance - Derma - HBMS - Baylie";
		}
		else if(merchant =="8880")
		{
			merchant_text.innerHTML = "NMA - Derma - Baylie";
		}
		else if(merchant =="5714")
		{
			merchant_text.innerHTML = "Meritus - Derma - Devan";
		}
		else if(merchant =="8080")
		{
			merchant_text.innerHTML = "PayAlliance - RB - Derma - Cynergy - Elaine";
		}
		else if(merchant =="0958")
		{
			merchant_text.innerHTML = "Choice - Bourke";
		}
		else
		{
			merchant_text.innerHTML = "";
		}
	}
</script>
<?php
}
?>