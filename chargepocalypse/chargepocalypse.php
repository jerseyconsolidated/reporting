<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require ('../includes/RayGun/RayGun.php');
require ('chargepocalypse_functions.php');
require ('invoice.php');
require ('chargepocalypse_TOS.php');
require ('chargepocalypse_email_template.php');
require ('cover_letter.php');
require ('notes.php');


ini_set('memory_limit', '-1');


global $mysqli;
$mysqli = initialize_db('limelight');

/*
 * Check if this page was posted or not:
 */
if(
	isset($_POST['first_account']) &&
	isset($_POST['last_account']) &&
	isset($_POST['product_type']) &&
	isset($_POST['merchant']) &&
	isset($_POST['cb_reason']) &&
	isset($_POST['case']) &&
	isset($_POST['company']) &&
	! isset($_POST['order_id'])
	//isset($_POST['user'])
)
{
	$first6 	= $_POST['first_account'];
	$last4  	= $_POST['last_account'];
	$type   	= $_POST['product_type'];
	$merchant	= $_POST['merchant'];
	$cb_reason	= $_POST['cb_reason'];
	$case 		= $_POST['case'];
	$company 	= $_POST['company'];
	$notes		= $_POST['notes'];

	//$user 		= $_POST['user'];

	$output = getLLData($first6, $last4);
	$all_orders = fixData($output);


	/*
	 * Well, now that our data is fixed and ready to go
	 */
	echo '<form method="post" id="form2">';
	echo "Which Order? <table border=1>";
	/*
	 * Print fields:
	 */
	$order_fields="order_id,shipping_first_name,shipping_last_name,order_total,cc_number,originally_ordered";
	$order_fields_arr = explode(',', $order_fields);

	//var_dump($all_orders);
	//die();
	/*
	$values = array();
	foreach($all_orders as $id=>$order)
	{
		foreach ($order_fields_arr as $field)
		{
			$values[] =$order[$field];
		}
	}
	*/

	//var_dump($values);
	//die();

	echo "<th></th>";
	foreach($all_orders as $id=>$order)
	{
		foreach($order as $i=>$this_order)
		{
			if(in_array($i, $order_fields_arr))
			{
				echo "<th>$i</th>";
			}
		}
		break;
	}
	/*
	 * Print all data:
	 */

	foreach($all_orders as $id=>$order)
	{
		$cb_order = $order;

		echo "<tr>";
		foreach($order as $i=>$this_order)
		{
			if(in_array($i, $order_fields_arr))
			{
				if(is_array($this_order))
				{
					echo "<td>";
					foreach($this_order as $individual)
					{
						echo $individual."<br>";
					}
					echo "</td>";
				}
				else
				{
					if($i == "order_id")
					{
						echo '<td><input type="radio" name="order_id" value="'.$this_order.'"></td>';
					}
					echo "<td>$this_order</td>";
				}
			}
		}
		echo "</tr>";
	}
	?>
	<INPUT TYPE="hidden" NAME="output" VALUE="<?php echo base64_encode(serialize($all_orders)); ?>">
	<input type="hidden" name="case" value="<?php echo $case;?>">
	<input type="hidden" name="reason" value="<?php echo $cb_reason;?>">
	<input type="hidden" name="merchant" value="<?php echo $merchant;?>">
	<input type="hidden" name="type" value="<?php echo $type;?>">
	<input type="hidden" name="company" value="<?php echo $company;?>">
	<input type="hidden" name="notes" value="<?php echo $notes;?>">


	<?php
	echo '</table><input type="submit" value="Submit"></form>';

	//var_dump($cb_order);
	//die();
	//doSheetsOutput($case,$cb_order,$merchant,$user, $first6);
}
else if(isset($_POST['order_id']))
{
	//order_id is set
	$order = $_POST['order_id'];
	$all_orders = unserialize(base64_decode($_POST["output"]));

	$cb_order = $all_orders[$order];
	$cb_reason = $_POST['reason'];
	$case = $_POST['case'];
	$merchant = $_POST['merchant'];
	$type = $_POST['type'];
	$company = $_POST['company'];
	$notes = $_POST['notes'];

	printScreen($cb_reason, $case, $cb_order, $merchant, $type, $company, $notes);
}
else
{
?>
	<form method="post" id="form">
	<table border=0>
	<tr><td>
	Merchant # (last 4)
	</td><td>
	<input type="text" id="merchant" name="merchant" placeholder="Insert Merchant #" onkeyup=showMerchant(); maxlength="4">
	</td><td>
	<text id="merchant_text" name="merchant_text"></text>
	</td></tr>
	<tr><td>
	Case #
	</td><td>
	<input type="text" id="case" name="case" placeholder="Insert Case #">
	<tr><td>
	CC #
	</td><td>
	<input type="text" id="first_account" name="first_account" placeholder="First 6 of CC #" onkeyup=showCC(); maxlength="6">
	<br>
	<input type="text" id="last_account" name="last_account" placeholder="Last 4 of CC #" maxlength="4">
	</td><td><text id="account_text" name="account_text"></text>
	</td></tr><tr><td>
	<!-- Chargeback Person
	</td><td>
	<input type="text" id="user" name="user" placeholder="Your Name" maxlength="25">
	</td></tr><tr><td>
	!-->
	<input type="radio" name="product_type" value="face" checked>Face
	<input type="radio" name="product_type" value="eye">Eye
	</td></tr><tr><td>
	<select name="cb_reason">
		<option value="refund_issued">Refund Issued</option>
		<option value="partial_refund_issued">Partial Refund Issued</option>
		<option value="no_auth">No Cardholder Authorization</option>
		<option value="cancelled_current">Cancelled Current Transaction</option>
		<option value="cancelled_recurring">Cancelled Recurring</option>
	</select>
	<br>
	<select name="company">
		<option value="dermaliv">Dermaliv</option>
		<option value="synergylife">Synergy Life</option>
		<option value="baylieskin">Baylie Skin</option>
		<option value="camileskin">Camile Skin</option>
		<option value="biohealthresearch">BioHealth Research</option>
	</select>
	</td><td>
	<textarea name="notes" placeholder="Put notes here..." maxlength="500"></textarea>
	</td></tr><tr><td>
	<input type="button" value="Submit" onclick="check()">
	</td></tr></table>
	<p id="results"></p>
	</form>


<script type="text/javascript">
	function check()
	{
		var merchant = document.getElementById("merchant");
		var first_account= document.getElementById("first_account");
		var last_account = document.getElementById("last_account");
		var case_number = document.getElementById("case");

		/*
		 * Check that the data that is left is not empty / incomplete
		 */
		if(case_number.value == "")
		{
			alert("Problem with Case #");
			return;
		}
		if(merchant.value == "" || merchant.value.length != 4)
		{
			alert("Problem with Merchant #");
			return;
		}
		if(first_account.value =="" || first_account.value.length != 6)
		{
			alert("Problem with first 6 digits");
			return;
		}
		if(last_account.value =="" || last_account.value.length != 4)
		{
			alert("Problem with last 4 digits");
			return;
		}
		/*
		 * checking is done, time to post it.
		 */
		document.forms["form"].submit();
	}
</script>
<script type="text/javascript">
	function showCC()
	{
		var account = document.getElementById("first_account").value;
		var account_text = document.getElementById("account_text");
		if(account.charAt(0) == "3" && (account.charAt(1) == "4" || account.charAt(1) == "7"))
		{
			account_text.innerHTML = "American Express";
		}
		else if(account.charAt(0) == "3" &! (account.charAt(1) == "4" || account.charAt(1) == "7"))
		{
			account_text.innerHTML = "Diner's Club";
		}
		else if(account.charAt(0) == "4")
		{
			account_text.innerHTML = "VISA";
		}
		else if(account.charAt(0) == "5")
		{
			account_text.innerHTML = "MasterCard";
		}
		else if(account.charAt(0) =="6")
		{
			account_text.innerHTML = "Discover";
		}
		else
		{
			account_text.innerHTML = "";
		}
	}
</script>
<?php
	printBankJS();
}
/*
 * Function stuff:
 */
function printScreen($reason, $case, $data, $merchant, $type, $company, $notes)
{
	/*
	 * Types:
	 * refund_issued
	 * no_auth
	 */
	doCoverLetter($case,$data,$type,$company,$reason, $merchant, $notes);
	//doInvoice($case,$data,$type,$company); //-> Done
	//doLetter($case,$data,$type,$company,$reason); // -> Done, but probably not used in place of "CoverLetter" now.
	//doNotes($case,$data); //-> Done
	//doPIPL($data); // -> Done
	printEmail($company, $data);
	//printNotes($notes); // -> Done, but probably not used in place of having notes on the cover page.
	//printfullTOS($company); // -> Done

	//doUPS($data["tracking_number"]); -> Done, unless we want to do the UPS API stuff
	//doIP4($data["ip_address"]); -> Done, but we should probably find a different IP source thing
	echo "
<script type=\"text/javascript\">
		function doWindows()
		{
			//openUPS();
			//openIP();
			openPIPL();
		}
	</script>
<body onLoad='doWindows();'>";

}
function doLetter($case,$data,$type,$company,$reason)
{
	echo '<div class="wrapper">';
	if($reason== "refund_issued")
	{
		RefundIssued($case, $data, $type, $company);
	}
	else if($reason == "partial_refund_issued")
	{
		PartialRefundIssued($case, $data, $type, $company);
	}
	else if($reason == "no_auth")
	{
		NoAuth($case, $data, $type, $company);
	}
	else if($reason == "cancelled_current")
	{
		CancelledCurrent($case, $data, $type, $company);
	}
	else if($reason == "cancelled_recurring")
	{
		CancelledRecurring($case, $data, $type, $company);
	}
	echo '</div>';
}
function doSheetsOutput($case, $data, $merchant,$user, $first6)
{
	/*
	 * CSV for 'main' looks like this:
	 * $MID,derma,$sale_date,$case,$order_number,$first_6,$name,$price,$reason,TODAY,paper,,$user
	 */
	$sale = $data["order_total"];
	$saledate = $data["time_stamp"];
	$order = $data["order_id"];
	$name = $data["billing_first_name"]." ".$data["billing_last_name"];
	$dt = date("m-d-Y H:i:s", time());
	echo "$merchant	derma	$saledate	$case	$order	$first6	$name	$sale	$case	$dt	paper		$user";

}
function printNotes($notes)
{
	if(! $notes =="")
	{
		?>
		<div class="wrapper">
		<h1>Order Notes</h1>
		<?php echo $notes; ?>
		</div>
		<?php
	}
}
function doIP($ip)
{
	echo '
	<div class="wrapper">
	<object data=http://whois.domaintools.com/'.$ip.' width="1200" height="1000" sandbox="">
	<embed src=http://whois.domaintools.com/'.$ip.' width="1200" height="1000">
	</embed> Error: Embedded data could not be displayed. </object>
	</div>
		';
}
function doIP2($ip)
{
	?>
	<script type="text/javascript">
		function openIP()
		{
			window.open('http://whois.domaintools.com/<?php echo $ip;?>','ip','height=1200,width=1000');
		}
	</script>

	<?php
}
function doIP4($ip)
{
	echo '
	<div class="wrapper" style="width:100%; height:90%; align-self: center;">
        <iframe style="width:100%; height:112%" sandbox="allow-same-origin allow-scripts allow-popups allow-forms" src="http://whatismyipaddress.com/ip/'.$ip.'" name = "ipframe" id="ipframe"></iframe>
    </div>
		';
}
function doUPS($tracking)
{
	echo '
	<div class="wrapper">
	<object data=https://tools.usps.com/go/TrackConfirmAction?tLabels='.$tracking.' width="1200" height="1000">
	<embed src=https://tools.usps.com/go/TrackConfirmAction?tLabels='.$tracking.' width="1200" height="1000">
	</embed> Error: Embedded data could not be displayed. </object>
	</div>
		';
}
function doUPS2($tracking)
{
	?>
	<script type="text/javascript">
		function openUPS()
		{
			window.open('https://tools.usps.com/go/TrackConfirmAction?tLabels=<?php echo $tracking;?>','ups','height=1200,width=1000');
		}
	</script>
<?php
}
function doPIPL($data)
{
	$name = $data["billing_first_name"]."+".$data["billing_last_name"];
	$address = $location = $data["shipping_city"]."%2C+".$data["shipping_state"];
	$address = str_replace(" ", "+", $address);
?>
	<script type="text/javascript">
	function openPIPL()
	{
		window.open('https://pipl.com/search/?q=<?php echo $name;?>&l=<?php echo $address;?>&sloc=&in=5','pipl','height=1200,width=1000');
	}
	</script>
<?php
}
function doDermaTOS()
{
	echo '
		<div>
		<object data=http://dermaliv.com/process/tos.html width="90%" height="90%">
		<embed src=http://dermaliv.com/process/tos.html width="90%" height="90%">
		</embed> Error: Embedded data could not be displayed. </object>
		</div>
	';
}


?>