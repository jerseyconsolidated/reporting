<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '-1');


function printHeader($case, $data)
{
	echo "<p>";
	echo "Case: $case <br>";
	$ccxp = substr($data["cc_expires"], 0,2)."/".substr($data["cc_expires"],2,2);
	echo "Credit Card Expiration Date: ".$ccxp."<br>";
	echo "</p>";
}
function printP1($type, $data, $company)
{
	$url  		= getCompanyURL($company);
	$company = makeCompanyPretty($company);
	/*
	if($type == "face")
	{
		$product = "Face Serum";
		$url = '"http://dermaliv.com/anti-aging-serum"';
	}
	else if($type == "eye")
	{
		$product = "Eye Cream";
		$url = '"http://dermaliv.com/t1/"';
	}
	*/
	echo "<p>";
	$name = $data["billing_first_name"]." ".$data["billing_last_name"];
	//echo ;
	$oo = strtotime($data["originally_ordered"]);
	$rd = strtotime($data["recurring_date"]);
	$hd = strtotime($data["hold_date"]);
	$dt = date("m-d-Y H:i:s", $oo);
	$dt = explode(" ",$dt);
	//echo $dt[0]." on ".$dt[1]."is time";
	$date = $dt[0];//." at ".$dt[1];
	$dt = date("m-d-Y H:i:s", $rd);
	$dt = explode(" ",$dt);
	//echo $dt[0]." on ".$dt[1]."is time";
	$RBdate = $dt[0];//." at ".$dt[1];
	$ip = $data["ip_address"];
	$order_id = $data["order_id"];

	$product_name = "";
	$product_price = 0;
	foreach($data["products"] as $product)
	{
		$product_name .= $product["name"];
		$product_price += $product["price"];
	}

	echo "$name (Customer) placed an order (Order #$order_id) on our website for a supply of $product_name. The order was placed on $date. This order was for a trial subscription of $product_name which was initially priced at $$product_price.";

/*
	if($rd < $hd)
	{
		echo "$company is a Limited Liability Company (LLC) engaged in selling skin care products including $product. $name ordered 1 auto-reflling bottle of $product on our website $url on $date from the IP address $ip (Screenshot Attached) and was rebilled on $RBdate";
	}
	else
	{
		echo "$company is a Limited Liability Company (LLC) engaged in selling skin care products including $product. $name ordered 1 auto-reflling bottle of $product on our website $url on $date from the IP address $ip (Screenshot Attached).";
	}
*/
	echo "</p>";
}
function printGeoIP($data)
{
	$name = $data["billing_first_name"]." ".$data["billing_last_name"];
	echo "<p>";
	$location = $data["shipping_city"].", ".$data["shipping_state"]." ".$data["shipping_country"];
	echo "Geo IP location locates this IP address to their location in $location, which indicates that this order was placed by $name.";
	echo "</p>";
}
function printSpokeo($data)
{
	$name = $data["billing_first_name"]." ".$data["billing_last_name"];
	$location = $data["shipping_city"].", ".$data["shipping_state"]." ".$data["shipping_country"];
	echo "<p>";
	echo "Spokeo People Search also shows $name at the address given in ".$data["shipping_state"].".";
	echo "</p>";
}
function printPIPL($data)
{
	$name = $data["billing_first_name"]." ".$data["billing_last_name"];
	$location = $data["shipping_city"].", ".$data["shipping_state"]." ".$data["shipping_country"];
	echo "<p>";
	echo "PIPL Address Verification software also shows $name at the address given in ".$data["shipping_state"].".";
	echo "</p>";
}
function printTracking($data)
{
	$tracking = $data["tracking_number"];
	echo "This order also shows as delivered with USPS tracking number: $tracking. <br> We are attaching a screenshot of the transaction, confirmed delivery time, and customer's invoice for reference.";
}
function printCancellation($data, $contacted)
{
	echo "<p>";
	$name = $data["billing_first_name"]." ".$data["billing_last_name"];
	if(! $contacted)
	{
		echo "There is no record of this customer contacting us to cancel by either phone or email. Therefore, this is not a cancelled recurring transaction and we ask that this case be reversed. Below is a link to our full Terms and Conditions including our full return policy.<br><br>http://dermaliv.com/process/tos.html";
	}
	else
	{
		$dt = date("m-d-Y H:i:s", strtotime($data["hold_date"]));
		$dt = explode(" ",$dt);
		//echo $dt[0]." on ".$dt[1]."is time";
		$holddate = $dt[0];//." at ".$dt[1];
		echo "The customer contacted us on $holddate about a cancellation. Since $name cancelled after the rebill date and the product has not been returned, we ask that this case be reversed. Below is a link to our full Terms and Conditions including our full return policy. <br><br>http://dermaliv.com/process/tos.html";
		echo "</p>";
	}
}
function printRefundIssued($data, $partial)
{
	$name = $data["billing_first_name"]." ".$data["billing_last_name"];
	/*
	 * If partial = 1, it is a partial refund. Else, it is a full refund. 
	 */
	if($partial)
	{
		echo "<p>";
		echo "PARTIAL REFUND ALREADY ISSUED: <br>";
		$date = date("m-d-Y", strtotime($data["hold_date"]));
		//$dt = explode(" ",$dt);
		//$date = $dt[0];//." at ".$dt[1];
		$refund = $data["amount_refunded_to_date"];
		echo "This customer called on $date to inquire about this charge. $name was given the choice of either taking a full refund in exchange for returning the product or a partial refund to keep the product.";
		echo "</p><p>";
		echo "<p>";
		echo "$name accepted a partial refund of \$$refund on $date.";
		echo "</p><p>";
		echo "Since this customer has already been refunded we request this case be reversed.";
		echo "</p>";
	}
	else
	{
		echo "<p>";
		echo "REFUND ALREADY ISSUED: <br>";
		$date = date("m-d-Y", strtotime($data["hold_date"]));
		//$dt = explode(" ",$dt);
		//$date = $dt[0];//." at ".$dt[1];
		$refund = $data["amount_refunded_to_date"];
		echo "We also show that this customer was already refunded on $date for \$$refund.";
		echo "</p><p>";
		echo "Since this customer has already been refunded we request this case be reversed.";
		echo "</p>";
	}
	
}
function printNotResponsible()
{
	echo "<p>";
	echo "Please note we are not responsible for return shipping costs.";
	echo "</p>";
}

function printTOS($inc_company)
{
	$company = makeCompanyPretty($inc_company);
	$phone = getCompanyPhone($inc_company);
	echo "<p>";
	echo "When placing an order on our website, the customer has to check a box that states they agree to the Terms of Service of the website. The terms and conditions can be found online at http://$inc_company.com/process/tos.html (also attached to this document). A relevant excerpt: ";
	echo "</p><p>";
	echo "<strong>INDEMNITY</strong>: You agree to indemnify, defend, and hold harmless Us and our affiliates, subsidiaries, officers, directors, employees, agents, licensors and licensees from any damages, liabilities, costs, and expenses, including reasonable attorneys’ fees, on account of any claim, suit, action, demand or proceeding made or brought against any such party, or on account of the investigation, defense or settlement thereof, arising in connection with your use of this website. ";
	echo "</p><p>";
	/*
	 * Warning, this next thing has some phone numbers in it and i don't know what they should be
	 */
	echo "<strong>Recurring Billing</strong>: Access to the Service may consist of an initial period, where you can try a product risk free, followed by recurring periodic charges as agreed to by you. By entering into this Agreement, you acknowledge that your risk free trial has an initial shipping and processing fee and recurring payment feature and you accept responsibility for all recurring charges prior to cancellation. To change or cancel your Service at any time, call our customer service representatives at $phone. Your products will be automatically shipped per the offer you originally accepted but you can change a shipment or product at any time by calling us at $phone. Your non-termination or continued use of the Service reaffirms that $company is authorized to charge your Payment Method. $company may submit those charges for payment and you will be responsible for such charges. ".strtoupper($company)." MAY SUBMIT PERIODIC CHARGES (E.G., MONTHLY) WITHOUT FURTHER AUTHORIZATION FROM YOU, UNTIL YOU AFFIRMATIVELY CANCEL YOUR SERVICE OR NOTIFY US THAT YOU WISH TO CHANGE YOUR PAYMENT METHOD. SUCH NOTICE WILL NOT AFFECT CHARGES SUBMITTED BEFORE ".strtoupper($company)." REASONABLY COULD ACT TO TERMINATE YOUR AUTHORIZATION OR CHANGE YOUR PAYMENT METHOD.";
	echo "</p>";
}
function printFooter($inc_company)
{
	$company = makeCompanyPretty($inc_company);
	echo "<p>";
	echo "Please let us know if you have any additional questions.";
	echo "</p><p>";
	echo "Thank you, <br> $company";
}

function CancelledRecurring($case, $data, $type, $company)
{
	//printHeader($case, $data);
	printP1($type, $data, $company);
	printGeoIP($data);
	printPIPL($data);
	printTracking($data);
	printCancellation($data, 0);
	printNotResponsible();
}
function CancelledCurrent($case, $data, $type, $company)
{
	//printHeader($case, $data);
	printP1($type, $data, $company);
	printGeoIP($data);
	printSpokeo($data);
	printTracking($data);
	printCancellation($data, 1);
	printNotResponsible();
}
function NoAuth($case, $data, $type, $company)
{
	//printHeader($case, $data);
	printP1($type, $data, $company);
	printGeoIP($data);
	printSpokeo($data);
	printTracking($data);
	echo "<p>";
	echo "Since this product has not yet been returned they are not eligible for a chargeback. Below is a link to our full Terms and Conditions including our full return policy.";
	echo "</p><p>";
	echo "http://dermaliv.com/process/tos.html";
	echo "</p>";
	printNotResponsible();
}
function PartialRefundIssued($case, $data, $type, $company)
{
	//printHeader($case, $data);
	printP1($type, $data, $company);
	printTracking($data);
	printRefundIssued($data, 1);
}
function RefundIssued($case, $data, $type, $company)
{
	//printHeader($case, $data);
	printP1($type, $data, $company);
	printTracking($data);
	printRefundIssued($data, 0);
}

?>