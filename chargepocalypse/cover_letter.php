<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '-1');
require ('chargepocalypse_letters.php');

function doCoverLetter($case,$data,$type,$inc_company,$reason, $merchant, $notes)
{
	$company = makeCompanyPretty($inc_company);
	$logo = getCompanyLogo($inc_company);
	$phone = getCompanyPhone($inc_company);
	?>


	<html>
	<head>
		<meta name="description" content="Invoice">
		<meta name="author" content="Dermaliv">
		<meta name="viewport" content="width=device-width">
		<link href="css/style.css" rel="stylesheet" />
	</head>
	<body>
	<div class="wrapper">
		<div id="idcase">
			<p>Merchant ID: <?php echo $merchant; ?></p>
				<p>Case #: <?php echo $case; ?></p>
				<?php $ccxp = substr($data["cc_expires"], 0,2)."/".substr($data["cc_expires"],2,2);
				echo "<p>CC Expiration: ".$ccxp."</p>"; ?>
			</p>
		</div>
		<div id="coverletterlogo">
			<img src="<?php echo $logo; ?>"/>
		</div>
		<div id="ouraddress">
			<p><?php echo $company?></p>
			<p>84 Washington st, 3rd floor West, NJ 07030</p>
			<p>Tel <?php echo $phone; ?></p>
		</div>

		<div class="clearfix">
		</div>
		<div id="customer_letter">
<?php
			if($reason== "refund_issued")
			{
				RefundIssued($case, $data, $type, $inc_company);
			}
			else if($reason == "partial_refund_issued")
			{
				PartialRefundIssued($case, $data, $type, $inc_company);
			}
			else if($reason == "no_auth")
			{
				NoAuth($case, $data, $type, $inc_company);
			}
			else if($reason == "cancelled_current")
			{
				CancelledCurrent($case, $data, $type, $inc_company);
			}
			else if($reason == "cancelled_recurring")
			{
				CancelledRecurring($case, $data, $type, $inc_company);
			}
			printTOS($inc_company);
			if($notes != "")
			{
				echo "<br><br>";
				echo "<h3>Order Notes</h3>";
				echo "<p>$notes</p>";
			}
			printFooter($company);
	?>
		</div>
	</div>

	</body>
	</html>



<?php
}
?>