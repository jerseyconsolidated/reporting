<?php
/*ini_set('xdebug.collect_params', '4');
ini_set('xdebug.collect_vars', 'on');
ini_set('xdebug.dump_globals', 'on');
ini_set('xdebug.show_local_vars', 'on');
ini_set('xdebug.overload_var_dump', '0');
ini_set('xdebug.dump.COOKIE', '*');
ini_set('xdebug.dump.GLOBALS', '*');
ini_set('xdebug.dump.FILES', '*');
ini_set('xdebug.dump.GET', '*');
ini_set('xdebug.dump.POST', '*');
ini_set('xdebug.dump.REQUEST', '*');
ini_set('xdebug.dump.SESSION', '*');
//ini_set('xdebug.dump.SERVER', '*');

xdebug_debug_zval();
xdebug_get_headers();
xdebug_print_function_stack();
xdebug_get_function_stack();
xdebug_call_line();
xdebug_get_declared_vars();
xdebug_dump_superglobals();
var_dump(xdebug_get_declared_vars());
var_dump(xdebug_get_function_stack());*/

require ('C:\inetpub\wwwroot\reports\includes\general_functions.php');
$mysqli = initialize_db('bank');

$XeroOAuth = prepare_xero_auth();

if(isset($_GET['action']) && $_GET['action'] == 'get_transactions') {

    $response = $XeroOAuth->request('GET', $XeroOAuth->url('Accounts', 'core'), array('Where' => 'Type=="BANK"'));
    if ($XeroOAuth->response['code'] == 200) {
        $accounts = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
    } else {
        outputError($XeroOAuth);
    }


//INSERT BANKS
    $accounts = unserialize(serialize(json_decode(json_encode((array)$accounts), 1)));

    $mysqli->query("TRUNCATE TABLE xero_banks") or die($mysqli->error);
    $mysqli->query("TRUNCATE TABLE xero_transactions") or die($mysqli->error);

    for ($i = 0; $i < count($accounts['Accounts']['Account']); $i++)
    {
        $accounts_each = $accounts['Accounts']['Account'][$i];
        foreach ($accounts_each as $key => $value)
        {
            $keys[] = 'xero_'.$key;
            $values[] = "'" . $value . "'";
        }
        $the_keys = implode(",", $keys);
        $the_values = implode(",", $values);

        $mysqli->query("INSERT INTO xero_banks ($the_keys ) VALUES ($the_values)");
        unset($keys);
        unset($values);
    }

//INSERT TRANSACTIONS
    $today = date('Y-m-d H:i:s');
    $bank_ids = $mysqli->query("SELECT xero_AccountID,xero_Name FROM xero_banks");

    while ($row = $bank_ids->fetch_assoc()) {

        $xero_AccountID = $row['xero_AccountID'];
//        $response = $XeroOAuth->request('GET', $XeroOAuth->url('Reports/BankStatement', 'core'), array('bankAccountID' => $xero_AccountID, 'fromDate' => '2013-01-01T00:00:00', 'toDate' => '2013-12-31T23:59:59'), '', 'json');
        $response = $XeroOAuth->request('GET', $XeroOAuth->url('Reports/BankStatement', 'core'), array('bankAccountID' => $xero_AccountID, 'fromDate' => date('Y-m-d H:i:s',strtotime('-364 days')), 'toDate' => $today), '', 'json');
        if ($XeroOAuth->response['code'] == 200) {
            $report = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
            $bank_trans = unserialize(serialize(json_decode(json_encode((array)$report), 1)));
            $xero_Name = $bank_trans['Reports'][0]['ReportTitles'][1];
            for ($i = 0; $i < count($bank_trans['Reports'][0]['Rows'][1]['Rows']); $i++) {
                $this_bank_trans = $bank_trans['Reports'][0]['Rows'][1]['Rows'][$i]['Cells'];
                $xero_Date = (isset($this_bank_trans[0]['Value']) ? mysqli_real_escape_string($mysqli, $this_bank_trans[0]['Value']) : "0");
                $xero_Description = (isset($this_bank_trans[1]['Value']) ? mysqli_real_escape_string($mysqli, $this_bank_trans[1]['Value']) : "");
                $xero_Reference = (isset($this_bank_trans[2]['Value']) ? mysqli_real_escape_string($mysqli, $xero_Description . " " . $this_bank_trans[2]['Value']) : "0");
                $xero_Reconciled = (isset($this_bank_trans[3]['Value']) ? mysqli_real_escape_string($mysqli, $this_bank_trans[3]['Value']) : "0");
                $xero_Source = (isset($this_bank_trans[4]['Value']) ? mysqli_real_escape_string($mysqli, $this_bank_trans[4]['Value']) : "0");
                $xero_Amount = (isset($this_bank_trans[5]['Value']) ? mysqli_real_escape_string($mysqli, $this_bank_trans[5]['Value']) : "0");
                $xero_Balance = (isset($this_bank_trans[6]['Value']) ? mysqli_real_escape_string($mysqli, $this_bank_trans[6]['Value']) : "0");
                $mysqli->query("INSERT INTO xero_transactions (xero_AccountID, xero_Name, xero_Date, xero_Description, xero_Reference, xero_Reconciled, xero_Source, xero_Amount, xero_Balance) VALUES('$xero_AccountID','$xero_Name','$xero_Date','$xero_Description','$xero_Reference','$xero_Reconciled','$xero_Source','$xero_Amount','$xero_Balance')") or die($mysqli->error);

            }
        } else {
            outputError($XeroOAuth);
        }
    }
}

//////GET CONTACTS
if(isset($_GET['action']) && $_GET['action'] == 'get_contacts') {
    $response = $XeroOAuth->request('GET', $XeroOAuth->url('Contacts', 'core'), array('where' => 'Name=="Facebook" OR Name.contains("publ")'));
    if ($XeroOAuth->response['code'] == 200) {
        $mysqlitacts = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
        echo "There are " . count($mysqlitacts->Contacts[0]) . " contacts in this Xero organisation, the first one is: </br>";
        d($mysqlitacts);
        d($mysqlitacts);
        pr($mysqlitacts->Contacts[0]->Contact);
    } else {
        outputError($XeroOAuth);
    }
}

$mysqli->close();
echo "get_xero_transactions - done";