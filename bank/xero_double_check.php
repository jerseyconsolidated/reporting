<html>
<head>
    <style>
        html,
        body {
            margin:0;
            padding:0;
            height:100%;
            font-size: 8pt;
        }
        .fixed-nav-bar {
            position: fixed;
            top: 0;
            right: 0;
            z-index: 9999;
            width: 200px;
            height: 50px;
            background-color: lightgray;
        }
        #bad_matches {
            background-color: #ebcccc;
        }
    </style>
</head>
<body>
    <?php
    include ('C:\inetpub\wwwroot\reports\includes\general_functions.php');
    if (authIP()) {

        echo "<nav class=\"fixed-nav-bar\">";
        echo "<a href=\"xero_double_check.php?view=grouped\">group</a><BR>";
        echo "<a href=\"#find_missing_mid\">Find Missing MID Transactions</a><BR>";
        echo "<a href=\"#find_bad_matches\">Find Bad Matches</a><BR>";
        echo "</nav>";

        $mysqli = initialize_db('bank');
        echo "<a name=\"find_missing_mid\"><h1>Ensure Only MID Transactions</h1>";
        echo "<table border=1 cellpadding='0'>";

        if(isset($_GET['view']) && $_GET['view'] == 'grouped') {
            $get_xero = $mysqli->query("SELECT distinct(xero_Reference) FROM xero_transactions WHERE xero_mid_touched IS NULL ORDER BY xero_Reference");
        } else {
            $get_xero = $mysqli->query("SELECT xero_id xero_Name,xero_Date,xero_Reference,xero_Reconciled,xero_Amount,xero_mid_touched FROM xero_transactions WHERE xero_mid_touched is null group by xero_Reference order by xero_Reference");
        }
        $get_xero_fields = mysqli_fetch_fields($get_xero);

        echo "<tr>";
        foreach ($get_xero_fields as $field) {
            echo "<td>" . $field->name . "</td>";
        }
        echo "</tr>";

        while ($get_xero_results = $get_xero->fetch_assoc()) {
            echo "<tr>";
            foreach ($get_xero_results as $key => $value) {
                echo "<td>" .  $value  . "</td>";
            }
            echo "</tr>";
        }
        echo "</tr></table></a>";

        echo "<div id=\"bad_matches\">";
        echo "<a name=\"find_bad_matches\"><h1>Find Bad MID Matches</h1>";
        echo "<table border=1 cellpadding='0'>";


        if(isset($_GET['view']) && $_GET['view'] == 'grouped') {
            $get_mid_trans = $mysqli->query("SELECT distinct(mid_bank_reference) FROM mid_transactions");
        } else {
            $get_mid_trans = $mysqli->query("SELECT * FROM mid_transactions ORDER BY mid_bank_reference");
        }

        $get_mid_trans_fields = mysqli_fetch_fields($get_mid_trans);

        echo "<tr>";
        foreach ($get_mid_trans_fields as $field) {
            echo "<td>" . $field->name . "</td>";
        }
        echo "</tr>";

        while ($get_mid_trans_results = $get_mid_trans->fetch_assoc()) {
            echo "<tr>";
            foreach ($get_mid_trans_results as $key => $value) {
                echo "<td>" .  $value  . "</td>";
            }
            echo "</tr>";
        }
        echo "</tr></table></a></div>";
    }
    $mysqli->close();
    ?>
</div>
</body>
</html>