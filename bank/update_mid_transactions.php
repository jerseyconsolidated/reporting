<?php
include('C:\inetpub\wwwroot\reports\includes\general_functions.php');

$mysqli = initialize_db('bank');
$mysqli->query("TRUNCATE TABLE mid_transactions") or die(mysqli_error($mysqli));
$mysqli->query("UPDATE xero_transactions SET xero_mid_touched = NULL") or die(mysqli_error($mysqli));
$mysqli->query("UPDATE mids SET mid_transactions_found = NULL, mid_transactions_updated = NULL") or die(mysqli_error($mysqli));

function insert_transaction($mysqli, $type, &$trans_fields){
    $mid_transactions_fields = "mid_num, mid_name, mid_bank_name, mid_bank_reference, mid_trans_type, mid_date, mid_amount";
    $mysqli->query("INSERT INTO mid_transactions ($mid_transactions_fields) VALUES ('".$trans_fields['mid_num']."','".$trans_fields['mid_name']."','".$trans_fields['xero_Name']."','".$trans_fields['xero_Reference']."','".$type."','".$trans_fields['xero_Date']."','".$trans_fields['xero_Amount']."')") or die(mysqli_error($mysqli));
}

$mids = $mysqli->query("SELECT * FROM mids WHERE mid_criteria1 != '' AND mid_criteria2 != ''") or die(mysqli_error($mysqli));

while($mid_row = $mids->fetch_assoc()){
    $mid_id = $mid_row['mid_id'];

    $categories = array('mid_chargeback','mid_chargeback_reversal', 'mid_sales', 'mid_refunds', 'mid_fees', 'mid_fees_misc', 'mid_adj_credit', 'mid_adj_debit');
    $the_query = "";
    foreach ($categories as $category){

        $the_query = $the_query." SELECT DISTINCT $category FROM mids WHERE $category IS NOT NULL AND mid_id = '$mid_id' UNION ";
    }
    $the_query = rtrim($the_query,"UNION ");

    $the_query = $mysqli->query($the_query);
    //   $the_fields2 = $mysqli->fetch_assoc($the_fields);
    $the_or = "";
    while ($unique_field = $the_query->fetch_array()) {
        $the_or .= " xero_Reference LIKE '%".$unique_field[0]."%' OR";
    }
    $the_or = rtrim($the_or, "OR");
    $mid_criteria = "xero_Reference LIKE '%".$mid_row['mid_criteria1']."%' AND xero_Reference LIKE '%".$mid_row['mid_criteria2']."%' AND (";
    $mid_criteria .= $the_or.")";
    $mid_transactions_found = $mysqli->query("SELECT xero_id, xero_Name, xero_Date, xero_Reference, xero_Amount FROM xero_transactions WHERE $mid_criteria") or die(mysqli_error($mysqli));
    //  echo "SELECT xero_id, xero_Name, xero_Date, xero_Reference, xero_Amount FROM xero_transactions WHERE $mid_criteria";
    $transactions_found_count = mysqli_num_rows($mid_transactions_found);

    $mysqli->query("UPDATE mids SET mid_transactions_found = '$transactions_found_count' WHERE mid_id = '".$mid_row['mid_id']."'");

    $i = 0;
//Start counting how many mid transactions are going to be updated

    while($xero_row = $mid_transactions_found->fetch_assoc()){

        $trans_fields = array(
            'mid_num' => $mid_row['mid_num'],
            'mid_name' => $mid_row['mid_name'],
            'xero_Name' => $xero_row['xero_Name'],
            'xero_Reference' => $xero_row['xero_Reference'],
            'xero_Date' => $xero_row['xero_Date'],
            'xero_Amount' => $xero_row['xero_Amount']
        );

        $found = 0;
        if (stristr($xero_row['xero_Reference'], $mid_row['mid_chargeback']) && ($xero_row['xero_Amount'] < 0)) { $found=1; insert_transaction($mysqli, 'mid_chargeback', $trans_fields);  } //mid_chargeback
        if (stristr($xero_row['xero_Reference'], $mid_row['mid_chargeback_reversal']) && ($xero_row['xero_Amount'] > 0)) { $found=1; insert_transaction($mysqli, 'mid_chargeback_reversal', $trans_fields);  } //mid_chargeback_reversal
        if (stristr($xero_row['xero_Reference'], $mid_row['mid_sales']) && ($xero_row['xero_Amount'] > 0)) { $found=1; insert_transaction($mysqli, 'mid_sales', $trans_fields);  } //mid_sales
        if (stristr($xero_row['xero_Reference'], $mid_row['mid_refunds']) && ($xero_row['xero_Amount'] < 0)) { $found=1; insert_transaction($mysqli, 'mid_refunds', $trans_fields);  } //mid_refunds
        if (stristr($xero_row['xero_Reference'], $mid_row['mid_fees']) && ($xero_row['xero_Amount'] < 0)) { $found=1; insert_transaction($mysqli, 'mid_fees', $trans_fields);  } //mid_fees
        if (stristr($xero_row['xero_Reference'], $mid_row['mid_fees_misc']) && ($xero_row['xero_Amount'] < 0)) { $found=1; insert_transaction($mysqli, 'mid_fees_misc', $trans_fields);  } //mid_fees_misc
        if (stristr($xero_row['xero_Reference'], $mid_row['mid_adj_credit']) && ($xero_row['xero_Amount'] > 0)) { $found=1; insert_transaction($mysqli, 'mid_adj_credit', $trans_fields);  } //mid_adj_credit
        if (stristr($xero_row['xero_Reference'], $mid_row['mid_adj_debit']) && ($xero_row['xero_Amount'] < 0)) {$found=1; insert_transaction($mysqli, 'mid_adj_debit', $trans_fields);  } //mid_adj_debit
        if ($found == 1){
            $i++;
            $mysqli->query("UPDATE xero_transactions SET xero_mid_touched = '".$mid_row['mid_name']."' WHERE xero_id = '".$xero_row['xero_id']."'") or die(mysqli_error($mysqli));
        } else {
            echo "REF NOT MATCHED : ".$mid_row['mid_id']." ".$xero_row['xero_Name']."--".$xero_row['xero_Reference']." Amount: ".$xero_row['xero_Amount']."<BR>";
        }
        $mysqli->query("UPDATE mids SET mid_transactions_updated = '$i' WHERE mid_id = '".$mid_row['mid_id']."'") or die(mysqli_error($mysqli));;
    }
}
echo "mid transactions updated";
$mysqli->close();
