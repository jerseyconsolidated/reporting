<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
if (isset($_GET['phpinfo'])) {
    echo phpinfo();
}

/*
 * Formats the 'echo' statements in the log files so that it is more pretty
 */
function logIt($error)
{
    if($error !== "")
    {
    //echo (date('r', time())." (UTC) : \n");
    //echo $error."\n";
    }
}

/*
 * inserts the message into the general_log_table
 */
function databaseGeneralLog($message)
{
    if($message !== "")
    {
        $query =
               "INSERT INTO
               general_log_table
               SET
               message = '$message'
               ";
        doDBthing($query);
    }
}
/*
 * inserts the message into the gateway_log_table
 */
function databaseGatewayLog($message, $gateway)
{
    if($message !== "")
    {
        $query =
            "INSERT INTO
               gateway_log_table
               SET
               gateway = '$gateway',
               message = '$message'
               ";
        doDBthing($query);
    }
}
/*
 * inserts the message into the worker_log_table
 */
function databaseWorkerLog($message, $gateway, $login)
{
    if($message !== "")
    {
        $query =
            "INSERT INTO
               worker_log_table
               SET
               gateway = '$gateway',
               login   = '$login',
               message = '$message'
               ";
        doDBthing($query);
    }
}

/*
 * THE FOLLOWING FUNCTION IS NO LONGER USED!
 * This allows any thread to write to a file.
 * Hopefully, the only foreign threads writing to a file are actually writing to files that they own, and not a shared resource.
 *
 * Things will probably break if they all try to write to 1 file.

function log_error($error, $file="default") {
    $myfile = fopen("/var/www/reporting.jcoffice.net/errors/nmi/$file", "a+"); //or die("Unable to open file!");
    fwrite($myfile, $error . " "  . date('Y-m-d H:i:s') . "\r\n\"");
    fclose($myfile);
}
*/
function initialize_db($mysqli, $username="biohealth1", $password="7s63VfusbHVFuq"){
//function initialize_db($mysqli){
        //var_dump(debug_backtrace());
   // die();
   //$mysqli = (!isset($mysqli) ? $mysqli = 'limelight' : $mysqli);

    //echo $mysqli;
    //open up our connection
    $mysqli = new mysqli("localhost",$username,$password,'mids');
    // Check connection
    if (mysqli_connect_errno()) {
        //printf("Connect failed: %s\n", mysqli_connect_error());
        logIt(mysqli_connect_error());
        exit();
    }
    return $mysqli;
}

function doDBthing($query)
{
   $mysqli = initialize_db(getDBName());

    if($prepared_mysqli = $mysqli->prepare($query))
    {
        $prepared_mysqli->execute();

        $prepared_mysqli->store_result();

        $prepared_mysqli->free_result();

        $prepared_mysqli->close();
    }
    else
    {
        logIt("\n $query \n");
        logIt(mysqli_connect_error());
    }
    return true;
}


function setUserBusy($username, $busystatus)
{
    $mysqli = initialize_db("limelight", "biohealth4", "FVJzA42euHVnE");

    if($busystatus)
    {
        $query =
            "INSERT INTO accounts_in_use SET account = '$username', in_use = '$busystatus';";
    }
    else
    {
        /*$query =
			"UPDATE
			accounts_in_use
			SET
			in_use = '$busystatus'
			WHERE
			account = '$username'
			";
		*/
        $query =
            "DELETE FROM accounts_in_use
        WHERE account = '$username'";
    }
    doDBthing($query, $mysqli);
}
/*
 * Log that this day was completed by a worker for a given day/year combo.
 *
 * OR!!! Claim that this day was taken by a given worker.
 */
function update_days_to_process($day, $year, $gateway, $records_updated, $worker, $mysqli)
{
    $query = "
            UPDATE days_to_process
            SET
              worker = '$worker',
              complete = '1',
              gateway_number = '$gateway',
              records_updated = '$records_updated'
            WHERE
            day = '$day'
            AND
            year = '$year'";
    doDBthing($query, $mysqli);
}
/*
* Prepares the database for a massive enema of data
*/
function prepare_database_butt()
{
    $mysqli = initialize_db('mids');

    if ($mysqli->connect_error)
    {
        logIt("Failed to connect to MySQL. Unable to modify database for HUGE data (date/year data) " .$mysqli->connect_error);
        die();
    }
    $mysqli->query("set global net_buffer_length=1000000");
    logIt($mysqli->error);
    $mysqli->query("set global max_allowed_packet=1000000000");
    logIt($mysqli->error);
    $mysqli->close();
}
/*
 * Truncate & Create the database starting at this year:
 */
function prepare_database($start_date, $end_date)
{
    prepare_database_butt(); //MUST do this otherwise this function WILL be too big to insert for multiple years

    $year = substr($start_date, 0, 4);
    $this_year = substr($end_date, 0, 4);
    $mysqli = initialize_db('mids');

    if ($mysqli->connect_error)
    {
        logIt("Failed to connect to MySQL. Unable to Truncate and Create.: " .$mysqli->connect_error);
        die();
    }
    $mysqli->query("TRUNCATE TABLE days_to_process");
    $mysqli->query("TRUNCATE TABLE accounts_in_use");
    $mysqli->query("TRUNCATE TABLE nmi_trans_detail");
    $mysqli->query("TRUNCATE TABLE general_log_table");
    $mysqli->query("TRUNCATE TABLE gateway_log_table");
    $mysqli->query("TRUNCATE TABLE worker_log_table");
	$mysqli->query("TRUNCATE TABLE gateway_refunds");
    //$mysqli->query("TRUNCATE TABLE mids_transactions");
    //$mysqli->query("TRUNCATE TABLE nmi_result_codes");
    $mysqli->query("ALTER TABLE days_to_process AUTO_INCREMENT = 1");

     //$this_year = date("Y"); //The current, real-life year.
        if($this_year < $year)
        {
            logIt("Given year is larger than This year. That's impossible.");
            die();
        }

       $query = "";
       //while($this_year >= $year)
       {
           //if the given year is a leap year...
           if(date('L', strtotime("$year-01-01")))
           {
                $query .= make_full_year_query_string(true, $start_date, $end_date);
           }
           else
           {
                $query .= make_full_year_query_string(false, $start_date, $end_date);
           }
           $year++;
       }

    $mysqli->multi_query($query);
    logIt($mysqli->error);
    $mysqli->close();
    return($query);
}
/*
 * Returns a query string that inserts 365 days in a year.
 * (Or, 366 if a leap year)
 */
function make_full_year_query_string($leap_year, $start_date, $end_date)
{

    $start = DateTime::createFromFormat('Ymd',$start_date);
    $end = DateTime::createFromFormat('Ymd',$end_date);

    $interval = DateInterval::createFromDateString('1 day');
    $period = new DatePeriod($start, $interval, $end);

/*
    $days = 365;
    $his_query="";
    if($leap_year)
    {
        $days++;
    }
    $this_day=1;
    while($this_day <= $days)
    {
*/

    $this_query="";
    $gateways = getAllGateways();
    foreach ($period as $dt)
    {
        //var_dump($dt);
        //logIt("\n\n";
        $year = $dt->format('Y');
        $this_day = $dt->format("z");
        foreach($gateways as $gateway)
        {
            $this_query.="INSERT INTO
    `mids`.`days_to_process`
    (`gateway_number`, `year`, `day`, `worker`, `complete`, `time_spent`, `records_updated`)
    VALUES
    ('$gateway', '$year', '$this_day', '0', '0', '0', '0');
    ";
        }
        //$this_day++;
    }


    return $this_query;
}

?>
