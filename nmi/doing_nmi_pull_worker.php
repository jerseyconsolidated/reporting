<?php
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    require('general_functions_nmi.php');
    require ('../includes/RayGun/RayGun.php');
    require('passwords.php');
    ini_set('memory_limit', '-1');
    date_default_timezone_set('UTC');

    $time_started = time();
    //logIt('\n'.$time_started.':\n');

    if (defined('STDIN'))
    {
        $gateway = $argv[1];
        $username= $argv[2];
        $action = $argv[3];
        $days_to_pull = $argv[4];
        $php_error_location = $argv[5];
    }
    else
    {
        databaseGeneralLog("standard input failed -> this is from an unknown gateway worker.");
        //logIt("STDIN failed");
        die("STDIN failed");
    }

    //get the password for this login
    $password = getPasswordFromLogin($username, $gateway, true);
    //get the actual days that we are going to work on
    $days_to_process = selectDaysToProcess($gateway, $username, $days_to_pull);
    updateDaysToProcess($days_to_process, $gateway, $username, 0, 0, 0);

    /*
     * MUST ADD +1 to "day" because we aren't working in our database anymore, we are dealing with REAL DATES.
     *
     * There is no "day 0" in a valid gregorian calender.
     *
     * Also, must SUBTRACT -1 from "days_to_process" because the array is 0-based and count() is not.
     */

    $days = $days_to_process[0]["day"]+1;
    $year = $days_to_process[0]["year"];
    $start_date = date('Ymd', mktime( 0, 0, 0, 1, $days, $year));
    $days = $days_to_process[count($days_to_process)-1]["day"];
    $year = $days_to_process[count($days_to_process)-1]["year"];
    $end_date = date('Ymd', mktime( 0, 0, 0, 1, $days, $year));

    $post_data = array(
        "username" => $username,
        "password" => $password,
        "action_type" => $action,
        "start_date" => $start_date,
        "end_date" => $end_date
    );

    $url = "https://secure.nmi.com/api/query.php";
    $curlSession = curl_init();
    curl_setopt($curlSession, CURLOPT_URL, $url);
    curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curlSession, CURLOPT_POST, 1);
    curl_setopt($curlSession, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($curlSession, CURLOPT_TIMEOUT, 500000);
    if (php_uname('s') == "Darwin")  //OSX is called Darwin for some reason
    {
        //curl_setopt($curlSession, CURLOPT_CAINFO, "/Users/jonjenne/reporting/nmi/cacert.pem"); //HTTP cert
        curl_setopt($curlSession, CURLOPT_CAINFO, "/Users/jonjenne/reporting/includes/ca-bundle.crt"); //HTTPS cert
    }
    else if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') // this might work for windows, idk
    {
        logIt('This is Windows! (Ryan, set your path to the cert here - We are now dying)');
        die();
    }
    else //some kind of Linux
    {
        curl_setopt($curlSession, CURLOPT_CAINFO, "/var/www/reporting.jcoffice.net/includes/ca-bundle.crt"); //HTTPS cert
        logIt("running under linux CHECK THE CERT PATH BEFORE CONTINUING (dieing)");
       //die(); //kill this so it reminds me to change this for the linux path.
    }
    $rawresponse = curl_exec($curlSession);
    curl_close($curlSession);

#var_dump($rawresponse);
    //logIt( $rawresponse;
    $array_good = simplexml_load_string($rawresponse);
    $array_good = array(unserialize(serialize(json_decode(json_encode((array)$array_good), 1))));

    $rows = 0; //how many rows were affected
    if (!isset($array_good[0]['error_response']) && isset($array_good[0]['transaction']))
    {
        for ($i = 0; $i < count($array_good[0]['transaction']); $i++) {
            if (isset($array_good[0]['transaction'][$i]['transaction_id']) && isset($array_good[0]['transaction'][$i]['action']['success']))
            {
                $date = strtotime($array_good[0]['transaction'][$i]['action']['date']);
                $date = date('Y-m-d H:i:s', $date);

				$mysqli = initialize_db("mid", "biohealth4", "FVJzA42euHVnE");

				$insertSQL = sprintf("INSERT INTO nmi_trans_detail (api_username,api_password,gateway_id,transId,partial_payment_id,partial_payment_balance,platform_id,transaction_type,condition_,order_id,authCode,ponumber,order_description,first_name,last_name,address_1,address_2,company,city,state,postal_code,country,email,phone,fax,cell_phone,customertaxid,customerid,website,shipping_first_name,shipping_last_name,shipping_address_1,shipping_address_2,shipping_company,shipping_city,shipping_state,shipping_postal_code,shipping_country,shipping_email,shipping_carrier,tracking_number,shipping_date,shipping,shipping_phone,cc_number,cc_hash,cc_exp,cavv,cavv_result,xid,avs_response,csc_response,cardholder_auth,cc_start_date,cc_issue_number,check_account,check_hash,check_aba,check_name,account_holder_type,account_type,sec_code,drivers_license_number,drivers_license_state,drivers_license_dob,social_security_number,processor_id,tax,currency,surcharge,tip,card_balance,card_available_balance,entry_mode,cc_bin,action_amount,action_type,action_date,action_success,ip_address,action_source,action_username,action_response_text,action_batch_id,action_processor_batch_id,action_response_code,action_processor_response_text,action_processor_response_code) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                    sanatize_values($username, $mysqli),
                    sanatize_values($password, $mysqli),
                    sanatize_values($gateway, $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['transaction_id'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['partial_payment_id'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['partial_payment_balance'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['platform_id'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['transaction_type'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['condition'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['order_id'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['authorization_code'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['ponumber'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['order_description'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['first_name'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['last_name'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['address_1'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['address_2'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['company'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['city'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['state'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['postal_code'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['country'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['email'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['phone'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['fax'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cell_phone'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['customertaxid'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['customerid'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['website'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_first_name'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_last_name'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_address_1'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_address_2'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_company'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_city'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_state'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_postal_code'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_country'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_email'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_carrier'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['tracking_number'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_date'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_phone'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cc_number'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cc_hash'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cc_exp'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cavv'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cavv_result'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['xid'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['avs_response'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['csc_response'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cardholder_auth'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cc_start_date'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cc_issue_number'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['check_account'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['check_hash'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['check_aba'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['check_name'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['account_holder_type'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['account_type'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['sec_code'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['drivers_license_number'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['drivers_license_state'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['drivers_license_dob'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['social_security_number'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['processor_id'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['tax'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['currency'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['surcharge'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['tip'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['card_balance'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['card_available_balance'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['entry_mode'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cc_bin'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['amount'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['action_type'], $mysqli),
                    sanatize_values($date, $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['success'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['ip_address'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['source'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['username'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['response_text'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['batch_id'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['processor_batch_id'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['response_code'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['processor_response_text'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['processor_response_code'], $mysqli));

				//logIt(throwing into DB";
                $mysqli->query($insertSQL) or die($mysqli->error);
                $rows=$rows+mysqli_affected_rows($mysqli);
				$mysqli->close();
				//sleep(1);
                //updateDayToProcess()
                //logIt( $rows.'AFFECTED';
                //logIt( $mysqli->error;
                //die();
            } else {
				continue;
            }
        }
		/*
		 * After the loop above.
		 */
        logIt("update successful");
        $time_finished= time();
        $time = $time_finished-$time_started;
        updateDaysToProcess($days_to_process, $gateway, $username, 1, $time, $rows); //blank out the days
        setUserBusy($username, 0);
        databaseWorkerLog("Successfully updated $rows rows. Time taken: ".$time, $gateway, $username);
        logIt("Time taken: ".$time);

	}
else
{
	$time_finished= time();
    $time = $time_finished-$time_started;
    updateDaysToProcess($days_to_process, $gateway, $username, 1, $time, $rows); //blank out the days

    if(! isset($array_good[0]['error_response']))
    {
        //logIt("problem updating.... probably no data? ");
        databaseWorkerLog("Returned 0 fields", $gateway, $username);
    }
    else
    {
        databaseWorkerLog($array_good[0]['error_response'], $gateway, $username);
        logIt("problem updating.... probably no data? ");
    }
    //logIt( $array_good[0]['error_response'];
    //logIt("Error response noted as: ".$array_good[0]['error_response']);
    //var_dump($array_good);
    //logIt(done";
    setUserBusy($username, 0);
    //sleep(1);
    //die();
}

//just incase the above conditions aren't met or something.
logIt("\n totally done \n");
logIt("Time taken: ".$time);
databaseWorkerLog("Totally Done. Successfully updated $rows rows. Time taken: ".$time, $gateway, $username);
setUserBusy($username, 0);
//sleep(1);
//die();

////////////////////////////////////// FUNCTIONS FOR THIS FILE FOLLOW BELOW //////////////////////////////////

/*
 * THIS FUNCTION IS ONLY FOR 1 DAY AT A TIME !!!
 * (this is probably not used...)
 *
 * Update the public record of day/year that
 * this worker is either now in charge of this day (complete = 0) or finished (complete = 1)
 *
 */
function updateDayToProcess($day, $year, $gateway, $username, $complete, $time_spent, $rows_affected)
{
    $mysqli = initialize_db("limelight", "biohealth4", "FVJzA42euHVnE");
    $query =
        "UPDATE
        days_to_process
        SET worker = '$username',
        complete = '$complete',
        time_spent = '$time_spent',
        records_updated = '$rows_affected'
        WHERE
        day = $day
        AND
        year = $year
        AND
        gateway_number = $gateway";
    $mysqli->multi_query($query) or die(mysqli_errno($mysqli));
    $mysqli->close();
}
/*
 * THIS FUNCTION IS FOR AN ARRAY OF DAYS (given as $days)
 *
 * Update the public record of days/years that
 * this worker is either now in charge of this day (complete = 0) or finished (complete = 1)
 *
 */
function updateDaysToProcess($days, $gateway, $username, $complete, $time_spent, $rows_affected)
{
    $query =
        "UPDATE
        days_to_process
        SET worker = '$username',
        complete = '$complete',
        time_spent = '$time_spent',
        records_updated = '$rows_affected'
        WHERE ";
    foreach($days as $day)
    {
		if(isset($day["day"]) && isset($day["year"]))
		{
        $this_day = $day["day"];
        $this_year = $day["year"];
        $query .= "(day = $this_day AND year = $this_year AND gateway_number = $gateway) OR";
		}
    }
	$mysqli = initialize_db("limelight", "biohealth4", "FVJzA42euHVnE");
	//die();
    $query = substr($query, 0, -3); //chop off last ' OR'
    $mysqli->multi_query($query) or die(mysqli_errno($mysqli));
    $mysqli->close();
}
/*
 *  Return the days (in an array) that this needs to process for a given gateway/year
 * and limit the number of days a thread should work on.
 */
function selectDaysToProcess($gateway, $username, $limit)
{
    if($gateway =="" || $username =="" || $limit=="")
    {
        logIt("something is not set right");
    }
    $mysqli = initialize_db('nmi', "biohealth2", "JaG3HMA86HxQc");

    $query = "
            SELECT DISTINCT (day), year
            FROM
              days_to_process
            WHERE
              (worker = '0'
              OR worker = '$username')
            AND
              complete = '0'
            AND
              gateway_number = '$gateway'
		  GROUP BY day
          ORDER BY day ASC
          LIMIT $limit;";

    //logIt(this is an important query";
    //logIt( $query;
    $query_result = $mysqli->prepare("$query");
    $query_result->execute(); // why is this failing?
    $query_result->bind_result($day, $year);

    //loop through each day
    $day_array[] = "";
    $index=0;
    while ($query_result->fetch())
    {
        $this_array["day"] = $day;
        $this_array["year"] = $year;
        $day_array[$index] = $this_array;
        $index++;
    }
    //var_dump($day_array);
    $mysqli->close();
    return $day_array;
}

/*
 * Some bizarre function that RK made
 */
function sanatize_values($theValue, $mysqli) {
    $theValue = (is_array($theValue) ? "" : $theValue);
    $theValue = $mysqli->real_escape_string($theValue);
    $theValue = "'".$theValue."'";
    return $theValue;
}
?>
