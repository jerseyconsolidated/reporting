<?php
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    require('general_functions_nmi.php');
    require ('../includes/RayGun/RayGun.php');
    require('passwords.php');
    ini_set('memory_limit', '-1');
    date_default_timezone_set('UTC');
    echo "\n"; //make it purdy

/*
 * reporting server
 */
$php_command        = "php /var/www/reporting.jcoffice.net/nmi/doing_nmi_pull_worker.php";
/*
 * JJ's machine
 */
//$php_command = "php /Users/jonjenne/reporting/nmi/doing_nmi_pull_worker.php";

/*
 * This file is the complicated one.
 * It has to determine how many user-accounts it has available, so that it can spawn a thread for each one.
 * Each thread will then claim a few days and get to work.
 */
    if (defined('STDIN'))
    {
        //$username = $argv[1];
        $gateway= $argv[1];
        $action_type = $argv[2];
        $start_date = $argv[3];     //Format: YYYYMMDD
        $end_date = $argv[4];       //Format: YYYYMMDD
        $days_per_thread = $argv[5];
        $accounts_too_busy = $argv[6];
        $php_error_location = $argv[7];
    }
    else
    {
        databaseGeneralLog("One of the mid-level workers had a problem trying to get it's input. So, that sucks. ");
        logIt("One of the mid-level workers had a problem trying to get it's input. So, that sucks. ");
        die();
    }

    $logins = getLoginFromGateway($gateway, true); //get all username records for this gateway

    $users_really_busy = 0;
    while(shouldContinue($gateway) ==0)
    {
        $this_user = getNextLogin($logins); //fetch an individual login on a rotating basis
        $error_location = $php_error_location.$this_user;
        if($users_really_busy >= $accounts_too_busy)
        {
            databaseGatewayLog("All accounts are busy now. I've been instructed to kill myself. =( ", $gateway);
            logIt("All accounts are busy now. I've been instructed to kill myself. =( ");
            die();
        }
        if(isUserBusy($this_user))
        {
            logIt("this user $this_user is busy\n");
            databaseGatewayLog("this user $this_user is busy", $gateway);
            $users_really_busy++;
            sleep(1);
            continue;
        }
        else
        {
            $users_really_busy = 0;
        }
        setUserBusy($this_user, 1);

        $command = "$php_command $gateway $this_user $action_type $days_per_thread $php_error_location >>$error_location &";
//whatever the heck i want just something
        logIt("\n".$command."\n");
        //databaseGatewayLog($command, $gateway);

        $result = pclose(popen($command, "r"));
        if($result)
        {
            logIt("\n failed to start $command");
            databaseGatewayLog("failed to start $command", $gateway);
        }
        sleep(1);
    }

logIt("Gateway $gateway is now finished. Dying off.");
databaseGatewayLog("Gateway $gateway is now finished. Dying off.", $gateway);
die();

///////////////////***End of main script. File-specific functions follow below***///////////////

/*
 * Is this user account already busy?
 *
 * Note, this method may be stupid/naive. =(
 * (and, by stupid/naive, I mean when this data is created by the worker to be checked here, it doesn't do proper
 * checks like it should.... like if the account already exists, or if there are more than 1 to be deleted. #yolo)
 */
function isUserBusy($this_user)
{
    $mysqli = initialize_db('nmi', "biohealth2", "JaG3HMA86HxQc");
    $query = "SELECT COUNT(account) FROM accounts_in_use WHERE in_use = '1' AND account = '$this_user' LIMIT 1;";
    //echo $query;
    //die();
    $check_cycle = $mysqli->query($query)->fetch_assoc();
    $mysqli->close();
    //echo "Check cycle is " . $check_cycle."\n";
    $check_cycle = $check_cycle['COUNT(account)'];
    //echo "Days Left to Process: " . $check_cycle."\n";

    //echo $check_cycle;
    //die();
    if($check_cycle >0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
 * Should return the next 'login' in the array, and rest if it's at the end.
 */
function getNextLogin(&$logins)
{
   if(next($logins) == "")
    {
        reset($logins);
    }
    return current($logins);
}
/*
 * Should we continue creating bursts of worker threads?
 *
 * This is determined by checking if we have any non-complete days in the days_to_process table.
 */
function shouldContinue($gateway)
{
    $mysqli = initialize_db('nmi', "biohealth2", "JaG3HMA86HxQc");

    $check_cycle = $mysqli->query("SELECT COUNT(day) FROM days_to_process WHERE complete = '0' AND gateway_number = '$gateway' LIMIT 1")->fetch_assoc();
    $mysqli->close();
    //echo "Check cycle is " . $check_cycle."\n";
    $check_cycle = $check_cycle['COUNT(day)'];
    //echo "Days Left to Process: " . $check_cycle."\n";

    $done = ($check_cycle == 0 ? 1 : 0);
    return $done;
}

?>

