<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require('general_functions_nmi.php');
require ('../includes/RayGun/RayGun.php');
require('passwords.php');
date_default_timezone_set('UTC');

ini_set('memory_limit', '-1');
global $mysqli;

echo "\n"; // just make things look purdy


/*
 * This has to be written this way because it seems like NMI is actually slower than limelight.
 * Additionally, Limelight has 40 accounts across 1 set of data. This is X number of accounts across X number of data sets.
 *
 * So, the complexity of Limelight was 40 workers + 1 polling thread + 1 master thread * 1 set of data.
 * This is X accounts * X number of data sets + 1 master thread.
 *
 *
 *                                                  This file's job:
 * This file is supposed to start the individual threads that will actually do the work. It kind of looks like this:
 *
 *                  Master / nmi_jon.php
 *                     |
 *                     |
 *                    \ /
 *                  Middle / doing_nmi_pull.php
 *                     |
 *                     |
 *                    \ /
 *                  Data   / doing_nmi_pull_worker.php
 *
 *
 * The Master level (this one) specifies what the start date should be, and other important variables
 * Then, it starts the middle worker.
 *
 * Each Middle level has a unique gateway assigned to it (from the master level).
 * For each unique gateway given, it spawns a new worker on a different account WITHIN THIS GATEWAY.
 *
 * The Data level is given a very specific start/end date, gateway, and account from the gateway-level worker.
 * This is the level that actually makes the API call to nmi.
 *
 *
 * UPDATE! 5/1/2015
 * Logging is probably all handled by the DB, and eventually we should totally get rid of all directory-based logging.
 */






/*
 *
 * GLOBAL VARIABLES BELOW!
 *
 */
$start_date         = "20120101"; //Format: YYYYMMDDhhmmss     (hhmmss optional)
$end_date = date('Ymd',strtotime("-1 days"));
$days_per_thread    = "10"; //How many days should each thread work on? If 1, 1 day = 1 thread. If 5, 5 days = 1 thread.
/*
 * How many attempts to contact busy accounts?
 * If an account is in use, the script will wait 1 second and try again. If it tries $accounts_too_busy times, it will stop.
 * If you have 2 accounts, this number is automatically * 2.
 * If you have 3 accounts, this number is automatically * 3.
 *
 * If an account was busy for a bit, but then becomes un-busy, this variable will reset.
 */
$accounts_too_busy  = "200";


/*
 * reporting server
 */
$php_command        = "php /var/www/reporting.jcoffice.net/nmi/doing_nmi_pull.php";
/*
 * Jon's machine
 */
//$php_command        = "php /Users/jonjenne/reporting/nmi/doing_nmi_pull.php";

/*
 * The Gateway/worker is the name of the error file. So, the path should be something like:
 * /Users/jonjenne/reporting/nmi/errors/
 *
 * BECAUSE! This script makes lots of error files, so we need to feed a DIRECTORY not a FILE.
 * AND GOD HELP YOU IF YOU FORGET THE LAST / AT THE END OF THE PATH!!!
 * WHO KNOWS WHAT WILL HAPPEN
 */
//$php_error_location = "/Users/jonjenne/reporting/nmi/errors/";
$php_error_location = "/var/www/reporting.jcoffice.net/nmi/errors/"; //where to put errors
//specify what type of transaction we want (probably, so don't touch it):
$action_type        = "sale,refund,credit,auth,capture,void,return,settlement";


//DONT TOUCH THE zxzzPPzxzz PART!! That's how we add in the 'gateway' later on.
//Also, the "errors.txt" that got added on is what we want to call the error file for THIS PARTICULAR file.
$run_command = "$php_command zxxzPPzxxz $action_type $start_date $end_date $days_per_thread $accounts_too_busy $php_error_location >";//"."general_errors.txt &";

/*
 * Truncate all tables, prepare "days_to_process" table for workers
 */
prepare_database($start_date, $end_date);
/*
 * DELETE ALL LOGS IN THE LOG FILE!! CAREFUL WITH THIS
 */

//get all mids/gateways
$mid_login = getAllGateways();
//for each gateway....
//also known as "mid"
foreach($mid_login as $gateway)
{
    $log_files = $php_error_location.$gateway.".txt";
    //databaseGeneralLog($log_files);
    //logIt($log_files);
    //die();

    if(! file_exists($log_files))
    {
        touch($log_files);
    }
    //die();
    $command = $run_command.$log_files." &";
    //echo "\n".$gateway;
    $command = str_replace("zxxzPPzxxz", $gateway, $command);
    databaseGeneralLog("starting...: ".$command);
    //databaseGatewayLog("testing the gateway logger", "gateway0");
    //databaseWorkerLog("testing the worker logger", "worker0", "gateway0");
    //echo $command ."\n";
    //die();
    //$command = "ls";
    $result = pclose(popen($command, "r"));
    if($result)
    {
        databaseGeneralLog("Failed to start $command ... This command did not start.");
    }
    //echo "\n".$command."\n";
    sleep(1);
}

databaseGeneralLog("aaaaaaaand we're done starting the MID (gateway) threads. They're probably running now. I hope.");
//logIt("\n\naaaaaaaand we're done starting the MID (gateway) threads. They're probably running now. I hope.\n");

?>