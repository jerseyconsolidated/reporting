<?php
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    require('general_functions_nmi.php');
    require ('../includes/RayGun/RayGun.php');
    require('passwords.php');
    ini_set('memory_limit', '-1');
    date_default_timezone_set('UTC');


echo "\n";

/*
 * This file is the complicated one.
 * It has to determine how many user-accounts it has available, so that it can spawn a thread for each one.
 * Each thread will then claim a few days and get to work.
 */
    if (defined('STDIN'))
    {
        //$username = $argv[1];
        $gateway= $argv[1];
        $action = $argv[2];
        $start_date = $argv[3];     //Format: YYYYMMDD
        $end_date = $argv[4];       //Format: YYYYMMDD
        $days_per_thread = $argv[5];
        $php_error_location = $argv[6];
    }
    else
    {
        echo "One of the mid-level workers had a problem trying to get it's input. So, that sucks. ";
        die();
    }

    $logins = getLoginFromGateway($gateway, true); //get all username records for this gateway

//echo $end_date;
    $start_time = strtotime($start_date);
    $date = getdate($start_time);
    $start_day = $date['yday']; //first day of the year that we should start this
    $start_year = $date['year'];
    $start_month = $date['mon'];

    $end_time = strtotime($end_date);
    $date = getdate($end_time);
    $end_day = $date['yday']; //end day of the year that we should end this
    $end_year = $date['year'];
    $end_month = $date['mon'];

//echo returnDays($start_date, $end_date);
/*
 * TODO for tomorrow: : :
 *
 * Now, we have to start workers based on the $days_per_thread variable.
 * When we spawn a worker, we have to figure out how many days it should be given.
 * It needs to have this specified as a $start_date and an $end_date.
 * Then, this should call x number of workers (x being how many accounts are available) to take over.
 *
 * SOMEHOW ! we should wait for these workers to finish before moving on and starting the next one?
 *
 * Or, do we have to wait? Can we double them up? Why not, right?
 */

die();
    //echo $date->format('Y-m-d');


    /*
     * Should we keep spawning workers here???
     */
    while(shouldContinue() ==0)
    {
        //$this_user = getNextLogin($logins);
        $this_user = getNextLogin($logins); //fetch an individual login
        $php_command = "php /Users/jonjenne/reporting/nmi/doing_nmi_pull_worker.php";
        $command = "$php_command $this_user $gateway $action_type $start_date $end_date $php_error_location & >$php_error_location";
        $result = pclose(popen($command, "r"));
        sleep(1);
    }


///////////////////***End of main script. File-specific functions follow below***///////////////

/*
 * Figure out the number of days that need to get worked on
 *
 */
function returnDays($start_date, $end_date)
{
    $secondstodays = 24*60*60;
    $difference = $end_date - $start_date;
    echo "\n".$difference."\n";;
    return $difference/($secondstodays);
}
/*
 * Should return the next 'login' in the array, and rest if it's at the end.
 */
function getNextLogin(&$logins)
{
   if(next($logins) == "")
    {
        reset($logins);
    }
    return current($logins);
}
/*
 * Should we continue creating bursts of worker threads?
 *
 * This is determined by checking if we have any non-complete days in the days_to_process table.
 */
function shouldContinue()
{
    $mysqli = initialize_db('nmi', "biohealth2", "JaG3HMA86HxQc");

    $check_cycle = $mysqli->query("SELECT COUNT(day) FROM days_to_process WHERE complete = '0' LIMIT 1")->fetch_assoc();
    $mysqli->close();
    //echo "Check cycle is " . $check_cycle."\n";
    $check_cycle = $check_cycle['COUNT(day)'];
    //echo "Days Left to Process: " . $check_cycle."\n";

    $done = ($check_cycle == 0 ? 1 : 0);
    return $done;
}

?>

