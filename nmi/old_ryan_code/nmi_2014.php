<?php
require('../includes/general_functions_limelight.php');
require('../includes/RayGun/RayGun.php');
ini_set('memory_limit', '-1');

global $mysqli;
$mysqli = initialize_db('mids');
if ($mysqli->connect_error) {
    echo "Failed to connect to MySQL: " .$mysqli->connect_error;
}

$mysqli->query("TRUNCATE TABLE nmi_trans_detail");

function sanatize_values($theValue, $mysqli) {
    $theValue = (is_array($theValue) ? "" : $theValue);
    $theValue = $mysqli->real_escape_string($theValue);
    $theValue = "'".$theValue."'";
    return $theValue;
}

$mid_login = array(
    array("gateway" => "4", "login" => "biohealth800", "pass" => "p8y9cA31q8B358i"),
    array("gateway" => "8", "login" => "biohealth", "pass" => "E.wxg74RL>b:@Y3"),
    array("gateway" => "9", "login" => "biohealth1", "pass" => "x788D974V4N2E"),
    array("gateway" => "10", "login" => "272600138585", "pass" => "73A4Nd1466Z1Y"),
    array("gateway" => "12", "login" => "BIOH732", "pass" => "bioh3alth!!"),
    array("gateway" => "13", "login" => "biogarcinia", "pass" => "I)61h8^2Z&?<N"),
    array("gateway" => "16", "login" => "biohealthnmc", "pass" => "E.wxg74RL>b:@Y3"),
    array("gateway" => "19", "login" => "bhealthresearch", "pass" => "-cj5GNT!9TL"),
    array("gateway" => "21", "login" => "ianator", "pass" => "Wo!=JovJ6^V"),
    array("gateway" => "23", "login" => "ryankiel1", "pass" => "B_GlJl3Qw}B"),
    array("gateway" => "26", "login" => "aubiohealth12", "pass" => "3c4Zlv7iIKz"),
    array("gateway" => "32", "login" => "biohealth865", "pass" => "5ufpS8SZkvSn"),
    array("gateway" => "36", "login" => "biohealthFDMS", "pass" => "H*o89pGQpxEA"),
    array("gateway" => "34", "login" => "vpsdermaliv", "pass" => "hxM2i0wO82a3"),
    array("gateway" => "38", "login" => "synergy111", "pass" => "ianballer1")
);


for ($b = 0; $b < count($mid_login); $b++) {
    $data1 = array(
        "username" => $mid_login[$b]['login'],
        "password" => $mid_login[$b]['pass'],
        "action_type" => "sale,refund,credit,auth,capture,void,return",
        "start_date" => "20141001"
    );
    $url = "https://secure.nmi.com/api/query.php";
    $curlSession = curl_init();
    curl_setopt($curlSession, CURLOPT_URL, $url);
    curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curlSession, CURLOPT_POST, 1);
    curl_setopt($curlSession, CURLOPT_POSTFIELDS, $data1);
    curl_setopt($curlSession, CURLOPT_TIMEOUT, 500000);
    curl_setopt($curlSession, CURLOPT_CAINFO, "C:\\inetpub\\wwwroot\\certs\\cacert.pem");

    $rawresponse = curl_exec($curlSession);
    curl_close($curlSession);
    $array_good = simplexml_load_string($rawresponse);
    $array_good = array(unserialize(serialize(json_decode(json_encode((array)$array_good), 1))));

    if (!isset($array_good[0]['error_response']) && isset($array_good[0]['transaction'])) {
        for ($i = 0; $i < count($array_good[0]['transaction']); $i++) {
            if (isset($array_good[0]['transaction'][$i]['transaction_id']) && isset($array_good[0]['transaction'][$i]['action']['success'])) {
                $date = strtotime($array_good[0]['transaction'][$i]['action']['date']);
                $date = date('Y-m-d H:i:s', $date);

                $insertSQL = sprintf("INSERT INTO nmi_trans_detail (api_username,api_password,gateway_id,transId,partial_payment_id,partial_payment_balance,platform_id,transaction_type,condition_,order_id,authCode,ponumber,order_description,first_name,last_name,address_1,address_2,company,city,state,postal_code,country,email,phone,fax,cell_phone,customertaxid,customerid,website,shipping_first_name,shipping_last_name,shipping_address_1,shipping_address_2,shipping_company,shipping_city,shipping_state,shipping_postal_code,shipping_country,shipping_email,shipping_carrier,tracking_number,shipping_date,shipping,shipping_phone,cc_number,cc_hash,cc_exp,cavv,cavv_result,xid,avs_response,csc_response,cardholder_auth,cc_start_date,cc_issue_number,check_account,check_hash,check_aba,check_name,account_holder_type,account_type,sec_code,drivers_license_number,drivers_license_state,drivers_license_dob,social_security_number,processor_id,tax,currency,surcharge,tip,card_balance,card_available_balance,entry_mode,cc_bin,action_amount,action_type,action_date,action_success,ip_address,action_source,action_username,action_response_text,action_batch_id,action_processor_batch_id,action_response_code,action_processor_response_text,action_processor_response_code) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                    sanatize_values($mid_login[$b]['login'], $mysqli),
                    sanatize_values($mid_login[$b]['pass'], $mysqli),
                    sanatize_values($mid_login[$b]['gateway'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['transaction_id'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['partial_payment_id'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['partial_payment_balance'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['platform_id'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['transaction_type'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['condition'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['order_id'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['authorization_code'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['ponumber'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['order_description'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['first_name'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['last_name'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['address_1'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['address_2'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['company'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['city'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['state'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['postal_code'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['country'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['email'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['phone'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['fax'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cell_phone'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['customertaxid'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['customerid'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['website'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_first_name'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_last_name'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_address_1'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_address_2'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_company'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_city'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_state'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_postal_code'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_country'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_email'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_carrier'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['tracking_number'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_date'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['shipping_phone'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cc_number'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cc_hash'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cc_exp'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cavv'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cavv_result'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['xid'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['avs_response'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['csc_response'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cardholder_auth'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cc_start_date'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cc_issue_number'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['check_account'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['check_hash'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['check_aba'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['check_name'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['account_holder_type'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['account_type'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['sec_code'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['drivers_license_number'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['drivers_license_state'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['drivers_license_dob'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['social_security_number'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['processor_id'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['tax'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['currency'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['surcharge'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['tip'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['card_balance'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['card_available_balance'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['entry_mode'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['cc_bin'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['amount'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['action_type'], $mysqli),
                    sanatize_values($date, $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['success'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['ip_address'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['source'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['username'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['response_text'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['batch_id'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['processor_batch_id'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['response_code'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['processor_response_text'], $mysqli),
                    sanatize_values($array_good[0]['transaction'][$i]['action']['processor_response_code'], $mysqli));

                $mysqli->query($insertSQL) or die($mysqli->error);

            } else {
                continue;
            }
        }
    }
}
echo "done";
