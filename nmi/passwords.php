<?php
/*
 * This file needs to exist, because some of those passwords are f-ing crazy and shouldn't be passed in bash.
 */

/*
 * Returns the array of usernames / passwords (CHANGE / ADD / REMOVE THEM HERE !)
 */
function getMidLogins()
{
    $mid_login = array(
        //array("gateway" => "999", "login" => "vpsdermaliv", "pass" => "hxM2i0wO82a3"), //given to me by RK 2/6/2015:
        array("gateway" => "21", "login" => "biohealthMEGA", "pass" => "U4NpAuw9ctzy"), //made by JJ

        //checked out in the google doc Jan/2015
        array("gateway" => "19", "login" => "bhealthresearch", "pass" => "-cj5GNT!9TL"),
        array("gateway" => "10", "login" => "272600138585", "pass" => "73A4Nd1466Z1Y"),
        array("gateway" => "21", "login" => "ianator", "pass" => "Wo!=JovJ6^V"),
        array("gateway" => "4", "login" => "biohealth800", "pass" => "p8y9cA31q8B358i"),
        array("gateway" => "12", "login" => "BIOH732", "pass" => "bioh3alth!!"),
        array("gateway" => "23", "login" => "ryankiel1", "pass" => "B_GlJl3Qw}B"),
        array("gateway" => "34", "login" => "vpsdermaliv", "pass" => "hxM2i0wO82a3"),
        array("gateway" => "38", "login" => "synergy111", "pass" => "ianballer1"),

        //not showing up in the google doc, but were in RK's code and still seem to work:
        array("gateway" => "8", "login" => "biohealth", "pass" => "E.wxg74RL>b:@Y3"),
        array("gateway" => "13", "login" => "biogarcinia", "pass" => "I)61h8^2Z&?<N"),
        array("gateway" => "16", "login" => "biohealthnmc", "pass" => "E.wxg74RL>b:@Y3"),
        array("gateway" => "26", "login" => "aubiohealth12", "pass" => "3c4Zlv7iIKz"),
        array("gateway" => "32", "login" => "biohealth865", "pass" => "5ufpS8SZkvSn"),
        array("gateway" => "36", "login" => "biohealthFDMS", "pass" => "H*o89pGQpxEA"),

        //"pw must be reset through merchant"
        //array("gateway" => "9", "login" => "biohealth1", "pass" => "M0rebot1*"), // doesn't seem to work with either this PW or the below one
        //added from the super mid sheet (this did not appear in RK's code.)
        // Also, "pw must be reset through merchant"
        array("gateway" => "9", "login" => "biohealth100", "pass" => "x788D974V4N2E"),

        //Given to me by Ian 2/10/2015:
        array("gateway" => "56", "login" => "camilecynergy", "pass" => "8Rq6HP8Zvt&C"),
        array("gateway" => "54", "login" => "bayliecynergy", "pass" => "W#&m^JfH&5%g"),
        array("gateway" => "52", "login" => "choicebaylie", "pass" => "wM3>jfuKW/AQ]A."),
        array("gateway" => "50", "login" => "dermaselectreuven", "pass" => "wJ90MmD9eZfB"),
        array("gateway" => "48", "login" => "dermacmsreuven", "pass" => "WPS3TzMQ3mYH"),
        array("gateway" => "46", "login" => "dermavpsreuven", "pass" => "Sr4zKlREHPTx"),
        array("gateway" => "44", "login" => "choicerhr", "pass" => "v2*D!!C0M*zv"),
		//all duplicates
		array("gateway" => "8", "login" => "biohealthweb", "pass" => "T79wDt1494n6erI"),
		array("gateway" => "13", "login" => "garcinicahealth", "pass" => "I)61h8^2Z&?<N"),
        array("gateway" => "16", "login" => "biohealthnmcweb", "pass" => "J4aH1rnjpIoA68M"),
        array("gateway" => "32", "login" => "yaconbiohealth", "pass" => "DYVS4gl8bnx"),
        array("gateway" => "36", "login" => "biohealthrb", "pass" => "2RLViJUiRdmf"),
        array("gateway" => "9", "login" => "biohealth100", "pass" => "x788D974V4N2E"),

		array("gateway" => "58", "login" => "BaylieHBMS", "pass" => "tRDVhEb12Bxr"),
		array("gateway" => "60", "login" => "CamileHBMS", "pass" => "MDgWibwGs87a"),
		array("gateway" => "62", "login" => "cardworksbaylie", "pass" => "VvyE1fBekydg"),
		array("gateway" => "64", "login" => "nmarhrbaylie", "pass" => "F&eU#*ewNqT2"),
		array("gateway" => "66", "login" => "chasepaytech", "pass" => "%JH6XW8vtuvU"),

		//Added by JJ as per Ian on 4/22/2015:
		array("gateway" => "70", "login" => "choicebourke", "pass" => "LQnF7sEFAUtc"),
		array("gateway" => "72", "login" => "payallelaineskin", "pass" => "935x*dg*JrZ&"),
		array("gateway" => "74", "login" => "payallcaressa", "pass" => '9TN&XRaKD$fP'),
		array("gateway" => "75", "login" => "choiceelaine", "pass" => "b8CR50zs5tNB"),
		array("gateway" => "76", "login" => "choicecaressa", "pass" => "JeZupKe1I5PF"),
		array("gateway" => "77", "login" => "choicedevan", "pass" => "eir22idSWvJe"),
		array("gateway" => "78", "login" => "cardworksdevan", "pass" => "8EA7bxczlS5D"),
		array("gateway" => "79", "login" => "cardworkselaine", "pass" => "b3iahf6GAHeV"),
		array("gateway" => "80", "login" => "elaineSkin", "pass" => "Nja26dBVdk0s"),
		array("gateway" => "81", "login" => "devanSkin", "pass" => "42FE1bXRcc6g"),
		array("gateway" => "82", "login" => "damiaSkin", "pass" => "w3z0qsHc8fKn"),
		array("gateway" => "83", "login" => "celesseSkin", "pass" => "w3z0qsHc8fKn"),
		array("gateway" => "84", "login" => "careassaSkin", "pass" => "Q1JAkEbHO6q6"),
		array("gateway" => "86", "login" => "gabrieltsys", "pass" => "F472XdB5IN46"),


	);



    return $mid_login;
}
/*
 * Get the Database Name
 */
function getDBName()
{
    return "mids";
}
/*
 * Get all unique gateways
 */
function getAllGateways()
{
    $mid_login = getMidLogins();
    $gateways = array();
    foreach($mid_login as $login)
    {
        $gateways[] = $login['gateway'];
    }
    return array_unique($gateways);
}

/*
 * Returns the password from a given username.
 *
 * Requires a Username AND Gateway, incase there are multiple identical usernames.
 *
 * Hopefully, however, we only have unique usernames for each gateway, or this might not return individual ones.
 *
 *  If returnStringArray is true, it will return a STRING ARRAY of all logins for the given gateway.
 *
 * If returnStringArray is false, it will return an ARRAY of ARRAYS of all logins (and passwords/gateways) for a given gateway.

 */
function getPasswordFromLogin($username, $gateway, $returnStringArray)
{
    $mid_login = getMidLogins();
    $return_array = array();

    foreach($mid_login as $login)
    {
        if(($login['login'] == $username) && ($login['gateway'] == $gateway))
        {
            if($returnStringArray)
            {
                $return_array = $login["pass"];
            }
            else
            {
                $return_array[] = $login;
            }
        }
    }
    return $return_array;
}

/*
 * Returns all logins for a given gateway.
 *
 * If returnStringArray is true, it will return a STRING ARRAY of all logins for the given gateway.
 *
 * If returnStringArray is false, it will return an ARRAY of ARRAYS of all logins (and passwords/gateways) for a given gateway.
 */
function getLoginFromGateway($gateway, $returnStringArray)
{
    $mid_login = getMidLogins();
    $return_array = array();

    foreach($mid_login as $login)
    {
        if($login['gateway'] == $gateway)
        {
            if($returnStringArray)
            {
                $return_array[] = $login["login"];
            }
            else
            {
                $return_array[] = $login;
            }
        }
    }
    return $return_array;
}
?>