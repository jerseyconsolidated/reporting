<?php
include ("affiliate_reports_functions.php");
require('../includes/auth_api/config.inc.php');
require('../includes/auth_api/AuthnetXML.class.php');
require('../includes/general_functions_all.php');
require('general_functions.php');
require('passwords.php');
set_time_limit(0);
ignore_user_abort(1);
date_default_timezone_set('UTC');

if (defined('STDIN'))
{
	$transaction_id = $argv[1];
	$login = $argv[2];
	$gateway_id = $argv[3];
}
else
{
	databaseClicksLog("standard input failed");
	die("STDIN failed");
}

	$mysqli = initialize_db(getDBName());

	$pass = getPasswordFromLogin($login, $gateway_id, true);
	try
	{
		$xml2 = new AuthnetXML(AUTHNET_LOGIN, AUTHNET_TRANSKEY, AuthnetXML::USE_PRODUCTION_SERVER);
		$xml2->getTransactionDetailsRequest(array(
			'transId' =>$transaction_id
		));
	}
	catch (AuthnetXMLException $e)
	{
		echo $e;
		exit;
	}
	/*
	 * this shit is out of order according to the json thing...... so that makes checking it annoying.
	 *
	 * Also, the insert_into stuff lines up with the values %s things. That makes it easier to count them.
	 */
	if(!isset($xml2->transaction->lineItems->lineItem->itemId))
	{
		$xml2->transaction->lineItems->lineItem->itemId = 0;
		$xml2->transaction->lineItems->lineItem->name = 0;
		$xml2->transaction->lineItems->lineItem->description = 0;
		$xml2->transaction->lineItems->lineItem->quantity = 0;
		$xml2->transaction->lineItems->lineItem->unitPrice = 0;
		$xml2->transaction->lineItems->lineItem->taxable = 0;
	}

	$insertSQL = sprintf("INSERT INTO auth_trans_detail
	(username, password, gateway_id,transId,submitTimeUTC,submitTimeLocal,transactionType,transactionStatus,responseCode,
	responseReasonCode,responseReasonDescription,AVSResponse,cardCodeResponse,batch_batchId,batch_settlementTimeUTC,
	batch_settlementTimeLocal,batch_settlementState,order_invoiceNumber,order_description,authAmount,settleAmount,taxExempt,
	payment_creditCard,payment_expirationDate,payment_cardType,customer_id,customer_email,bill_firstName,bill_lastName,bill_address,
	bill_city,bill_state,bill_zip,bill_country,bill_phoneNumber,ship_firstName,ship_lastName,ship_address,ship_city,ship_state,
	ship_zip,ship_country,recurringBilling,ip_address,product,marketType,line_item_id,line_item_name,line_item_description,line_item_quantity,
	line_item_unit_price,line_item_taxable)
	VALUES
	(
	%s,%s,%s,%s,%s,%s,%s,%s,%s,
	%s,%s,%s,%s,%s,%s,
	%s,%s,%s,%s,%s,%s,%s,
	%s,%s,%s,%s,%s,%s,%s,%s,
	%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
	%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
	%s,%s
	)",
		GetSQLValueString($login, "text", $mysqli),
		GetSQLValueString($pass, "text", $mysqli),
		GetSQLValueString($gateway_id, "text", $mysqli),
		GetSQLValueString($xml2->transaction->transId, "text", $mysqli),
		GetSQLValueString($xml2->transaction->submitTimeUTC, "text", $mysqli),
		GetSQLValueString($xml2->transaction->submitTimeLocal, "text", $mysqli),
		GetSQLValueString($xml2->transaction->transactionType, "text", $mysqli),
		GetSQLValueString($xml2->transaction->transactionStatus, "text", $mysqli),
		GetSQLValueString($xml2->transaction->responseCode, "text", $mysqli),
		GetSQLValueString($xml2->transaction->responseReasonCode, "text", $mysqli),
		GetSQLValueString($xml2->transaction->responseReasonDescription, "text", $mysqli),
		//The json response doesn't have anything called authCode that I could find.....
		//GetSQLValueString($xml2->transaction->authCode, "text", $mysqli),
		GetSQLValueString($xml2->transaction->AVSResponse, "text", $mysqli),
		GetSQLValueString($xml2->transaction->cardCodeResponse, "text", $mysqli),
		GetSQLValueString($xml2->transaction->batch->batchId, "text", $mysqli),
		GetSQLValueString($xml2->transaction->batch->settlementTimeUTC, "text", $mysqli),
		GetSQLValueString($xml2->transaction->batch->settlementTimeLocal, "text", $mysqli),
		GetSQLValueString($xml2->transaction->batch->settlementState, "text", $mysqli),
		GetSQLValueString($xml2->transaction->order->invoiceNumber, "text", $mysqli),
		GetSQLValueString($xml2->transaction->order->description, "text", $mysqli),
		GetSQLValueString($xml2->transaction->authAmount, "text", $mysqli),
		GetSQLValueString($xml2->transaction->settleAmount, "text", $mysqli),
		GetSQLValueString($xml2->transaction->taxExempt, "text", $mysqli),
		GetSQLValueString($xml2->transaction->payment->creditCard->cardNumber, "text", $mysqli),
		GetSQLValueString($xml2->transaction->payment->creditCard->expirationDate, "text", $mysqli),
		GetSQLValueString($xml2->transaction->payment->creditCard->cardType, "text", $mysqli),
		GetSQLValueString($xml2->transaction->customer->id, "text", $mysqli),
		GetSQLValueString($xml2->transaction->customer->email, "text", $mysqli),
		GetSQLValueString($xml2->transaction->billTo->firstName, "text", $mysqli),
		GetSQLValueString($xml2->transaction->billTo->lastName, "text", $mysqli),
		GetSQLValueString($xml2->transaction->billTo->address, "text", $mysqli),
		GetSQLValueString($xml2->transaction->billTo->city, "text", $mysqli),
		GetSQLValueString($xml2->transaction->billTo->state, "text", $mysqli),
		GetSQLValueString($xml2->transaction->billTo->zip, "text", $mysqli),
		GetSQLValueString($xml2->transaction->billTo->country, "text", $mysqli),
		GetSQLValueString($xml2->transaction->billTo->phoneNumber, "text", $mysqli),
		GetSQLValueString($xml2->transaction->shipTo->firstName, "text", $mysqli),
		GetSQLValueString($xml2->transaction->shipTo->lastName, "text", $mysqli),
		GetSQLValueString($xml2->transaction->shipTo->address, "text", $mysqli),
		GetSQLValueString($xml2->transaction->shipTo->city, "text", $mysqli),
		GetSQLValueString($xml2->transaction->shipTo->state, "text", $mysqli),
		GetSQLValueString($xml2->transaction->shipTo->zip, "text", $mysqli),
		GetSQLValueString($xml2->transaction->shipTo->country, "text", $mysqli),
		GetSQLValueString($xml2->transaction->recurringBilling, "text", $mysqli),
		GetSQLValueString($xml2->transaction->customerIP, "text", $mysqli),
		GetSQLValueString($xml2->transaction->product, "text", $mysqli),
		GetSQLValueString($xml2->transaction->marketType, "text", $mysqli),
		GetSQLValueString($xml2->transaction->lineItems->lineItem->itemId, "text", $mysqli),
		GetSQLValueString($xml2->transaction->lineItems->lineItem->name, "text", $mysqli),
		GetSQLValueString($xml2->transaction->lineItems->lineItem->description, "text", $mysqli),
		GetSQLValueString($xml2->transaction->lineItems->lineItem->quantity, "text", $mysqli),
		GetSQLValueString($xml2->transaction->lineItems->lineItem->unitPrice, "text", $mysqli),
		GetSQLValueString($xml2->transaction->lineItems->lineItem->taxable, "text", $mysqli)
	);
	doDBthing($insertSQL, getDBName());
?>