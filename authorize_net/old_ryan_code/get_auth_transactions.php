<?php
$mysqli = initialize_db(getDBName());
date_default_timezone_set('America/New_York');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require('../includes/RayGun/RayGun.php');
set_time_limit(0);
include("affiliate_reports_functions.php");
//include ("bio_db.php");
require('../includes/auth_api/AuthnetXML.class.php');
require('general_functions.php');
require('../includes/general_functions_all.php');
date_default_timezone_set('UTC');

if (defined('STDIN'))
{
	  $gateway_id = $argv[1];
	  $login = $argv[2];
	  $pass = $argv[3];
	  $start = $argv[4];
	  $end = $argv[5];
	  $month_year = $argv[6];
}
else
{
	databaseClicksLog("standard input failed");
	//logIt("STDIN failed");
	die("STDIN failed");
}
	define('AUTHNET_LOGIN', $login);
	define('AUTHNET_TRANSKEY', $pass);
	
    if (!function_exists('curl_init'))
    {
        throw new Exception('CURL PHP extension not installed');
    }

    if (!function_exists('simplexml_load_file'))
    {
        throw new Exception('SimpleXML PHP extension not installed');
    }

    ///////////////////////////////////////////////////////////////////////////////
	 try
    {
        $xml = new AuthnetXML(AUTHNET_LOGIN, AUTHNET_TRANSKEY, AuthnetXML::USE_PRODUCTION_SERVER);
        $xml->getSettledBatchListRequest(array(
            'includeStatistics' => 'true',
            'firstSettlementDate' => $start,
            'lastSettlementDate' => $end,
        ));
    }
    catch (AuthnetXMLException $e)
    {
        echo $e;
        exit;
    }
if ($xml->messages->message->code == 'I00001')
{
	$arrayData = xmlToArray($xml->batchList);
	$array_good = json_encode($arrayData);
	$array_good = json_decode($array_good, true);
	$number_of_batches = count($array_good['batchList']['batch']);

	for ($b = 0; $b < $number_of_batches; $b++)
	{

		$batchId = $array_good['batchList']['batch'][$b]['batchId'];
		$insertSQL = sprintf("INSERT INTO auth_batch_list (username, password, gateway_id, batchId, month_year) VALUES (%s, %s, %s, %s, %s)",
		GetSQLValueString($login, "text", $mysqli),
		GetSQLValueString($pass, "text", $mysqli),
		GetSQLValueString($gateway_id, "text", $mysqli),
		GetSQLValueString($batchId, "text", $mysqli),
		GetSQLValueString($month_year, "text", $mysqli));
		$Result1 = mysql_query($insertSQL, $bio_db) or die(mysql_error());
		}
}
//else { $fh = fopen('a+','/home/biohealt/public_html/reports/authorize_net/log.txt'); fwrite($fh,print_r($xml,true)); }

		//////////////////////////////////////////GET TRANSACTIONS FROM BATCH IDS/////////////////////////////////////
		        mysql_select_db($database_bio_db, $bio_db);
             $query_find_batch_id = "SELECT distinct batchId, gateway_id FROM auth_batch_list where gateway_id = '".$gateway_id."' AND month_year = '".$month_year."'";
             $find_batch_id = mysql_query($query_find_batch_id, $bio_db) or die(mysql_error());
             $row_find_batch_id = mysql_fetch_assoc($find_batch_id);
             $totalRows_find_batch_ids = mysql_num_rows($find_batch_id);

		   
do
{
    try
    {
        $xml = new AuthnetXML(AUTHNET_LOGIN, AUTHNET_TRANSKEY, AuthnetXML::USE_PRODUCTION_SERVER);
        $xml->getTransactionListRequest(array(
            'batchId' => $row_find_batch_id['batchId']
        ));
    }
    catch (AuthnetXMLException $e)
    {
        echo $e;
        exit;
    }
    
foreach ($xml->transactions->transaction as $transaction)
    {
	    
$insertSQL = sprintf("INSERT INTO auth_trans_in_batch (username, password, gateway_id, transId, submitTimeUTC, submitTimeLocal, month_year) VALUES (%s, %s, %s, %s, %s, %s, %s)",
	GetSQLValueString($login, "text", $mysqli),
	GetSQLValueString($pass, "text", $mysqli),
	GetSQLValueString($gateway_id, "text", $mysqli),
	GetSQLValueString($transaction->transId, "text", $mysqli),
	GetSQLValueString($transaction->submitTimeUTC, "text", $mysqli),
	GetSQLValueString($transaction->submitTimeLocal, "text", $mysqli),
	GetSQLValueString($month_year, "text", $mysqli));
mysql_select_db($database_bio_db, $bio_db);
$Result1 = mysql_query($insertSQL, $bio_db) or die(mysql_error());

  	  }
}
while ($row_find_batch_id = mysql_fetch_assoc($find_batch_id));

///////////////////////////////////////////GET TRANSACTION DETAILS//////////////////////////////////////
             mysql_select_db($database_bio_db, $bio_db);
             $query_find_transactions = "SELECT distinct transId, gateway_id, month_year FROM auth_trans_in_batch WHERE gateway_id = '".$gateway_id."' and month_year = '".$month_year."'";
             $find_transactions = mysql_query($query_find_transactions, $bio_db) or die(mysql_error());
             $row_find_transactions = mysql_fetch_assoc($find_transactions);
             $totalRows_find_transactions = mysql_num_rows($find_transactions);
//echo $row_find_transactions['transId']."<BR>";
do
{
    try
    {
        $xml = new AuthnetXML(AUTHNET_LOGIN, AUTHNET_TRANSKEY, AuthnetXML::USE_PRODUCTION_SERVER);
        $xml->getTransactionDetailsRequest(array(
            'transId' => $row_find_transactions['transId']
        ));
    }
    catch (AuthnetXMLException $e)
    {
        echo $e;
        exit;
    }
    
//foreach ($xml->transactions->transaction as $transaction) {
//$fh = fopen('a+','/home/biohealt/public_html/reports/authorize_net/log.txt'); fwrite($fh,print_r($xml,true));
$insertSQL = sprintf("INSERT INTO auth_trans_detail (username, password, gateway_id,transId,submitTimeUTC,submitTimeLocal,transactionType,transactionStatus,responseCode,responseReasonCode,responseReasonDescription,authCode,AVSResponse,cardCodeResponse,batch_batchId,batch_settlementTimeUTC,batch_settlementTimeLocal,batch_settlementState,order_invoiceNumber,order_description,authAmount,settleAmount,taxExempt,payment_creditCard,payment_expirationDate,payment_cardType,customer_id,customer_email,bill_firstName,bill_lastName,bill_address,bill_city,bill_state,bill_zip,bill_country,bill_phoneNumber,ship_firstName,ship_lastName,ship_address,ship_city,ship_state,ship_zip,ship_country,recurringBilling,ip_address) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
	GetSQLValueString($login, "text", $mysqli),
	GetSQLValueString($pass, "text", $mysqli),
	GetSQLValueString($gateway_id, "text", $mysqli),
	GetSQLValueString($xml->transaction->transId, "text", $mysqli),
	GetSQLValueString($xml->transaction->submitTimeUTC, "text", $mysqli),
	GetSQLValueString($xml->transaction->submitTimeLocal, "text", $mysqli),
	GetSQLValueString($xml->transaction->transactionType, "text", $mysqli),
	GetSQLValueString($xml->transaction->transactionStatus, "text", $mysqli),
	GetSQLValueString($xml->transaction->responseCode, "text", $mysqli),
	GetSQLValueString($xml->transaction->responseReasonCode, "text", $mysqli),
	GetSQLValueString($xml->transaction->responseReasonDescription, "text", $mysqli),
	GetSQLValueString($xml->transaction->authCode, "text", $mysqli),
	GetSQLValueString($xml->transaction->AVSResponse, "text", $mysqli),
	GetSQLValueString($xml->transaction->cardCodeResponse, "text", $mysqli),
	GetSQLValueString($xml->transaction->batch->batchId, "text", $mysqli),
	GetSQLValueString($xml->transaction->batch->settlementTimeUTC, "text", $mysqli),
	GetSQLValueString($xml->transaction->batch->settlementTimeLocal, "text", $mysqli),
	GetSQLValueString($xml->transaction->batch->settlementState, "text", $mysqli),
	GetSQLValueString($xml->transaction->order->invoiceNumber, "text", $mysqli),
	GetSQLValueString($xml->transaction->order->description, "text", $mysqli),
	GetSQLValueString($xml->transaction->authAmount, "text", $mysqli),
	GetSQLValueString($xml->transaction->settleAmount, "text", $mysqli),
	GetSQLValueString($xml->transaction->taxExempt, "text", $mysqli),
	GetSQLValueString($xml->transaction->payment->creditCard->cardNumber, "text", $mysqli),
	GetSQLValueString($xml->transaction->payment->creditCard->expirationDate, "text", $mysqli),
	GetSQLValueString($xml->transaction->payment->creditCard->cardType, "text", $mysqli),
	GetSQLValueString($xml->transaction->customer->id, "text", $mysqli),
	GetSQLValueString($xml->transaction->customer->email, "text", $mysqli),
	GetSQLValueString($xml->transaction->billTo->firstName, "text", $mysqli),
	GetSQLValueString($xml->transaction->billTo->lastName, "text", $mysqli),
	GetSQLValueString($xml->transaction->billTo->address, "text", $mysqli),
	GetSQLValueString($xml->transaction->billTo->city, "text", $mysqli),
	GetSQLValueString($xml->transaction->billTo->state, "text", $mysqli),
	GetSQLValueString($xml->transaction->billTo->zip, "text", $mysqli),
	GetSQLValueString($xml->transaction->billTo->country, "text", $mysqli),
	GetSQLValueString($xml->transaction->billTo->phoneNumber, "text", $mysqli),
	GetSQLValueString($xml->transaction->shipTo->firstName, "text", $mysqli),
	GetSQLValueString($xml->transaction->shipTo->lastName, "text", $mysqli),
	GetSQLValueString($xml->transaction->shipTo->address, "text", $mysqli),
	GetSQLValueString($xml->transaction->shipTo->city, "text", $mysqli),
	GetSQLValueString($xml->transaction->shipTo->state, "text", $mysqli),
	GetSQLValueString($xml->transaction->shipTo->zip, "text", $mysqli),
	GetSQLValueString($xml->transaction->shipTo->country, "text", $mysqli),
	GetSQLValueString($xml->transaction->recurringBilling, "text", $mysqli),
	GetSQLValueString($xml->transaction->customerIP, "text", $mysqli));
mysql_select_db($database_bio_db, $bio_db);
$Result1 = mysql_query($insertSQL, $bio_db) or die(mysql_error());
//  	  }
}
while ($row_find_transactions = mysql_fetch_assoc($find_transactions));

?>
