<?php
include ("affiliate_reports_functions.php");
//include ("db.php");
require('../includes/auth_api/config.inc.php');
require('../includes/auth_api/AuthnetXML.class.php');
require('../includes/general_functions_all.php');
require('general_functions.php');
require('passwords.php');

date_default_timezone_set('UTC');

echo "\n";

/*
 * Initialize variables:
 */

$start_date = "2013-01-01";
$end_date   = date('Y-m-d',strtotime("-1 day"));

$start = DateTime::createFromFormat('Y-m-d',$start_date);
$end = DateTime::createFromFormat('Y-m-d',$end_date);
$interval = DateInterval::createFromDateString('30 days');
$period = new DatePeriod($start, $interval, $end);

$this_query="";
$gateways = getAllGateways();

$previous_year = $start->format('Y');
$previous_day = $start->format('d');
$previous_month = $start->format('m');

prepare_batch_list_database();
$mysqli = initialize_db('auth_net', "biohealth1", "7s63VfusbHVFuq");

foreach ($period as $dt)
{
	$year = $dt->format('Y');
	$day = $dt->format("d");
	$month = $dt -> format("m");

	$second_date  = $year."-".$month."-".$day."T00:00:01";
	$first_date = $previous_year."-".$previous_month."-".$previous_day."T00:00:01";

	try
	{
		$xml = new AuthnetXML(AUTHNET_LOGIN, AUTHNET_TRANSKEY, AuthnetXML::USE_PRODUCTION_SERVER);
		$xml->getSettledBatchListRequest
		(
			array
			(
				'includeStatistics' => 'true',
				'firstSettlementDate' => $first_date,
				'lastSettlementDate' => $second_date,
			)
		);
	}
	catch (AuthnetXMLException $e)
	{
		echo $e;
		die();
	}
	/*
	 * Check if there's data:
	 */
	if ($xml->messages->message->code == 'I00001')
	{
		$arrayData = xmlToArray($xml->batchList);
		$array_good = json_encode($arrayData);
		$array_good = json_decode($array_good, true);
		/*
		 * For all the batch data
		 */
		for ($b = 0;$b<count($array_good['batchList']['batch']);$b++)
		{
			$batchId = $array_good['batchList']['batch'][$b]['batchId'];
			$settlementTimeUTC = $array_good['batchList']['batch'][$b]['settlementTimeUTC'];
			$settlementTimeLocal = $array_good['batchList']['batch'][$b]['settlementTimeLocal'];
			$settlementState = $array_good['batchList']['batch'][$b]['settlementState'];
			$paymentMethod = $array_good['batchList']['batch'][$b]['paymentMethod'];
			$insertSQL = sprintf("
			INSERT INTO auth_batch_list
			(batch_id,settlementTimeUTC,settlementTimeLocal,settlementState,paymentMethod)
			VALUES (%s,%s,%s,%s,%s)",
				GetSQLValueString($batchId, "text", $mysqli),
				GetSQLValueString($settlementTimeUTC, "text", $mysqli),
				GetSQLValueString($settlementTimeLocal, "text", $mysqli),
				GetSQLValueString($settlementState, "text", $mysqli),
				GetSQLValueString($paymentMethod, "text", $mysqli)
			);
			doDBthing($insertSQL, 'auth_net');
			/*
			 * For all the statistics data (if there even is any)
			 */
			if(isset($array_good['batchList']['batch'][$b]['statistics']['statistic']))
			{
				$total_stats = count($array_good['batchList']['batch'][$b]['statistics']['statistic']);
				for ($c = 0;$c <$total_stats; $c++)
				{
					/*
					 * For some really weird / stupid reason, if there is only 1 'statistic' for a given batch ID,
					 * it will NOT put it in an array of 1, it will just totally list it as an array of 8 values.
					 * As in, for only one 'statistic', the array looks like this:
					 * ["statistics"]=>
						array(1)
						{
						  ["statistic"]=>
						  array(8)
							{
								["accountType"]=>
								string(4) "Visa"
								["chargeAmount"]=>
								string(6) "148.00"
								["chargeCount"]=>
								string(1) "1"
								["refundAmount"]=>
								string(4) "0.00"
								["refundCount"]=>
								string(1) "0"
								["voidCount"]=>
								string(1) "0"
								["declineCount"]=>
								string(1) "0"
								["errorCount"]=>
								string(1) "0"
						  	}
						}

					 * HOWEVER!!! If there is 2, then it incloses it (properly, I might add) into another array, like this:
					 * ["statistics"]=>
						array(1)
						{
						  ["statistic"]=>
						  array(2)
						{
							[0]=>
							array(8)
					 		{
							  ["accountType"]=>
							  string(10) "MasterCard"
							  ["chargeAmount"]=>
							  string(5) "98.00"
							  ["chargeCount"]=>
							  string(1) "1"
							  ["refundAmount"]=>
							  string(4) "0.00"
							  ["refundCount"]=>
							  string(1) "0"
							  ["voidCount"]=>
							  string(1) "0"
							  ["declineCount"]=>
							  string(1) "0"
							  ["errorCount"]=>
							  string(1) "0"
							}
							[1]=>
							array(8)
							{
							  ["accountType"]=>
							  string(4) "Visa"
							  ["chargeAmount"]=>
							  string(6) "213.90"
							  ["chargeCount"]=>
							  string(1) "3"
							  ["refundAmount"]=>
							  string(4) "0.00"
							  ["refundCount"]=>
							  string(1) "0"
							  ["voidCount"]=>
							  string(1) "0"
							  ["declineCount"]=>
							  string(1) "2"
							  ["errorCount"]=>
							  string(1) "0"
							}
						  }
						}
					  }

					*
					 * SO! in conclusion. If there is only one statistic, the 'total_stats' will be exactly 8. If there is NOT 8 'stats'
					 * then we can be sure that there are actually multiple 'statistic' fields, and that we should loop through them.
					 * Notice how if there's 8, we immediately set the counter to 9 so that it doesn't loop again, because it's done.
					 */
					if($total_stats == 8)
					{
						//echo "SINGLE batchID $batchId";
						$accountType = $array_good['batchList']['batch'][$b]['statistics']['statistic']['accountType'];
						$chargeAmount = $array_good['batchList']['batch'][$b]['statistics']['statistic']['chargeAmount'];
						$chargeCount = $array_good['batchList']['batch'][$b]['statistics']['statistic']['chargeCount'];
						$refundAmount = $array_good['batchList']['batch'][$b]['statistics']['statistic']['refundAmount'];
						$refundCount = $array_good['batchList']['batch'][$b]['statistics']['statistic']['refundCount'];
						$voidCount = $array_good['batchList']['batch'][$b]['statistics']['statistic']['voidCount'];
						$declineCount = $array_good['batchList']['batch'][$b]['statistics']['statistic']['declineCount'];
						$errorCount = $array_good['batchList']['batch'][$b]['statistics']['statistic']['errorCount'];
						$c =9;
					}
					else
					{
						$accountType = $array_good['batchList']['batch'][$b]['statistics']['statistic'][$c]['accountType'];
						$chargeAmount = $array_good['batchList']['batch'][$b]['statistics']['statistic'][$c]['chargeAmount'];
						$chargeCount = $array_good['batchList']['batch'][$b]['statistics']['statistic'][$c]['chargeCount'];
						$refundAmount = $array_good['batchList']['batch'][$b]['statistics']['statistic'][$c]['refundAmount'];
						$refundCount = $array_good['batchList']['batch'][$b]['statistics']['statistic'][$c]['refundCount'];
						$voidCount = $array_good['batchList']['batch'][$b]['statistics']['statistic'][$c]['voidCount'];
						$declineCount = $array_good['batchList']['batch'][$b]['statistics']['statistic'][$c]['declineCount'];
						$errorCount = $array_good['batchList']['batch'][$b]['statistics']['statistic'][$c]['errorCount'];
					}
					$insertSQL = sprintf("
					INSERT INTO auth_statistics
					(batch_id,accountType,chargeAmount,chargeCount,refundAmount,refundCount,voidCount,declineCount,errorCount)
					VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)",
					GetSQLValueString($batchId, "text", $mysqli),
					GetSQLValueString($accountType, "text", $mysqli),
					GetSQLValueString($chargeAmount, "text", $mysqli),
					GetSQLValueString($chargeCount, "text", $mysqli),
					GetSQLValueString($refundAmount, "text", $mysqli),
					GetSQLValueString($refundCount, "text", $mysqli),
					GetSQLValueString($voidCount, "text", $mysqli),
					GetSQLValueString($declineCount, "text", $mysqli),
					GetSQLValueString($errorCount, "text", $mysqli)
						);
					doDBthing($insertSQL, 'auth_net');
				}
			}
		}
	}
	$previous_day = $day;
	$previous_month = $month;
	$previous_year = $year;
}

?>

