<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require ('../includes/RayGun/RayGun.php');
set_time_limit(0);
include ("affiliate_reports_functions.php");
require('../includes/auth_api/AuthnetXML.class.php');
require('general_functions.php');
require('../includes/general_functions_all.php');
require('passwords.php');
date_default_timezone_set('UTC');
echo "\n";

prepare_transactions_database();

$mysqli = initialize_db(getDBName());

$accounts = getMidLogins();
/*
 * foreach account.....
 */
foreach($accounts as $account)
{
	$login = $account['login'];
	$pass = $account['pass'];
	$gateway = $account['gateway'];

//	define('AUTHNET_LOGIN', $login);
//	define('AUTHNET_TRANSKEY', $pass);
	
    if (!function_exists('curl_init'))
    {
        throw new Exception('CURL PHP extension not installed');
    }
    if (!function_exists('simplexml_load_file'))
    {
        throw new Exception('SimpleXML PHP extension not installed');
    }

	/*
	 * First, get the transaction list.... apparently we need batchIDs for this?
	 */
	$batches = getBatchIDs();
	foreach($batches as $batch)
	{
		try
		{
			$xml = new AuthnetXML($login, $pass, AuthnetXML::USE_PRODUCTION_SERVER);
			$xml->getTransactionListRequest(array(
				'batchId' => $batch
			));
		}
		catch (AuthnetXMLException $e)
		{
			echo $e;
			exit;
		}
		/*
		 * For each transaction, get the details of it and DB it
		 */
		foreach ($xml->transactions->transaction as $transaction)
		{
			$insertSQL = sprintf("INSERT INTO auth_trans_in_batch
	(transId, submitTimeUTC, submitTimeLocal, transactionStatus, invoiceNumber, firstName, lastName, accountType, accountNumber, settleAmount, marketType, product, username, password, gateway_id, month_year)
	VALUES (%s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s)",

			GetSQLValueString($transaction->transId, "text", $mysqli),
			GetSQLValueString($transaction->submitTimeUTC, "text", $mysqli),
			GetSQLValueString($transaction->submitTimeLocal, "text", $mysqli),
			GetSQLValueString($transaction->transactionStatus, "text", $mysqli),
			GetSQLValueString($transaction->invoiceNumber, "text", $mysqli),
			GetSQLValueString($transaction->firstName, "text", $mysqli),
			GetSQLValueString($transaction->lastName, "text", $mysqli),
			GetSQLValueString($transaction->accountType, "text", $mysqli),
			GetSQLValueString($transaction->accountNumber, "text", $mysqli),
			GetSQLValueString($transaction->settleAmount, "text", $mysqli),
			GetSQLValueString($transaction->marketType, "text", $mysqli),
			GetSQLValueString($transaction->product, "text", $mysqli),
			GetSQLValueString($login, "text", $mysqli),
			GetSQLValueString($pass, "text", $mysqli),
			GetSQLValueString($gateway, "text", $mysqli), //used to be "$gateway_id"
			GetSQLValueString("0", "text", $mysqli)); //used to be "$month_year" or something like that
		doDBthing($insertSQL, getDBName());
			/*
			 * Start the new threads for the tran details here:*
			 */
			//$php_command        = "php /Users/jonjenne/reporting/authorize_net/thread_auth_transactions_jon.php $transaction->transId $login $gateway  &";
			$php_command        = "php /var/www/reporting.jcoffice.net/authorize_net/thread_auth_transactions_jon.php $transaction->transId $login $gateway  &";
			//$php_command      = "php /var/www/reporting.jcoffice.net/meritus/doing_meritus_pull.php";

			$result = pclose(popen($php_command, "r"));
			usleep(150000);
		}
	}
}

function getBatchIds()
{
	$mysqli = initialize_db(getDBName(), "biohealth2", "JaG3HMA86HxQc");

	$query = "SELECT `batch_id` FROM `auth_batch_list`";
	$query_result = $mysqli->prepare("$query");
	$query_result->execute(); // why is this failing?
	$query_result->bind_result($batch_id);

	$arr = array();
	$index=0;
	while ($query_result->fetch())
	{
		$arr[$index] = $batch_id;
		$index++;
	}
	//var_dump($arr);
	//die();
	$mysqli->close();
	return $arr;
}
?>
