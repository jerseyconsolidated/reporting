<?php

if (isset($_GET['phpinfo'])) {
    echo phpinfo();
}
/*
 * Some standard functions:
 */
function getDBName()
{
	return "auth_net";
}
/*
 * Truncate the database(s)
 */
function prepare_batch_list_database()
{
	$mysqli = initialize_db(getDBName());

	if ($mysqli->connect_error)
	{
		logIt("Failed to connect to MySQL. Unable to Truncate: " .$mysqli->connect_error);
		die();
	}
	$mysqli->query("TRUNCATE TABLE auth_batch_list");
	$mysqli->query("TRUNCATE TABLE auth_statistics");
	$mysqli->close();
}
/*
 * Truncate the database(s)
 */
function prepare_transactions_database()
{
	$mysqli = initialize_db(getDBName());

	if ($mysqli->connect_error)
	{
		logIt("Failed to connect to MySQL. Unable to Truncate: " .$mysqli->connect_error);
		die();
	}
	$mysqli->query("TRUNCATE TABLE auth_trans_detail");
	$mysqli->query("TRUNCATE TABLE auth_trans_in_batch");
	$mysqli->close();
}

function GetSQLValueString($theValue, $theType, $mysqli, $theDefinedValue = "", $theNotDefinedValue = "")
{
    $theValue = $mysqli->real_escape_string($theValue);
    switch ($theType) {
        case "text":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "long":
        case "int":
            $theValue = ($theValue != "") ? intval($theValue) : "NULL";
            break;
        case "double":
            $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
            break;
        case "date":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "defined":
            $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
            break;
    }
    return $theValue;
}
function initialize_db($mysqli, $username="biohealth1", $password="7s63VfusbHVFuq")
{
    //open up our connection
    $mysqli = new mysqli("localhost",$username,$password,$mysqli);
    // Check connection
    if (mysqli_connect_errno())
	{
		echo $mysqli->error;
        exit();
    }
    return $mysqli;
}
function xmlToArray($xml, $options = array())
{
	$defaults = array(
		'namespaceSeparator' => ':',//you may want this to be something other than a colon
		'attributePrefix' => '@',   //to distinguish between attributes and nodes with the same name
		'alwaysArray' => array(),   //array of xml tag names which should always become arrays
		'autoArray' => true,        //only create arrays for tags which appear more than once
		'textContent' => '$',       //key used for the text content of elements
		'autoText' => true,         //skip textContent key if node has no attributes or child nodes
		'keySearch' => false,       //optional search and replace on tag and attribute names
		'keyReplace' => false       //replace values for above search values (as passed to str_replace())
	);

	$options = array_merge($defaults, $options);
	$namespaces = $xml->getDocNamespaces();
	$namespaces[''] = null; //add base (empty) namespace

	//get attributes from all namespaces
	$attributesArray = array();
	foreach ($namespaces as $prefix => $namespace)
	{
		foreach ($xml->attributes($namespace) as $attributeName => $attribute)
		{
			//replace characters in attribute name
			if ($options['keySearch']) $attributeName =
				str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
			$attributeKey = $options['attributePrefix']
				. ($prefix ? $prefix . $options['namespaceSeparator'] : '')
				. $attributeName;
			$attributesArray[$attributeKey] = (string)$attribute;
		}
	}

	//get child nodes from all namespaces
	$tagsArray = array();
	foreach ($namespaces as $prefix => $namespace)
	{
		foreach ($xml->children($namespace) as $childXml)
		{
			//recurse into child nodes
			$childArray = xmlToArray($childXml, $options);
			list($childTagName, $childProperties) = each($childArray);

			//replace characters in tag name
			if ($options['keySearch']) $childTagName =
				str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
			//add namespace prefix, if any
			if ($prefix) $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;

			if (!isset($tagsArray[$childTagName]))
			{
				//only entry with this key
				//test if tags of this type should always be arrays, no matter the element count
				$tagsArray[$childTagName] =
					in_array($childTagName, $options['alwaysArray']) || !$options['autoArray']
						? array($childProperties) : $childProperties;
			}
			elseif (is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName]) === range(0, count($tagsArray[$childTagName]) - 1))
			{
				//key already exists and is integer indexed array
				$tagsArray[$childTagName][] = $childProperties;
			}
			else
			{
				//key exists so convert to integer indexed array with previous value in position 0
				$tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
			}
		}
	}

	//get text content of node
	$textContentArray = array();
	$plainText = trim((string)$xml);
	if ($plainText !== '') $textContentArray[$options['textContent']] = $plainText;

	//stick it all together
	$propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
		? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;

	//return node as array
	return array($xml->getName() => $propertiesArray);
}


