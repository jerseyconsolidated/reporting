<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml">
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"
                media-type="application/xhtml+xml" encoding="iso-8859-1"
                doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
                doctype-system=
                        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
    <xsl:template match="/">
        <html>
            <head>
                <title><xsl:value-of select="/chapter/title"/></title>
                <meta http-equiv="content-type"
                      content="application/xhtml+xml; charset=iso-8859-1"/>
            </head>
            <body>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="title">
        <h1><xsl:apply-templates/></h1>
    </xsl:template>
    <xsl:template match="para">
        <p><xsl:apply-templates/></p>
    </xsl:template>
    <xsl:template match="b">
        <b><xsl:apply-templates/></b>
    </xsl:template>

    <xsl:template match="chapter/title">
        <h1><xsl:apply-templates/></h1>
    </xsl:template>
    <xsl:template match="chapter/section/title">
        <h2><xsl:apply-templates/></h2>
    </xsl:template>
    <xsl:template match="chapter/section/section/title">
        <h3><xsl:apply-templates/></h3>
    </xsl:template>
    <xsl:template match="chapter/section/section/section/title">
        <h4><xsl:apply-templates/></h4>
    </xsl:template>

    <xsl:template match="para[@type='intro']" priority="1">
        <p><i><xsl:apply-templates/></i></p>
    </xsl:template>

    <xsl:template match="para[@type='warning']" priority="1">
        <p style="background-color: #cccccc; border: thin solid;
 width:300px; color:#ff0000;">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="para[@type='note']" priority="1">
        <p style="background-color: #cccccc; border: thin solid;
 width:300px;">
            <b><xsl:apply-templates/></b>
        </p>
    </xsl:template>
</xsl:stylesheet>