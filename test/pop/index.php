<html>
<head>
    <style>
        body{
            font: 12px Verdana, sans-serif;
        }
        #notes {
            float: left;
            width: 100%;
            padding-right: 2%;
        }
        .customer_note {
            padding-bottom: 10px;
        }
        .indent_notes {
            margin-left: 2%;
        }
    </style>
</head>
<body>
<?php
include 'pop_includes.php';

$customers = array();
$order_ids = array();
$tracking_numbers = array();
$all_notes = array();


$no_result = "noresult";
$cusomter_id = $_GET['customer_id'];
//$phone = '3866750358';

$customer = new find_local();
$customer_id_email = $customer->customer_ids_email_from_phone($cusomter_id);
if ($customer_id_email->num_rows > 0) {
    while ($customer_row = $customer_id_email->fetch_assoc()) {

        $customers[] = array('customer_id' => $customer_row['customer_id'],
            'customer_email' => $customer_row['email_address']);
        ////
        //get orders associated
        ///
        $orders = new find_local();
        $order_num_trk = $orders->tracking_from_customer_id($customer_row['customer_id']);
        if ($order_num_trk->num_rows > 0) {
            while ($order_row = $order_num_trk->fetch_assoc()) {
                $order_ids[] = array('customer_id' => $customer_row['customer_id'],
                    'order_id' => $order_row['order_id']);
                if (isset($order_row['tracking_number'])) {
                    $tracking_numbers[] = array('order_id' => $order_row['order_id'],
                        'tracking_number' => $order_row['tracking_number']);
                }
                /////
                ///get employee  notes associated
                /////
                $notes = new find_local();
                $notes_result = $notes->notes_from_order_id($order_row['order_id'], 'employee');
                if ($notes_result->num_rows > 0) {
                    while ($notes_row = $notes_result->fetch_assoc()) {
                        if (!isset($all_notes[$customer_row['email_address']])) {
                            $all_notes[$customer_row['email_address']] = array();
                        }
                        if (!isset($all_notes[$customer_row['email_address']][$order_row['order_id']])) {
                            $all_notes[$customer_row['email_address']][$order_row['order_id']] = array('system' => array(), 'employee' => array());
                        }

                        $all_notes[$customer_row['email_address']][$order_row['order_id']]['employee'][] = array(
                            'employee_note' => $notes_row['employee_note'],
                            'note_date' => $notes_row['note_date']
                        );
                    }
                }
                $notes = new find_local();
                $notes_result = $notes->notes_from_order_id($order_row['order_id'], 'system');
                if ($notes_result->num_rows > 0) {
                    while ($notes_row = $notes_result->fetch_assoc()) {
                        if (!isset($all_notes[$customer_row['email_address']])) {
                            $all_notes[$customer_row['email_address']] = array();
                        }
                        if (!isset($all_notes[$customer_row['email_address']][$order_row['order_id']])) {
                            $all_notes[$customer_row['email_address']][$order_row['order_id']] = array('system' => array(), 'employee' => array());
                        }

                        $all_notes[$customer_row['email_address']][$order_row['order_id']]['system'][] = array(
                            'system_note' => $notes_row['system_note'],
                            'note_date' => $notes_row['note_date']
                        );
                    }
                }
            }
        }
    }
} else {
    echo $no_result;
}
/*
print_r($customers); //1:1 customer_id, customer_email
print_r($order_ids); //1:many customer_id, order_id
print_r($tracking_numbers); //1:1 order_id, tracking_number
print_r($notes_employee); //1:1 customer_email, order_id, employee_note, note_date
print_r($notes_system); //1:1 customer_email, order_id, system_note, note_date
*/
?>
<div id="notes">
    All Notes<br/>
    <?php
    foreach ($customers as $customer) {
    ?>
    <div class="customer_note">
        <?php
        echo "<b>Customer Email: </b>" . $customer['customer_email'] . "<br />";
        foreach ($all_notes[$customer['customer_email']] as $order_id => $the_note) {
            echo "<br /><b>Order ID: </b>" . $order_id . "<br />";
            ?>
            <div class="indent_notes">
                <?php
                foreach ($the_note['employee'] as $note) {
                    echo "<b>Employee Note: </b>" . $note['employee_note'] . "<br />";
                }
                foreach ($the_note['system'] as $note) {
                    echo "<b>System Note: </b>" . $note['system_note'] . "<br />";
                }
                ?>
            </div>
        <?php
        }

        }
            ?>
</div>
</body>
</html>