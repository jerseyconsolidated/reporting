function ToggleActions(obj)
{
   if (obj.value != '')
   {
      $('.test_form').hide();
      $('#' + obj.value).show();
      $('#' + obj.value + 'password').val($('#password').val());
      $('#' + obj.value + 'username').val($('#username').val());
      $('#' + obj.value + '_site').val($('#site').val());
   }
}

function UpdateAttributes(parent)
{
   var prod_id = '',
       attr_vals = {},
       opt_vals  = {},
       all_attrs = '';

   $('.prod-attr').each(function()
   {
      prod_id            = $(this).attr('name').replace(/[^0-9]+/, '');
      attr_vals[prod_id] = $(this).val();
      opt_vals[prod_id]  = $("input[name='prod_opt_" + prod_id + "']").val();
   });

   $.each(attr_vals, function (prod_id, vals)
   {
      var values   = vals.split(',');
      var num_vals = values.length;
      var opts     = opt_vals[prod_id].split(',');

      for (i = 0; i < num_vals; i++)
      {
         the_attr = values[i];
         the_opt  = opts[i];

         if (typeof(the_attr) != 'undefined' && the_attr != '' && typeof(the_opt) != 'undefined' && the_opt != '')
         {
            all_attrs += '<input type="hidden" name="product_attribute['+prod_id+'][' + the_attr + ']" value="'+the_opt+'">';
         }
      }
   });

   $(parent + ' #product_attribs').html(all_attrs);
}

function UpdateUpsellQuantities(val, parent)
{
   var upsells_on = $('#upsellCount').val();
   var upsellHtml = '';

   if (upsells_on == 1)
   {
      var upsells = val.split(',');
      var upsellLength = upsells.length - 1;

      for(i=0;i<=upsellLength;i++)
      {
         upsellHtml += '<br/>product_qty_' + upsells[i] + '<input name="product_qty_'+upsells[i]+'" value="1">';
         upsellHtml += '<br/>product attributes ' + upsells[i] + '<input name="prod_attr_'+upsells[i]+'" class="prod-attr" value="" onblur="UpdateAttributes(\''+parent+'\');">';
         upsellHtml += 'product options ' + upsells[i] + '<input name="prod_opt_'+upsells[i]+'"  class="prod-opt" value="" onblur="UpdateAttributes(\''+parent+'\');">';
      }
   }
   return upsellHtml;
}

jQuery(document).ready(
function()
{
   ToggleActions(document.getElementById('method'));
   $('.subscriptionDropDown').change();
});