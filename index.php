<?php
include ('includes/general_functions_limelight.php');
authIP();
$header = <<<EOF
<h1>Reporting Controls</h1>
Bank/Mid Reconciliation:<BR>
-<a href="https://reporting.jcoffice.net/reports/bank/get_xero_transactions.php?action=get_transactions">Get Xero Transactions In</a> Truncates tables and gets all from this year (/reports/bank/get_xero_transactions.php?get_transactions)<BR>
-<a href="https://reporting.jcoffice.net/reports/bank/update_mid_transactions.php">Pull out all the MID transactions</a> (/reports/bank/update_mid_transactions.php)<BR>
--<a href="https://reporting.jcoffice.net/reports/bank/xero_double_check.php">Check to see if any transactions were missed</a> (/reports/bank/xero_double_check.php)

<h2>LimeLight</h2>
-<a href="http://reporting.jcoffice.net/reports/limelight/custom_reports/ptel.php">PTEL Notes Download</a><BR>
-<a href="https://reporting.jcoffice.net/reports/limelight/live_data.php?action=view">Live Action View</a>
-<a href=""></a>
-<a href=""></a>
-<a href=""></a>

<h2>NMI</h2>
-<a href="/reports/mids/nmi/nmi_2014.php">NMI</a>
EOF;
echo $header;
?>
