<?php
/**
 * Created by PhpStorm.
 * User: jonjenne
 * Date: 5/7/15
 * Time: 5:21 PM
 */

	error_reporting(E_ALL);
	ini_set("display_errors", 1);
	ini_set('memory_limit', '2048M');
	require_once('../includes/RayGun/RayGun.php');
	require('../includes/general_functions_all.php');
	require('../includes/twilio-php-master/Services/Twilio.php');
	date_default_timezone_set('UTC');
	$mysqli = initialize_db("google_accounts");

	//Does the user want to stop getting messages?
	if($_GET['Body'] == "stop")
	{
		$number = GetSQLValueString($_GET['From'], "text", $mysqli);
		doDBthing("UPDATE numbers_to_get_texts SET active = '0' WHERE number LIKE '$number'");
		die();
	}
	//Does the user want to START getting messages?
	if($_GET['Body'] == "start")
	{
		$number = GetSQLValueString($_GET['From'], "text", $mysqli);
		doDBthing("UPDATE numbers_to_get_texts SET active = '1' WHERE number LIKE '$number'");
		die();
	}

	$MessageSid = GetSQLValueString($_GET['MessageSid'], "text", $mysqli);
	$SmsSid = GetSQLValueString($_GET['SmsSid'], "text", $mysqli);
	$AccountSid = GetSQLValueString($_GET['AccountSid'], "text", $mysqli);
	$From = GetSQLValueString($_GET['From'], "text", $mysqli);
	$To = GetSQLValueString($_GET['To'], "text", $mysqli);
	$Body = GetSQLValueString($_GET['Body'], "text", $mysqli);
	$NumMedia = GetSQLValueString($_GET['NumMedia'], "text", $mysqli);
	$FromCity = GetSQLValueString($_GET['FromCity'], "text", $mysqli);
	$FromState = GetSQLValueString($_GET['FromState'], "text", $mysqli);
	$FromZip = GetSQLValueString($_GET['FromZip'], "text", $mysqli);
	$FromCountry = GetSQLValueString($_GET['FromCountry'], "text", $mysqli);
	$ToCity = GetSQLValueString($_GET['ToCity'], "text", $mysqli);
	$ToState = GetSQLValueString($_GET['ToState'], "text", $mysqli);
	$ToZip = GetSQLValueString($_GET['ToZip'], "text", $mysqli);
	$ToCountry = GetSQLValueString($_GET['ToCountry'], "text", $mysqli);
	$insert_sql =
		"INSERT INTO texts_db
(`messageSid`, `smsSid`, `accountSid`, `from`, `to`, `body`, `numMedia`, `fromCity`, `fromState`, `fromZip`, `fromCountry`, `toCity`, `toState`, `toZip`, `toCountry`)
VALUES
($MessageSid, $SmsSid, $AccountSid, $From, $To, $Body, $NumMedia, $FromCity, $FromState, $FromZip, $FromCountry, $ToCity, $ToState, $ToZip, $ToCountry)
	";
	doDBthing($insert_sql, $mysqli);

	/*
	 * Now that the message has been captured and thrown into the DB,
	 * we should blast out a message to everybody that wants to get one
	 */

	$accountsid = "AC200409e6448f5dfab81949036f74cc2b";
	$authtoken = "84f8d0d825105518afff3160337e9c11";

	//a change for change sake
	$client = new Services_Twilio($AccountSid, $AuthToken);

/*
 * Find numbers to send a text to when a text comes in:
 */
	$searchsql = 'SELECT * from numbers_to_get_texts';
	$result = mysqli_query($mysqli, $searchsql);
	//if more than 1....
	if($result->num_rows > 0)
	{
		//for each one....
		while($row = $result->fetch_row())
		{
			//get data about each number...
			$id = $row[0];
			$number = $row[1];
			$name = $row[2];
			$active = $row[3];
			//if this number is active...
			if($active)
			{
				//send the message!
				$sms = $client->account->messages->sendMessage
				(
					$From, $number, $Body
				);
			}
		}
	}


function GetSQLValueString($theValue, $theType, $mysqli, $theDefinedValue = "", $theNotDefinedValue = "")
{
	$theValue = $mysqli->real_escape_string($theValue);
	switch ($theType) {
		case "text":
			$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
			break;
		case "long":
		case "int":
			$theValue = ($theValue != "") ? intval($theValue) : "NULL";
			break;
		case "double":
			$theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
			break;
		case "date":
			$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
			break;
		case "defined":
			$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
			break;
	}
	return $theValue;
}

function log_error($error)
{
	$mysqli = initialize_db("google_accounts");
	$sql = 'INSERT INTO google_errors (error) VALUES ("'.$error.'")';
	doDBthing($sql, $mysqli);
	$mysqli->close();
}
?>
