<?php
/**
 * Created by PhpStorm.
 * User: jonjenne
 * Date: 5/4/15
 * Time: 4:24 PM
 */

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '2048M');
require_once('../includes/general_functions_limelight.php');
require_once('../includes/general_functions_all.php');
require_once('../includes/RayGun/RayGun.php');
require_once('getForceBillOrders.php');
require_once('get_first_six_cc_from_jcoffice.php');
//require_once('setMostRecentlyApprovedAncestor.php');
require_once('buildCustomReportingDBs.php');
date_default_timezone_set('UTC');
