<?php
require('require_all_limelight.php');

//getForceBillOrderData();
/*
 * Get Order Data
 */
function getForceBillOrderData()
{
	//echo "about to start force bill order data \n";
	$mysqli = initialize_db('limelight');
	$mysqli->query("TRUNCATE TABLE force_bill_these_orders") or die(mysqli_error($mysqli));

	/*
	 * Get the ancestor IDs of declined orders that have also had an approved order somewhere along the line
	 *
	 * The nested query gets the ancestor Id itself.
	 * The outer query gets the actual trial order / approved order that the user placed.
	 */
	$query = "
SELECT
			orders.order_id,
			orders.child_id,
            orders.hold_date
		FROM
			(
				SELECT
					o_ord.ancestor_id
				FROM
					transactions tran
				LEFT JOIN orders o_ord USING (order_id)
				WHERE
					tran.decline_reason LIKE '%a card security code has%'
				AND o_ord.ancestor_id > 0
				AND o_ord.time_stamp >= CURRENT_DATE() - INTERVAL 45 DAY
				GROUP BY
					ancestor_id
			) ids
		LEFT JOIN orders ON
		(
			orders.order_id = ids.ancestor_id
		)
		AND	orders.time_stamp >= CURRENT_DATE() - INTERVAL 60 DAY
		";

	//echo "query is: $query \n\n";
	$query_result = $mysqli->prepare("$query");
	//echo $mysqli->error;
	$query_result->execute(); // why is this failing?
	//echo $mysqli->error;
	$query_result->bind_result($order_id, $child_id, $hold_date);
	//echo $mysqli->error;

	//echo "done processing THAT query \n";
	while ($query_result->fetch())
	{
		$mysqlii = initialize_db('limelight');

		//echo "while processing query1 \n";
		if($child_id != null)
		{
			/*
			 * This one should get the most recent, valid, child order of status 2 or 8.
			 */
			/*$query2 = "
		select order_id, ancestor_id, order_status, time_stamp, recurring_date, gateway_id, campaign_id
		from orders
		where order_id in ($child_id)
		and order_status in (2,8)
		order by order_id desc
		LIMIT 1
		";

			*/
			//echo "first, finding out if there's any order_status 6s here";
			$query2 = "
select orders.order_id, ancestor_id, order_status, time_stamp, recurring_date, orders.gateway_id, campaign_id, transactions.`decline_reason`
from orders
LEFT JOIN transactions ON orders.order_id = transactions.order_id
where orders.order_id in ($child_id)
AND transactions.`decline_reason` NOT LIKE 'Insufficient funds'
AND orders.order_status in (2,8)
order by order_id desc
		";
			//echo "query 2 set as $query2 \n";
			$second_query = $mysqlii->prepare("$query2");
			//echo $mysqlii->error;
			$second_query->execute();
			//echo $mysqlii->error;
			$second_query->bind_result($order, $ancestor, $order_status, $time_stamp, $recurring_date, $gateway_id, $campaign_id, $decline_reason);
			//echo $mysqlii->error;
			//echo "done processing query 2 \n";
			$we_good = 1;
			/*
			 * This while loops checks if any order in this familiy tree has an order_status of 6.
			 */
			while($second_query->fetch())
			{
				if($order_status == 6)
				{
					$we_good = 0;
					break;
				}
				else if(($order_id > $order) && ($decline_reason == "Insufficient funds"))
				{
					$we_good = 0;
					break;
				}
			}
			//echo "we good, no order 6's here";
			/*
			 * Now, if all of the orders in this order tree have no 6s, move on to the below if statement
			 */
			if($we_good)
			{
				$query2 = "
select orders.order_id, ancestor_id, order_status, time_stamp, recurring_date, orders.gateway_id, campaign_id
from orders
LEFT JOIN transactions ON orders.order_id = transactions.order_id
where orders.order_id in ($child_id)
AND transactions.`decline_reason` NOT LIKE 'Insufficient funds'
AND orders.order_status in (2,8)
order by order_id desc
LIMIT 1";
				//echo "query 2  part 2 set as $query2 \n";
				$second_query = $mysqlii->prepare("$query2");
				//echo $mysqlii->error;
				$second_query->execute();
				//echo $mysqlii->error;
				$second_query->bind_result($order, $ancestor, $order_status, $time_stamp, $recurring_date, $gateway_id, $campaign_id);
				//echo $mysqlii->error;
				//echo "done processing query 2 part 2 \n";
				while($second_query->fetch())
				{
					$query3 ="
				INSERT INTO force_bill_these_orders
				(order_id, ancestor_id, order_status, time_stamp, recurring_date, gateway_id, campaign_id)
				VALUES
				('$order', '$ancestor', '$order_status','$time_stamp','$recurring_date','$gateway_id','$campaign_id')
				";
					//echo "and now, query 3 as $query3 \n";
					$mysqliii = initialize_db('limelight');
					doDBthing($query3, $mysqliii);
					$mysqliii->close();
					//echo "done with query 3\n";
				}
			}
		}
		$mysqlii->close();
	}

	$mysqli->close();

	//return $order;
}
function getForceBillOrderData2()
{
	//echo "about to start force bill order data \n";
	$mysqli = initialize_db('limelight');
	$mysqli->query("TRUNCATE TABLE force_bill_these_orders") or die(mysqli_error($mysqli));

	/*
	 * Get the ancestor IDs of declined orders that have also had an approved order somewhere along the line
	 *
	 * The nested query gets the ancestor Id itself.
	 * The outer query gets the actual trial order / approved order that the user placed.
	 */
	$query = "SELECT
					*
				FROM
					transactions tran
				LEFT JOIN orders o_ord USING (order_id)
				WHERE
					tran.decline_reason LIKE '%a card security code has%'
				AND o_ord.ancestor_id > 0
				AND o_ord.time_stamp >= CURRENT_DATE() - INTERVAL 45 DAY
				GROUP BY
					ancestor_id
";


	$result = mysqli_query($mysqli, $query);

	if($result->num_rows > 0)
	{
		while($row = $result->fetch_row())
		{
			$order_id = $row[0];
			$ancestor_id = $row[1];
			$query2 = "
			select order_id, ancestor_id, order_status, billing_cycle
			from orders
			where ancestor_id like $ancestor_id
			AND order_status IN (2,8)
			ORDER BY billing_cycle ASC
			";
		}
	}

}
?>