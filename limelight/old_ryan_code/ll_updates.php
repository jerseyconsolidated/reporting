<?php
require('C:\inetpub\wwwroot\reports\limelight\ll_functions.php');

function pull_shipping()
{

///FINDS ALL OVERDUE (DECLINES) FROM THE PAST 365 DAYS
    $mysqli = initialize_db('limelight');
    if ($mysqli->connect_error) {
        echo "Failed to connect to MySQL: " .$mysqli->connect_error;
    }
//empty old data
    $mysqli->query("TRUNCATE TABLE overdue_orders");

//define what we want to do with LL first
    $data = array(
        "method" => "order_find_overdue",
        "days" => 5
    );
//get results from LL API
    $output = ll_api($data);
//get the order ids
    $overdue_orders = $output['ancestor_id'];
    echo $overdue_orders."<BR>";
    $overdue_orders = explode(',',$overdue_orders);
    echo count($overdue_orders) . "<BR>";
    /*foreach ($overdue_orders as $overdue_order) {
        $data = array(
            "method" => "order_view",
            "order_id" => $overdue_order
        );
        $data = ll_api($data);
        $data['order_id'] = $overdue_order;
        $order_fields = "order_id,response_code,ancestor_id,customer_id,parent_id,child_id,order_status,is_recurring,shipping_first_name,shipping_last_name,shipping_street_address,shipping_street_address2,shipping_city,shipping_state,shipping_state_id,shipping_postcode,shipping_country,customers_telephone,main_product_id,main_product_quantity,upsell_product_id,upsell_product_quantity,time_stamp,recurring_date,retry_date,shipping_method_name,shipping_id,tracking_number,on_hold,on_hold_by,hold_date,gateway_id,order_confirmed,order_confirmed_date,is_chargeback,is_fraud,is_rma,rma_number,rma_reason,ip_address,affiliate,sub_affiliate,campaign_id,order_total,order_sales_tax,order_sales_tax_amount,processor_id,created_by_user_name,created_by_employee_name,billing_cycle,click_id";
        $order_fields_arr = explode(',', $order_fields);
        $values = array();
        foreach ($order_fields_arr as $field) {
            //call getsqlvaluestring to put quotes around and escape bad characters
            if(substr($data[$field],2,1) == "/" && substr($data[$field],5,1) == "/") {
                $values[] = "'".date("Y-m-d", strtotime($data[$field]))."'";
            }
            else {
                $values[] = GetSQLValueString($data[$field], "text", $mysqli);
            }
        }

        $values = implode(",",$values);
        $mysqli->query("INSERT INTO overdue_orders ($order_fields) VALUES ($values)") or die(mysqli_error($mysqli));
        echo  $data['response_code'];

    }*/

}
function pull_products()
{
    ///LL DOESNT LET US QUERY A PRODUCT ID LIST SO WE'RE GOING TO TRY EVERY PRODUCT ID FROM 1-1000 KILLING AT $die_at SO WE DONT MISS ANY.. LETS GO!!
//max not founds to tolerate
    $die_at = 20;

//lets make sure we're not counting to far
    $bads_count = 0;

//open our mysql connection
    $mysqli = mysqli_connect("localhost","root","M0rebottles**","limelight");
// Check connection
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }
//empty old data
    $mysqli->query("TRUNCATE TABLE products");

//pull!!!!!!!!!!!
    for ($i=0;$i<=1000;$i++){
        $data = array(
            "method" => "product_index",
            "product_id" => $i
        );
//get results from LL API
        $output = ll_api($data);
        //keeping track with our bads count.. 600 is product_id not found
        if ($output['response_code'] == 600){$bads_count++;}
//check if bads are greater than tollerated max bads
        if ($bads_count > $die_at) {die("found all, over max count, good job!"); }
        //find the good products
        if ($output['response_code'] == 100){
            $bads_count = 0;
            $product_id = $i;

            //prepare to process the response by defining the fields and keys
            $db_field = array();
            $db_value = array();

//process the results into an array.. first field is the primary key
            $db_field[] = 'product_id';
            $db_value[] = GetSQLValueString($product_id,"text",$mysqli);
            foreach($output as $thefield => $thevalue){
                $db_field[] = $thefield;

                //call getsqlvaluestring to put quotes around and escape bad characters
                $db_value[] = GetSQLValueString($thevalue,"text",$mysqli);
            }
            //turn them from an array into csv
            $fields = implode(",",$db_field);
            $values = implode(",",$db_value);
            $mysqli->query("INSERT INTO products ($fields) VALUES ($values)") or die($mysqli->error);
            //unset for the next loop
            unset($db_field);
            unset($db_value);
        }
    }
    mysqli_close($mysqli);
}
function pull_overdue()
{

///FINDS ALL OVERDUE (DECLINES) FROM THE PAST 365 DAYS
    $mysqli = initialize_db('limelight');
    if ($mysqli->connect_error) {
        echo "Failed to connect to MySQL: " .$mysqli->connect_error;
    }
//empty old data
    $mysqli->query("TRUNCATE TABLE overdue_orders");

//define what we want to do with LL first
    $data = array(
        "method" => "order_find_overdue",
        "days" => 5
    );
//get results from LL API
    $output = ll_api($data);
//get the order ids
    $overdue_orders = $output['ancestor_id'];
    echo $overdue_orders."<BR>";
    $overdue_orders = explode(',',$overdue_orders);
    echo count($overdue_orders) . "<BR>";
    /*foreach ($overdue_orders as $overdue_order) {
        $data = array(
            "method" => "order_view",
            "order_id" => $overdue_order
        );
        $data = ll_api($data);
        $data['order_id'] = $overdue_order;
        $order_fields = "order_id,response_code,ancestor_id,customer_id,parent_id,child_id,order_status,is_recurring,shipping_first_name,shipping_last_name,shipping_street_address,shipping_street_address2,shipping_city,shipping_state,shipping_state_id,shipping_postcode,shipping_country,customers_telephone,main_product_id,main_product_quantity,upsell_product_id,upsell_product_quantity,time_stamp,recurring_date,retry_date,shipping_method_name,shipping_id,tracking_number,on_hold,on_hold_by,hold_date,gateway_id,order_confirmed,order_confirmed_date,is_chargeback,is_fraud,is_rma,rma_number,rma_reason,ip_address,affiliate,sub_affiliate,campaign_id,order_total,order_sales_tax,order_sales_tax_amount,processor_id,created_by_user_name,created_by_employee_name,billing_cycle,click_id";
        $order_fields_arr = explode(',', $order_fields);
        $values = array();
        foreach ($order_fields_arr as $field) {
            //call getsqlvaluestring to put quotes around and escape bad characters
            if(substr($data[$field],2,1) == "/" && substr($data[$field],5,1) == "/") {
                $values[] = "'".date("Y-m-d", strtotime($data[$field]))."'";
            }
            else {
                $values[] = GetSQLValueString($data[$field], "text", $mysqli);
            }
        }

        $values = implode(",",$values);
        $mysqli->query("INSERT INTO overdue_orders ($order_fields) VALUES ($values)") or die(mysqli_error($mysqli));
        echo  $data['response_code'];

    }*/

}
function pull_campaigns()
{
    ///THIS USES TWO CALLS TO LL API, FIRST CALL LOADS CAMPAIGN IDS SECOND CALL REQUIRES ONLY ONE FOREACH TO LOOP THROUGH AND CALL A CAMPAIGN_VIEW METHOD ON EACH CAMPAIGN ID
//open our mysql connection
    $mysqli = mysqli_connect("localhost","root","M0rebottles**","limelight");
// Check connection
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }
//empty old data
    $mysqli->query("TRUNCATE TABLE campaigns");

//define what we want to do with LL first
    $data = array(
        "method" => "campaign_find_active"
    );
//get results from LL API
    $output = ll_api($data);
//load campaign ids to go into a view search
    $active_campaigns = explode(",",$output['campaign_id']);

//take active campaigns and do a view on each one
    for($i=0;$i<count($active_campaigns);$i++){
        $data = array(
            "method" => "campaign_view",
            "campaign_id" => $active_campaigns[$i]
        );
        $output = ll_api($data);
        //prepare to process the response by defining the fields and keys
        $db_field = array();
        $db_value = array();

//process the results into an array.. first field is the primary key
        $db_field[] = 'campaign_id';
        $db_value[] = GetSQLValueString($active_campaigns[$i],"text",$mysqli);
        foreach($output as $thefield => $thevalue){
            $db_field[] = $thefield;
            //call getsqlvaluestring to put quotes around and escape bad characters
            $db_value[] = GetSQLValueString($thevalue,"text",$mysqli);
        }
        //turn them from an array into csv
        $fields = implode(",",$db_field);
        $values = implode(",",$db_value);
        $mysqli->query("INSERT INTO campaigns ($fields) VALUES ($values)") or die($mysqli->error);
        //unset for the next loop
        unset($db_field);
        unset($db_value);
    }

    mysqli_close($mysqli);
}