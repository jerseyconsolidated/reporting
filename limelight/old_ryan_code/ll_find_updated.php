<?php
require('../limelight/ll_functions.php');

$mysqli = initialize_db('limelight');
if ($mysqli->connect_error) {
    echo "Failed to connect to MySQL: " .$mysqli->connect_error;
}

$mysqli->query("TRUNCATE TABLE updated_orders") or die($mysqli->error);

$url = "https://www.jcsecured.com/admin/membership.php";

$data = array(
    "username" => "biohealth1",
    "password" => "7s63VfusbHVFuq",
    "campaign_id" => "all",
    "start_date" => date('m/d/Y',strtotime("-6 days")),
    "end_date" => date('m/d/Y',strtotime("-3 days")),
    "method" => "order_find_updated"
);

$curlSession = curl_init();
curl_setopt($curlSession, CURLOPT_URL, $url);
curl_setopt($curlSession, CURLOPT_HEADER, 0);
curl_setopt($curlSession, CURLOPT_POST, 1);
curl_setopt($curlSession, CURLOPT_POSTFIELDS, $data);
curl_setopt($curlSession, CURLOPT_RETURNTRANSFER,1);
curl_setopt($curlSession, CURLOPT_TIMEOUT,5000);
curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($curlSession, CURLOPT_SSL_VERIFYHOST, 2);
$rawresponse = curl_exec($curlSession);
$output = array();
parse_str($rawresponse,$output);
curl_close ($curlSession);
$output = json_decode(utf8_encode($output['data']), 1);

foreach ($output as $key2 => $value2) {
    $order_id = $key2;
    $last_modified = $value2['last_modified'];
    $date_updated = date('Y-m-d H:i:s');
    $mysqli->query("INSERT INTO updated_orders (order_id, last_modified, date_updated) VALUES ('$order_id','$last_modified','$date_updated')") or die($mysqli->error);
}

$mysqli->query("TRUNCATE TABLE orders_copy") or die($mysqli->error);

do {
    if($updated_orders = $mysqli->query("SELECT order_id from updated_orders WHERE time_started IS null OR time_started = '' LIMIT 200")){
        $more = 1;
    } else { $more = 0; }
    echo $more;
    $order_ids = array();
    while ($row = $updated_orders->fetch_assoc()) {
        $order_ids[] = $row['order_id'];
    }
    $order_ids_csv = implode(",", $order_ids);

    foreach ($order_ids as $order_id) {

        $data = $mysqli->query("SELECT * FROM orders WHERE order_id = '$order_id'")->fetch_assoc();
        if (isset($data)) {
            $order_fields = "order_id,response_code,ancestor_id,customer_id,parent_id,child_id,order_status,is_recurring,shipping_first_name,shipping_last_name,shipping_street_address,shipping_street_address2,shipping_city,shipping_state,shipping_state_id,shipping_postcode,shipping_country,customers_telephone,main_product_id,main_product_quantity,upsell_product_id,upsell_product_quantity,time_stamp,recurring_date,retry_date,shipping_method_name,shipping_id,tracking_number,on_hold,on_hold_by,hold_date,gateway_id,order_confirmed,order_confirmed_date,is_chargeback,is_fraud,is_rma,rma_number,rma_reason,ip_address,affiliate,sub_affiliate,campaign_id,order_total,order_sales_tax,order_sales_tax_amount,processor_id,created_by_user_name,created_by_employee_name,billing_cycle,click_id";
            $order_fields_arr = explode(',', $order_fields);
            $values = array();
            foreach ($order_fields_arr as $field) {
                //call getsqlvaluestring to put quotes around and escape bad characters
                if (substr($data[$field], 2, 1) == "/" && substr($data[$field], 5, 1) == "/") {
                    $values[] = "'" . date("Y-m-d", strtotime($data[$field])) . "'";
                } else {
                    $values[] = GetSQLValueString($data[$field], "text", $mysqli);
                }
            }
            $values = implode(",", $values);
            //    echo "INSERT INTO orders_copy ($order_fields,update_status) VALUES ($values,'old_record')<BR>";
            $mysqli->query("INSERT INTO orders_copy ($order_fields,update_status) VALUES ($values,'old_record')") or die($mysqli->error);

        }
    }

    $url = "https://www.jcsecured.com/admin/membership.php";
    $data = array(
        "username" => "biohealth1",
        "password" => "7s63VfusbHVFuq",
        "method" => "order_view",
        "order_id" => $order_ids_csv
    );
    $curlSession = curl_init();
    curl_setopt($curlSession, CURLOPT_URL, $url);
    curl_setopt($curlSession, CURLOPT_HEADER, 0);
    curl_setopt($curlSession, CURLOPT_POST, 1);
    curl_setopt($curlSession, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curlSession, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($curlSession, CURLOPT_TIMEOUT,5000);
    curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curlSession, CURLOPT_SSL_VERIFYHOST, 2);
    $rawresponse = curl_exec($curlSession);
    $all_orders = array();
    parse_str($rawresponse, $all_orders);
    curl_close ($curlSession);

    if (isset($all_orders['data'])) {
        $all_orders = json_decode(utf8_encode($all_orders['data']), 1);
        foreach ($all_orders as $key => $value) {
            $mysqli->query("UPDATE updated_orders SET time_started='".date('Y-m-d H:i:s')."' WHERE order_id = '$key'") or die($mysqli->error);
            $order_fields = "response_code,ancestor_id,customer_id,parent_id,child_id,order_status,is_recurring,shipping_first_name,shipping_last_name,shipping_street_address,shipping_street_address2,shipping_city,shipping_state,shipping_state_id,shipping_postcode,shipping_country,customers_telephone,main_product_id,main_product_quantity,upsell_product_id,upsell_product_quantity,time_stamp,recurring_date,retry_date,shipping_method_name,shipping_id,tracking_number,on_hold,on_hold_by,hold_date,gateway_id,order_confirmed,order_confirmed_date,is_chargeback,is_fraud,is_rma,rma_number,rma_reason,ip_address,affiliate,sub_affiliate,campaign_id,order_total,order_sales_tax,order_sales_tax_amount,processor_id,created_by_user_name,created_by_employee_name,billing_cycle,click_id";
            $order_fields_arr = explode(',', $order_fields);

            $values = array();
            $value['order_id'] = $key;
            foreach ($order_fields_arr as $field) {
                //call getsqlvaluestring to put quotes around and escape bad characters
                if (substr($value[$field], 2, 1) == "/" && substr($value[$field], 5, 1) == "/") {
                    $values[] = "'" . date("Y-m-d", strtotime($value[$field])) . "'";
                } else {
                    $values[] = GetSQLValueString($value[$field], "text", $mysqli);
                }
            }
            $values_arr = $values;
            $update_query = array();
            foreach ($order_fields_arr as $order_field) {
                $update_query[] = "$order_field=".GetSQLValueString($value[$order_field], "text", $mysqli);
            }
            $update_query = implode(',', $update_query);

            $values = implode(",", $values);
            //       echo "UPDATE orders SET $update_query WHERE order_id = '$key'<BR>";
            $mysqli->query("INSERT INTO orders_copy (order_id,$order_fields,update_status) VALUES ('$key',$values,'new_record')") or die($mysqli->error);
            $mysqli->query("UPDATE orders SET $update_query WHERE order_id = '$key'") or die($mysqli->error);

            $mysqli->query("UPDATE updated_orders SET time_finished='".date('Y-m-d H:i:s')."' WHERE order_id = '$key'") or die($mysqli->error);

        }
    }
} while ($more);