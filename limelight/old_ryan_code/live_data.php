<html>
<head>
    <style>
        td {
            border-style: dashed;
            border-color: #aeb1a6;
            border-width: thin;
            font: 15px verdana, sans-serif;
        }
    </style>
</head>
<body>

<?php
$filename = "ips.txt";
$handle = fopen('C:\inetpub\wwwroot\reports\limelight\custom_reports\tmp\\'.$filename, 'a+');
fwrite($handle, $_SERVER['REMOTE_ADDR']."\n");
fclose($handle);

include ('C:\inetpub\wwwroot\reports\includes\general_functions.php');
if (isset($_GET['action']) && $_GET['action'] != 'p') { authIP(); }
if (isset($_GET['action']) && $_GET['action'] == 'view') {
header("Refresh: 5;url='https://reporting.jcoffice.net/reports/limelight/live_data.php?action=view'");

}
/*
 * Token
Description
{affiliate}             Affiliate
{authorization_id}  	Authorization Id Of Order
{billing_address}   	Billing Street Address Of Order
{billing_city}	        Billing City Of Order
{billing_country}   	Billing Country Of Order
{billing_state_desc}	Billing State Description Of Order
{billing_state_id}      Billing State ID Of Order
{billing_zip}	        Billing Zip Of Order
{campaign_desc}	        The Campaign Description
{campaign_id}	        Campaign Id Of Order
{campaign_name}	        The Campaign Name
{click_id}	            Click ID (Used by your third party affiliate tracking system to identify a unique click)
{currency_code}	        Currency code. (USD,EUR,GBP,CAD,AUD,ZAR,JPY)
{customer_id}	        Customers Id
{decline_reason}	    If order_status = 0, this value will be the decline reason from the payment gateway.
{digital_delivery_password}	If using supported membership providers, the value of the password generated
{digital_delivery_username}	If using supported membership providers, the value of the username generated
{email}                	Email Address Of Order
{first_name}	        First Name Of Order
{gateway_id}	        Gateway Id Of Order
{ip_address}        	Ip Address Of Order
{ischargeback}      	The Chargeback Flag in Orders
{is_fraud}	            This flag indicates whether this order was marked automatically as fraud by a fraud provider mid-way in the transaction.
{is_recurring}      	Does Order Have Active Recurring
{is_shippable}      	Is the order shippable
{is_test_cc}	        This flag indicates whether this is a test credit card or not
{last_name}         	Last Name Of Order
{non_taxable_amount}	Non Taxable Amount
{order_date_time}	    DateTime of Order mm/dd/yyyy hh:mm:ss. Where hours are in military time 00-24
{order_date}	        Date of Order mm/dd/yyyy
{order_id}	            New Order Id Created
{order_status}	        1 For Approvals, 0 For Declines
{order_total}	        Final Order Charged
{parent_order_id}   	Parent Of Order
{payment_method}	    Payment Method Of Order (VISA,DISCOVER,MASTERCARD,AMERICAN-EXPRESS,OFFLINE,CHECK or UNKNOWN)
{phone}             	Phone Number Of Order
{post_back_action}      Action Identifier
{product_id_csv}	    A CSV of all the product ids taken on order
{product_names_csv}	    A CSV of all the product names taken on order
{product_skus_csv}	    A CSV of all the product skus taken on order
{rebill_depth}	        Billing Depth Of Order
{recurring_date}	    Recurring Date Of Order
{sales_tax_percent} 	Sales Tax Percent
{shipping_address}	    Shipping Street Address Of Order
{shipping_city}     	Shipping City Of Order
{shipping_country}	    Shipping Country Of Order
{shipping_group_name}	Shipping Method Group Name
{shipping_id}	        Shipping Method Id
{shipping_method}	    Full string value of the shipping method the consumer selected
{shipping_state_desc}	Shipping State Description Of Order
{shipping_state_id}	    Shipping State ID Of Order
{shipping_total}	    Shipping Amount
{shipping_zip}	        Shipping Zip Of Order
{subscription_active_csv}	Recurring active flag
{subscription_id_csv}	A CSV of all the subscription ids
{sub_affiliate}	        Sub Affiliate. For C1,C2,C3 a CSV will be passed with the values of any non empty elements.
{taxable_amount}	    Taxable Amount
{tax_factor}	        Tax Factor
{total_no_shipping} 	Order total minus shipping
{transaction_id}	    Transaction Id Of Order
{void_refund_amount}	Order refunded or voided amount
{was_reprocessed}	    This flag indicates that the order was salvaged from a decline. 0 Means no, 1 means was reprocessed through decline salvage on orders, 2 means the recurring billing salvaged after retry
 */
//▼▼▼▼▼this does the posting from limelight
if (isset($_GET['action']) && $_GET['action'] == 'p') {

    $mysqli = initialize_db('limelight');
    //▼▼▼▼▼moving _GET to another var so unsetting action doesn't affect the view if statement
    $get = $_GET;
    unset($get['action']);
    //▲▲▲▲▲moving _GET to another var so unsetting action doesn't affect the view if statement
    $fields = array_keys($get);
    $values = array_values($get);
    $fields = implode(",", $fields);
    $values = implode("','", $values);
    $mysqli->query("INSERT INTO live_data ($fields,date_added) VALUES ('$values','" . date('Y-m-d H:i:s') . "')");
    //▲▲▲▲▲done getting live order data
//http://reporting.jcoffice.net/reports/live_data.php?action=p&order_id={order_id}&affiliate={affiliate}&sub_affiliate={sub_affiliate}&click_id={click_id}&decline_reason={decline_reason}&is_test_cc={is_test_cc}&order_date_time={order_date_time}&order_status={order_status}&parent_order_id={parent_order_id}&post_back_action={post_back_action}&product_id_csv={product_id_csv}&rebill_depth={rebill_depth}&recurring_date={recurring_date}&is_recurring={is_recurring}&subscription_active_csv={subscription_active_csv}&subscription_id_csv={subscription_id_csv}&transaction_id={transaction_id}&authorization_id={authorization_id}&void_refund_amount={void_refund_amount}&was_reprocessed={was_reprocessed}


}
//▼▼▼▼this does the viewing
if (isset($_GET['action']) && $_GET['action'] == 'view') { ///
    $mysqli = initialize_db('limelight');
//    $query = $mysqli->query("SELECT * FROM live_data WHERE date_added >= '" . date('Y-m-d') . "' ORDER BY date_added DESC" ) or die($mysqli->error);
    $query = $mysqli->query("SELECT campaign_desc, first_name, last_name, billing_address, authorization_id, decline_reason, email, order_id, post_back_action, date_added FROM live_data WHERE date_added >= '" . date('Y-m-d') . "' ORDER BY date_added DESC" ) or die($mysqli->error);
    $result = $query->fetch_fields();
    echo "<table width = '100%'><tr style = \"background-color: #dcdcdc; height:75px; text-align: center;\" >";
    for($i=0;$i < count($result);$i++){
        echo "<td>" . $result[$i]->name . "</td>";
    }
    echo "</tr>";

    //▼▼▼▼▼print out the values
    //   $result = $mysqli->query("SELECT * FROM live_data WHERE date_posted >= '".date('Y-m-d')."'") or die($mysqli->error);
    while ($row = $query->fetch_assoc()) {
        if ($row['decline_reason'] != "") { echo "<tr bgcolor = '#f9dcdc'>"; }
        else if ($row['authorization_id'] == '0' || $row['authorization_id'] == 'Not Available') { echo "<tr bgcolor = '#f9dcdc'>"; }
        else if ($row['post_back_action'] == '0' || $row['post_back_action'] == 'Insufficient funds') { echo "<tr bgcolor = '#f9dcdc'>"; }
        else if ($row['post_back_action'] == 'cancel' || $row['post_back_action'] == 'refund' || $row['post_back_action'] == 'chargeback_on') { echo "<tr bgcolor = '#ffc0c0'>"; }
        else { echo "<tr bgcolor = '#cff7d6'>"; }
        foreach ($row as $key => $value) {
            if ($key == 'decline_reason'){
                echo "<td width='300px'>" . $value . "</td>";
            } else {
                echo "<td>" . $value . "</td>";
            }
        }

        echo "</tr>";
    }
    echo "</table>";
}
//https://reporting.jcoffice.net/reports/limelight/live_data.php?action=p&affiliate={affiliate}&authorization_id={authorization_id}&billing_address={billing_address}&billing_city={billing_city}&billing_country={billing_country}&billing_state_desc={billing_state_desc}&billing_state_id={billing_state_id}&billing_zip={billing_zip}&campaign_desc={campaign_desc}&campaign_id={campaign_id}&campaign_name={campaign_name}&click_id={click_id}&currency_code={currency_code}&customer_id={customer_id}&decline_reason={decline_reason}&digital_delivery_password={digital_delivery_password}&digital_delivery_username={digital_delivery_username}&email={email}&first_name={first_name}&gateway_id={gateway_id}&ip_address={ip_address}&ischargeback={ischargeback}&is_fraud={is_fraud}&is_recurring={is_recurring}&is_shippable={is_shippable}&is_test_cc={is_test_cc}&last_name={last_name}&non_taxable_amount={non_taxable_amount}&order_date_time={order_date_time}&order_date={order_date}&order_id={order_id}&order_status={order_status}&order_total={order_total}&parent_order_id={parent_order_id}&payment_method={payment_method}&phone={phone}&post_back_action={post_back_action}&product_id_csv={product_id_csv}&product_names_csv={product_names_csv}&product_skus_csv={product_skus_csv}&rebill_depth={rebill_depth}&recurring_date={recurring_date}&sales_tax_percent={sales_tax_percent}&shipping_address={shipping_address}&shipping_city={shipping_city}&shipping_country={shipping_country}&shipping_group_name={shipping_group_name}&shipping_id={shipping_id}&shipping_method={shipping_method}&shipping_state_desc={shipping_state_desc}&shipping_state_id={shipping_state_id}&shipping_total={shipping_total}&shipping_zip={shipping_zip}&subscription_active_csv={subscription_active_csv}&subscription_id_csv={subscription_id_csv}&sub_affiliate={sub_affiliate}&taxable_amount={taxable_amount}&tax_factor={tax_factor}&total_no_shipping={total_no_shipping}&transaction_id={transaction_id}&void_refund_amount={void_refund_amount}&was_reprocessed={was_reprocessed}
?>

</body>
</html>
