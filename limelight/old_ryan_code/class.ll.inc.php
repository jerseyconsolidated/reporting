<?php
class LlApi {
    const API_MEMBERSHIP = 'https://www.jcsecured.com/admin/membership.php';
    const API_SUCCESS = 100;
    //arrays can be shown to user. Strings must be thrown as exception (unexpected errors)
    protected $_transactionResponses = array(
        '100'=>'Success',
        '101'=>'Order is 3DS and needs to go to bank url (use 3DS redirect method)',
        '200'=>'Invalid login credentials',
        '303'=>'Invalid upsell product Id found',
        '304'=>array('Invalid first name of () found'),
        '305'=>array('Invalid last name of () found'),
        '306'=>array('Invalid shipping address1 of () found'),
        '307'=>array('Invalid shipping city of () found'),
        '308'=>array('Invalid shipping state of () found'),
        '309'=>array('Invalid shipping zip of () found'),
        '310'=>array('Invalid shipping country of () found'),
        '311'=>array('Invalid billing address1 of () found'),
        '312'=>array('Invalid billing city of () found'),
        '313'=>array('Invalid billing state of () found'),
        '314'=>array('Invalid billing zip of () found'),
        '315'=>array('Invalid billing country of () found'),
        '316'=>array('Invalid phone number of (%s) found','phone'),
        '317'=>array('Invalid email address of (%s) found','email'),
        '318'=>array('Invalid credit card type of (%s) found','type'),
        '319'=>array('Invalid credit card number found'),
        '320'=>array('Invalid expiration date of (%s%s) found','exp_month','exp_year'),
        '321'=>array('Invalid IP address of (%s) found','ip'),
        '322'=>'Invalid shipping id found',
        '323'=>array('CVV is required for tranType "Sale"'),
        '324'=>array('Supplied CVV has an invalid length'),
        '325'=>array('Shipping state must be 2 characters for a shipping country of US'),
        '326'=>array('Billing state must be 2 characters for a billing country of US'),
        '327'=>array('Invalid payment type'),
        '328'=>array('Expiration month must be between 01 and 12'),
        '329'=>array('Expiration date must be 4 digits long'),
        '330'=>'Could not find prospect record',
        '331'=>'Missing previous OrderId',
        '332'=>'Could not find original order Id',
        '333'=>'Order has been black listed',
        '334'=>'The credit card number or email address has already purchased this product(s)',
        '335'=>'Invalid Dynamic Price Format',
        '336'=>'checkRoutingNumber must be passed when checking is the payment type is checking or eft_germany',
        '337'=>'checkAccountNumber must be passed when checking is the payment type is checking or eft_germany',
        '338'=>'Invalid campaign to perform sale on.  No checking account on this campaign.',
        '339'=>'tranType missing or invalid',
        '340'=>'Invalid employee username of (XXX) found',
        '341'=>'Campaign Id (XXX) restricted to user (XXX)',
        '342'=>'The credit card has expired',
        '400'=>'Invalid campaign Id of (XXX) found',
        '411'=>'Invalid subscription field',
        '412'=>'Missing subscription field',
        '413'=>'Product is not subscription based',
        '414'=>'The product that is being purchased has a different subscription type than the next recurring product',
        '415'=>'Invalid subscription value',
        '600'=>'Invalid product Id of (XXX) found',
        '700'=>'Invalid method supplied',
        '705'=>'Order is not 3DS related',
        '800'=>'Transaction was declined',
        '900'=>'SSL is required to run a transaction',
        '901'=>'Alternative payment payer id is required for this payment type',
        '902'=>'Alternative payment token is required for this payment type',
        '1000'=>'Could not add record',
    );

    public static function getInstance() {
        return new self();
    }

    public function apiCall($url,$method,$data) {
        $api_acounts = array(
            "biohealth1" => "7s63VfusbHVFuq",
            "biohealth2" => "JaG3HMA86HxQc",
            "biohealth3" => "RvTfExUyrf8HEX",
            "biohealth4" => "FVJzA42euHVnE",
            "biohealth5" => "c8WJabw4RqjEVu",
            "biohealth6" => "qFAEgHEyM5CNRR",
            "biohealth7" => "pd8hdHn9KFzcaS",
            "biohealth8" => "qbERYrGWNgWMms",
            "biohealth9" => "JwxVaYuzChFyEA",
            "biohealth10" => "3nJmtDarHNwSJU",
            "biohealth11" => "vB3CAe9TgN9cax",
            "biohealth12" => "TWSva4KXDEq6kC",
            "biohealth13" => "aTkNp9k2rVttYf",
            "biohealth14" => "sDpwfF6kvZn9r",
            "biohealth15" => "wmBvW8hVDxwWTh",
            "biohealth16" => "xRSy2CnhQysu",
            "biohealth17" => "EZNhYXn6Zcbs3",
            "biohealth18" => "JF9Bfhzu4FFwgJ",
            "biohealth19" => "m7pKdtqpQpsv5",
            "biohealth20" => "qrQ2MHYRj9yvrg",
            "biohealth21" => "6mMsrbtuH5UR5K",
            "biohealth22" => "XF7yPJnMpnCB5Z",
            "biohealth23" => "TypgxDt6vXkBfZ",
            "biohealth24" => "U2zuPZbck83jXT",
            "biohealth25" => "btUpuWVyFjCQaA",
            "biohealth26" => "yvsssJJ2Ey6Ngw",
            "biohealth27" => "GThxpNWDZjGHdv",
            "biohealth28" => "HB9H3aCfGGGQX2",
            "biohealth29" => "TWBd9kFh5TxB3",
            "biohealth30" => "jCj9x5zTdJ8tJx",
            "biohealth31" => "VspMtsJ4sYYxkC",
            "biohealth32" => "rtAKKc5bh4nNMz",
            "biohealth33" => "Hjy5ABqpuwzdKQ",
            "biohealth34" => "4YVkApehYFmwjd",
            "biohealth35" => "ued8xBuDskfAap",
            "biohealth36" => "nVyDQrDBWqFcxT",
            "biohealth37" => "svK8E4T4wXSeMb",
            "biohealth38" => "7NBVPV6htYgQz2",
            "biohealth39" => "h4bwtsBPjkNXZh",
            "biohealth40" => "HrhDx74Pnsk5f"
        );

        $data["username"] = array_rand($api_acounts);
        $data["password"] = $api_acounts[$data["username"]];
        $data["method"] = $method;

        $curlSession = curl_init();
        curl_setopt($curlSession, CURLOPT_URL, $url);
        curl_setopt($curlSession, CURLOPT_HEADER, 0);
        curl_setopt($curlSession, CURLOPT_POST, 1);
        curl_setopt($curlSession, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curlSession, CURLOPT_TIMEOUT,5000);
        curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curlSession, CURLOPT_SSL_VERIFYHOST, 2);

        $rawresponse = curl_exec($curlSession);

        $output = array();
        parse_str($rawresponse,$output);
        curl_close ($curlSession);
        return $output;
    }


    public function findOrder($conditions) {
        if(isset($conditions['order_id'])) {
            unset($conditions['order_id']);
        }

        $cond = '';
        foreach($conditions as $name=>$value) {
            $cond .= sprintf("%s=%s,",$name,$value);
        }

        $cond = substr($cond,0,-1);

        $data = array(
            "campaign_id" => "all",
            "start_date" => "07/01/2013",
            "start_time"=>"00:00:00",
            "end_date" => date("m/d/Y"),
            "end_time"=>"23:59:59",
            "search_type"=>"all",
            "return_type"=>"order_view",
            "criteria"=>$cond,
        );

        return $this->apiCall(self::API_MEMBERSHIP,'order_find',$data);
    }

    public function addNote($order_id,$note) {
        $data = array(
            "order_ids" => $order_id,
            "actions" => "notes",
            "values"=>$note,
        );

        return $this->apiCall(self::API_MEMBERSHIP,'order_update',$data);
    }

    public function viewOrder($order_id) {
        $data = array(
            "order_id" => $order_id,
        );

        return $this->apiCall(self::API_MEMBERSHIP,'order_view',$data);
    }

    public function getTransactionResponse($code,LlOrderModel $order) {
        if($code == self::API_SUCCESS) {
            return '';
        }

        if(!isset($this->_transactionResponses[$code])) {
            return '';
        }

        $response = $this->_transactionResponses[$code];

        if(is_string($response)) {
            return $response;
        }

        $format_string = array_shift($response);
        $args = array();

        foreach($response as $res) {
            $args[] = $order->$res;
        }

        return vsprintf($format_string,$args);
    }

    public function checkTransactionResponse($code,LlOrderModel $order) {
        if($code == self::API_SUCCESS) {
            return '';
        }

        if(!isset($this->_transactionResponses[$code])) {
            return '';
        }

        $response = $this->_transactionResponses[$code];

        if(is_string($response)) {
            throw new Exception($response);
        }

        $format_string = array_shift($response);
        $args = array();

        foreach($response as $res) {
            $args[] = $order->$res;
        }

        throw new Exception(vsprintf($format_string,$args),1000);
    }
}