<?php
require('require_all.php');

?>
<form method="post">
	<?php
	/*
	 * why does this have to be done up here??? grr
	 */
	if(isset($_POST['dropdown']))
	{
		$dropdown = $_POST['dropdown'];
	}
	else
	{
		$dropdown = "";
	}
	?>
	<select class="dropdown" id="dropdown" name="dropdown">
	<?php

	/*
	 * print out all options from PHP (useful in-case this gets databased later, and also so it can show what was checked)
	 */
	/*
	$options = array(
		"",
		"#RMA",
		"#savedsale",
		"#failsale",
		"#freebottle",
		"#replacementbottle",
		"#day17",
		"#threat",
		"#needslove",
		"#happy",
		"#priceproblem",
		"#allergic",
		"#badreview",
		"#claimsfraud",
		"#shippingproblem",
		"#upsell",
		"#outboundwin",
		"#refund",
		"#returnrefund",
		"#buyout",
		"#initialdecline",
		"#repeatdecline",
		"#saveddecline",
		"#cbfraud",
		"#cbunauthorized",
		"#cbcancellation",
		"#cbquality",
		"#cbcredit",
		"#cbnotdelivered",
		"#cbretrieval",
		"#ethoca",
		"#cbwon",
		"#cblost",
		"#prearb"
	);
	*/
	$mysqli = initialize_db("limelight");

	$searchsql = 'SELECT * from hashtag_page';

	$result = mysqli_query($mysqli, $searchsql);

	if($result->num_rows > 1)
	{
		echo "<option></option>";
		while($row = $result->fetch_row())
		{
			$id = $row[0];
			$hashtag = $row[1];

			if($dropdown == $hashtag)
			{
				echo "<option selected>$hashtag</option>";
			}
			else
			{
				echo "<option>$hashtag</option>";
			}
		}
		echo "</table>";
	}
	?>
</select>
	<br>
<input type="text" name="tag" id="tag" value="<?php if(isset($_POST['tag'])) echo $_POST['tag']; ?>" />
<- Hashtag (or whatever) to search for goes here
<br>
	<input type="checkbox" name="save">Save Search Term
	<br>
	<input type="text" name="amount" id="amount" value="<?php if(isset($_POST['amount'])) echo $_POST['amount']; ?>" />
<- How many results max? (If blank, 30)
	<br>
	After: <input type="text" name="start_date" id="start_date" value="<?php if(isset($_POST['start_date'])) echo $_POST['start_date']; ?>" />
	<br>
	Before: <input type="text" name="end_date" id="end_date" value="<?php if(isset($_POST['end_date'])) echo $_POST['end_date']; ?>" />
	<br>
	<input type="submit" value="Submit">
</form>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
	$(document).ready(function(){
	$( ".dropdown" ).change(function() {
		//alert( "Handler for .change() called." );
		$("#tag").val($(".dropdown").val());
	}
	)});
</script>
<script>
	$(function() {
		$( "#start_date" ).datepicker();
	});
</script>
<script>
	$(function() {
		$( "#end_date" ).datepicker();
	});
</script>
<?php
if(isset($_POST['tag']))
{
	$mysqli = initialize_db("limelight");
	/*
	 * If we want to save the search term ('hashtag')
	 */
	if(isset($_POST['save']) && $_POST['save'] == "on")
	{

		$hashtag_to_save = GetSQLValueString($_POST['tag'], "text", $mysqli);

		/*
		 * First, check if this exists:
		 */
		$searchsql = 'SELECT * from hashtag_page WHERE hash_tag LIKE '.$hashtag_to_save.';';
		$result = mysqli_query($mysqli, $searchsql);

		if(! $result->num_rows >= 1)
		{
			doDBthing("INSERT INTO hashtag_page (hash_tag) VALUES ($hashtag_to_save);", $mysqli);
		}
	}
	if(! isset($_POST['amount']) || $_POST['amount'] != "")
	{
		$limit = $_POST['amount'];
	}
	else
	{
		$limit = "30";
	}

	if(! isset($_POST['start_date']) || $_POST['start_date'] != "")
	{
		$sd = GetSQLValueString($_POST['start_date'], "text", $mysqli);
		$sd = str_replace("'", "", $sd);
		$sdd = explode("/", $sd);
		$start_date = $sdd[2]."-".$sdd[0]."-".$sdd[1];
	}
	else
	{
		$start_date = '2013-01-01';
	}
	if(! isset($_POST['end_date']) || $_POST['end_date'] != "")
	{
		$ed = GetSQLValueString($_POST['end_date'], "text", $mysqli);
		$ed = str_replace("'", "", $ed);
		$edd = explode("/", $ed);
		//echo "<br><br>";
		$end_date = $edd[2]."-".$edd[0]."-".$edd[1];
	}
	else
	{
		$end_date = '2999-01-01';
	}
	$tag = GetSQLValueString($_POST['tag'], "text", $mysqli);
	$tag = str_replace("'", "", $tag);
	//echo $_POST['start_date'];
	/*
	 * do the magical sql query to find the tags in the notes!
	 *
	 * Apparent fun fact: there is no 'contains' in MySQL and we have to use wildcards (%) to search.
	 */
	$searchsql = 'SELECT * from notes_employee WHERE employee_note LIKE "%'.$tag.'%"
	AND note_date > "'.$start_date.'"
	AND note_date < "'.$end_date.'"
	ORDER BY note_date DESC LIMIT '.$limit;

	//echo $searchsql;
	$result = mysqli_query($mysqli, $searchsql);
	
	if($result->num_rows > 0)
	{
		echo $result->num_rows." total results.";
		echo "<table border=1>";
		echo "<th>Employee Note Key</th>";
		echo "<th>Order ID</th>";
		echo "<th>Note</th>";
		echo "<th>Date</th>";
		while($row = $result->fetch_row())
		{
			$employee_note_key = $row[0];
			$order_id = $row[1];
			$note = $row[2];
			$date = $row[3];
			echo "<tr><td>$employee_note_key</td>";
			echo "<td>$order_id</td>";
			echo "<td>$note</td>";
			echo "<td>$date</td></tr>";
		}
		echo "</table>";
	}
	//echo $searchsql;
	//echo $mysqli->error;

	//echo $searchsql;
	$mysqli->close();
}

?>