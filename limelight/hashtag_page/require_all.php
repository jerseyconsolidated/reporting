<?php
/**
 * Created by PhpStorm.
 * User: jonjenne
 * Date: 5/6/15
 * Time: 4:24 PM
 */

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '2048M');

require_once('../../includes/general_functions_limelight.php');
require_once('../../includes/general_functions_all.php');
require_once('../../includes/RayGun/RayGun.php');
date_default_timezone_set('UTC');
