<?php
require('require_all_limelight.php');


setMostRecentlyApprovedAncestor();

/*
 * Get Order Data
 */
function setMostRecentlyApprovedAncestor()
{
	//echo "about to start force bill order data \n";
	$mysqli = initialize_this_db('limelight');
	$mysqlii = initialize_this_db('limelight');
	$mysqliii = initialize_this_db('limelight');
	//$mysqli->query("TRUNCATE TABLE most_recently_approved_ancestor") or die(mysqli_error($mysqli));

	/*
	 * Get the ancestor IDs of declined orders that have also had an approved order somewhere along the line
	 *
	 * The nested query gets the ancestor Id itself.
	 * The outer query gets the actual trial order / approved order that the user placed.
	 */
	$query = "
	SELECT orders_to_process.order_id, orders.`ancestor_id`
FROM orders_to_process
LEFT JOIN orders ON `orders_to_process`.order_id = orders.`order_id`
		";

	//echo "query is: $query \n\n";
	$query_result = $mysqli->prepare("$query");
	//echo $mysqli->error;
	$query_result->execute(); // why is this failing?
	//echo $mysqli->error;
	$query_result->bind_result($order_id,$ancestor_id);
	//echo $mysqli->error;

	//echo "done processing THAT query \n";
	while ($query_result->fetch())
	{
		if($ancestor_id == "" || $ancestor_id == null || $ancestor_id == "0")
		{
			$query3 =
				"UPDATE orders SET most_recently_approved_ancestor_id=0
			WHERE order_id LIKE $order_id";
			$query_result3 = $mysqliii->prepare("$query3");
			//echo $mysqliii->error;
			$query_result3->execute(); // why is this failing?
			//echo "\n setting $order_id to 0";
			//echo $query3."\n\n";
			continue;
		}

		$query2 = "
		SELECT order_id FROM orders
		WHERE ancestor_id LIKE $ancestor_id
		AND order_status in (2,8)
		ORDER BY order_id DESC LIMIT 1";
		//echo "\n".$query2."\n";
		//echo "query is: $query \n\n";
		$query_result2 = $mysqlii->prepare("$query2");
		//echo $mysqlii->error;
		$query_result2->execute(); // why is this failing?
		//echo $mysqli->error;
		$query_result2->bind_result($second_order_id);
		//echo $mysqli->error;

		while ($query_result2->fetch())
		{
			$query3 =
			"UPDATE orders SET most_recently_approved_ancestor_id=$second_order_id
			WHERE order_id LIKE $order_id";
			$query_result3 = $mysqliii->prepare("$query3");
			//echo $mysqliii->error;
			$query_result3->execute(); // why is this failing?
			//$query_result3->get_result();
			//echo $query3."\n\n";
		}
		//var_dump($query_result2);
		//die();
	}

	$mysqli->close();
	$mysqlii->close();
	$mysqliii->close();

}

/*
 * This stupid function has to exist (and override the general_functions.php file) because
 * this class can be run by itself, and when it does, it needs to be able to initialize the database itself.
 *
 * If it's called from the start_ll_pull.php file, general_functions will have already been declared and throw an error.
 * If it's called by itself, it will have to be able to initilize without having general_functions included, so this is here.
 */
function initialize_this_db($mysqli, $username="biohealth1", $password="7s63VfusbHVFuq"){
//function initialize_db($mysqli){
	//var_dump(debug_backtrace());
	// die();
	//$mysqli = (!isset($mysqli) ? $mysqli = 'limelight' : $mysqli);

	//echo $mysqli;
	//open up our connection
	$mysqli = new mysqli("localhost",$username,$password,'limelight');
	// Check connection
	if (mysqli_connect_errno()) {
		//printf("Connect failed: %s\n", mysqli_connect_error());
		log_error(mysqli_connect_error(), "error".getmypid());
		exit();
	}
	return $mysqli;
}

?>