<?php
require('require_all_limelight.php');


/*
 * how long should a 'burst' be? (time between starting and stopping threads)
 *
 * Also, if this is below $time_between_threads, it WILL NOT start all 40 threads.
 * Each thread starts at $time_between_threads, so if the $time_between_bursts is smaller, not all threads will start.
 *
 * Variable explaination:
 * $time_between_bursts is the time to wait for a given thread to launch.
 * So, if we launch thread 0, wait 41 seconds after thread 0 launches to launch it again. (default is 41)
 *
 * $time_between_threads is the time to wait between each thread is started (default is 1)
 *
 * $time_to_rest_between_bursts is the time to wait in between each 'burst' of threads running. (default is 1)
 *
 * $how_many_days_to_keep_backups is how long the backups should be kept
 *
 * $backup_databases_on_fresh_runs is whether or not (1 if yes 0 if no) the data in the databases should be backed up before
 * a full pull of the DB should take place.
 *
 * $backup_databases_on_updates is whether or not the (1 if yes 0 if no) the data in the databases should be backed up before
 * the database should be updated / synched up with the limelight server.
 *
 * **NOTE! If backups aren't run, but they exist, they won't be deleted properly. Backups are deleted $how_many_days_to_keep_backups ago.
 *
 * Example:
 * If it's Jan 10th and $how_many_days_to_keep_backups is 10, the Jan 1st backup will be deleted.
 * If the above is still true, but it's Jan 11th instead of 10th, the Jan 2nd backup will be deleted, but the Jan 1st backup will be spared.
 *
 * So, keep that in mind. It only EVER clears out the backup from exactly $how_many_days_to_keep_backups ago.
 */
$time_between_bursts = 41; //in seconds //overridden during 'update'
$time_between_threads = 2; //in seconds //overridden 'update'
$time_to_rest_between_bursts = 1; //in seconds //overridden during 'update'
$how_many_days_to_keep_backups = 7; //in days
$backup_databases_on_fresh_runs = 1;
$backup_databases_on_updates = 0;


if ($argc > 1)
{
    $var = $argv[1];
}
else
{
    print "Argument numbers not correct (this page MUST be called like 'php start_ll_pull.php start') ";
    die();
}

if($var == "start")
{
    $mysqli = initialize_db('limelight', "biohealth1", "7s63VfusbHVFuq");

	if($backup_databases_on_fresh_runs)
	{
		createDBBackup($mysqli, $how_many_days_to_keep_backups);
	}
	/*
	 * Truncate the orders_to_process seperately (this matters for resuming / updating)
	 */
	$mysqli->query("TRUNCATE TABLE orders_to_process") or die(mysqli_error($mysqli));
	/*
	 * Empty all tables
	 */
	//▼▼▼▼▼▼EMPTY TABLES TO BEGIN▼▼▼▼▼▼
	$mysqli->query("TRUNCATE TABLE limelight_errors") or die(mysqli_error($mysqli));
	log_error("Starting the main ll_puller with parameters start", "main");
	$mysqli->query("TRUNCATE TABLE orders_to_process") or die(mysqli_error($mysqli));
	$mysqli->query("TRUNCATE TABLE orders") or die(mysqli_error($mysqli));
	$mysqli->query("TRUNCATE TABLE customers") or die(mysqli_error($mysqli));
	$mysqli->query("TRUNCATE TABLE transactions") or die(mysqli_error($mysqli));
	$mysqli->query("TRUNCATE TABLE order_details") or die(mysqli_error($mysqli));
	$mysqli->query("TRUNCATE TABLE notes_system") or die(mysqli_error($mysqli));
	$mysqli->query("TRUNCATE TABLE notes_employee") or die(mysqli_error($mysqli));
	//$mysqli->query("TRUNCATE TABLE worker_speed") or die(mysqli_error($mysqli));
	$mysqli->query("TRUNCATE TABLE poll") or die(mysqli_error($mysqli));
	log_error("Done truncating LL tables.", "main");


	/*
	 * This is handled automagically by the 'force_bill' method called in a different spot
	 *
	 * $mysqli->query("TRUNCATE TABLE force_bill_these_orders") or die(mysqli_error($mysqli));
	 */

    $data = array(
        "method" => "order_find",
        "campaign_id" => "all",
       //"start_date" => "12/01/2014",
        "start_date" => "06/01/2013",
		//"start_date" => "01/01/2015",
        "end_date" => date('m/d/Y',strtotime("today")),
        //"end_date" => '12/31/2014',
        "search_type" => "any",
        "return_type" => "",
        "criteria" => "all"
    );
    $output = getLLOrders($data);
	log_error("Finished pulling LL orders", "main");
//load all order id's to work on
    $order_ids = explode(",", $output['order_ids']);
    foreach($order_ids as $order_id)
	{
        $mysqli->query("INSERT INTO orders_to_process (order_id,worker,complete) VALUES ('$order_id','0','0')") or die(mysqli_error($mysqli));
    }
	log_error("Finished putting all workable orders to process into orders_to_process", "main");

	ignoreBadOrders($mysqli);
    //$result = pclose(popen("php /var/www/reporting.jcoffice.net/limelight/poll.php &", "r"));
    echo exec("php /var/www/reporting.jcoffice.net/limelight/poll.php > /dev/null &"); //kill the workers (does this need to be root?)
    $mysqli->close();
	log_error("Fired off the poll.php file, now going to start worker loops", "main");

    workerLoop($time_between_bursts, $time_between_threads, $time_to_rest_between_bursts);
	log_error("Finished with all. Closing", "main");
}

//if we were told to 'resume' from the command line....
if($var == "resume")
{
	log_error("Told to resume, so resuming.", "main");
	workerLoop($time_between_bursts, $time_between_threads, $time_to_rest_between_bursts);
	log_error("Done resuming.", "main");
}
//if we have some data, but want to get data from another time...
if($var == "continue")
{
	$mysqli = initialize_db('limelight', "biohealth1", "7s63VfusbHVFuq");
	$data = array(
		"method" => "order_find",
		"campaign_id" => "all",
		//"start_date" => "12/01/2014",
		"start_date" => "06/01/2013",
		//"start_date" => "01/01/2015",
		//"end_date" => date('m/d/Y',strtotime("today")),
		"end_date" => '12/31/2014',
		"search_type" => "any",
		"return_type" => "",
		"criteria" => "all"
	);
	$output = getLLOrders($data);
	log_error("Finished pulling LL orders", "main");
	//load all order id's to work on
	$order_ids = explode(",", $output['order_ids']);
	foreach($order_ids as $order_id)
	{
		$mysqli->query("INSERT INTO orders_to_process (order_id,worker,complete, note_about_order) VALUES ('$order_id','0','0', '0')") or die(mysqli_error($mysqli));
	}
	log_error("Finished putting all workable orders to process into orders_to_process", "main");

	ignoreBadOrders($mysqli);
	//$result = pclose(popen("php /var/www/reporting.jcoffice.net/limelight/poll.php &", "r"));
	echo exec("php /var/www/reporting.jcoffice.net/limelight/poll.php > /dev/null &"); //kill the workers (does this need to be root?)
	$mysqli->close();
	log_error("Fired off the poll.php file, now going to start worker loops", "main");

	workerLoop($time_between_bursts, $time_between_threads, $time_to_rest_between_bursts);
	log_error("Finished with all. Closing", "main");
}








/*
 * If we are performing routine data updates (re-syncing with limelight)
 * So, this will include things like orders added after x date
 * as well as all orders that have been updated/modified/changed since x date.
 */
if($var == "update")
{
	$time_to_rest_between_bursts = 41; //in seconds
	$time_between_bursts = 1; //in seconds
	$time_between_threads = 2; //in seconds

	$mysqli = initialize_db('limelight', "biohealth1", "7s63VfusbHVFuq");

	if($backup_databases_on_updates)
	{
		createDBBackup($mysqli, $how_many_days_to_keep_backups);
	}

	$mysqli->query("TRUNCATE TABLE limelight_errors") or die(mysqli_error($mysqli));
	log_error("Told to update all orders since the most recent order in the DB. So lets update.", "main");


	/*
	 * This part gets the most recent timestamp of orders processed before they are truncated,
	 * so that we can know where to 'resume' the 'update'.
	 */
	//start_timestamp is formatted like 2015-01-31 23:59:59:
	$start_timestamp = getLatestProcessedOrderTimestamp($mysqli);
	//So, it must be broken up into just the date:
	$start_timestamp = explode(" ",$start_timestamp);
	//Then, from the date, explode out the year/month/day:
	$start_timestamp2 = explode("-",$start_timestamp[0]);
	//Then put it all back together in the right order
	$start_date = $start_timestamp2[1]."/".$start_timestamp2[2]."/".$start_timestamp2[0]." ".$start_timestamp[1];




	//$start_date = "07/30/2015 12:12:12";





	/*
	 * Truncate the orders_to_process separately (this matters for resuming / updating)
	 */
	$mysqli->query("TRUNCATE TABLE orders_to_process") or die(mysqli_error($mysqli));
	$mysqli->query("TRUNCATE TABLE poll") or die(mysqli_error($mysqli));

	$data = array(
		"method" => "order_find",
		"campaign_id" => "all",
		"start_date" => $start_date,
		//"start_date" => "06/01/2013",
		"end_date" => date('m/d/Y',strtotime("today")),
		//"end_date" => '12/31/2013',
		"search_type" => "any",
		"return_type" => "",
		"criteria" => "all"
	);

	$output = getLLOrders($data);
	$order_ids = explode(",", $output['order_ids']);
	foreach($order_ids as $order_id)
	{
		if(isset($order_id) && $order_id != "")
		{
			doDBthing("INSERT INTO orders_to_process (order_id,worker,complete) VALUES ('$order_id','0','0')", $mysqli);
		}
	}
	log_error("Finished pulling *NEW* LL orders since $start_date", "main");

	$data = array(
		"method" => "order_find_updated",
		"campaign_id" => "all",
		"start_date" => $start_date,
		//"start_date" => "06/01/2013",
		"end_date" => date('m/d/Y',strtotime("today")),
		//"end_date" => '12/31/2013',
		//"search_type" => "any",
		//"return_type" => "",
		//"criteria" => "all"
		//group_keys => "all"
	);
	$output = getLLOrders($data);
	$order_ids = explode(",",$output['order_ids']);
	$querystring = "";
	$queryinsert = "";
	//log_error(sizeof($order_ids)." is the size");

	$del_orders = $output['order_ids'];
	$mysqli->query("DELETE FROM orders WHERE order_id IN($del_orders)");
	$mysqli->query("DELETE FROM transactions WHERE order_id IN($del_orders)");
	$mysqli->query("DELETE FROM order_details WHERE order_id IN($del_orders)");
	$mysqli->query("DELETE FROM notes_employee WHERE order_id IN($del_orders)");
	$mysqli->query("DELETE FROM notes_system WHERE order_id IN($del_orders)");

	foreach($order_ids as $order_id)
	{
		/*
		$querystring .= " DELETE FROM orders WHERE order_id LIKE $order_id;";
		//$querystring .= " DELETE FROM orders_to_process WHERE order_id LIKE $order_id;";
		$querystring .= " DELETE FROM transactions WHERE order_id LIKE $order_id;";
		$querystring .= " DELETE FROM order_details WHERE order_id LIKE $order_id;";
		$querystring .= " DELETE FROM notes_employee WHERE order_id LIKE $order_id;";
		$querystring .= " DELETE FROM notes_system WHERE order_id LIKE $order_id;";
		*/
		$querystring .= "INSERT INTO orders_to_process (order_id,worker,complete) VALUES ('$order_id','0','0');";

		log_error("deleting / inserting: $order_id");
	}


	$mysqli->multi_query($querystring);
	log_error("Finished pulling *UPDATED* LL orders since $start_date", "main");

	$result = pclose(popen("php /var/www/reporting.jcoffice.net/limelight/poll.php &", "r"));
	$mysqli->close();
	//log_error("Fired off the poll.php file, now going to start worker loops", "main");

	workerLoop($time_between_bursts, $time_between_threads, $time_to_rest_between_bursts);
	log_error("Finished with all. Closing", "main");
}


function workerLoop($time_between_bursts, $time_between_threads, $time_to_rest_between_bursts)
{
	while (shouldContinue() ==0)
    {
        burst($time_between_bursts, $time_between_threads);
        sleep($time_to_rest_between_bursts);//give time for reset
    }
	log_error("Done with thread bursting. Going to build custom reporting DBs", "main");
	buildCustomReportingDBs(); //build databases for Mori
	log_error("Done with Reporting DBs. Going to getForceBillOrderData", "main");
	getForceBillOrderData();
	log_error("Done with ForceBillOrderData, going to set MostRecentlyApprovedAncestor", "main");
	//get_first_six(); TURN THIS OFF SO IT DOESNT DOUBEL UP ON DATA SINCE THIS DOESNT GET TRUNCATED ANYMORE
	//setMostRecentlyApprovedAncestor(); this is handled on it's own task now.
	log_error("Done with MostRecentlyApprovedAncestory", "main");
}
/*
 * Should we continue creating bursts of worker threads?
 */
function shouldContinue()
{
    $mysqli = initialize_db('limelight', "biohealth2", "JaG3HMA86HxQc");

    $check_cycle = $mysqli->query("SELECT COUNT(order_id) FROM orders_to_process WHERE complete = '0' LIMIT 1")->fetch_assoc();
    $mysqli->close();
    $check_cycle = $check_cycle['COUNT(order_id)'];

    $done = ($check_cycle == 0 ? 1 : 0);
    return $done;
}
/*
 * Perform 1 'burst'
 * (A 'burst' is 1 x-second cycle of start/stop for the 40 worker threads)
 */
function burst($seconds, $time_between_threads)
{
    startWorkers($time_between_threads);
    sleep($seconds);
    //stopWorkers(); they don't need to stop anymore; they die on their own. that cuts down on sql leaks
}
/*
 * Stops the workers (PROBABLY BROKEN! The page is called doing_ll_pull_jon.php now, and we don't even use it anymore.)
 */
function stopWorkers()
{
    echo exec("kill -9 `ps au | grep doing_ll_pull.php | awk '{print $2}'`"); //kill the workers (does this need to be root?)
}
/*
 * Starts the workers
 */
function startWorkers($time_between_threads)
{
    $worker = "1";
	$api_acounts = getApiAccounts();

	foreach ($api_acounts as $username => $password)
	{
		if(shouldContinue() == 0)
		{
			//echo $username;
	//   $result =  pclose(popen("start /b php -f C:\\inetpub\\wwwroot\\reports\\limelight\\doing_ll_pull.php $username $password w$worker > NUL 2> NUL","r"));
			//$result =  pclose(popen("start /b php -f C:\\inetpub\\wwwroot\\reports\\limelight\\doing_ll_pull.php $username $password w$worker >C:\\inetpub\\wwwroot\\errors\\error_stdin_w$worker.txt 2>NUL" ,"r"));
			//$result = pclose(popen("php /Users/jonjenne/reporting/limelight/doing_ll_pull_jon.php $username $password w$worker & >/var/www/reporting.jcoffice.net/errors/error_stdin_resume_w$worker.txt 2>/dev/null" , "r"));
			$result = pclose(popen("php /var/www/reporting.jcoffice.net/limelight/doing_ll_pull_jon.php $username $password w$worker &", "r"));
			$worker++;
			sleep($time_between_threads);
		}
		else
		{
			log_error("should not continue starting workers.");
		}
    }
}

/*
 * This function call is required because it is the one that gets all of the initial orders.
 */
function getLLOrders($data_in)
{
	$url = "https://www.jcsecured.com/admin/membership.php";
	$api_acounts = getApiAccounts();
	$rand = rand(1,count($api_acounts));
	$username = "biohealth".$rand;
	$password = $api_acounts['biohealth'.$rand];

	$data = array(
		"username" => $username,
		"password" => $password,
	);
	$data = array_merge_recursive($data, $data_in);
	$curlSession = curl_init();
	curl_setopt($curlSession, CURLOPT_URL, $url);
	curl_setopt($curlSession, CURLOPT_HEADER, 0);
	curl_setopt($curlSession, CURLOPT_POST, 1);
	curl_setopt($curlSession, CURLOPT_POSTFIELDS, $data);
	curl_setopt($curlSession, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($curlSession, CURLOPT_TIMEOUT,5000);
	curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($curlSession, CURLOPT_SSL_VERIFYHOST, 2);
	$rawresponse = curl_exec($curlSession);
	$output = array();
	parse_str($rawresponse,$output);
	curl_close ($curlSession);
	return $output;
}
/*
 * The one and only place that the API accounts should be. They shouldn't be copied and pasted to every method that needs them.
 */
function getApiAccounts()
{
	$api_acounts = array(
		"biohealth1" => "7s63VfusbHVFuq",
		"biohealth2" => "JaG3HMA86HxQc",
		"biohealth3" => "RvTfExUyrf8HEX",
		"biohealth4" => "FVJzA42euHVnE",
		"biohealth5" => "c8WJabw4RqjEVu",
		"biohealth6" => "qFAEgHEyM5CNRR",
		"biohealth7" => "pd8hdHn9KFzcaS",
		"biohealth8" => "qbERYrGWNgWMms",
		"biohealth9" => "JwxVaYuzChFyEA",
		"biohealth10" => "3nJmtDarHNwSJU",
		"biohealth11" => "vB3CAe9TgN9cax",
		"biohealth12" => "TWSva4KXDEq6kC",
		"biohealth13" => "aTkNp9k2rVttYf",
		"biohealth14" => "sDpwfF6kvZn9r",
		"biohealth15" => "wmBvW8hVDxwWTh",
		"biohealth16" => "xRSy2CnhQysu",
		"biohealth17" => "EZNhYXn6Zcbs3",
		"biohealth18" => "JF9Bfhzu4FFwgJ",
		"biohealth19" => "m7pKdtqpQpsv5",
		"biohealth20" => "qrQ2MHYRj9yvrg",
		"biohealth21" => "6mMsrbtuH5UR5K",
		"biohealth22" => "XF7yPJnMpnCB5Z",
		"biohealth23" => "TypgxDt6vXkBfZ",
		"biohealth24" => "U2zuPZbck83jXT",
		"biohealth25" => "btUpuWVyFjCQaA",
		"biohealth26" => "yvsssJJ2Ey6Ngw",
		"biohealth27" => "GThxpNWDZjGHdv",
		"biohealth28" => "HB9H3aCfGGGQX2",
		"biohealth29" => "TWBd9kFh5TxB3",
		"biohealth30" => "jCj9x5zTdJ8tJx",
		"biohealth31" => "VspMtsJ4sYYxkC",
		"biohealth32" => "rtAKKc5bh4nNMz",
		"biohealth33" => "Hjy5ABqpuwzdKQ",
		"biohealth34" => "4YVkApehYFmwjd",
		"biohealth35" => "ued8xBuDskfAap",
		"biohealth36" => "nVyDQrDBWqFcxT",
		"biohealth37" => "svK8E4T4wXSeMb",
		"biohealth38" => "7NBVPV6htYgQz2",
		"biohealth39" => "h4bwtsBPjkNXZh",
		"biohealth40" => "HrhDx74Pnsk5f"
	);
	return $api_acounts;
}
/*
 * Find the most recent order that was processed from orders_to_process to figure out the last date that this script was run.
 *
 * Also, it should sort by if it was sucessful or not.
 */
function getLatestProcessedOrderTimestamp($mysqli)
{
	$sql = "SELECT timestamp FROM orders_to_process WHERE complete LIKE 1 ORDER BY timestamp DESC LIMIT 1";
	$result = mysqli_query($mysqli, $sql);
	$row = $result->fetch_row();
	$date = $row[0];
	return $date;
}
/*
 * IGNORE BAD ORDERS!
 */
function ignoreBadOrders($mysqli)
{
	$mysqli->query("UPDATE `limelight`.`orders_to_process` SET `worker` = '-1', `complete` = '1' WHERE `orders_to_process`.`order_id` = '70102'; ") or die(mysqli_error($mysqli));
	$mysqli->query("UPDATE `limelight`.`orders_to_process` SET `worker` = '-1', `complete` = '1' WHERE `orders_to_process`.`order_id` = '640608'; ") or die(mysqli_error($mysqli));
	$mysqli->query("UPDATE `limelight`.`orders_to_process` SET `worker` = '-1', `complete` = '1' WHERE `orders_to_process`.`order_id` = '633916'; ") or die(mysqli_error($mysqli));
	$mysqli->query("UPDATE `limelight`.`orders_to_process` SET `worker` = '-1', `complete` = '1' WHERE `orders_to_process`.`order_id` = '630402'; ") or die(mysqli_error($mysqli));
	$mysqli->query("UPDATE `limelight`.`orders_to_process` SET `worker` = '-1', `complete` = '1' WHERE `orders_to_process`.`order_id` = '629252'; ") or die(mysqli_error($mysqli));
	log_error("Finished ignoring bad orders: 70102, 640608, 633916, 630402, 629252", "main");
}
/*
 * Backup the databases
 */
function createDBBackup($mysqli, $days)
{
	/*
	 * Create backup tables:
	 */
	$today = date('m_d_Y',strtotime("today"));
	$mysqli->query("create table orders_$today SELECT * FROM orders");
	$mysqli->query("create table transactions_$today SELECT * FROM transactions");
	$mysqli->query("create table refunds_by_order_$today SELECT * FROM refunds_by_order");
	$mysqli->query("create table notes_employee_$today SELECT * FROM notes_employee");
	$mysqli->query("create table notes_system_$today SELECT * FROM notes_system");

	/*
	 * Delete old backup tables:
	 */
	$daysago = date('m_d_Y',strtotime("-".$days." days"));
	$mysqli->query("DROP TABLE IF EXISTS orders_$daysago");
	$mysqli->query("DROP TABLE IF EXISTS transactions_$daysago");
	$mysqli->query("DROP TABLE IF EXISTS refunds_by_order_$daysago");
	$mysqli->query("DROP TABLE IF EXISTS notes_employee_$daysago");
	$mysqli->query("DROP TABLE IF EXISTS notes_system_$daysago");
}

?>
