<?php
require('require_all_limelight.php');

/*
 * DO NOT TURN THIS ON OR ELSE IT RUNS ON TOP OF OTHER LIMELIGHT STUFF!
 * <oops />
 */
//buildCustomReportingDBs();

function buildCustomReportingDBs()
{
	refundsByOrder();
	sales_by_gateway_for_this_month();
	//bills_by_order_for_this_month();
}
function bills_by_order_for_this_month()
{
	$mysqli = initialize_db("limelight");
	$mysqli->query("TRUNCATE TABLE bills_by_order_for_this_month");

	$searchsql = "
SELECT
";
	$result = mysqli_query($mysqli, $searchsql);
	while($row = $result->fetch_row())
	{
		$gateway_id = $row[0];
		$gross_sales = $row[1];
		$refund = $row[2];
		$gross = $gross_sales - $refund;
		$insertSQL = "
		INSERT INTO `limelight`.`bills_by_order_for_this_month`
		(`gateway_id`, `gross_sales`)
		VALUES
		('$gateway_id', '$gross');
		";
		doDbthing($insertSQL);
	}
	$mysqli->close();
}
function sales_by_gateway_for_this_month()
{
	$mysqli = initialize_db("limelight");
	$mysqli->query("TRUNCATE TABLE sales_by_gateway_for_this_month");

	$searchsql = "
SELECT gateway_id, total_sales, IFNULL(refunds,0) as refunds ,(total_sales - IFNULL(refunds,0)) as GROSS_sales
FROM
(SELECT orders.gateway_id, SUM(orders.`order_total`) as total_sales, (SELECT SUM(IFNULL(refunds_by_order.`refund_amount`, 0))
FROM refunds_by_order
WHERE refunds_by_order.date > '".date("Y-m-01")."'
AND refunds_by_order.date < '".date('Y-m-d',strtotime("today"))."'
AND gateway_id LIKE orders.gateway_id
GROUP BY gateway_id
) as refunds
FROM orders
WHERE orders.time_stamp > '".date("Y-m-01")."'
AND orders.time_stamp < '".date('Y-m-d',strtotime("today"))."'
AND orders.order_status IN (2,6,8)
GROUP BY gateway_id
)
as data
";
	$result = mysqli_query($mysqli, $searchsql);
	while($row = $result->fetch_row())
	{
		$gateway_id = $row[0];
		$total_sales = $row[1];
		$refund = $row[2];
		$gross_sales = $row[3];
		$insertSQL = "
		INSERT INTO `limelight`.`sales_by_gateway_for_this_month`
		(`gateway_id`,`total_sales`,`refunds`, `gross_sales`)
		VALUES
		('$gateway_id', '$total_sales', '$refund', '$gross_sales');
		";
		doDbthing($insertSQL, $mysqli);
	}
	$mysqli->close();
}
	/*
	 * Get Order Data
	 */
function refundsByOrder()
{
	$mysqli = initialize_db("limelight");
	$mysqli->query("TRUNCATE TABLE refunds_by_order");

	$searchsql = 'SELECT system_note_key, notes_system.order_id, system_note, note_date, orders.gateway_id
from notes_system
LEFT JOIN orders ON orders.order_id = notes_system.order_id
WHERE system_note LIKE "%Order refunded.%"
ORDER BY note_date DESC';
	$result = mysqli_query($mysqli, $searchsql);
	while($row = $result->fetch_row())
	{
		$system_note_key = $row[0];
		$order_id = $row[1];
		$note = $row[2];
		$date = $row[3];
		$gateway = $row[4];

		/*
		 * this is [as] messy as liquid shit, but has to be done because of how inconsistent the limelight system notes can occasionally be.
		 */
//**********first, find the username
//$username = [below code]
			$fragments = explode("-", $note);
			/*
			 * Should make $fragments look like this:
			 * [0] => date string from the note
			 * [1] => username
			 * [2] => username pt 2 OR! the rest of the note (Order refunded. + price + 'Transaction ID' xxxxxxx)
			 * [3] => The rest of the note if [2] turns out to be 'username pt 2'
			 */
			//if ptel username (or something else?)
			if(count($fragments) > 3)
			{
				$username = $fragments[1]."-".$fragments[2];
			}
			else
			{
				$username = $fragments[1];
			}
//**********Second, find the order refunded amount
//$refunded = [below code]
			$dollar_sign = strpos($note, "$");
			$dollar_sign++;
			$second_dot = strrpos($note, ".");
			$pos = $second_dot - $dollar_sign;
			$refunded = substr($note, $dollar_sign, $pos);
//**********Third, find the transaction ID
//$transactionId = [below code]
			$transactionId = trim(substr($note,strrpos($note,":")+1,strlen($note)));

	$insertSQL = "
		INSERT INTO `limelight`.`refunds_by_order`
		(`order_id`, `transaction_id`, `cs_rep`, `refund_amount`, `note`, `date`, `gateway_id`)
		VALUES
		('$order_id', '$transactionId', '$username', '$refunded', '$note', '$date', '$gateway');
		";
		doDbthing($insertSQL, $mysqli);
	}
	$mysqli->close();
}
?>