<?php
require('require_all_limelight.php');
/*
 * Config for poll.php:
 */
/*
 * The frequency to run checks on orders. Default is 10.
 */
$check_every_this_many_seconds = 10;
/*
 * When the orders are checked, if the numbers match this many times in a row, initiate a 'stall' resolution
 * Default is 10.
 */
$how_many_times_should_orders_match = 10;
/*
 * How many times is too many times to stall? As in, after this many times, do something different, because
 * stalling out has become a normal situation and we don't like that.
 * Default is 3.
 */
$how_many_times_is_too_many_times_to_stall = 3;



$mysqli = initialize_db('limelight');
/*
 * Some variables to keep track of where we are in this script:
 */
$done = 0;
$number_processed = 0;
$times_orders_matched = 0;
$times_stalled =0;
while ($done < 6) //we do need this, because sometimes it takes like 5 seconds for workers to
{
    $check = $mysqli->query("SELECT count(order_id) FROM orders_to_process WHERE complete = 0")->fetch_assoc();
    $remaining = $check['count(order_id)'];
	if ($remaining > 0)
	{
        $done =0; //reset this, incase the workers were killed or something.
        $orders_pulled = $mysqli->query("SELECT count(order_id) FROM orders")->fetch_assoc();
        $orders_pulled = $orders_pulled['count(order_id)'];
        $mysqli->query("INSERT INTO poll (count_, when_) VALUES ('$orders_pulled', NOW())");
		/*
		 * This if statement checks if the numbers pulled from the last time this was run are still the same (a stall condition)
		 * We need to check many times though, because it could just be that the workers are still loading and not actually stalled.
		 */
		if($orders_pulled === $number_processed)
		{
			log_error("orders matched, incrementing from $times_orders_matched to +1 of that", "poll");
			$times_orders_matched++;
		}
		else
		{
			log_error("orders pulled is $orders_pulled and number_left is $number_processed. Resetting times_orders_matched to 0 from $times_orders_matched", "poll");
			$number_processed = $orders_pulled;
			$times_orders_matched = 0;
		}
		/*
		 * Now, we should probably check if we have stalled on orders that haven't worked...
		 */
		if($times_orders_matched > $how_many_times_should_orders_match)
		{
			log_error("Orders have matched more than allowable.... We've probably stalled.", "poll");
			/*
			 * If we haven't stalled out too many times....
			 */
			if($times_stalled < $how_many_times_is_too_many_times_to_stall)
			{
				log_error("Updating stalled orders to hopefully be re-run", "poll");
				$mysqli->query("UPDATE orders_to_process SET worker = '0' WHERE complete = '0' AND worker NOT LIKE '0'");
				log_error($mysqli->affected_rows." sql rows affected by UPDATE orders_to_process SET worker = '0' WHERE complete = '0' AND worker NOT LIKE '0'", "poll");
				$times_stalled++;
				$times_orders_matched = 0;
			}
			/*
			 * Too many stalls, just give up and move on....
			 */
			else
			{
				log_error("too many stalls, just marking EVERYTHING! complete so things can move along", "poll");
				$mysqli->query("UPDATE orders_to_process SET complete = '1'");
				$times_stalled = 0;
			}
		}
        sleep($check_every_this_many_seconds);
    }
	else
	{
		log_error("poll has detected that the orders are done pulling now....", "poll");
		$done++;
		sleep($check_every_this_many_seconds);
	}
}

log_error("poll is now finished, since there are no more orders left to pull.", "poll");
