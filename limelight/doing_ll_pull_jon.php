<?php
require('require_all_limelight.php');
if (defined('STDIN')) {
    $username = $argv[1];
    $password = $argv[2];
    $worker = $argv[3];
} else {
    die("STDIN failed");
}


//$username = "biohealth4";
//$password = "FVJzA42euHVnE";
//$worker = "w3";
$done=0;



log_error("$username $worker has just started", $worker);

//start pulling and only stop when all orders are handled

    $mysqli = initialize_db("limelight", "biohealth4", "FVJzA42euHVnE");
//log_error("$username $worker has just DBed", $worker);


    $query_result = $mysqli->prepare("SELECT order_id FROM orders_to_process WHERE (worker = '0' OR worker = '$worker') AND complete = '0' LIMIT 200");

    //$query_result = $mysqli->prepare("SELECT order_id FROM orders_to_process WHERE order_id LIKE '664786' OR order_id LIKE '664610'");
    $query_result->execute();

    $query_result->bind_result($curr_order_id);

    $query_string =""; //build a mass-update query so that we can assign orders to a worker
    $ids_to_get = "";  //build a big string with all of our IDs

    $query_string = "UPDATE orders_to_process SET worker = '$worker' WHERE ";
    //build ID string
    while ($query_result->fetch())
    {
        $ids_to_get .= $curr_order_id.","; //build the string of IDs to get so that we can use all these IDs and update our data.
        $query_string .= "order_id = '$curr_order_id' OR ";
    }
	//log_error("$username $worker has just updated orders to process as $query_string with ids $ids_to_get", $worker);


    $query_string = substr($query_string, 0, -3); //chop off last ','
    $query_string .= ";";
    $ids_to_get = substr($ids_to_get, 0, -1); //chop off last ','
    $mysqli->multi_query($query_string) or die(mysqli_errno($mysqli));

	//log_error($ids_to_get, $worker);

    //call LL order_view with order id
    $data = array(
        "method" => "order_view",
        "order_id" => $ids_to_get,
		"version" => "2"
    );
    //get results from LL API

    $output = worker_ll_api_jon($data, $username, $password);

//log_error(" data: ".var_export($output), $worker);

    /*
     * Here's where it's potentially gunna get a bit tricky.
     * We've got to parse through each order now, and database it.
     */
    if(@$output['response_code'] == '100')
    {
        if(isset($output['data']))
        {
            $output['data'] = str_replace("\t",'',$output['data']);
            /*
             * The following crazy pregreplace thing removes all non-alphanumeric characters
             *
             * and allows php's json_decode function to work properly. Apparently limelight was giving us non-utf8 stuff?
             */
            $replace = '/[^A-Za-z0-9\s!@#$%\^&*()\[\]\{\}\"\\\';:,<\.>\?\`\~\\\|\/\+\-\_\=*]/';
            $output['data'] = preg_replace($replace, '', $output['data'], -1, $count);

            //echo "REPLACED: ".$count;
            //$output['data'] = str_replace("\b",'',$output['data']);
            //$output['data'] = str_replace("\f",'',$output['data']);
            //$output['data'] = str_replace("\m",'',$output['data']);
            //$output['data'] = str_replace("\n",'',$output['data']);
            //$output['data'] = str_replace("\r",'',$output['data']);
            //$output['data'] = str_replace("\u",'',$output['data']);


            $orders = json_decode($output['data'],true);

            if(json_last_error() == 5)
            {
                //var_dump($output['data']);
				/*
                log_error("Bad data in some way:

                  ".$output['data']."

                ".date('Y-m-d H:i:s')."

                ".json_last_error_msg(), $worker);
*/
				log_error("1/2 Bad data in some way".json_last_error_msg(), $worker);
				log_error("2/2 Data: ".var_export($output), $worker);
				//log_error("2/2 Data: ".$output['data']);
				//var_dump($output['data']);
                $done++;
                exit();
            }
            else
            {
                //echo "this code is good";
            }
            foreach($orders as $order_id=>$order_data)
            {
                $all_orders[$order_id] = array();

                foreach($order_data as $field=>$value)
                {
                    if(strpos($field,'[') !== false)
                    {
                        $arr = array();
                        parse_str($field,$arr);

                        fixLLFields($all_orders[$order_id],$arr,$value);
                    }
                    else
                    {
                        $all_orders[$order_id][$field] = $value;
                    }
                }
            }

            foreach($all_orders as $id=>$order)
            {
                {
                    if(no_collision($id, $worker))
                    {
						//log_error("$worker about to run DB entry methods", $worker);
						insert_orders_jon($id, $order,$mysqli) or die("order died");
                        insert_customers_jon($id, $order,$mysqli) or die("customers died");
                        insert_transactions_jon($id, $order,$mysqli) or die("transactions died");
                        insert_order_details_jon($id, $order,$mysqli) or die("order details died");
                        insert_notes_jon($id, $order,$mysqli) or die("notes died");
                        update_orders_to_process($id, $worker, $mysqli);
						//log_error("$worker just finished db entry", $worker);
                    }
                    else
                    {
                        log_error("\n COLLISION ".date('Y-m-d H:i:s'), $worker);
                        continue;
                    }
                }
            }
        }
    }
    else
	{
		/*
        log_error("error parsing somehow. Data:

        ".$output['data'], $worker);
			$myfile = fopen($worker."_error".time(), "a+"); //or die("Unable to open file!");
			fwrite($myfile, $output['data'] . " "  . date('Y-m-d H:i:s') . "\r\n\"");
			fclose($myfile);
		*/
		log_error("Error parsing data: ".var_export($output), $worker);
	}
    $mysqli->close();
	log_error("$username $worker has just FINISHED", $worker);

/*
 * Check that another worker isn't currently working on our thing.
 *
 * Cuz then they'll crash into each other like that Dave Matthews song comes CRAASSHHH innnn. tooooo. youuuuuuu!
 *
 * You're welcome for the earworm. <3
 */
function no_collision($id, $worker)
{
    //double check again to make sure another worker didn't already update this one
    $mysqli = initialize_db("limelight", "biohealth5", "c8WJabw4RqjEVu");
    $query_result = $mysqli->prepare("SELECT order_id, worker FROM orders_to_process WHERE order_id = '$id' LIMIT 1");
    $query_result->execute();
    $query_result->bind_result($id_stuff, $wid);

    //build ID string
    while ($query_result->fetch())
    {
        if($wid != $worker)
        {
            $mysqli->close();
            return 0;
        }
        else
        {
            $mysqli->close();
            return 1;
        }
    }
}

function fixLLFields(&$order_array_nest,$fields,$final_value) {
    foreach($fields as $key=>$value) {
        if(is_array($value)) {
            if(!isset($order_array_nest[$key])) {
                $order_array_nest[$key] = array();
            }
            fixLLFields($order_array_nest[$key],$value,$final_value);
        }
        else {
            $order_array_nest[$key] = $final_value;
        }
        return;
    }
}
?>
