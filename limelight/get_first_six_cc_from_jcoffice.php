<?php
require('require_all_limelight.php');

//get_first_six();

/*
 * Get Order Data
 */
function get_first_six()
{
	//$start_date = "2010-01-01";
	//$end_date = "2015-04-08";
	$start_date         =  date('Y-m-d',strtotime("-1 days"));
	$end_date = date('Y-m-d',strtotime("today"));

	$post_data = array(
		"username" => "reporting",
		"password" => "jFKL#$0*%)GFnms334098sdfds",
		"start_date" => $start_date,
		"end_date" => $end_date
	);

	$url = "https://jcoffice.net/api/rest/logging/getBinData";
	$curlSession = curl_init();
	curl_setopt($curlSession, CURLOPT_URL, $url);
	curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curlSession, CURLOPT_POST, 1);
	curl_setopt($curlSession, CURLOPT_POST,count($post_data));
	curl_setopt($curlSession, CURLOPT_POSTFIELDS, $post_data);
	curl_setopt($curlSession, CURLOPT_TIMEOUT, 500000);
	$rawresponse = curl_exec($curlSession);
	curl_close($curlSession);

	$array_good = json_decode($rawresponse, 1);

	if($array_good["code"] == "100")
	{
		$mysqli = initialize_db('limelight');
		/*
		 * Foreach entry
		 */
		foreach($array_good["data"] as $ll_order=>$data)
		{
			$first_six = $data["first_six"];
			$insertSQL = sprintf("INSERT INTO first_6_cc_numbers_of_orders
			(order_id, first_six)
			VALUES (%s,%s)",

			sanatize_valuess($ll_order, $mysqli),
			sanatize_valuess($first_six, $mysqli));

			$mysqli->query($insertSQL) or die($mysqli->error);
		}
		$mysqli->close();
	}
}
/*
	 * Some bizarre function that RK made
	 */
function sanatize_valuess($theValue, $mysqli)
{
	$theValue = (is_array($theValue) ? "" : $theValue);
	$theValue = $mysqli->real_escape_string($theValue);
	$theValue = "'".$theValue."'";
	return $theValue;
}
/*
function initialize_db($mysqli, $username="biohealth1", $password="7s63VfusbHVFuq")
{
	$mysqli = new mysqli("localhost",$username,$password,'limelight');
	// Check connection
	if (mysqli_connect_errno())
	{
		printf("Connect failed: %s\n", mysqli_connect_error());
		//log_error(mysqli_connect_error(), "error".getmypid());
		exit();
	}
	return $mysqli;
}
*/
?>