<?php
/**
 * Created by PhpStorm.
 * User: jonjenne
 * Date: 2/26/15
 * Time: 1:48 PM
 */
function getDBName()
{
	return "hitpath_jumbleberry";
}
/*
 * Formats the 'echo' statements in the log files so that it is more pretty
 */
function logIt($error)
{
	if($error !== "")
	{
		//echo (date('r', time())." (UTC) : \n");
		//echo $error."\n";
	}
}

function initialize_db($mysqli, $username="biohealth1", $password="7s63VfusbHVFuq"){
//function initialize_db($mysqli){
	//var_dump(debug_backtrace());
	// die();
	//$mysqli = (!isset($mysqli) ? $mysqli = 'limelight' : $mysqli);

	//echo $mysqli;
	//open up our connection
	$mysqli = new mysqli("localhost",$username,$password,$mysqli);
	// Check connection
	if (mysqli_connect_errno()) {
		//printf("Connect failed: %s\n", mysqli_connect_error());
		logIt(mysqli_connect_error());
		exit();
	}
	return $mysqli;
}

function doDBthing($query)
{
	$mysqli = initialize_db(getDBName());

	if($prepared_mysqli = $mysqli->prepare($query))
	{
		$prepared_mysqli->execute();

		$prepared_mysqli->store_result();

		$prepared_mysqli->free_result();

		$prepared_mysqli->close();
	}
	else
	{
		logIt("\n $query \n");
		logIt(mysqli_connect_error());
	}
	return true;
}
/*
 * Truncate the database
 */
function prepare_database()
{
	$mysqli = initialize_db(getDBName());

	if ($mysqli->connect_error)
	{
		logIt("Failed to connect to MySQL. Unable to Truncate: " .$mysqli->connect_error);
		die();
	}
	$mysqli->query("TRUNCATE TABLE clicks");
	$mysqli->close();
}
?>
