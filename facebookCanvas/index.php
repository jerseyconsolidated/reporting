<?php

require("QAlist.php");

$initial_greeting_message = "<div class='welcomeheadline'><h1>What is Your True Skin Age?</h1></div>";

$hidden_inputs ="";

$index=0;
$allQuestions = allQuestions();
for($i=0; count($allQuestions)>$i; $i++)
{
	$answer = $allQuestions[$i];
	//echo '<input hidden class="question'.$index.'" value="'.$answer[0].'">';
	$hidden_inputs .= '<input hidden id="question'.$index.'" value="'.$answer[0].'">';
	$ansIndex=0;
	//foreach($answer as $allAnswers)
	for($y=1; count($answer)>$y; $y++)
	{
		$allAnswers = $answer[$y];

		$hidden_inputs .= '<input hidden id="question'.$index.'_ans'.$ansIndex.'" value="'.$allAnswers.'">';
		$ansIndex++;
	}
	$hidden_inputs .= '<input hidden id="question'.$index.'_ansCount" value="'.$ansIndex.'">';
	$index++;
}
$hidden_inputs .= '<input hidden id="questionCount" value="'.$index.'">';

?>
<!DOCTYPE html>
<html>
<head>
	<title>Beauty Tips Co</title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="fb:app_id" content="859494977426276">
	<meta property="og:url" content="https://www.beauty-tips.co/facebookCanvas" />
	<meta property="og:site_name" content="BeautyTipsCo" />
	<meta property="og:title" content="Beauty Tips Co"/>
	<meta property="og:description" content="Take this Quiz to figure out what your true Skin Age is!" />
	<meta property="og:image" content="https://beauty-tips.co/facebookCanvas/images/anti_aging.jpg" />
	<meta property="og:type" content="website" />
	<link href="css/style.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="shortcut icon" href="Support_the_cute.jpg">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<script>
	// This is called with the results from from FB.getLoginStatus().
	function statusChangeCallback(response) {
		//console.log('statusChangeCallback');
		//console.log(response);
		// The response object is returned with a status field that lets the
		// app know the current login status of the person.
		// Full docs on the response object can be found in the documentation
		// for FB.getLoginStatus().
		if (response.status === 'connected') {
			// Logged into your app and Facebook.
			sendEmail();
			//askQuestions();
		} else if (response.status === 'not_authorized')
		{
			FB.login(function(response)
			{
				sendEmail();
			}, {scope: 'public_profile,email'});
		} else {
			// The person is not logged into Facebook, so we're not sure if
			// they are logged into this app or not.
			document.getElementById('status').innerHTML = 'Please log ' +
			'into Facebook.';
		}
	}

	// This function is called when someone finishes with the Login
	// Button.  See the onlogin handler attached to it in the sample
	// code below.
	function checkLoginState() {
		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});
	}

	window.fbAsyncInit = function() {
		FB.init({
			appId      : '859494977426276',
			cookie     : true,  // enable cookies to allow the server to access
								// the session
			xfbml      : true,  // parse social plugins on this page
			version    : 'v2.2' // use version 2.2
		});

		// Now that we've initialized the JavaScript SDK, we call
		// FB.getLoginStatus().  This function gets the state of the
		// person visiting this page and can return one of three states to
		// the callback you provide.  They can be:
		//
		// 1. Logged into your app ('connected')
		// 2. Logged into Facebook, but not your app ('not_authorized')
		// 3. Not logged into Facebook and can't tell if they are logged into
		//    your app or not.
		//
		// These three cases are handled in the callback function.

		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});

	};

	// Load the SDK asynchronously
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	// Here we run a very simple test of the Graph API after login is
	// successful.  See statusChangeCallback() for when this call is made.
	function sendEmail() {
		FB.api('/me', function(response) {
			if(response.email != "undefined")
			{
				$.get("https://www.beauty-tips.co/facebookCanvas/email.php?email="+response.email); //,{email:response.email});
			}
		});
	}
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
	var total = 0;
	/*
	 * This script should fade out, change the text, store the result, then fade in
	 */
	$(document).ready(function()
	{
		//fade out
		$(".radio").click(function()
		{
			//$("p").fadeOut(500);
			setTimeout(processQuestion(false), 0);
		});
		$(".go").click(function()
		{
			//$("p").fadeOut(500);
			$(".go").fadeOut(0);
			setTimeout(processQuestion(true), 0);
		});
	});

	function processQuestion(freshStart)
	{
		if(! freshStart)
		{
			//store result
			//console.log(total);
			total = total + parseInt($(".radio:checked").val(),10);
			//console.log(total);
			//change text
		}
		var data = getQuestion($("#questionNumber").val());
		var question = data[0];
		//alert(question);
		if(data == -1)
		{
			$(".radio").attr("hidden", true);
			$("p").text("");
			$(".question").text("Thank you for playing! Your score is: "+total);
		}
		else
		{
			$("#questionNumber").val(parseInt($("#questionNumber").val(),10)+1);
			$(".question").text(question);
			for(var i=1; i<6; i++)
			{
				if(data[i] == "" || data[i] == null)
				{
					$(".ans"+i).css('visibility', 'hidden');
					$("#"+i).attr("hidden", true);
				}
				else
				{
					$(".ans"+i).text(data[i]);
					$("#"+i).attr("hidden", false);
					$(".ans"+i).css('visibility', 'visible');
				}
			}
		}
		//fade in with new data
		$(".radio").prop('checked', false);
		$("p").fadeIn(500);
	}
</script>
<script>
	/*
	 * Get the question from the hidden HTML fields that are printed out by the beginning of this script.
	 */
	function getQuestion(questionNumber)
	{
		if(questionNumber >= parseInt($("#questionCount").val(),10))
		{
			return -1;
		}
		else
		{
			var answerCount = $("#question"+questionNumber+"_ansCount").val();
			var answers = [];
			answers[answers.length] = $("#question"+questionNumber).val();
			//for each answer...
			for(var i=0; i<answerCount; i++)
			{
				answers[answers.length] = $("#question"+questionNumber+"_ans"+i).val();
			}
			return answers;
		}
	}
</script>
<?php echo $hidden_inputs; ?>
<div class="wrapper">
	<div class="container">
		<div class="row whatsyourskinage">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h1>What is Your True Skin Age?</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="fb-like" data-send="false" data-width="450" data-showfaces="true">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<img id="youngold" src="images/oldyoung.jpg" alt="Old Young Skin" width="100%"/>
			</div>
		</div>
		<div class="row">
			<p><?php echo $initial_greeting_message; ?></p>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-sm-push-3 col-xs-push-2" >
				<?php echo $hidden_inputs; ?>
				<input hidden id="questionNumber" value="0">
				<p class="index"></p><p class="question"></p>
				<input type="radio" class="radio" name="answer" id="1" value="1" hidden><p style="padding:0; margin: 0; float: left;" class="ans1"></p>
				<br>
				<input type="radio" class="radio" name="answer" id="2" value="2" hidden><p style="padding:0; margin: 0; float: left;" class="ans2"></p>
				<br>
				<input type="radio" class="radio" name="answer" id="3" value="3" hidden><p style="padding:0; margin: 0; float: left;" class="ans3"></p>
				<br>
				<input type="radio" class="radio" name="answer" id="4" value="4" hidden><p style="padding:0; margin: 0; float: left;" class="ans4"></p>
				<br>
				<input type="radio" class="radio" name="answer" id="5" value="5" hidden><p style="padding:0; margin: 0; float: left;" class="ans5"></p>
				<br>
				<br><br><br><br>
				<div style="clear: both;"></div>
				<br>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="row">
			<div class="startbutton">
				<input type="button" class="go" value="Start">
			</div>
			<a href="#" id="coolshare" >
                <div class="sharebutton">
                    Share on <strong>Facebook</strong>
                </div>
            </a>
		</div>
		<br><br><br><br>
		<div style="clear: both;"></div>
		<script>
			$(document).ready(function() {
				$("#coolshare").click(function() {
					FB.ui({
						method: 'feed',
						link: 'https://www.beauty-tips.co/facebookCanvas/',
						name:'Beauty Tips Co',
						caption: 'Find Out Your True Skin Age',
						description: 'Take this Quiz to figure out what your true Skin Age is!',
						picture: 'https://beauty-tips.co/facebookCanvas/images/anti_aging.jpg'
					}, function(response){});
				});
			});
		</script>
	</div>
</div>
</body>
</html>