<?php

function allQuestions()
{
	/*
	 * The format for questions is below. The questions will be asked in the order that they are listed.
	 *
	 * Each question is it's own array with the question itself in the 0th position. Answers (up to 5 answers) follow.
	 *
	 * An example is like:
	 	array(
			"Why can't Greece pay it's bills?",
				"I can't either, so who can?",
				"They're lazy",
				"The EU is doomed to fail",
				"Because Germenay will bail them out"),

	 * TAKE NOTE! The ()'s and "'s and ,'s MUST BE THERE!!!
	 *
	 * Also, DO NOT include the ^ symbol in either the question or the answer(s). That's used as a delimiter.
	 */
	$arry = array(
		array(
			"Why do you think you get tired?",
			"Because I'm always tired",
			"some other answer",
			"because I was up late last night",
			"because ?",
			"dolphins don't, so why should i?"),
		array(
			"Why can't Greece pay it's bills?",
			"I can't either, so who can?",
			"They're lazy",
			"The EU is doomed to fail",
			"Because Germenay will bail them out"),
		array(
			"What if Japan moved to California?",
			"because ?",
			"I can't either, so who can?",
			"They're lazy",
			"California would speak Japanese!"),
		array(
			"What if the North Korean regime falls apart before I get to visit?",
			"Bummer.",
			"They won't",
			"They'll open up a disney park in Pyongyang"),
		array(
			"What will I do without Jon Stewart?",
			"Watch Trevor Noah")
	);
	return $arry;
}

function getQuestion($question)
{
	$arry = allQuestions();
	$finalanswer="";
	if(isset($arry[$question]))
	{
		//$question = $arry[$question][0];
		$answers = $arry[$question];
		$index=0;
		foreach($answers as $answer)
		{
			//var_dump($answers);
			if($index ==0)
			{
				$finalanswer=$answer;
			}
			else
			{
				$finalanswer.="^".$answer;
			}
			$index++;
		}
		return $finalanswer;
	}
	else
	{
		return -1;
	}
}

//echo getQuestion($_GET['question_number']);

?>