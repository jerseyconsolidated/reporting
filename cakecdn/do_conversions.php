<?php
/**
 * Created by PhpStorm.
 * User: jonjenne
 * Date: 2/26/15
 * Time: 1:45 PM
 */
error_reporting(E_ALL);
ini_set("display_errors", 1);
require('general_functions.php');
require ('../includes/RayGun/RayGun.php');
ini_set('memory_limit', '-1');
date_default_timezone_set('UTC');

echo "\n"; // just make things look purdy
/*
 * This API works so gosh-darn fast that we don't have to do anything wonky with it.
 * We can just straight-up ask for all the data, and *BLAM* it comes, in like 2 seconds or less.
 *
 * GLOBAL VARIABLES BELOW!
 */
$start_date         = "2014-01-01"; //Format: YYYYMMDDhhmmss     (hhmmss optional)
$end_date = date('Y-m-d',strtotime("-1 days"));
$offer_id = 0; //make it '0' for "all"

$post_data = array(
	"api_key" => "NuGQRL8TqcM",
	"affiliate_id" => "4961",
	"start_date" => $start_date,
	"end_date" => $end_date,
	"currency_id" => 0,
	"disposition" => "",
	"exclude_bot_traffic" => "true",
	"offer_id" => 0,
	"start_at_row" => 1,
	"row_limit" => 0
);
$post_array_string = "";
//url-ify the data (the API wants this as a string, not an array)
foreach($post_data as $key=>$value)
{
	$post_array_string .= $key.'='.$value.'&';
}
$url = "http://c2mtrax.com/affiliates/api/6/reports.asmx/Conversions";
$curlSession = curl_init();
curl_setopt($curlSession, CURLOPT_URL, $url);
curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curlSession, CURLOPT_POST, 1);
curl_setopt($curlSession, CURLOPT_POST,count($post_data));
curl_setopt($curlSession, CURLOPT_POSTFIELDS, $post_array_string);
curl_setopt($curlSession, CURLOPT_TIMEOUT, 500000);
$rawresponse = curl_exec($curlSession);
curl_close($curlSession);

//var_dump($rawresponse);
$array_good = simplexml_load_string($rawresponse);
$array_good = array(unserialize(serialize(json_decode(json_encode((array)$array_good), 1))));
$mysqli = initialize_db(getDBName(), "biohealth4", "FVJzA42euHVnE");

if (isset($array_good[0]['success']))
{
	for ($i = 0; $i < $array_good[0]['row_count']; $i++)
	{
		if(isset($array_good[0]['conversions']['conversion'][$i]['order_id']))
		{
			$insertSQL = sprintf("INSERT INTO conversions
			(id, conversion_id, order_id, conversion_date, offer_id, offer_name, campaign_id, creative_id, subid_1,
			subid_2, subid_3, subid_4, subid_5, price, disposition, test, currency_symbol)
			VALUES ('',%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
			sanatize_values($array_good[0]['conversions']['conversion'][$i]['conversion_id'], $mysqli),
			sanatize_values($array_good[0]['conversions']['conversion'][$i]['order_id'], $mysqli),
			sanatize_values($array_good[0]['conversions']['conversion'][$i]['conversion_date'], $mysqli),
			sanatize_values($array_good[0]['conversions']['conversion'][$i]['offer_id'], $mysqli),
			sanatize_values($array_good[0]['conversions']['conversion'][$i]['offer_name'], $mysqli),
			sanatize_values($array_good[0]['conversions']['conversion'][$i]['campaign_id'], $mysqli),
			sanatize_values($array_good[0]['conversions']['conversion'][$i]['creative_id'], $mysqli),
			sanatize_values($array_good[0]['conversions']['conversion'][$i]['subid_1'], $mysqli),
			sanatize_values($array_good[0]['conversions']['conversion'][$i]['subid_2'], $mysqli),
			sanatize_values($array_good[0]['conversions']['conversion'][$i]['subid_3'], $mysqli),
			sanatize_values($array_good[0]['conversions']['conversion'][$i]['subid_4'], $mysqli),
			sanatize_values($array_good[0]['conversions']['conversion'][$i]['subid_5'], $mysqli),
			sanatize_values($array_good[0]['conversions']['conversion'][$i]['price'], $mysqli),
			sanatize_values($array_good[0]['conversions']['conversion'][$i]['disposition'], $mysqli),
			sanatize_values($array_good[0]['conversions']['conversion'][$i]['test'], $mysqli),
			sanatize_values($array_good[0]['conversions']['conversion'][$i]['currency_symbol'], $mysqli));

			$mysqli->query($insertSQL) or die($mysqli->error);
			//$rows=$rows+mysqli_affected_rows($mysqli);
		}
	}
}
$mysqli->close();

/*
 * Some bizarre function that RK made
 */
function sanatize_values($theValue, $mysqli)
{
	$theValue = (is_array($theValue) ? "" : $theValue);
	$theValue = $mysqli->real_escape_string($theValue);
	$theValue = "'".$theValue."'";
	return $theValue;
}
?>