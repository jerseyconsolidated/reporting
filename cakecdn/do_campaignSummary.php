<?php
/**
 * Created by PhpStorm.
 * User: jonjenne
 * Date: 2/26/15
 * Time: 1:45 PM
 */
error_reporting(E_ALL);
ini_set("display_errors", 1);
require('general_functions.php');
require ('../includes/RayGun/RayGun.php');
ini_set('memory_limit', '-1');
date_default_timezone_set('UTC');

echo "\n"; // just make things look purdy
/*
 * This API works so gosh-darn fast that we don't have to do anything wonky with it.
 * We can just straight-up ask for all the data, and *BLAM* it comes, in like 2 seconds or less.
 *
 * GLOBAL VARIABLES BELOW!
 */
$start_date         = "2014-01-01"; //Format: YYYYMMDDhhmmss     (hhmmss optional)
$end_date = date('Y-m-d',strtotime("-1 days"));
$offer_id = 0; //make it '0' for "all"

$post_data = array(
	"api_key" => "NuGQRL8TqcM",
	"affiliate_id" => "4961",
	"start_date" => $start_date,
	"end_date" => $end_date,
	"sub_affiliate" => "",
	"sort_field" => "offer_id",
	"sort_descending" => "true",
	"start_at_row" => 1,
	"row_limit" => 0
);
$post_array_string = "";
//url-ify the data (the API wants this as a string, not an array)
foreach($post_data as $key=>$value)
{
	$post_array_string .= $key.'='.$value.'&';
}
$url = "http://c2mtrax.com/affiliates/api/4/reports.asmx/CampaignSummary";
$curlSession = curl_init();
curl_setopt($curlSession, CURLOPT_URL, $url);
curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curlSession, CURLOPT_POST, 1);
curl_setopt($curlSession, CURLOPT_POST,count($post_data));
curl_setopt($curlSession, CURLOPT_POSTFIELDS, $post_array_string);
curl_setopt($curlSession, CURLOPT_TIMEOUT, 500000);
$rawresponse = curl_exec($curlSession);
curl_close($curlSession);

//var_dump($rawresponse);
$array_good = simplexml_load_string($rawresponse);
$array_good = array(unserialize(serialize(json_decode(json_encode((array)$array_good), 1))));
$mysqli = initialize_db(getDBName(), "biohealth4", "FVJzA42euHVnE");

if (isset($array_good[0]['success']))
{
	/*
	 * This one is a little bit more complicated. We have 2 sets of data to pull.
	 *
	 * First, we have to pull the campaign_summary.
	 *
	 * Then, we have to pull all the campaign data.
	 */

	/*
	 * First, the campaign summary.
	 */
	$insertSQL = sprintf("INSERT INTO campaign_summary
		(id, clicks, conversions, conversion_rate, revenue, revenue_converted, currency_symbol,
		currency_symbol_converted)
		VALUES ('',%s,%s,%s,%s,%s,%s,%s)",
		sanatize_values($array_good[0]['summary']['clicks'], $mysqli),
		sanatize_values($array_good[0]['summary']['conversions'], $mysqli),
		sanatize_values($array_good[0]['summary']['conversion_rate'], $mysqli),
		sanatize_values($array_good[0]['summary']['revenue'], $mysqli),
		sanatize_values($array_good[0]['summary']['revenue_converted'], $mysqli),
		sanatize_values($array_good[0]['summary']['currency_symbol'], $mysqli),
		sanatize_values($array_good[0]['summary']['currency_symbol_converted'], $mysqli));

	$mysqli->query($insertSQL) or die($mysqli->error);
	/*
	 * Now, for the campaign data.
	 */
	for ($i = 0; $i < $array_good[0]['row_count']; $i++)
	{
		$insertSQL = sprintf("INSERT INTO campaigns
		(id, offer_id, offer_name, campaign_id, vertical_name, price_format, price, impressions,
		clicks, conversions, conversions_rate, revenue, revenue_converted, epc, currency_symbol,
		currency_symbol_converted)
		VALUES ('',%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
		sanatize_values($array_good[0]['campaigns']['campaign'][$i]['offer_id'], $mysqli),
		sanatize_values($array_good[0]['campaigns']['campaign'][$i]['offer_name'], $mysqli),
		sanatize_values($array_good[0]['campaigns']['campaign'][$i]['campaign_id'], $mysqli),
		sanatize_values($array_good[0]['campaigns']['campaign'][$i]['vertical_name'], $mysqli),
		sanatize_values($array_good[0]['campaigns']['campaign'][$i]['price_format'], $mysqli),
		sanatize_values($array_good[0]['campaigns']['campaign'][$i]['price'], $mysqli),
		sanatize_values($array_good[0]['campaigns']['campaign'][$i]['impressions'], $mysqli),
		sanatize_values($array_good[0]['campaigns']['campaign'][$i]['clicks'], $mysqli),
		sanatize_values($array_good[0]['campaigns']['campaign'][$i]['conversions'], $mysqli),
		sanatize_values($array_good[0]['campaigns']['campaign'][$i]['conversion_rate'], $mysqli),
		sanatize_values($array_good[0]['campaigns']['campaign'][$i]['revenue'], $mysqli),
		sanatize_values($array_good[0]['campaigns']['campaign'][$i]['revenue_converted'], $mysqli),
		sanatize_values($array_good[0]['campaigns']['campaign'][$i]['epc'], $mysqli),
		sanatize_values($array_good[0]['campaigns']['campaign'][$i]['currency_symbol'], $mysqli),
		sanatize_values($array_good[0]['campaigns']['campaign'][$i]['currency_symbol_converted'], $mysqli));

		$mysqli->query($insertSQL) or die($mysqli->error);
		//$rows=$rows+mysqli_affected_rows($mysqli);
	}
}
else
{
	var_dump($rawresponse);
}
$mysqli->close();

/*
 * Some bizarre function that RK made
 */
function sanatize_values($theValue, $mysqli)
{
	$theValue = (is_array($theValue) ? "" : $theValue);
	$theValue = $mysqli->real_escape_string($theValue);
	$theValue = "'".$theValue."'";
	return $theValue;
}
?>