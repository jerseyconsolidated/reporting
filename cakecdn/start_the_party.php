<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require ('../includes/RayGun/RayGun.php');
ini_set('memory_limit', '-1');
echo "\n"; // just make things look purdy

/*
 * This file is responsible for starting the party!!!
 *
 * It clears out the database for each script, then starts each script.
 */



/*
 * servadora
 */
$path = "/var/www/reporting.jcoffice.net/cakecdn"; //DO NOT INCLUDE THE /
/*
 * Jon-a-dora
 */
//$path = "/Users/jonjenne/reporting/cakecdn"; //DO NOT INCLUDE THE /
$commands[] = "php $path/do_bills.php > /dev/null";
$commands[] = "php $path/do_campaignSummary.php > /dev/null";
$commands[] = "php $path/do_conversions.php > /dev/null";
$commands[] = "php $path/do_dailySummary.php > /dev/null";
$commands[] = "php $path/do_hourlySummary.php > /dev/null";

/*
 * Truncate all tables, prepare "days_to_process" table for workers
 */
prepare_database();
/*
 * Run each command.
 */
foreach($commands as $command)
{
	//echo "\n".$command."\n";
	//die();
	$result = pclose(popen("$command", "r"));

	//sleep(1);
	if($result)
	{
		echo ("Failed to start $command ... This command did not start.");
	}
}


/*
 * Any general functions that might be required
 */
/*
 * Clear out these DB tables
 */
function prepare_database()
{
	$mysqli = initialize_db('cakecdn');
	$mysqli->query("TRUNCATE TABLE bills");
	$mysqli->query("TRUNCATE TABLE campaign_summary");
	$mysqli->query("TRUNCATE TABLE campaigns");
	$mysqli->query("TRUNCATE TABLE conversions");
	$mysqli->query("TRUNCATE TABLE daily_summary");
	$mysqli->query("TRUNCATE TABLE hourly_summary");
	$mysqli->close();
}
function initialize_db($mysqli, $username="biohealth1", $password="7s63VfusbHVFuq")
{
	$mysqli = new mysqli("localhost",$username,$password,$mysqli);
	if (mysqli_connect_errno())
	{
		exit();
	}
	return $mysqli;
}
?>