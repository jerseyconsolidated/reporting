<?php
/**
 * Created by PhpStorm.
 * User: jonjenne
 * Date: 2/26/15
 * Time: 1:45 PM
 */
error_reporting(E_ALL);
ini_set("display_errors", 1);
require('general_functions.php');
require ('../includes/RayGun/RayGun.php');
ini_set('memory_limit', '-1');
date_default_timezone_set('UTC');

echo "\n"; // just make things look purdy
/*
 * This API works so gosh-darn fast that we don't have to do anything wonky with it.
 * We can just straight-up ask for all the data, and *BLAM* it comes, in like 2 seconds or less.
 *
 * GLOBAL VARIABLES BELOW!
 */
$start_date         = "2014-01-01"; //Format: YYYYMMDDhhmmss     (hhmmss optional)
$end_date = date('Y-m-d',strtotime("-1 days"));
$offer_id = 0; //make it '0' for "all"

$post_data = array(
	"api_key" => "NuGQRL8TqcM",
	"affiliate_id" => "4961",
	"start_date" => $start_date,
	"end_date" => $end_date,
	"offer_id" => $offer_id
);
$post_array_string = "";
//url-ify the data (the API wants this as a string, not an array)
foreach($post_data as $key=>$value)
{
	$post_array_string .= $key.'='.$value.'&';
}
$url = "http://c2mtrax.com/affiliates/api/2/reports.asmx/HourlySummary";
$curlSession = curl_init();
curl_setopt($curlSession, CURLOPT_URL, $url);
curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curlSession, CURLOPT_POST, 1);
curl_setopt($curlSession, CURLOPT_POST,count($post_data));
curl_setopt($curlSession, CURLOPT_POSTFIELDS, $post_array_string);
curl_setopt($curlSession, CURLOPT_TIMEOUT, 500000);
$rawresponse = curl_exec($curlSession);
curl_close($curlSession);

//var_dump($rawresponse);
$array_good = simplexml_load_string($rawresponse);
$array_good = array(unserialize(serialize(json_decode(json_encode((array)$array_good), 1))));


$mysqli = initialize_db(getDBName(), "biohealth4", "FVJzA42euHVnE");

if (isset($array_good[0]['success']))
{
	for ($i = 0; $i < $array_good[0]['row_count']; $i++)
	{

		$insertSQL = sprintf("INSERT INTO hourly_summary
		(id, date, impressions, clicks, conversions, conversion_rate, revenue, epc, currency_symbol)
		VALUES ('',%s,%s,%s,%s,%s,%s,%s,%s)",
		sanatize_values($array_good[0]['hours']['hour'][$i]['date'], $mysqli),
		sanatize_values($array_good[0]['hours']['hour'][$i]['impressions'], $mysqli),
		sanatize_values($array_good[0]['hours']['hour'][$i]['clicks'], $mysqli),
		sanatize_values($array_good[0]['hours']['hour'][$i]['conversions'], $mysqli),
		sanatize_values($array_good[0]['hours']['hour'][$i]['conversion_rate'], $mysqli),
		sanatize_values($array_good[0]['hours']['hour'][$i]['revenue'], $mysqli),
		sanatize_values($array_good[0]['hours']['hour'][$i]['epc'], $mysqli),
		sanatize_values($array_good[0]['hours']['hour'][$i]['currency_symbol'], $mysqli));

		//echo '\n'.$insertSQL.'\n';
		$mysqli->query($insertSQL) or die($mysqli->error);
		//$rows=$rows+mysqli_affected_rows($mysqli);
	}
}

$mysqli->close();
/*
 * Some bizarre function that RK made
 */
function sanatize_values($theValue, $mysqli)
{
	$theValue = (is_array($theValue) ? "" : $theValue);
	$theValue = $mysqli->real_escape_string($theValue);
	$theValue = "'".$theValue."'";
	return $theValue;
}
?>