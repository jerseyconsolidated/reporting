<?php
/*
 * nmi
 */
function getNMI($username, $password, $transaction)
{
	$post_data = array(
		"username" => $username,
		"password" => $password,
		"transaction_id" => $transaction, //optional
		//"order_id" => 1234567890, //optional
		"action_type" => "sale,refund,credit,auth,capture,void,return,settlement"
		//"start_date" => $start_date, //optional
		//"end_date" => $end_date //optional
	);

	$url = "https://secure.nmi.com/api/query.php";
	$curlSession = curl_init();
	curl_setopt($curlSession, CURLOPT_URL, $url);
	curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curlSession, CURLOPT_POST, 1);
	curl_setopt($curlSession, CURLOPT_POSTFIELDS, $post_data);
	curl_setopt($curlSession, CURLOPT_TIMEOUT, 500000);
	if (php_uname('s') == "Darwin")  //OSX is called Darwin for some reason
	{
		//curl_setopt($curlSession, CURLOPT_CAINFO, "/Users/jonjenne/reporting/nmi/cacert.pem"); //HTTP cert
		curl_setopt($curlSession, CURLOPT_CAINFO, "/Users/jonjenne/reporting/reporting_api/v2/ca-bundle.crt"); //HTTPS cert
	}
	else if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') // this might work for windows, idk
	{
		//logIt('This is Windows! (Ryan, set your path to the cert here - We are now dying)');
		die();
	}
	else //some kind of Linux
	{
		curl_setopt($curlSession, CURLOPT_CAINFO, "/var/www/reporting.jcoffice.net/nmi/ca-bundle.crt"); //HTTPS cert
		//logIt("running under linux CHECK THE CERT PATH BEFORE CONTINUING (dieing)");
		//die(); //kill this so it reminds me to change this for the linux path.
	}
	$rawresponse = curl_exec($curlSession);
	curl_close($curlSession);
	$array_good = simplexml_load_string($rawresponse);
	$array_good = array(unserialize(serialize(json_decode(json_encode((array)$array_good), 1))));
	return $array_good;
}