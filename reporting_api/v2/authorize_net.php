<?php
require('../../includes/auth_api/config.inc.php');
require('../../includes/auth_api/AuthnetXML.class.php');

/*
 * authorize
 */
function getAuth($transaction_id, $login, $pass)
{
	try
	{
		$xml2 = new AuthnetXML($login, $pass, AuthnetXML::USE_PRODUCTION_SERVER);
		$xml2->getTransactionDetailsRequest(array(
			'transId' =>$transaction_id
		));
	}
	catch (AuthnetXMLException $e)
	{
		echo $e;
		exit;
	}
	/*
	 * this shit is out of order according to the json thing...... so that makes checking it annoying.
	 *
	 * Also, the insert_into stuff lines up with the values %s things. That makes it easier to count them.
	 */
	if(!isset($xml2->transaction->lineItems->lineItem->itemId))
	{
		$xml2->transaction->lineItems->lineItem->itemId = 0;
		$xml2->transaction->lineItems->lineItem->name = 0;
		$xml2->transaction->lineItems->lineItem->description = 0;
		$xml2->transaction->lineItems->lineItem->quantity = 0;
		$xml2->transaction->lineItems->lineItem->unitPrice = 0;
		$xml2->transaction->lineItems->lineItem->taxable = 0;
	}
	return $xml2;
}