<?php
/*
 * meritus
 */
function getMeritus($transaction, $merchantID, $merchantKey)
{
	$url = "https://webservice.paymentxp.com/wh/WebHost.aspx";
	$data1 = array(
		"MerchantID" => $merchantID,
		"MerchantKey" => $merchantKey,
		"ReportName" => "GetAchTransaction",
		"ReportFormat" => "JSON",
		"TransID" => $transaction
	);
	$curlSession = curl_init();
	curl_setopt($curlSession, CURLOPT_URL, $url);
	curl_setopt($curlSession, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($curlSession, CURLOPT_POST, 1);
	curl_setopt($curlSession, CURLOPT_POSTFIELDS, $data1);
	curl_setopt($curlSession, CURLOPT_TIMEOUT,500000);
	if (php_uname('s') == "Darwin")  //OSX is called Darwin for some reason
	{
		//curl_setopt($curlSession, CURLOPT_CAINFO, "/Users/jonjenne/reporting/nmi/cacert.pem"); //HTTP cert
		curl_setopt($curlSession, CURLOPT_CAINFO, "/Users/jonjenne/reporting/reporting_api/v2/ca-bundle.crt"); //HTTPS cert
	}
	else if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') // this might work for windows, idk
	{
		//logIt('This is Windows! (Ryan, set your path to the cert here - We are now dying)');
		die();
	}
	else //some kind of Linux
	{
		curl_setopt($curlSession, CURLOPT_CAINFO, "/var/www/reporting.jcoffice.net/nmi/ca-bundle.crt"); //HTTPS cert
		//logIt("running under linux CHECK THE CERT PATH BEFORE CONTINUING (dieing)");
		//die(); //kill this so it reminds me to change this for the linux path.
	}

	$rawresponse = curl_exec($curlSession);
	curl_close ($curlSession);
	$transactions = json_decode($rawresponse, true);
	var_dump($rawresponse);
	return $transactions;
}