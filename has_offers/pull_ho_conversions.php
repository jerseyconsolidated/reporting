<?php
include("../includes/has_offers/src/SimpleHasOffersApi.php");
include("general_functions.php");
$mysqli = initialize_db('has_offers');
//empty old data
$mysqli->query("TRUNCATE TABLE ho_conversions") or die($mysqli->error);
date_default_timezone_set('America/New_York');


//define(DATETIME_FORMAT, 'Y-m-d');
$next_date = "2013-01-01";

$date1 = new DateTime($next_date);
$date2 = new DateTime(date('Y-m-d'));
$diff = $date2->diff($date1)->format("%a");

for ($i=0 ; $i<=$diff ; $i++) {
    $next_date = date('Y-m-d', strtotime($next_date . ' + 1 day'));

//PULL AFFILIATES
    $api = new SimpleHasOffersApi();
    $api->prepare('Report', 'getStats');
    $api->addParameter('fields', array('Affiliate.id'
    ,'Affiliate.company'
    ,'Offer.id'
    ,'Offer.name'
    ,'Stat.clicks'
    ,'Stat.conversions'
    ,'Stat.ltr'
    ,'Stat.payout'
    ,'Stat.cpc'
    ,'Stat.cpa'
    ,'Stat.revenue'
    ,'Stat.rpc'
    ,'Stat.rpa'
    ,'Stat.profit'
    ,'Stat.date'
    ));
    $api->addParameter('filters', array(
        'Stat.date' => array('conditional' => 'EQUAL_TO', 'values' => $next_date)
    ,'Stat.conversions' => array('conditional' => 'GREATER_THAN_OR_EQUAL_TO', 'values' => '1')
    //,'Offer.name' => array('conditional' => 'LIKE', 'values' => '%Bio%')
    ));
    $api->addParameter('sort', array('Stat.date' => 'asc'));
    $api->addParameter('groups', array(
        'Affiliate.company'
    ,'Offer.name'
    ,'Stat.date'
    ));
//$api->addParameter('totals', false);
    $afils_with_conv = $api->execute();
 //   print_r($afils_with_conv);
//LOOP THROUGH HASOFFERS DATA
    for ($b=0; $b < count($afils_with_conv->response->data->data); $b++)
	{
        $the_date = $afils_with_conv->response->data->data[$b]->Stat->date." 00:00:01";
        $insertSQL = sprintf("INSERT INTO ho_conversions (affiliate_id,affil_company,offer_id,offer_name,clicks,conversions,cr,payout,cpc,cpa,revenue,rpc,rpa,profit,report_date,status) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
            GetSQLValueString($afils_with_conv->response->data->data[$b]->Affiliate->id, "text", $mysqli),
            GetSQLValueString($afils_with_conv->response->data->data[$b]->Affiliate->company, "text", $mysqli),
            GetSQLValueString($afils_with_conv->response->data->data[$b]->Offer->id, "text", $mysqli),
            GetSQLValueString($afils_with_conv->response->data->data[$b]->Offer->name, "text", $mysqli),
            GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->clicks, "text", $mysqli),
            GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->conversions, "text", $mysqli),
            GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->ltr, "text", $mysqli),
            GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->payout, "text", $mysqli),
            GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->cpc, "text", $mysqli),
            GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->cpa, "text", $mysqli),
            GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->revenue, "text", $mysqli),
            GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->rpc, "text", $mysqli),
            GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->rpa, "text", $mysqli),
            GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->profit, "text", $mysqli),
            GetSQLValueString($the_date, "text", $mysqli),
            GetSQLValueString($afils_with_conv->response->status, "text", $mysqli));
        $Result1 = $mysqli->query($insertSQL) or die($mysqli->error);
    }
}
echo "done";
?>

