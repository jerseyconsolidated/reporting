<?php
include("../includes/has_offers/src/SimpleHasOffersApi.php");
include("general_functions.php");
$mysqli = initialize_db('has_offers');
$mysqli->query("TRUNCATE TABLE ho_clicks") or die($mysqli->error);
date_default_timezone_set('America/New_York');

function objectToArray($d) {
    if (is_object($d)) {
        // Gets the properties of the given object
        // with get_object_vars function
        $d = get_object_vars($d);
    }

    if (is_array($d)) {
        /*
        * Return array converted to object
        * Using __FUNCTION__ (Magic constant)
        * for recursive call
        */
        return array_map(__FUNCTION__, $d);
    }
    else {
        // Return array
        return $d;
    }
}

$next_date = "2013-01-01";
$date1 = new DateTime($next_date);
$date2 = new DateTime(date('Y-m-d'));
$diff = $date2->diff($date1)->format("%a");
for ($i=0 ; $i<$diff ; $i++) {

    $next_date = date('Y-m-d', strtotime($next_date . ' + 1 day'));

    $api = new SimpleHasOffersApi();
    $api->prepare('Report', 'getConversions');
    $api->addParameter('fields', array(
        'Affiliate.id'
    ,'Stat.affiliate_id'
    ,'Affiliate.company'
    ,'Stat.id'
    ,'Stat.ip'
    ,'Stat.ad_id'
    ,'Stat.source'
    ,'Stat.affiliate_info1'
    ,'Stat.affiliate_info2'
    ,'Stat.affiliate_info3'
    ,'Stat.affiliate_info4'
    ,'Stat.affiliate_info5'
    ,'Stat.advertiser_info'
    ,'Stat.refer'
    ,'Stat.pixel_refer'
    ,'Stat.status'
    ,'Stat.payout'
    ,'Stat.revenue'
    ,'Stat.sale_amount'
    ,'Stat.user_agent'
    ,'Stat.is_adjustment'
    ,'Stat.datetime'
    ,'Offer.id'
    ,'Offer.name'
    ,'Stat.browser_id'
    ,'Browser.display_name'
    ));
    $api->addParameter('filters', array(
        'Stat.date' => array('conditional' => 'EQUAL_TO', 'values' => $next_date)
    ,'Offer.name' => array('conditional' => 'LIKE', 'values' => '%Bio%')
    ));
    $api->addParameter('limit',2500);
//$api->addParameter('sort', array('Stat.date' => 'asc'));
    $result = objectToArray($api->execute());

//d($result);
	//var_dump($result);
	//die();
    $records = $result['response']['data']['count'];

    for ($b=0; $b < $records; $b++) {
		echo "$b / $records / $next_date \n";
        $affiliate_id = $result['response']['data']['data'][$b]['Affiliate']['id'];
        $affiliate_company = $result['response']['data']['data'][$b]['Affiliate']['company'];
        $mysqliversion_id = $result['response']['data']['data'][$b]['Stat']['id'];
        $ip = $result['response']['data']['data'][$b]['Stat']['ip'];
        $click_id = $result['response']['data']['data'][$b]['Stat']['ad_id'];
        $source = $result['response']['data']['data'][$b]['Stat']['source'];
        $affiliate_info1 = $result['response']['data']['data'][$b]['Stat']['affiliate_info1'];
        $affiliate_info2 = $result['response']['data']['data'][$b]['Stat']['affiliate_info2'];
        $affiliate_info3 = $result['response']['data']['data'][$b]['Stat']['affiliate_info3'];
        $affiliate_info4 = $result['response']['data']['data'][$b]['Stat']['affiliate_info4'];
        $affiliate_info5 = $result['response']['data']['data'][$b]['Stat']['affiliate_info5'];
        $advertiser_info = $result['response']['data']['data'][$b]['Stat']['advertiser_info'];
        $refer = $result['response']['data']['data'][$b]['Stat']['refer'];
        $pixel_refer = $result['response']['data']['data'][$b]['Stat']['pixel_refer'];
        $status = $result['response']['data']['data'][$b]['Stat']['status'];
        $payout = $result['response']['data']['data'][$b]['Stat']['payout'];
        $revenue = $result['response']['data']['data'][$b]['Stat']['revenue'];
        $sale_amount = $result['response']['data']['data'][$b]['Stat']['sale_amount'];
        $user_agent = $result['response']['data']['data'][$b]['Stat']['user_agent'];
        $is_adjustment = $result['response']['data']['data'][$b]['Stat']['is_adjustment'];
        $datetime = $result['response']['data']['data'][$b]['Stat']['datetime'];
        $offer_id = $result['response']['data']['data'][$b]['Offer']['id'];
        $offer_name = $result['response']['data']['data'][$b]['Offer']['name'];
        $browser = $result['response']['data']['data'][$b]['Browser']['display_name'];
        $insertSQL = sprintf("INSERT INTO ho_clicks (affiliate_id,affiliate_company,conversion_id,ip_address,click_id,source,affiliate_info1,affiliate_info2,affiliate_info3,affiliate_info4,affiliate_info5,advertiser_info,refer,pixel_refer,status,payout,revenue,sale_amount,user_agent,is_adjustment,datetime,offer_id,offer_name,browser) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
            GetSQLValueString($affiliate_id, "text", $mysqli),
            GetSQLValueString($affiliate_company, "text", $mysqli),
            GetSQLValueString($mysqliversion_id, "text", $mysqli),
            GetSQLValueString($ip, "text", $mysqli),
            GetSQLValueString($click_id, "text", $mysqli),
            GetSQLValueString($source, "text", $mysqli),
            GetSQLValueString($affiliate_info1, "text", $mysqli),
            GetSQLValueString($affiliate_info2, "text", $mysqli),
            GetSQLValueString($affiliate_info3, "text", $mysqli),
            GetSQLValueString($affiliate_info4, "text", $mysqli),
            GetSQLValueString($affiliate_info5, "text", $mysqli),
            GetSQLValueString($advertiser_info, "text", $mysqli),
            GetSQLValueString($refer, "text", $mysqli),
            GetSQLValueString($pixel_refer, "text", $mysqli),
            GetSQLValueString($status, "text", $mysqli),
            GetSQLValueString($payout, "text", $mysqli),
            GetSQLValueString($revenue, "text", $mysqli),
            GetSQLValueString($sale_amount, "text", $mysqli),
            GetSQLValueString($user_agent, "text", $mysqli),
            GetSQLValueString($is_adjustment, "text", $mysqli),
            GetSQLValueString($datetime, "text", $mysqli),
            GetSQLValueString($offer_id, "text", $mysqli),
            GetSQLValueString($offer_name, "text", $mysqli),
            GetSQLValueString($browser, "text", $mysqli));

        $Result1 = $mysqli->query($insertSQL) or die(mysql_error($mysqli));
    }
}
echo "done ha_conversions";

/*             mysql_select_db($database_bio_db, $bio_db);
             $query_pull_dupes = "select count(click_id), click_id from ha_conversions GROUP BY click_id having count(click_id) > 1 ORDER BY count(click_id) desc";
             $pull_dupes = mysql_query($query_pull_dupes, $bio_db) or die(mysql_error());
             $row_pull_dupes = mysql_fetch_assoc($pull_dupes);
             $totalRows_pull_dupes = mysql_num_rows($pull_dupes);

		   do {
			   	$insertSQL = sprintf("DELETE FROM ha_conversions WHERE click_id = '".$row_pull_dupes['click_id']."'");
	mysql_select_db($database_bio_db, $bio_db);
	$Result1 = mysql_query($insertSQL, $bio_db) or die(mysql_error());
			   } while ($row_pull_dupes = mysql_fetch_assoc($pull_dupes));*/
?>