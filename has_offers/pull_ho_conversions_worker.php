<?php
include("../includes/has_offers/src/SimpleHasOffersApi.php");
include("general_functions.php");
$mysqli = initialize_db(getDBName());
date_default_timezone_set('America/New_York');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require ('../includes/RayGun/RayGun.php');

if (defined('STDIN'))
{
	$days_to_pull 	= $argv[1];
	$given_date		= $argv[2];
}
else
{
	databaseConversionsLog("standard input failed");
	//logIt("STDIN failed");
	die("STDIN failed");
}
databaseConversionsLog("Date Range : $given_date + $days_to_pull days started.");


for ($this_day=0 ; $this_day<$days_to_pull; $this_day++)
{
	/*
	 * The following line takes the given date and adds the number of days we should be incrementing to it.
	 *
	 * So, if this were called as 2013/01/01 with 3 days to pull, it will show as [$this_day] : [$next_date]
	 * 0 : 2013/01/01
	 * 1 : 2013/01/02
	 * 2 : 3013/01/03
	 * etc
	 */
	$next_date = date('Y-m-d', strtotime($given_date. ' + '.$this_day.' day'));
	$api = new SimpleHasOffersApi();
	$api->prepare('Report', 'getStats');
	$api->addParameter('fields', array('Affiliate.id'
	,'Affiliate.company'
	,'Offer.id'
	,'Offer.name'
	,'Stat.clicks'
	,'Stat.conversions'
	,'Stat.ltr'
	,'Stat.payout'
	,'Stat.cpc'
	,'Stat.cpa'
	,'Stat.revenue'
	,'Stat.rpc'
	,'Stat.rpa'
	,'Stat.profit'
	,'Stat.date'
	));
	$api->addParameter('filters', array(
		'Stat.date' => array('conditional' => 'EQUAL_TO', 'values' => $next_date)
	,'Stat.conversions' => array('conditional' => 'GREATER_THAN_OR_EQUAL_TO', 'values' => '1')
	,'Offer.name' => array('conditional' => 'LIKE', 'values' => '%Bio%')
	));
	$api->addParameter('sort', array('Stat.date' => 'asc'));
	$api->addParameter('groups', array(
		'Affiliate.company'
	,'Offer.name'
	,'Stat.date'
	));
	/*
	 * Apparently, if we don't have a limit (even a ridiculously high one like 10 million)
	 * this WILL LAG SO BADLY AND TAKE UP 100% OF THE CPU!!!!!!!!!!
	 *
	 * The documentation says this is the number per page, but we aren't going through pages?
	 *
	 *        		 : \
                     ;\ \_                   _
                     ;@: ~:              _,-;@)
                     ;@: ;~:          _,' _,'@;
                     ;@;  ;~;      ,-'  _,@@@,'
                    |@(     ;      ) ,-'@@@-;
                    ;@;   |~~(   _/ /@@@@@@/
                    \@\   ; _/ _/ /@@@@@@;~
                     \@\   /  / ,'@@@,-'~
                       \\  (  ) :@@(~
                    ___ )-'~~~~`--/ ___
                   (   `--_    _,--'   )
                  (~`- ___ \  / ___ -'~)
                 __~\_(   \_~~_/   )_/~__
 /\ /\ /\     ,-'~~~~~`-._ 0\/0 _,-'~~~~~`-.
| |:  ::|    ;     ______ `----'  ______    :
| `'  `'|    ;    {      \   ~   /      }   |
 \_   _/     `-._      ,-,' ~~  `.-.      _,'        |\
   \ /_          `----' ,'       `, `----'           : \
   |_( )                `-._/#\_,-'                  :  )
 ,-'  ~)           _,--./  (###)__                   :  :
 (~~~~_)          /       ; `-'   `--,               |  ;
 (~~~' )         ;       /@@@@@@.    `.              | /
 `.HH~;        ,-'  ,-   |@@@ @@@@.   `.             .')
  `HH `.      ,'   /     |@@@@@ @@@@.  `.           / /(~)
   HH   \_   ,'  _/`.    |@@@@@ @@@@@;  `.          ; (~~)
   ~~`.   \_,'  /   ;   .@@@@@ @@@@@@;\_  \___      ; H~\)
       \_     _/    `.  |@@@@@@ @@@@@;  \     `----'_HH[~)
         \___/       `. :@@@@@ @@@@@@'   \__,------' HH ~
        ______        ; |@@@@@@ @@@'                 HH
      _)      \_,     ; :@@@@@@@@@;                  ~~
    _;          \\   ,' |@@@@@@@@@:
  ,'     ; :      \_,   :@@@@@@@@@@.
  `.__,-'~~`._,-.  ,    :@@@@@@@@@@`.
                 \/    /@@@@@@@@@@@@:
                 /    ,@@@@@@@@@@@@@@.
                |    ,@@@@@@@@@@@@@@@:
                |    :@@@@@@@@@@@@@@@'
                `.   \@@@@/  `@@@@@/(
                  )   ~~~/    \~~~~  \
                  :     /       \_    \
                  (    /          \_   `.
                  /   ;             \_  `.
                 /   /                \  `.
                /   /                  `.  \
              ,'  ,'/~~)                ;  /
              {   `'   (               /  /
              `.___,-'  \             /  /
                 __/     |           /  /
                /        |           : :   __
                :        |           ; : _;  )__
                (  |  |  /          /  `,'  ~   )_
                 `-:__;-'          :  ,'      ~~  ;
                                  /          (_,--'
                                 (       ,-'~~
                                  \__,-'~
	 */
   // $api->addParameter('limit',10000000); //WHY IS THIS NECESSARY?? IT"S SO SILLY!

	$result = objectToArray($api->execute());

	if(isset($result['response']['data']['count']))
	{
		$afils_with_conv = $api->execute();

		for ($b=0; $b < count($afils_with_conv->response->data->data); $b++)
		{
			$the_date = $afils_with_conv->response->data->data[$b]->Stat->date." 00:00:01";
			$insertSQL = sprintf("INSERT INTO ho_conversions (affiliate_id,affil_company,offer_id,offer_name,clicks,conversions,cr,payout,cpc,cpa,revenue,rpc,rpa,profit,report_date,status) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
				GetSQLValueString($afils_with_conv->response->data->data[$b]->Affiliate->id, "text", $mysqli),
				GetSQLValueString($afils_with_conv->response->data->data[$b]->Affiliate->company, "text", $mysqli),
				GetSQLValueString($afils_with_conv->response->data->data[$b]->Offer->id, "text", $mysqli),
				GetSQLValueString($afils_with_conv->response->data->data[$b]->Offer->name, "text", $mysqli),
				GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->clicks, "text", $mysqli),
				GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->conversions, "text", $mysqli),
				GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->ltr, "text", $mysqli),
				GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->payout, "text", $mysqli),
				GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->cpc, "text", $mysqli),
				GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->cpa, "text", $mysqli),
				GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->revenue, "text", $mysqli),
				GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->rpc, "text", $mysqli),
				GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->rpa, "text", $mysqli),
				GetSQLValueString($afils_with_conv->response->data->data[$b]->Stat->profit, "text", $mysqli),
				GetSQLValueString($the_date, "text", $mysqli),
				GetSQLValueString($afils_with_conv->response->status, "text", $mysqli));
			$Result1 = $mysqli->query($insertSQL) or die($mysqli->error);
		}
	}
	else
	{
		if(isset($result['response']['errorMessage']))
		{
			databaseConversionsLog("Date Range : $given_date + $days_to_pull (day $this_day) : ERROR: ".$result['response']['errorMessage']);
		}
		else
		{
			//databaseConversionsLog("Date Range : $given_date + $days_to_pull (day $this_day) : ERROR: no error specified (No records returned?)");
		}
	}
	sleep(1);
}
databaseConversionsLog("Date Range : $given_date + $days_to_pull ALL $this_day finished.");

/*
 * functions follow below:
 */
function objectToArray($d) {
	if (is_object($d)) {
		// Gets the properties of the given object
		// with get_object_vars function
		$d = get_object_vars($d);
	}

	if (is_array($d)) {
		/*
		* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return array_map(__FUNCTION__, $d);
	}
	else {
		// Return array
		return $d;
	}
}
?>