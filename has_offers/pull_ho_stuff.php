<?php
include("../includes/has_offers/src/SimpleHasOffersApi.php");
include("general_functions.php");
$mysqli = initialize_db(getDBName());
$mysqli->query("TRUNCATE TABLE ho_clicks") or die($mysqli->error);
$mysqli->query("TRUNCATE TABLE ho_conversions") or die($mysqli->error);
$mysqli->query("TRUNCATE TABLE general_clicks_log_table") or die($mysqli->error);
$mysqli->query("TRUNCATE TABLE general_conversions_log_table") or die($mysqli->error);

date_default_timezone_set('America/New_York');

/*
 *
 * GLOBAL VARIABLES BELOW!
 *
 */
$start_date         = "2013-01-01"; //Format: YYYY/MM/DD
$end_date = date('Y-m-d',strtotime("-1 days"));
$days_per_thread    = 960; //How many days should each thread work on? If 1, 1 day = 1 thread. If 5, 5 days = 1 thread.
/*
 * reporting servadora
 */
$clicks_command        = "php /var/www/reporting.jcoffice.net/has_offers/pull_ho_clicks_worker.php";
$conversions_command   = "php /var/www/reporting.jcoffice.net/has_offers/pull_ho_conversions_worker.php";

/*
 * Jon's machine

$clicks        = "php /Users/jonjenne/reporting/has_offers/pull_ho_clicks_worker.php";
$conversions  = "php /Users/jonjenne/reporting/has_offers/pull_ho_conversions_worker.php";
*/
$date1 = new DateTime($start_date);
$date2 = new DateTime(date('Y-m-d'));
/*
 * $diff represents the number of days between $start_date and $end_date +1
 */
$diff = $date2->diff($date1)->format("%a");
/*
 * loop through every day between the $start_date and the $end_date.
 * Days will be incremented by $days_per_thread
 */
$next_date = $start_date;
for ($index=0; $index<$diff;)
{
	$clicks = "$clicks_command $days_per_thread $next_date > /dev/null &";
	$conversions = "$conversions_command $days_per_thread $next_date > /dev/null &";

	//echo $conversions_command;
	//die();
	//echo $command."\n";
	//die();
	$result = pclose(popen($clicks, "r"));
	if($result)
	{
		databaseGeneralLog("Failed to start $clicks ... This command did not start.");
	}
	//die();
	/*
	 * Temporarily turning off conversions so I can work on clicks
	 */
	$result = pclose(popen($conversions, "r"));
	if($result)
	{
		databaseGeneralLog("Failed to start $conversions ... This command did not start.");
	}
	//die();
	//echo "\n".$command."\n";
	//die();
	sleep(8); // it's crazy that this has to be so high.
    $next_date = date('Y-m-d', strtotime($next_date . ' + '.$days_per_thread.' day'));
	$index = $index + $days_per_thread;
}
?>