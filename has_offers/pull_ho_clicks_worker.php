<?php
include("../includes/has_offers/src/SimpleHasOffersApi.php");
include("general_functions.php");
$mysqli = initialize_db(getDBName());
date_default_timezone_set('America/New_York');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require ('../includes/RayGun/RayGun.php');

if (defined('STDIN'))
{
	$days_to_pull 	= $argv[1];
	$given_date		= $argv[2];
}
else
{
	databaseClicksLog("standard input failed");
	//logIt("STDIN failed");
	die("STDIN failed");
}
databaseClicksLog("Date Range : $given_date + $days_to_pull days started.");


for ($this_day=0 ; $this_day<$days_to_pull; $this_day++)
{
	/*
	 * The following line takes the given date and adds the number of days we should be incrementing to it.
	 *
	 * So, if this were called as 2013/01/01 with 3 days to pull, it will show as [$this_day] : [$next_date]
	 * 0 : 2013/01/01
	 * 1 : 2013/01/02
	 * 2 : 2013/01/03
	 * etc
	 */
	$next_date = date('Y-m-d', strtotime($given_date. ' + '.$this_day.' day'));
    $api = new SimpleHasOffersApi();
    $api->prepare('Report', 'getConversions');
    $api->addParameter('fields', array(
        'Affiliate.id'
    ,'Stat.affiliate_id'
    ,'Affiliate.company'
    ,'Stat.id'
    ,'Stat.ip'
    ,'Stat.ad_id'
    ,'Stat.source'
    ,'Stat.affiliate_info1'
    ,'Stat.affiliate_info2'
    ,'Stat.affiliate_info3'
    ,'Stat.affiliate_info4'
    ,'Stat.affiliate_info5'
    ,'Stat.advertiser_info'
    ,'Stat.refer'
    ,'Stat.pixel_refer'
    ,'Stat.status'
    ,'Stat.payout'
    ,'Stat.revenue'
    ,'Stat.sale_amount'
    ,'Stat.user_agent'
    ,'Stat.is_adjustment'
    ,'Stat.datetime'
    ,'Offer.id'
    ,'Offer.name'
    ,'Stat.browser_id'
    ,'Browser.display_name'
    ));
    $api->addParameter('filters', array(
        'Stat.date' => array('conditional' => 'EQUAL_TO', 'values' => $next_date)
   // ,'Offer.name' => array('conditional' => 'LIKE', 'values' => '%Bio%')
    ));
	/*
	 * Apparently, if we don't have a limit (even a ridiculously high one like 10 million)
	 * this WILL LAG SO BADLY AND TAKE UP 100% OF THE CPU!!!!!!!!!!
	 *
	 * The documentation says this is the number per page, but we aren't going through pages?
	 *
	 *        		 : \
                     ;\ \_                   _
                     ;@: ~:              _,-;@)
                     ;@: ;~:          _,' _,'@;
                     ;@;  ;~;      ,-'  _,@@@,'
                    |@(     ;      ) ,-'@@@-;
                    ;@;   |~~(   _/ /@@@@@@/
                    \@\   ; _/ _/ /@@@@@@;~
                     \@\   /  / ,'@@@,-'~
                       \\  (  ) :@@(~
                    ___ )-'~~~~`--/ ___
                   (   `--_    _,--'   )
                  (~`- ___ \  / ___ -'~)
                 __~\_(   \_~~_/   )_/~__
 /\ /\ /\     ,-'~~~~~`-._ 0\/0 _,-'~~~~~`-.
| |:  ::|    ;     ______ `----'  ______    :
| `'  `'|    ;    {      \   ~   /      }   |
 \_   _/     `-._      ,-,' ~~  `.-.      _,'        |\
   \ /_          `----' ,'       `, `----'           : \
   |_( )                `-._/#\_,-'                  :  )
 ,-'  ~)           _,--./  (###)__                   :  :
 (~~~~_)          /       ; `-'   `--,               |  ;
 (~~~' )         ;       /@@@@@@.    `.              | /
 `.HH~;        ,-'  ,-   |@@@ @@@@.   `.             .')
  `HH `.      ,'   /     |@@@@@ @@@@.  `.           / /(~)
   HH   \_   ,'  _/`.    |@@@@@ @@@@@;  `.          ; (~~)
   ~~`.   \_,'  /   ;   .@@@@@ @@@@@@;\_  \___      ; H~\)
       \_     _/    `.  |@@@@@@ @@@@@;  \     `----'_HH[~)
         \___/       `. :@@@@@ @@@@@@'   \__,------' HH ~
        ______        ; |@@@@@@ @@@'                 HH
      _)      \_,     ; :@@@@@@@@@;                  ~~
    _;          \\   ,' |@@@@@@@@@:
  ,'     ; :      \_,   :@@@@@@@@@@.
  `.__,-'~~`._,-.  ,    :@@@@@@@@@@`.
                 \/    /@@@@@@@@@@@@:
                 /    ,@@@@@@@@@@@@@@.
                |    ,@@@@@@@@@@@@@@@:
                |    :@@@@@@@@@@@@@@@'
                `.   \@@@@/  `@@@@@/(
                  )   ~~~/    \~~~~  \
                  :     /       \_    \
                  (    /          \_   `.
                  /   ;             \_  `.
                 /   /                \  `.
                /   /                  `.  \
              ,'  ,'/~~)                ;  /
              {   `'   (               /  /
              `.___,-'  \             /  /
                 __/     |           /  /
                /        |           : :   __
                :        |           ; : _;  )__
                (  |  |  /          /  `,'  ~   )_
                 `-:__;-'          :  ,'      ~~  ;
                                  /          (_,--'
                                 (       ,-'~~
                                  \__,-'~
	 */
   // $api->addParameter('limit',10000000); //WHY IS THIS NECESSARY?? IT"S SO SILLY!

//$api->addParameter('sort', array('Stat.date' => 'asc'));
    $result = objectToArray($api->execute());

//d($result);
	//var_dump($result);
	//die();
	if(isset($result['response']['data']['count']))
	{
		$records = $result['response']['data']['count'];
		//echo " $this_day / $days_to_pull / $records";
		for ($b=0; $b < $records; $b++)
		{
			$affiliate_id = "";
			$affiliate_company = "";
			$mysqliversion_id = "";
			$ip = "";
			$click_id = "";
			$source = "";
			$affiliate_info1 = "";
			$affiliate_info2 = "";
			$affiliate_info3 = "";
			$affiliate_info4 = "";
			$affiliate_info5 = "";
			$advertiser_info = "";
			$refer = "";
			$pixel_refer = "";
			$status = "";
			$payout = "";
			$revenue = "";
			$sale_amount = "";
			$user_agent = "";
			$is_adjustment = "";
			$datetime = "";
			$offer_id = "";
			$offer_name = "";
			$browser = "";
			
			if(isset($result['response']['data']['data'][$b]['Affiliate']['id']))
			{
				$affiliate_id = $result['response']['data']['data'][$b]['Affiliate']['id'];
			}
			else
			{
				continue;
				//var_dump($result);
				//die();
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Affiliate']['company']))
			{
				$affiliate_company = $result['response']['data']['data'][$b]['Affiliate']['company'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['id']))
			{
				$mysqliversion_id = $result['response']['data']['data'][$b]['Stat']['id'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['ip']))
			{
				$ip = $result['response']['data']['data'][$b]['Stat']['ip'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['ad_id']))
			{
				$click_id = $result['response']['data']['data'][$b]['Stat']['ad_id'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['source']))
			{
				$source = $result['response']['data']['data'][$b]['Stat']['source'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['affiliate_info1']))
			{
				$affiliate_info1 = $result['response']['data']['data'][$b]['Stat']['affiliate_info1'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['affiliate_info2']))
			{
				$affiliate_info2 = $result['response']['data']['data'][$b]['Stat']['affiliate_info2'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['affiliate_info3']))
			{
				$affiliate_info3 = $result['response']['data']['data'][$b]['Stat']['affiliate_info3'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['affiliate_info4']))
			{
				$affiliate_info4 = $result['response']['data']['data'][$b]['Stat']['affiliate_info4'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['affiliate_info5']))
			{
				$affiliate_info5 = $result['response']['data']['data'][$b]['Stat']['affiliate_info5'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['advertiser_info']))
			{
				$advertiser_info = $result['response']['data']['data'][$b]['Stat']['advertiser_info'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['refer']))
			{
				$refer = $result['response']['data']['data'][$b]['Stat']['refer'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['pixel_refer']))
			{
				$pixel_refer = $result['response']['data']['data'][$b]['Stat']['pixel_refer'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['status']))
			{
				$status = $result['response']['data']['data'][$b]['Stat']['status'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['payout']))
			{
				$payout = $result['response']['data']['data'][$b]['Stat']['payout'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['revenue']))
			{
				$revenue = $result['response']['data']['data'][$b]['Stat']['revenue'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['sale_amount']))
			{
				$sale_amount = $result['response']['data']['data'][$b]['Stat']['sale_amount'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['user_agent']))
			{
				$user_agent = $result['response']['data']['data'][$b]['Stat']['user_agent'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['is_adjustment']))
			{
				$is_adjustment = $result['response']['data']['data'][$b]['Stat']['is_adjustment'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Stat']['datetime']))
			{
				$datetime = $result['response']['data']['data'][$b]['Stat']['datetime'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Offer']['id']))
			{
				$offer_id = $result['response']['data']['data'][$b]['Offer']['id'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Offer']['name']))
			{
				$offer_name = $result['response']['data']['data'][$b]['Offer']['name'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}
			if(isset($result['response']['data']['data'][$b]['Browser']['display_name']))
			{
				$browser = $result['response']['data']['data'][$b]['Browser']['display_name'];
			}
			else
			{
				//databaseClicksLog("Error with ".$result); 
			}

			//var_dump($result);
				
			$insertSQL = sprintf("INSERT INTO ho_clicks (affiliate_id,affiliate_company,conversion_id,ip_address,click_id,source,affiliate_info1,affiliate_info2,affiliate_info3,affiliate_info4,affiliate_info5,advertiser_info,refer,pixel_refer,status,payout,revenue,sale_amount,user_agent,is_adjustment,datetime,offer_id,offer_name,browser) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
				GetSQLValueString($affiliate_id, "text", $mysqli),
				GetSQLValueString($affiliate_company, "text", $mysqli),
				GetSQLValueString($mysqliversion_id, "text", $mysqli),
				GetSQLValueString($ip, "text", $mysqli),
				GetSQLValueString($click_id, "text", $mysqli),
				GetSQLValueString($source, "text", $mysqli),
				GetSQLValueString($affiliate_info1, "text", $mysqli),
				GetSQLValueString($affiliate_info2, "text", $mysqli),
				GetSQLValueString($affiliate_info3, "text", $mysqli),
				GetSQLValueString($affiliate_info4, "text", $mysqli),
				GetSQLValueString($affiliate_info5, "text", $mysqli),
				GetSQLValueString($advertiser_info, "text", $mysqli),
				GetSQLValueString($refer, "text", $mysqli),
				GetSQLValueString($pixel_refer, "text", $mysqli),
				GetSQLValueString($status, "text", $mysqli),
				GetSQLValueString($payout, "text", $mysqli),
				GetSQLValueString($revenue, "text", $mysqli),
				GetSQLValueString($sale_amount, "text", $mysqli),
				GetSQLValueString($user_agent, "text", $mysqli),
				GetSQLValueString($is_adjustment, "text", $mysqli),
				GetSQLValueString($datetime, "text", $mysqli),
				GetSQLValueString($offer_id, "text", $mysqli),
				GetSQLValueString($offer_name, "text", $mysqli),
				GetSQLValueString($browser, "text", $mysqli));

			doDBthing($insertSQL);
			//$Result1 = $mysqli->query($insertSQL) or die(mysql_error($mysqli));
		}
	}
	else
	{
		if(isset($result['response']['errorMessage']))
		{
			databaseClicksLog("Date Range : $given_date + $days_to_pull (day $this_day) : ERROR: ".$result['response']['errorMessage']);
		}
		else
		{
			//databaseClicksLog("Date Range : $given_date + $days_to_pull (day $this_day) : ERROR: no error specified (No records returned?)");
		}
	}
	sleep(1);
}
$mysqli->close();
databaseClicksLog("Date Range : $given_date + $days_to_pull ALL $this_day finished.");

/*
 * functions follow below:
 */
function objectToArray($d) {
	if (is_object($d)) {
		// Gets the properties of the given object
		// with get_object_vars function
		$d = get_object_vars($d);
	}

	if (is_array($d)) {
		/*
		* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return array_map(__FUNCTION__, $d);
	}
	else {
		// Return array
		return $d;
	}
}
/*
 * If isset, return data. Else return blank string.
 */
function checkSet($string)
{
	if(isset($string) && $string != "")
	{
		return $string;
	}
	else
	{
		return "";
	}
}
?>