<?php
/**
 * Created by PhpStorm.
 * User: jonjenne
 * Date: 4/28/15
 * Time: 4:48 PM
 */

/*
    lastName: Last Name of lead.
    prefix: Prefix for name of the lead.
    requirements: Requirements of the lead
    homePhone: Home Phone of the lead
    mobilePhone: Mobile Phone of the lead
    email: Email Id of the lead
    timezone: Time Zone of the lead e.g. (-630)
    language: Language of the lead
    street: Street Address Line 1 of the lead
    street1: Street Address Line 2 of the lead
    street2: Street Address Line 3 of the lead
    city: City of the lead
    state: State of the lead
    zipPostalCode: Postal code of the lead
    country: Country of the lead
    webSite: Website of the lead
    company: Company of the lead
    noOfEmployees: Number of employees in the lead's company
    industry: Type of industry of the lead
    lastDialedNumber: dialed number or possibly the next number to be dialed
    startTime:Scheduled start time of the lead
    endTime:Scheduled end time of the lead
    primaryPhone: Primary Phone can be any of the phone field names defined in the call center
 */
$lead = array(
	"firstName" => "Johnny",
	"workPhone" => rand(1000000000, 9999999999),
	"projectName" => "test_outbound",
);
addLeadArray($lead);
/*
 * This function adds a lead with a variable amount of variables stored in an array.
 */
function addLeadArray($datas)
{
	$key = "16l5GdeZOAIgX4QyB6wEwSNPOvC5Gu-92AExdhxKaG9UKqpx7nxDcYDA3I8RW1AKRHs98qqh44cdfD8p6nVs0w";
	$url = "https://svr07.3ccloud.com/ms/portal/leadapi?q=addLead&key=$key";

	foreach($datas as $name=>$value)
	{

		$url.="&".$name."=".$value;
	}
	//echo $url;
	//die();
	$curlSession = curl_init();
	curl_setopt($curlSession, CURLOPT_URL, $url);
	curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curlSession, CURLOPT_POST, 1);
	curl_setopt($curlSession, CURLOPT_TIMEOUT, 500000);
	$rawresponse = curl_exec($curlSession);
	curl_close($curlSession);
	return json_decode($rawresponse);
}
/*
 * This function adds a standard lead
 */
function addLead($fname, $number, $project)
{
	$key = "16l5GdeZOAIgX4QyB6wEwSNPOvC5Gu-92AExdhxKaG9UKqpx7nxDcYDA3I8RW1AKRHs98qqh44cdfD8p6nVs0w";
	$url = "https://svr07.3ccloud.com/ms/portal/leadapi?q=addLead&key=$key&firstName=$fname&workPhone=$number&leadlists=tagName&projectName=$project";

	$curlSession = curl_init();
	curl_setopt($curlSession, CURLOPT_URL, $url);
	curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curlSession, CURLOPT_POST, 1);
	curl_setopt($curlSession, CURLOPT_TIMEOUT, 500000);
	$rawresponse = curl_exec($curlSession);
	curl_close($curlSession);
	return json_decode($rawresponse);
}