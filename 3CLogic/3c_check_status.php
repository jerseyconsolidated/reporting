<?php
/**
 * Created by PhpStorm.
 * User: jonjenne
 * Date: 4/28/15
 * Time: 4:48 PM
 */

/*
 * Use this key for using Data Integration APIs
Public Key	AKIAJVIVOUX7HIHFL76A
Private Key	o0C+u249G+AAYBG1JP2tdVHWRDnhn3yaPWyPZ1b8

firstname: First Name of lead
lastname: Last Name of lead
e164workphone: Work Phone of the lead
e164mobilephone: Mobile Phone of the lead
e164homephone: Home Phone of the lead
email: Email Address of the lead
timezone: Time Zone of the lead e.g. (GMT +0530)
language: Language of the lead
timezoneoffset: Time Zone offset of the lead e.g. (19800 for GMT +0530)
street: Street Address of the lead
street1: Street Address Line 1 of the lead
street2: Street Address Line 2 of the lead
city: City of the lead
state: State of the lead
zippostalcode: Postal code of the lead
country: Country of the lead
webSite: Website of the lead
company: Company of the lead
noofemployees: No of Employees of the lead
revenue: Revenue of the lead
requirements: Requirements of the lead
industry: Type of industry of the lead
lastDialedNumber: dialed number or possibly the next number to be dialed
primaryPhone: Primary Phone can be any of the phone field names defined in the call center
 */

$lead = array(
    "q" => "getLeadsByFieldValue",
    "key" => "16l5GdeZOAIgX4QyB6wEwSNPOvC5Gu-92AExdhxKaG9UKqpx7nxDcYDA3I8RW1AKRHs98qqh44cdfD8p6nVs0w",
	"fieldName" => "dnc",
	"fieldValue" => "0"
);
$url = "https://svr07.3ccloud.com/ms/portal/leadapi?ccid=1607";

$curlSession = curl_init();
curl_setopt($curlSession, CURLOPT_URL, $url);
curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curlSession, CURLOPT_POST, 1);
curl_setopt($curlSession,CURLOPT_POSTFIELDS, $lead);
curl_setopt($curlSession, CURLOPT_TIMEOUT, 500000);
$rawresponse = curl_exec($curlSession);
curl_close($curlSession);
$response = json_decode($rawresponse);
print_r($response);

/*