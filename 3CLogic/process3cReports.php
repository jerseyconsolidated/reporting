<?php
/**
 * Created by PhpStorm.
 * User: Ryan
 * 
 * Date: 2015-05-13
 * Time: 1:37 PM
 */

/*
 * Brings the files from production to dev server
 */
$ftp_server = "reporting.jcoffice.net";
$ftp = ftp_connect($ftp_server,'21') or die("Could not connect to $ftp_server");
$login = ftp_login($ftp, 'callcenter', 'ZGhLveUpSM9n');
ftp_chdir($ftp , '3cReports');
$files = ftp_nlist($ftp, '.');

foreach ($files as $file) {
    ftp_get($ftp, '3cReports/'.$file, $file, FTP_BINARY);

}

/*
 * puts all csvs into call records db
 */
$databasehost = "localhost";
$databasename = "3c";
$databasetable = "callRecords";
$databaseusername = "root";
$databasepassword = "M0rebottles**";
$fieldseparator = ",";
$lineseparator = "\n";

$pdo1 = new PDO("mysql:host=$databasehost;dbname=$databasename",$databaseusername, $databasepassword,
    array(PDO::MYSQL_ATTR_INIT_COMMAND =>
        'TRUNCATE TABLE `'.$databasetable.'`
'));
$affectedRows = $pdo1->exec("TRUNCATE TABLE $databasetable");
echo $affectedRows . " rows deleted\n";

foreach (glob("3cReports/*.csv") as $filename) {
    echo "$filename size " . filesize($filename) . "\n";

    $csvfile = $filename;

    if (!file_exists($csvfile)) {
        die("File not found. Make sure you specified the correct path.");
    }

    try {
        $pdo = new PDO("mysql:host=$databasehost;dbname=$databasename",
            $databaseusername, $databasepassword,
            array(
                PDO::MYSQL_ATTR_LOCAL_INFILE => true,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            )
        );
    } catch (PDOException $e) {
        die("database connection failed: " . $e->getMessage());
    }

    $affectedRows = $pdo->exec("
    LOAD DATA LOCAL INFILE " . $pdo->quote($csvfile) . " INTO TABLE `$databasetable`
      FIELDS TERMINATED BY " . $pdo->quote($fieldseparator) . "
      LINES TERMINATED BY " . $pdo->quote($lineseparator));

    echo "Loaded a total of $affectedRows records from this csv file.\n";

}
